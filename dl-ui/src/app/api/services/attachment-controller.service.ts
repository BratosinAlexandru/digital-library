/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { GridFsResource } from '../models/grid-fs-resource';
import { Attachment } from '../models/attachment';

/**
 * Attachment Controller
 */
@Injectable({
  providedIn: 'root',
})
class AttachmentControllerService extends __BaseService {
  static readonly deleteAttachmentsUsingDELETEPath = '/attachment/delete';
  static readonly downloadUsingGETPath = '/attachment/download';
  static readonly uploadUsingPOSTPath = '/attachment/upload';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * deleteAttachments
   * @param ids ids
   */
  deleteAttachmentsUsingDELETEResponse(ids: Array<string>): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (ids || []).forEach(val => {if (val != null) __params = __params.append('ids', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/attachment/delete`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteAttachments
   * @param ids ids
   */
  deleteAttachmentsUsingDELETE(ids: Array<string>): __Observable<null> {
    return this.deleteAttachmentsUsingDELETEResponse(ids).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * download
   * @param id id
   * @return OK
   */
  downloadUsingGETResponse(id: string): __Observable<__StrictHttpResponse<GridFsResource>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (id != null) __params = __params.set('id', id.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/attachment/download`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<GridFsResource>;
      })
    );
  }
  /**
   * download
   * @param id id
   * @return OK
   */
  downloadUsingGET(id: string): __Observable<GridFsResource> {
    return this.downloadUsingGETResponse(id).pipe(
      __map(_r => _r.body as GridFsResource)
    );
  }

  /**
   * upload
   * @param file file
   * @return OK
   */
  uploadUsingPOSTResponse(file?: Blob): __Observable<__StrictHttpResponse<Attachment>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (file != null) { __formData.append('file', file as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/attachment/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Attachment>;
      })
    );
  }
  /**
   * upload
   * @param file file
   * @return OK
   */
  uploadUsingPOST(file?: Blob): __Observable<Attachment> {
    return this.uploadUsingPOSTResponse(file).pipe(
      __map(_r => _r.body as Attachment)
    );
  }
}

module AttachmentControllerService {
}

export { AttachmentControllerService }
