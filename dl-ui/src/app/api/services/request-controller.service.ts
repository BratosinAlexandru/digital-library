/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Request } from '../models/request';
import { RequestPage } from '../models/request-page';
import { PaginationRequestDtoFiltersRequest } from '../models/pagination-request-dto-filters-request';
import { RequestHistoryDto } from '../models/request-history-dto';

/**
 * Request Controller
 */
@Injectable({
  providedIn: 'root',
})
class RequestControllerService extends __BaseService {
  static readonly acceptRequestUsingPUTPath = '/request/acceptRequest';
  static readonly createRequestUsingPOSTPath = '/request/create';
  static readonly deleteAllUsingDELETEPath = '/request/deleteAll';
  static readonly getPagedUsingPOST1Path = '/request/getPaged';
  static readonly getHistoryUsingGETPath = '/request/history/{requestId}';
  static readonly updateRequestUsingPUTPath = '/request/update';
  static readonly updateEnableUsingPUTPath = '/request/updateEnable';
  static readonly getRequestUsingGETPath = '/request/{requestId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * acceptRequest
   * @param requestIds requestIds
   */
  acceptRequestUsingPUTResponse(requestIds: Array<string>): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestIds;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/request/acceptRequest`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * acceptRequest
   * @param requestIds requestIds
   */
  acceptRequestUsingPUT(requestIds: Array<string>): __Observable<null> {
    return this.acceptRequestUsingPUTResponse(requestIds).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * createRequest
   * @param request request
   * @return Created
   */
  createRequestUsingPOSTResponse(request: Request): __Observable<__StrictHttpResponse<Request>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/request/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Request>;
      })
    );
  }
  /**
   * createRequest
   * @param request request
   * @return Created
   */
  createRequestUsingPOST(request: Request): __Observable<Request> {
    return this.createRequestUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as Request)
    );
  }

  /**
   * deleteAll
   * @param requestIds requestIds
   */
  deleteAllUsingDELETEResponse(requestIds: Array<string>): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestIds;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/request/deleteAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteAll
   * @param requestIds requestIds
   */
  deleteAllUsingDELETE(requestIds: Array<string>): __Observable<null> {
    return this.deleteAllUsingDELETEResponse(requestIds).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getPaged
   * @param pageRequest pageRequest
   * @return OK
   */
  getPagedUsingPOST1Response(pageRequest: PaginationRequestDtoFiltersRequest): __Observable<__StrictHttpResponse<RequestPage>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = pageRequest;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/request/getPaged`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RequestPage>;
      })
    );
  }
  /**
   * getPaged
   * @param pageRequest pageRequest
   * @return OK
   */
  getPagedUsingPOST1(pageRequest: PaginationRequestDtoFiltersRequest): __Observable<RequestPage> {
    return this.getPagedUsingPOST1Response(pageRequest).pipe(
      __map(_r => _r.body as RequestPage)
    );
  }

  /**
   * getHistory
   * @param requestId requestId
   * @return OK
   */
  getHistoryUsingGETResponse(requestId: string): __Observable<__StrictHttpResponse<Array<RequestHistoryDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/request/history/${encodeURIComponent(String(requestId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RequestHistoryDto>>;
      })
    );
  }
  /**
   * getHistory
   * @param requestId requestId
   * @return OK
   */
  getHistoryUsingGET(requestId: string): __Observable<Array<RequestHistoryDto>> {
    return this.getHistoryUsingGETResponse(requestId).pipe(
      __map(_r => _r.body as Array<RequestHistoryDto>)
    );
  }

  /**
   * updateRequest
   * @param request request
   */
  updateRequestUsingPUTResponse(request: Request): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/request/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateRequest
   * @param request request
   */
  updateRequestUsingPUT(request: Request): __Observable<null> {
    return this.updateRequestUsingPUTResponse(request).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * updateEnable
   * @param params The `RequestControllerService.UpdateEnableUsingPUTParams` containing the following parameters:
   *
   * - `requestIds`: requestIds
   *
   * - `enable`: enable
   */
  updateEnableUsingPUTResponse(params: RequestControllerService.UpdateEnableUsingPUTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.requestIds;
    if (params.enable != null) __params = __params.set('enable', params.enable.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/request/updateEnable`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateEnable
   * @param params The `RequestControllerService.UpdateEnableUsingPUTParams` containing the following parameters:
   *
   * - `requestIds`: requestIds
   *
   * - `enable`: enable
   */
  updateEnableUsingPUT(params: RequestControllerService.UpdateEnableUsingPUTParams): __Observable<null> {
    return this.updateEnableUsingPUTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getRequest
   * @param requestId requestId
   * @return OK
   */
  getRequestUsingGETResponse(requestId: string): __Observable<__StrictHttpResponse<Request>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/request/${encodeURIComponent(String(requestId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Request>;
      })
    );
  }
  /**
   * getRequest
   * @param requestId requestId
   * @return OK
   */
  getRequestUsingGET(requestId: string): __Observable<Request> {
    return this.getRequestUsingGETResponse(requestId).pipe(
      __map(_r => _r.body as Request)
    );
  }
}

module RequestControllerService {

  /**
   * Parameters for updateEnableUsingPUT
   */
  export interface UpdateEnableUsingPUTParams {

    /**
     * requestIds
     */
    requestIds: Array<string>;

    /**
     * enable
     */
    enable: boolean;
  }
}

export { RequestControllerService }
