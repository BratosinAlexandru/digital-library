/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AppKey } from '../models/app-key';

/**
 * App Keys Controller
 */
@Injectable({
  providedIn: 'root',
})
class AppKeysControllerService extends __BaseService {
  static readonly getAppKeysUsingGETPath = '/appkey';
  static readonly updateAppKeyUsingPUTPath = '/appkey/update';
  static readonly findByKeyUsingGETPath = '/appkey/{key}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getAppKeys
   * @return OK
   */
  getAppKeysUsingGETResponse(): __Observable<__StrictHttpResponse<Array<AppKey>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/appkey`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<AppKey>>;
      })
    );
  }
  /**
   * getAppKeys
   * @return OK
   */
  getAppKeysUsingGET(): __Observable<Array<AppKey>> {
    return this.getAppKeysUsingGETResponse().pipe(
      __map(_r => _r.body as Array<AppKey>)
    );
  }

  /**
   * updateAppKey
   * @param appKey appKey
   */
  updateAppKeyUsingPUTResponse(appKey: AppKey): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = appKey;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/appkey/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateAppKey
   * @param appKey appKey
   */
  updateAppKeyUsingPUT(appKey: AppKey): __Observable<null> {
    return this.updateAppKeyUsingPUTResponse(appKey).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * findByKey
   * @param key key
   * @return OK
   */
  findByKeyUsingGETResponse(key: string): __Observable<__StrictHttpResponse<AppKey>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/appkey/${encodeURIComponent(String(key))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<AppKey>;
      })
    );
  }
  /**
   * findByKey
   * @param key key
   * @return OK
   */
  findByKeyUsingGET(key: string): __Observable<AppKey> {
    return this.findByKeyUsingGETResponse(key).pipe(
      __map(_r => _r.body as AppKey)
    );
  }
}

module AppKeysControllerService {
}

export { AppKeysControllerService }
