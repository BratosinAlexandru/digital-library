/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RequestType } from '../models/request-type';

/**
 * Request Type Controller
 */
@Injectable({
  providedIn: 'root',
})
class RequestTypeControllerService extends __BaseService {
  static readonly getRequestTypesUsingGETPath = '/request/type';
  static readonly createRequestTypeUsingPOSTPath = '/request/type/create';
  static readonly deleteRequestTypeUsingDELETEPath = '/request/type/delete/{requestTypeId}';
  static readonly updateRequestTypeUsingPUTPath = '/request/type/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getRequestTypes
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getRequestTypesUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<RequestType>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/request/type`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RequestType>>;
      })
    );
  }
  /**
   * getRequestTypes
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getRequestTypesUsingGET(includeDisabled?: boolean): __Observable<Array<RequestType>> {
    return this.getRequestTypesUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<RequestType>)
    );
  }

  /**
   * createRequestType
   * @param requestType requestType
   * @return Created
   */
  createRequestTypeUsingPOSTResponse(requestType: RequestType): __Observable<__StrictHttpResponse<RequestType>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestType;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/request/type/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RequestType>;
      })
    );
  }
  /**
   * createRequestType
   * @param requestType requestType
   * @return Created
   */
  createRequestTypeUsingPOST(requestType: RequestType): __Observable<RequestType> {
    return this.createRequestTypeUsingPOSTResponse(requestType).pipe(
      __map(_r => _r.body as RequestType)
    );
  }

  /**
   * deleteRequestType
   * @param requestTypeId requestTypeId
   */
  deleteRequestTypeUsingDELETEResponse(requestTypeId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/request/type/delete/${encodeURIComponent(String(requestTypeId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteRequestType
   * @param requestTypeId requestTypeId
   */
  deleteRequestTypeUsingDELETE(requestTypeId: string): __Observable<null> {
    return this.deleteRequestTypeUsingDELETEResponse(requestTypeId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * updateRequestType
   * @param requestType requestType
   */
  updateRequestTypeUsingPUTResponse(requestType: RequestType): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestType;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/request/type/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateRequestType
   * @param requestType requestType
   */
  updateRequestTypeUsingPUT(requestType: RequestType): __Observable<null> {
    return this.updateRequestTypeUsingPUTResponse(requestType).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module RequestTypeControllerService {
}

export { RequestTypeControllerService }
