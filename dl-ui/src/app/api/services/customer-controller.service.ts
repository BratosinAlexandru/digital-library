/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Customer } from '../models/customer';

/**
 * Customer Controller
 */
@Injectable({
  providedIn: 'root',
})
class CustomerControllerService extends __BaseService {
  static readonly getCustomerByEmailUsingGETPath = '/customer/getCustomerByEmail';
  static readonly getCustomersByQueryUsingGETPath = '/customer/getCustomersByQuery';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getCustomerByEmail
   * @param email email
   * @return OK
   */
  getCustomerByEmailUsingGETResponse(email: string): __Observable<__StrictHttpResponse<Array<Customer>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (email != null) __params = __params.set('email', email.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/getCustomerByEmail`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Customer>>;
      })
    );
  }
  /**
   * getCustomerByEmail
   * @param email email
   * @return OK
   */
  getCustomerByEmailUsingGET(email: string): __Observable<Array<Customer>> {
    return this.getCustomerByEmailUsingGETResponse(email).pipe(
      __map(_r => _r.body as Array<Customer>)
    );
  }

  /**
   * getCustomersByQuery
   * @param params The `CustomerControllerService.GetCustomersByQueryUsingGETParams` containing the following parameters:
   *
   * - `query`: query
   *
   * - `hasRequestsEnabled`: hasRequestsEnabled
   *
   * @return OK
   */
  getCustomersByQueryUsingGETResponse(params: CustomerControllerService.GetCustomersByQueryUsingGETParams): __Observable<__StrictHttpResponse<Array<Customer>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.query != null) __params = __params.set('query', params.query.toString());
    if (params.hasRequestsEnabled != null) __params = __params.set('hasRequestsEnabled', params.hasRequestsEnabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/customer/getCustomersByQuery`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Customer>>;
      })
    );
  }
  /**
   * getCustomersByQuery
   * @param params The `CustomerControllerService.GetCustomersByQueryUsingGETParams` containing the following parameters:
   *
   * - `query`: query
   *
   * - `hasRequestsEnabled`: hasRequestsEnabled
   *
   * @return OK
   */
  getCustomersByQueryUsingGET(params: CustomerControllerService.GetCustomersByQueryUsingGETParams): __Observable<Array<Customer>> {
    return this.getCustomersByQueryUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<Customer>)
    );
  }
}

module CustomerControllerService {

  /**
   * Parameters for getCustomersByQueryUsingGET
   */
  export interface GetCustomersByQueryUsingGETParams {

    /**
     * query
     */
    query: string;

    /**
     * hasRequestsEnabled
     */
    hasRequestsEnabled?: boolean;
  }
}

export { CustomerControllerService }
