/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Domain } from '../models/domain';

/**
 * Domain Controller
 */
@Injectable({
  providedIn: 'root',
})
class DomainControllerService extends __BaseService {
  static readonly getDomainsUsingGETPath = '/domain';
  static readonly createDomainUsingPOSTPath = '/domain/create';
  static readonly deleteDomainUsingDELETEPath = '/domain/delete';
  static readonly updateDomainUsingPUTPath = '/domain/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getDomains
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getDomainsUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<Domain>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/domain`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Domain>>;
      })
    );
  }
  /**
   * getDomains
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getDomainsUsingGET(includeDisabled?: boolean): __Observable<Array<Domain>> {
    return this.getDomainsUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<Domain>)
    );
  }

  /**
   * createDomain
   * @param domainDto domainDto
   * @return Created
   */
  createDomainUsingPOSTResponse(domainDto: Domain): __Observable<__StrictHttpResponse<Domain>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = domainDto;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/domain/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Domain>;
      })
    );
  }
  /**
   * createDomain
   * @param domainDto domainDto
   * @return Created
   */
  createDomainUsingPOST(domainDto: Domain): __Observable<Domain> {
    return this.createDomainUsingPOSTResponse(domainDto).pipe(
      __map(_r => _r.body as Domain)
    );
  }

  /**
   * deleteDomain
   * @param domainId domainId
   */
  deleteDomainUsingDELETEResponse(domainId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (domainId != null) __params = __params.set('domainId', domainId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/domain/delete`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteDomain
   * @param domainId domainId
   */
  deleteDomainUsingDELETE(domainId: string): __Observable<null> {
    return this.deleteDomainUsingDELETEResponse(domainId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * updateDomain
   * @param domainDto domainDto
   * @return Accepted
   */
  updateDomainUsingPUTResponse(domainDto: Domain): __Observable<__StrictHttpResponse<Domain>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = domainDto;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/domain/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Domain>;
      })
    );
  }
  /**
   * updateDomain
   * @param domainDto domainDto
   * @return Accepted
   */
  updateDomainUsingPUT(domainDto: Domain): __Observable<Domain> {
    return this.updateDomainUsingPUTResponse(domainDto).pipe(
      __map(_r => _r.body as Domain)
    );
  }
}

module DomainControllerService {
}

export { DomainControllerService }
