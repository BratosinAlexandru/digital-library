/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RequestStatus } from '../models/request-status';

/**
 * Request Status Controller
 */
@Injectable({
  providedIn: 'root',
})
class RequestStatusControllerService extends __BaseService {
  static readonly getRequestStatuesUsingGETPath = '/request/status';
  static readonly createRequestStatusUsingPOSTPath = '/request/status/create';
  static readonly deleteRequestStatusUsingDELETEPath = '/request/status/delete/{requestStatusId}';
  static readonly updateRequestStatusUsingPUTPath = '/request/status/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getRequestStatues
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getRequestStatuesUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<RequestStatus>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/request/status`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RequestStatus>>;
      })
    );
  }
  /**
   * getRequestStatues
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getRequestStatuesUsingGET(includeDisabled?: boolean): __Observable<Array<RequestStatus>> {
    return this.getRequestStatuesUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<RequestStatus>)
    );
  }

  /**
   * createRequestStatus
   * @param requestStatus requestStatus
   * @return Created
   */
  createRequestStatusUsingPOSTResponse(requestStatus: RequestStatus): __Observable<__StrictHttpResponse<RequestStatus>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestStatus;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/request/status/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RequestStatus>;
      })
    );
  }
  /**
   * createRequestStatus
   * @param requestStatus requestStatus
   * @return Created
   */
  createRequestStatusUsingPOST(requestStatus: RequestStatus): __Observable<RequestStatus> {
    return this.createRequestStatusUsingPOSTResponse(requestStatus).pipe(
      __map(_r => _r.body as RequestStatus)
    );
  }

  /**
   * deleteRequestStatus
   * @param requestStatusId requestStatusId
   */
  deleteRequestStatusUsingDELETEResponse(requestStatusId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/request/status/delete/${encodeURIComponent(String(requestStatusId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteRequestStatus
   * @param requestStatusId requestStatusId
   */
  deleteRequestStatusUsingDELETE(requestStatusId: string): __Observable<null> {
    return this.deleteRequestStatusUsingDELETEResponse(requestStatusId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * updateRequestStatus
   * @param requestStatus requestStatus
   */
  updateRequestStatusUsingPUTResponse(requestStatus: RequestStatus): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = requestStatus;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/request/status/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateRequestStatus
   * @param requestStatus requestStatus
   */
  updateRequestStatusUsingPUT(requestStatus: RequestStatus): __Observable<null> {
    return this.updateRequestStatusUsingPUTResponse(requestStatus).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module RequestStatusControllerService {
}

export { RequestStatusControllerService }
