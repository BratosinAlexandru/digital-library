/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Notification } from '../models/notification';

/**
 * Notification Controller
 */
@Injectable({
  providedIn: 'root',
})
class NotificationControllerService extends __BaseService {
  static readonly getNotificationsUsingGETPath = '/notification';
  static readonly updateNotificationUsingPUTPath = '/notification/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getNotifications
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getNotificationsUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<Notification>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/notification`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Notification>>;
      })
    );
  }
  /**
   * getNotifications
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getNotificationsUsingGET(includeDisabled?: boolean): __Observable<Array<Notification>> {
    return this.getNotificationsUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<Notification>)
    );
  }

  /**
   * updateNotification
   * @param notification notification
   */
  updateNotificationUsingPUTResponse(notification: Notification): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = notification;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/notification/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateNotification
   * @param notification notification
   */
  updateNotificationUsingPUT(notification: Notification): __Observable<null> {
    return this.updateNotificationUsingPUTResponse(notification).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module NotificationControllerService {
}

export { NotificationControllerService }
