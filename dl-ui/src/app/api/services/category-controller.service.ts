/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Category } from '../models/category';

/**
 * Category Controller
 */
@Injectable({
  providedIn: 'root',
})
class CategoryControllerService extends __BaseService {
  static readonly getCategoriesUsingGETPath = '/category';
  static readonly createCategoryUsingPOSTPath = '/category/create';
  static readonly deleteCategoryUsingDELETEPath = '/category/delete';
  static readonly getCategoriesByDomainUsingGETPath = '/category/getByDomains';
  static readonly getCategoryUsingGETPath = '/category/getCategory';
  static readonly updateCategoryUsingPUTPath = '/category/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getCategories
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getCategoriesUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<Category>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/category`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Category>>;
      })
    );
  }
  /**
   * getCategories
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getCategoriesUsingGET(includeDisabled?: boolean): __Observable<Array<Category>> {
    return this.getCategoriesUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<Category>)
    );
  }

  /**
   * createCategory
   * @param categoryDto categoryDto
   * @return Created
   */
  createCategoryUsingPOSTResponse(categoryDto: Category): __Observable<__StrictHttpResponse<Category>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = categoryDto;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/category/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Category>;
      })
    );
  }
  /**
   * createCategory
   * @param categoryDto categoryDto
   * @return Created
   */
  createCategoryUsingPOST(categoryDto: Category): __Observable<Category> {
    return this.createCategoryUsingPOSTResponse(categoryDto).pipe(
      __map(_r => _r.body as Category)
    );
  }

  /**
   * deleteCategory
   * @param categoryId categoryId
   */
  deleteCategoryUsingDELETEResponse(categoryId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (categoryId != null) __params = __params.set('categoryId', categoryId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/category/delete`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteCategory
   * @param categoryId categoryId
   */
  deleteCategoryUsingDELETE(categoryId: string): __Observable<null> {
    return this.deleteCategoryUsingDELETEResponse(categoryId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getCategoriesByDomain
   * @param params The `CategoryControllerService.GetCategoriesByDomainUsingGETParams` containing the following parameters:
   *
   * - `domainIds`: domainIds
   *
   * - `includeDisabled`: includeDisabled
   *
   * @return OK
   */
  getCategoriesByDomainUsingGETResponse(params: CategoryControllerService.GetCategoriesByDomainUsingGETParams): __Observable<__StrictHttpResponse<Array<Category>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (params.domainIds || []).forEach(val => {if (val != null) __params = __params.append('domainIds', val.toString())});
    if (params.includeDisabled != null) __params = __params.set('includeDisabled', params.includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/category/getByDomains`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Category>>;
      })
    );
  }
  /**
   * getCategoriesByDomain
   * @param params The `CategoryControllerService.GetCategoriesByDomainUsingGETParams` containing the following parameters:
   *
   * - `domainIds`: domainIds
   *
   * - `includeDisabled`: includeDisabled
   *
   * @return OK
   */
  getCategoriesByDomainUsingGET(params: CategoryControllerService.GetCategoriesByDomainUsingGETParams): __Observable<Array<Category>> {
    return this.getCategoriesByDomainUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<Category>)
    );
  }

  /**
   * getCategory
   * @param categoryId categoryId
   * @return OK
   */
  getCategoryUsingGETResponse(categoryId: string): __Observable<__StrictHttpResponse<Category>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (categoryId != null) __params = __params.set('categoryId', categoryId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/category/getCategory`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Category>;
      })
    );
  }
  /**
   * getCategory
   * @param categoryId categoryId
   * @return OK
   */
  getCategoryUsingGET(categoryId: string): __Observable<Category> {
    return this.getCategoryUsingGETResponse(categoryId).pipe(
      __map(_r => _r.body as Category)
    );
  }

  /**
   * updateCategory
   * @param categoryDto categoryDto
   */
  updateCategoryUsingPUTResponse(categoryDto: Category): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = categoryDto;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/category/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateCategory
   * @param categoryDto categoryDto
   */
  updateCategoryUsingPUT(categoryDto: Category): __Observable<null> {
    return this.updateCategoryUsingPUTResponse(categoryDto).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module CategoryControllerService {

  /**
   * Parameters for getCategoriesByDomainUsingGET
   */
  export interface GetCategoriesByDomainUsingGETParams {

    /**
     * domainIds
     */
    domainIds: Array<string>;

    /**
     * includeDisabled
     */
    includeDisabled?: boolean;
  }
}

export { CategoryControllerService }
