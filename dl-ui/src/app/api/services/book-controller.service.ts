/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { BookOverviewPage } from '../models/book-overview-page';
import { GetModel } from '../models/get-model';
import { Book } from '../models/book';
import { BookOverview } from '../models/book-overview';
import { ByteArrayResource } from '../models/byte-array-resource';

/**
 * Book Controller
 */
@Injectable({
  providedIn: 'root',
})
class BookControllerService extends __BaseService {
  static readonly getBooksUsingPOSTPath = '/book';
  static readonly createBookUsingPOSTPath = '/book/create';
  static readonly deleteBookUsingDELETEPath = '/book/delete/{bookId}';
  static readonly getBooksOverviewUsingGETPath = '/book/getOverviews';
  static readonly updateBookUsingPUTPath = '/book/update';
  static readonly getBookUsingGETPath = '/book/{bookId}';
  static readonly disableBookUsingPUTPath = '/book/{bookId}/disable';
  static readonly enableBookUsingPUTPath = '/book/{bookId}/enable';
  static readonly getImageUsingGETPath = '/book/{bookId}/image';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getBooks
   * @param params The `BookControllerService.GetBooksUsingPOSTParams` containing the following parameters:
   *
   * - `getBooksDto`: getBooksDto
   *
   * - `size`: size
   *
   * - `page`: page
   *
   * @return OK
   */
  getBooksUsingPOSTResponse(params: BookControllerService.GetBooksUsingPOSTParams): __Observable<__StrictHttpResponse<BookOverviewPage>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.getBooksDto;
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/book`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BookOverviewPage>;
      })
    );
  }
  /**
   * getBooks
   * @param params The `BookControllerService.GetBooksUsingPOSTParams` containing the following parameters:
   *
   * - `getBooksDto`: getBooksDto
   *
   * - `size`: size
   *
   * - `page`: page
   *
   * @return OK
   */
  getBooksUsingPOST(params: BookControllerService.GetBooksUsingPOSTParams): __Observable<BookOverviewPage> {
    return this.getBooksUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as BookOverviewPage)
    );
  }

  /**
   * createBook
   * @param book book
   * @return Created
   */
  createBookUsingPOSTResponse(book: Book): __Observable<__StrictHttpResponse<Book>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = book;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/book/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Book>;
      })
    );
  }
  /**
   * createBook
   * @param book book
   * @return Created
   */
  createBookUsingPOST(book: Book): __Observable<Book> {
    return this.createBookUsingPOSTResponse(book).pipe(
      __map(_r => _r.body as Book)
    );
  }

  /**
   * deleteBook
   * @param bookId bookId
   */
  deleteBookUsingDELETEResponse(bookId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/book/delete/${encodeURIComponent(String(bookId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteBook
   * @param bookId bookId
   */
  deleteBookUsingDELETE(bookId: string): __Observable<null> {
    return this.deleteBookUsingDELETEResponse(bookId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getBooksOverview
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getBooksOverviewUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<BookOverview>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/book/getOverviews`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<BookOverview>>;
      })
    );
  }
  /**
   * getBooksOverview
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getBooksOverviewUsingGET(includeDisabled?: boolean): __Observable<Array<BookOverview>> {
    return this.getBooksOverviewUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<BookOverview>)
    );
  }

  /**
   * updateBook
   * @param bookDto bookDto
   */
  updateBookUsingPUTResponse(bookDto: Book): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = bookDto;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/book/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateBook
   * @param bookDto bookDto
   */
  updateBookUsingPUT(bookDto: Book): __Observable<null> {
    return this.updateBookUsingPUTResponse(bookDto).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getBook
   * @param bookId bookId
   * @return OK
   */
  getBookUsingGETResponse(bookId: string): __Observable<__StrictHttpResponse<Book>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/book/${encodeURIComponent(String(bookId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Book>;
      })
    );
  }
  /**
   * getBook
   * @param bookId bookId
   * @return OK
   */
  getBookUsingGET(bookId: string): __Observable<Book> {
    return this.getBookUsingGETResponse(bookId).pipe(
      __map(_r => _r.body as Book)
    );
  }

  /**
   * disableBook
   * @param bookId bookId
   */
  disableBookUsingPUTResponse(bookId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/book/${encodeURIComponent(String(bookId))}/disable`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * disableBook
   * @param bookId bookId
   */
  disableBookUsingPUT(bookId: string): __Observable<null> {
    return this.disableBookUsingPUTResponse(bookId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * enableBook
   * @param bookId bookId
   */
  enableBookUsingPUTResponse(bookId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/book/${encodeURIComponent(String(bookId))}/enable`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * enableBook
   * @param bookId bookId
   */
  enableBookUsingPUT(bookId: string): __Observable<null> {
    return this.enableBookUsingPUTResponse(bookId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getImage
   * @param bookId bookId
   * @return OK
   */
  getImageUsingGETResponse(bookId: string): __Observable<__StrictHttpResponse<ByteArrayResource>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/book/${encodeURIComponent(String(bookId))}/image`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ByteArrayResource>;
      })
    );
  }
  /**
   * getImage
   * @param bookId bookId
   * @return OK
   */
  getImageUsingGET(bookId: string): __Observable<ByteArrayResource> {
    return this.getImageUsingGETResponse(bookId).pipe(
      __map(_r => _r.body as ByteArrayResource)
    );
  }
}

module BookControllerService {

  /**
   * Parameters for getBooksUsingPOST
   */
  export interface GetBooksUsingPOSTParams {

    /**
     * getBooksDto
     */
    getBooksDto: GetModel;

    /**
     * size
     */
    size?: number;

    /**
     * page
     */
    page?: number;
  }
}

export { BookControllerService }
