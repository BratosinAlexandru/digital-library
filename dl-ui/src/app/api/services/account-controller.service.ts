/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Account } from '../models/account';
import { PageAccount } from '../models/page-account';
import { PaginationRequestDtoFiltersRequest } from '../models/pagination-request-dto-filters-request';

/**
 * Account Controller
 */
@Injectable({
  providedIn: 'root',
})
class AccountControllerService extends __BaseService {
  static readonly createAccountUsingPOSTPath = '/account/create';
  static readonly deleteAccountsUsingDELETEPath = '/account/delete';
  static readonly deleteAccountUsingDELETEPath = '/account/delete/{accountId}';
  static readonly getAccountsByQueryUsingGETPath = '/account/getAccountsByQuery';
  static readonly getPagedUsingPOSTPath = '/account/getPaged';
  static readonly resetPasswordUsingPUTPath = '/account/resetPassword';
  static readonly getRolesUsingGETPath = '/account/roles';
  static readonly updateAccountUsingPUTPath = '/account/update';
  static readonly getByEmailUsingGETPath = '/account/{email}';
  static readonly isUniqueUsingGETPath = '/account/{email}/isUnique';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * createAccount
   * @param accountDto accountDto
   * @return Created
   */
  createAccountUsingPOSTResponse(accountDto: Account): __Observable<__StrictHttpResponse<Account>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = accountDto;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/account/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Account>;
      })
    );
  }
  /**
   * createAccount
   * @param accountDto accountDto
   * @return Created
   */
  createAccountUsingPOST(accountDto: Account): __Observable<Account> {
    return this.createAccountUsingPOSTResponse(accountDto).pipe(
      __map(_r => _r.body as Account)
    );
  }

  /**
   * deleteAccounts
   * @param ids ids
   */
  deleteAccountsUsingDELETEResponse(ids: Array<string>): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (ids || []).forEach(val => {if (val != null) __params = __params.append('ids', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/account/delete`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteAccounts
   * @param ids ids
   */
  deleteAccountsUsingDELETE(ids: Array<string>): __Observable<null> {
    return this.deleteAccountsUsingDELETEResponse(ids).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * deleteAccount
   * @param accountId accountId
   */
  deleteAccountUsingDELETEResponse(accountId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/account/delete/${encodeURIComponent(String(accountId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteAccount
   * @param accountId accountId
   */
  deleteAccountUsingDELETE(accountId: string): __Observable<null> {
    return this.deleteAccountUsingDELETEResponse(accountId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getAccountsByQuery
   * @param params The `AccountControllerService.GetAccountsByQueryUsingGETParams` containing the following parameters:
   *
   * - `query`: query
   *
   * - `hasRequestEnabled`: hasRequestEnabled
   *
   * @return OK
   */
  getAccountsByQueryUsingGETResponse(params: AccountControllerService.GetAccountsByQueryUsingGETParams): __Observable<__StrictHttpResponse<Array<Account>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.query != null) __params = __params.set('query', params.query.toString());
    if (params.hasRequestEnabled != null) __params = __params.set('hasRequestEnabled', params.hasRequestEnabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/account/getAccountsByQuery`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Account>>;
      })
    );
  }
  /**
   * getAccountsByQuery
   * @param params The `AccountControllerService.GetAccountsByQueryUsingGETParams` containing the following parameters:
   *
   * - `query`: query
   *
   * - `hasRequestEnabled`: hasRequestEnabled
   *
   * @return OK
   */
  getAccountsByQueryUsingGET(params: AccountControllerService.GetAccountsByQueryUsingGETParams): __Observable<Array<Account>> {
    return this.getAccountsByQueryUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<Account>)
    );
  }

  /**
   * getPaged
   * @param pageRequest pageRequest
   * @return OK
   */
  getPagedUsingPOSTResponse(pageRequest: PaginationRequestDtoFiltersRequest): __Observable<__StrictHttpResponse<PageAccount>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = pageRequest;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/account/getPaged`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PageAccount>;
      })
    );
  }
  /**
   * getPaged
   * @param pageRequest pageRequest
   * @return OK
   */
  getPagedUsingPOST(pageRequest: PaginationRequestDtoFiltersRequest): __Observable<PageAccount> {
    return this.getPagedUsingPOSTResponse(pageRequest).pipe(
      __map(_r => _r.body as PageAccount)
    );
  }

  /**
   * resetPassword
   * @param email email
   */
  resetPasswordUsingPUTResponse(email: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = email;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/account/resetPassword`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * resetPassword
   * @param email email
   */
  resetPasswordUsingPUT(email: string): __Observable<null> {
    return this.resetPasswordUsingPUTResponse(email).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getRoles
   * @return OK
   */
  getRolesUsingGETResponse(): __Observable<__StrictHttpResponse<Array<string>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/account/roles`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<string>>;
      })
    );
  }
  /**
   * getRoles
   * @return OK
   */
  getRolesUsingGET(): __Observable<Array<string>> {
    return this.getRolesUsingGETResponse().pipe(
      __map(_r => _r.body as Array<string>)
    );
  }

  /**
   * updateAccount
   * @param accountDto accountDto
   */
  updateAccountUsingPUTResponse(accountDto: Account): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = accountDto;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/account/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateAccount
   * @param accountDto accountDto
   */
  updateAccountUsingPUT(accountDto: Account): __Observable<null> {
    return this.updateAccountUsingPUTResponse(accountDto).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getByEmail
   * @param email email
   * @return OK
   */
  getByEmailUsingGETResponse(email: string): __Observable<__StrictHttpResponse<Account>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/account/${encodeURIComponent(String(email))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Account>;
      })
    );
  }
  /**
   * getByEmail
   * @param email email
   * @return OK
   */
  getByEmailUsingGET(email: string): __Observable<Account> {
    return this.getByEmailUsingGETResponse(email).pipe(
      __map(_r => _r.body as Account)
    );
  }

  /**
   * isUnique
   * @param email email
   * @return OK
   */
  isUniqueUsingGETResponse(email: string): __Observable<__StrictHttpResponse<boolean>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/account/${encodeURIComponent(String(email))}/isUnique`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return (_r as HttpResponse<any>).clone({ body: (_r as HttpResponse<any>).body === 'true' }) as __StrictHttpResponse<boolean>
      })
    );
  }
  /**
   * isUnique
   * @param email email
   * @return OK
   */
  isUniqueUsingGET(email: string): __Observable<boolean> {
    return this.isUniqueUsingGETResponse(email).pipe(
      __map(_r => _r.body as boolean)
    );
  }
}

module AccountControllerService {

  /**
   * Parameters for getAccountsByQueryUsingGET
   */
  export interface GetAccountsByQueryUsingGETParams {

    /**
     * query
     */
    query: string;

    /**
     * hasRequestEnabled
     */
    hasRequestEnabled?: boolean;
  }
}

export { AccountControllerService }
