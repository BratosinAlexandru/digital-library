/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CommunicationChannel } from '../models/communication-channel';

/**
 * Communication Channel Controller
 */
@Injectable({
  providedIn: 'root',
})
class CommunicationChannelControllerService extends __BaseService {
  static readonly getCommunicationChannelsUsingGETPath = '/communication';
  static readonly createCommunicationChannelUsingPOSTPath = '/communication/create';
  static readonly deleteCommunicationChannelUsingDELETEPath = '/communication/delete/{communicationChannelId}';
  static readonly updateCommunicationChannelUsingPUTPath = '/communication/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getCommunicationChannels
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getCommunicationChannelsUsingGETResponse(includeDisabled?: boolean): __Observable<__StrictHttpResponse<Array<CommunicationChannel>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includeDisabled != null) __params = __params.set('includeDisabled', includeDisabled.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/communication`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CommunicationChannel>>;
      })
    );
  }
  /**
   * getCommunicationChannels
   * @param includeDisabled includeDisabled
   * @return OK
   */
  getCommunicationChannelsUsingGET(includeDisabled?: boolean): __Observable<Array<CommunicationChannel>> {
    return this.getCommunicationChannelsUsingGETResponse(includeDisabled).pipe(
      __map(_r => _r.body as Array<CommunicationChannel>)
    );
  }

  /**
   * createCommunicationChannel
   * @param communicationChannel communicationChannel
   * @return Created
   */
  createCommunicationChannelUsingPOSTResponse(communicationChannel: CommunicationChannel): __Observable<__StrictHttpResponse<CommunicationChannel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = communicationChannel;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/communication/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CommunicationChannel>;
      })
    );
  }
  /**
   * createCommunicationChannel
   * @param communicationChannel communicationChannel
   * @return Created
   */
  createCommunicationChannelUsingPOST(communicationChannel: CommunicationChannel): __Observable<CommunicationChannel> {
    return this.createCommunicationChannelUsingPOSTResponse(communicationChannel).pipe(
      __map(_r => _r.body as CommunicationChannel)
    );
  }

  /**
   * deleteCommunicationChannel
   * @param communicationChannelId communicationChannelId
   */
  deleteCommunicationChannelUsingDELETEResponse(communicationChannelId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/communication/delete/${encodeURIComponent(String(communicationChannelId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteCommunicationChannel
   * @param communicationChannelId communicationChannelId
   */
  deleteCommunicationChannelUsingDELETE(communicationChannelId: string): __Observable<null> {
    return this.deleteCommunicationChannelUsingDELETEResponse(communicationChannelId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * updateCommunicationChannel
   * @param communicationChannel communicationChannel
   */
  updateCommunicationChannelUsingPUTResponse(communicationChannel: CommunicationChannel): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = communicationChannel;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/communication/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateCommunicationChannel
   * @param communicationChannel communicationChannel
   */
  updateCommunicationChannelUsingPUT(communicationChannel: CommunicationChannel): __Observable<null> {
    return this.updateCommunicationChannelUsingPUTResponse(communicationChannel).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module CommunicationChannelControllerService {
}

export { CommunicationChannelControllerService }
