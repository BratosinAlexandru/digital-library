/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { NotificationTemplate } from '../models/notification-template';

/**
 * Notification Template Controller
 */
@Injectable({
  providedIn: 'root',
})
class NotificationTemplateControllerService extends __BaseService {
  static readonly getTemplatesByNotificationIdUsingGETPath = '/notification/templates';
  static readonly createTemplateUsingPOSTPath = '/notification/templates/create';
  static readonly deleteTemplateUsingDELETEPath = '/notification/templates/delete/{id}';
  static readonly getTypesUsingGETPath = '/notification/templates/types';
  static readonly updateNotificationTemplateUsingPUTPath = '/notification/templates/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getTemplatesByNotificationId
   * @param id id
   * @return OK
   */
  getTemplatesByNotificationIdUsingGETResponse(id: string): __Observable<__StrictHttpResponse<{[key: string]: Array<NotificationTemplate>}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (id != null) __params = __params.set('id', id.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/notification/templates`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: Array<NotificationTemplate>}>;
      })
    );
  }
  /**
   * getTemplatesByNotificationId
   * @param id id
   * @return OK
   */
  getTemplatesByNotificationIdUsingGET(id: string): __Observable<{[key: string]: Array<NotificationTemplate>}> {
    return this.getTemplatesByNotificationIdUsingGETResponse(id).pipe(
      __map(_r => _r.body as {[key: string]: Array<NotificationTemplate>})
    );
  }

  /**
   * createTemplate
   * @param template template
   * @return OK
   */
  createTemplateUsingPOSTResponse(template: NotificationTemplate): __Observable<__StrictHttpResponse<NotificationTemplate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = template;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/notification/templates/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<NotificationTemplate>;
      })
    );
  }
  /**
   * createTemplate
   * @param template template
   * @return OK
   */
  createTemplateUsingPOST(template: NotificationTemplate): __Observable<NotificationTemplate> {
    return this.createTemplateUsingPOSTResponse(template).pipe(
      __map(_r => _r.body as NotificationTemplate)
    );
  }

  /**
   * deleteTemplate
   * @param id id
   */
  deleteTemplateUsingDELETEResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/notification/templates/delete/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * deleteTemplate
   * @param id id
   */
  deleteTemplateUsingDELETE(id: string): __Observable<null> {
    return this.deleteTemplateUsingDELETEResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getTypes
   * @return OK
   */
  getTypesUsingGETResponse(): __Observable<__StrictHttpResponse<Array<string>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/notification/templates/types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<string>>;
      })
    );
  }
  /**
   * getTypes
   * @return OK
   */
  getTypesUsingGET(): __Observable<Array<string>> {
    return this.getTypesUsingGETResponse().pipe(
      __map(_r => _r.body as Array<string>)
    );
  }

  /**
   * updateNotificationTemplate
   * @param notificationTemplateDto notificationTemplateDto
   */
  updateNotificationTemplateUsingPUTResponse(notificationTemplateDto: NotificationTemplate): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = notificationTemplateDto;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/notification/templates/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * updateNotificationTemplate
   * @param notificationTemplateDto notificationTemplateDto
   */
  updateNotificationTemplateUsingPUT(notificationTemplateDto: NotificationTemplate): __Observable<null> {
    return this.updateNotificationTemplateUsingPUTResponse(notificationTemplateDto).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module NotificationTemplateControllerService {
}

export { NotificationTemplateControllerService }
