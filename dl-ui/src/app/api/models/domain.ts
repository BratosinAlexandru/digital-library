/* tslint:disable */
export interface Domain {
  enabled?: boolean;
  id?: string;
  name?: string;
  orderId?: number;
}
