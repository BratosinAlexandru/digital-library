/* tslint:disable */
import { Customer } from './customer';
import { RequestStatus } from './request-status';
import { RequestType } from './request-type';
export interface FiltersRequest {
  category?: string;
  customer?: Customer;
  enabled?: boolean;
  endDate?: string;
  importantRequest?: boolean;
  requestStatus?: RequestStatus;
  requestType?: Array<RequestType>;
  startDate?: string;
  type?: string;
}
