/* tslint:disable */
export interface GetModel {
  allBooks?: boolean;
  authorName?: string;
  categoryIds?: Array<string>;
  disabledOnly?: boolean;
  domainIds?: Array<string>;
  from?: string;
  myBooks?: boolean;
  name?: string;
  numberOfPages?: number;
  to?: string;
}
