/* tslint:disable */
export interface RequestStatus {
  approachRequest?: boolean;
  color?: string;
  defaultValue?: boolean;
  enabled?: boolean;
  id?: string;
  name?: string;
  orderId?: number;
}
