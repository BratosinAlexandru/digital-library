/* tslint:disable */
import { BookOverview } from './book-overview';
import { CommunicationChannel } from './communication-channel';
import { Customer } from './customer';
import { RequestStatus } from './request-status';
import { RequestType } from './request-type';
export interface Request {
  accountId?: string;
  book?: BookOverview;
  college?: string;
  communicationChannels?: Array<CommunicationChannel>;
  createdOn?: string;
  customer?: Customer;
  enabled?: boolean;
  id?: string;
  importantRequest?: boolean;
  otherMentions?: string;
  requestDate?: string;
  requestStatus?: RequestStatus;
  requestType?: Array<RequestType>;
  updatedOn?: string;
}
