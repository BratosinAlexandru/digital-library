/* tslint:disable */
export interface Attachment {
  chunkSize?: number;
  contentType?: string;
  filename?: string;
  id?: string;
  length?: number;
  md5?: string;
  metadata?: {[key: string]: {}};
  uploadDate?: string;
}
