/* tslint:disable */
export interface Customer {
  createdOn?: string;
  emails?: Array<string>;
  enabled?: boolean;
  id?: string;
  name?: string;
  phones?: Array<string>;
  updatedOn?: string;
}
