/* tslint:disable */
import { PaginationRequests } from './pagination-requests';
import { RequestStatusCount } from './request-status-count';
import { Count } from './count';
export interface RequestPage {
  pagination?: PaginationRequests;
  requestStatus?: Array<RequestStatusCount>;
  totalElements?: Array<Count>;
}
