/* tslint:disable */
import { Account } from './account';
import { Pageable } from './pageable';
import { Sort } from './sort';
export interface PageAccount {
  content?: Array<Account>;
  empty?: boolean;
  first?: boolean;
  last?: boolean;
  number?: number;
  numberOfElements?: number;
  pageable?: Pageable;
  size?: number;
  sort?: Sort;
  totalElements?: number;
  totalPages?: number;
}
