/* tslint:disable */
import { BookOverview } from './book-overview';
export interface BookOverviewPage {
  content?: Array<BookOverview>;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
