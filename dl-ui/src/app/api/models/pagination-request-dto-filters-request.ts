/* tslint:disable */
import { FiltersRequest } from './filters-request';
export interface PaginationRequestDtoFiltersRequest {
  filtersRequest?: FiltersRequest;
  pageNumber?: number;
  pageSize?: number;
  query?: string;
  sortDirection?: string;
  sortProperties?: Array<string>;
}
