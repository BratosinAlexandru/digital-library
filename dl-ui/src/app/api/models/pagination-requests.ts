/* tslint:disable */
import { Request } from './request';
export interface PaginationRequests {
  content?: Array<Request>;
  pageNumber?: number;
  pageSize?: number;
  totalElements?: number;
}
