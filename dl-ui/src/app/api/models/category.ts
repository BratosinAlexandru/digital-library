/* tslint:disable */
import { Domain } from './domain';
export interface Category {
  domain?: Domain;
  enabled?: boolean;
  id?: string;
  name?: string;
  orderId?: number;
}
