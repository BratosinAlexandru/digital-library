/* tslint:disable */
export interface RequestType {
  defaultValue?: boolean;
  enabled?: boolean;
  id?: string;
  name?: string;
}
