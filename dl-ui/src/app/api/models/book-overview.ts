/* tslint:disable */
export interface BookOverview {
  authorName?: string;
  authorRights?: boolean;
  categoryName?: string;
  createdOn?: string;
  domainName?: string;
  enabled?: boolean;
  id?: string;
  name?: string;
  numberOfPages?: number;
  updatedOn?: string;
}
