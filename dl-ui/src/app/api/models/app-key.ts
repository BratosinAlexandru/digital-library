/* tslint:disable */
export interface AppKey {
  enabled?: boolean;
  id?: string;
  key?: string;
  value?: {};
}
