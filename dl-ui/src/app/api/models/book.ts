/* tslint:disable */
import { Attachment } from './attachment';
export interface Book {
  attachment?: Array<Attachment>;
  authorName?: string;
  authorRights?: boolean;
  categoryId?: string;
  description?: string;
  domainId?: string;
  enabled?: boolean;
  from?: number;
  id?: string;
  image?: ArrayBuffer;
  name?: string;
  numberOfPages?: number;
  to?: number;
}
