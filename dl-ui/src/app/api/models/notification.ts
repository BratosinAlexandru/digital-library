/* tslint:disable */
export interface Notification {
  emails?: Array<string>;
  enableToMain?: boolean;
  enabled?: boolean;
  id?: string;
  key?: string;
  name?: string;
  variables?: Array<string>;
}
