/* tslint:disable */
import { Notification } from './notification';
export interface NotificationTemplate {
  content?: string;
  default?: boolean;
  enabled?: boolean;
  id?: string;
  language?: string;
  notification?: Notification;
  subject?: string;
  type?: string;
}
