/* tslint:disable */
export interface Account {
  email?: string;
  enabled?: boolean;
  id?: string;
  name?: string;
  password?: string;
  profilePicture?: string;
  roles?: Array<string>;
}
