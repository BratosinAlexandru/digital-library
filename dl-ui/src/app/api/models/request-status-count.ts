/* tslint:disable */
import { RequestStatus } from './request-status';
export interface RequestStatusCount {
  count?: number;
  requestStatus?: Array<RequestStatus>;
}
