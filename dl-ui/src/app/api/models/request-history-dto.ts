/* tslint:disable */
import { Request } from './request';
export interface RequestHistoryDto {
  author?: string;
  changed?: Array<string>;
  changes?: Array<string>;
  state?: Request;
  type?: string;
  updatedOn?: string;
  version?: number;
}
