/* tslint:disable */
export interface CommunicationChannel {
  enabled?: boolean;
  id?: string;
  name?: string;
}
