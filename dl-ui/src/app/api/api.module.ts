/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { AccountControllerService } from './services/account-controller.service';
import { AppKeysControllerService } from './services/app-keys-controller.service';
import { AttachmentControllerService } from './services/attachment-controller.service';
import { BookControllerService } from './services/book-controller.service';
import { CategoryControllerService } from './services/category-controller.service';
import { CommunicationChannelControllerService } from './services/communication-channel-controller.service';
import { CustomerControllerService } from './services/customer-controller.service';
import { DomainControllerService } from './services/domain-controller.service';
import { NotificationControllerService } from './services/notification-controller.service';
import { NotificationTemplateControllerService } from './services/notification-template-controller.service';
import { RequestControllerService } from './services/request-controller.service';
import { RequestStatusControllerService } from './services/request-status-controller.service';
import { RequestTypeControllerService } from './services/request-type-controller.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    AccountControllerService,
    AppKeysControllerService,
    AttachmentControllerService,
    BookControllerService,
    CategoryControllerService,
    CommunicationChannelControllerService,
    CustomerControllerService,
    DomainControllerService,
    NotificationControllerService,
    NotificationTemplateControllerService,
    RequestControllerService,
    RequestStatusControllerService,
    RequestTypeControllerService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
