import { LOCATION_INITIALIZED } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Injector } from "@angular/core";
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateService } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { SupportedLanguageEnum } from "src/app/models/enums/supported-language";

export function translateAoTLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/login/', '.json');
}

export function translateInitFactory(translateService: TranslateService, injector: Injector) {
    return () =>
        new Promise((resolve) => {
            injector.get(LOCATION_INITIALIZED, Promise.resolve(null)).then(() => {
                translateService.setDefaultLang(SupportedLanguageEnum.RO);
                if (localStorage.getItem('language') != null) {
                    translateService.use(localStorage.getItem('language'));
                } else {
                    translateService.use(SupportedLanguageEnum.RO);
                }

                resolve(null);
            });
        });
}

export class GlobalMissingTranslationHandlerService implements MissingTranslationHandler {
    public handle(params: MissingTranslationHandlerParams) {
        console.warn(`%cMissing translation key: ${params.key}`, 'color: Red; font-weight: bold');
        return params.key;
    }
}