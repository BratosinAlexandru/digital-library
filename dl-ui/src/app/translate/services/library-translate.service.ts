import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  LangChangeEvent,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateLoader,
  TranslateParser,
  TranslateService,
  TranslateStore,
} from '@ngx-translate/core';
import { merge, Observable, of, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { SupportedLanguageEnum } from '../../models/enums/supported-language';

@Injectable({
  providedIn: 'root'
})
export class LibraryTranslateService extends TranslateService {

  constructor(
    private _angularTranslateService: TranslateService,
    private _angularTranslateStore: TranslateStore,
    private _angularTranslateLoader: TranslateLoader,
    private _angularTranslateCompiler: TranslateCompiler,
    private _angularTranslateParser: TranslateParser,
    private _angularTranslateHandler: MissingTranslationHandler,
    private _httpClient: HttpClient
  ) {
    super(
      _angularTranslateStore,
      _angularTranslateLoader,
      _angularTranslateCompiler,
      _angularTranslateParser,
      _angularTranslateHandler,
      true,
      true,
      true,
      SupportedLanguageEnum.RO
    );
  }

  public addTranslations(path: string): Observable<any> {
    const subject$: Subject<boolean> = new Subject();

    const observableList$: Observable<string>[] = [];
    observableList$.push(of(this._angularTranslateStore.currentLang));
    observableList$.push(this._angularTranslateStore.onLangChange.pipe(map((event: LangChangeEvent) => event.lang)));

    merge(...observableList$)
      .pipe(
        switchMap((selectedLanguage: string) => {
          return this._httpClient.get(`assets/i18n/${path}/${selectedLanguage}.json`).pipe(
            map((json) => {
              return [selectedLanguage, json];
            })
          );
        })
      )
      .subscribe(([selectedLanguage, json]: [string, any]) => {
        this._angularTranslateService.setTranslation(selectedLanguage, json, true);

        if (!subject$.closed) {
          subject$.next(true);
          subject$.complete();
        }
      });

    return subject$;
  }
}
