// import { AdminTranslateLoader } from './../modules/admin/admin-translate.loader';
import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localeRo from '@angular/common/locales/ro';
import { APP_INITIALIZER, Injector, LOCALE_ID, NgModule } from '@angular/core';
import {
  MissingTranslationHandler,
  TranslateLoader,
  TranslateModule as AngularTranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { AdminTranslateLoader } from '../modules/admin/admin-translate.loader';
import { CatalogTranslateLoader } from '../modules/catalog/catalog-translate.module';
import { RequestsTranslateLoader } from '../modules/requests/requests-translate.loader';
import { localeFactory } from './factories/locale/locale.factory';
import {
  GlobalMissingTranslationHandlerService,
  translateAoTLoaderFactory,
  translateInitFactory,
} from './factories/translate/translate.factory';
import { LibraryTranslateService } from './services/library-translate.service';

@NgModule({
  imports: [
    HttpClientModule,
    AngularTranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateAoTLoaderFactory,
        deps: [HttpClient],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: GlobalMissingTranslationHandlerService,
      },
    }),
  ],
  providers: [
    LibraryTranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: translateInitFactory,
      deps: [TranslateService, Injector],
      multi: true,
    },
    {
      provide: LOCALE_ID,
      useFactory: localeFactory,
      deps: [],
    },
    AdminTranslateLoader,
    CatalogTranslateLoader,
    RequestsTranslateLoader,
  ],
  exports: [AngularTranslateModule],
})
export class LibraryTranslateModule { }

registerLocaleData(localeRo);
