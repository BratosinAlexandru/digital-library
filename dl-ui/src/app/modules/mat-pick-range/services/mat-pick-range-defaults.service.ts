import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Options } from '../model/model';

@Injectable({
  providedIn: 'root'
})
export class MatPickRangeDefaultsService {

  public startProjectDate = new Date(2021, 4, 2);

  constructor() { }

  public getDefaultOptions(filters?: any): Options {
    const today = new Date();
    const fromMax = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    const toMax = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    const backDate = (numberOfDays: number) => {
      const todayNow = new Date();
      return new Date(todayNow.setDate(todayNow.getDate() - numberOfDays));
    }
    const currentMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currentMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    const presets = [
      { presetLabel: 'Azi', range: { fromDate: new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0), toDate: today } },
      { presetLabel: 'Ultimile 7 zile', range: { fromDate: backDate(7), toDate: today } },
      { presetLabel: 'Luna aceasta', range: { fromDate: currentMonthStart, toDate: currentMonthEnd } },
      { presetLabel: 'Luna trecută', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } },
      { presetLabel: 'Anul trecut', range: { fromDate: new Date(today.getFullYear() - 1, 0, 1, 0, 0, 0), toDate: new Date(today.getFullYear() - 1, 11, 31, 23, 59, 59) } },
      { presetLabel: 'Toată perioada', range: { fromDate: this.startProjectDate, toDate: today } }
    ];

    const startDate = new Date(today.getFullYear(), today.getMonth() - 6, today.getDate(), 0, -1 * today.getTimezoneOffset() + 59, 59, 0);
    return {
      presets,
      format: 'dd/MM/yyyy',
      locale: 'ro-RO',
      applyLabel: 'Ok',
      startDatePrefix: 'DIN',
      endDatePrefix: 'PANA IN',
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: true,
        hasBackdrop: true
      },
      cancelLabel: 'Cancel',
      fromMinMax: { fromDate: this.startProjectDate, toDate: fromMax },
      toMinMax: { fromDate: this.startProjectDate, toDate: toMax },
      range: {
        fromDate: filters && filters.startDate ? new Date(filters.startDate) : startDate,
        toDate: filters && filters.endDate ? new Date(filters.endDate) : today
      },
    }
  }
}
