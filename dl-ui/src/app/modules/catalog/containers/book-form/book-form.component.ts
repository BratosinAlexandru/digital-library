import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { Attachment, Book, Category, Domain } from 'src/app/api/models';
import * as FileSaver from 'file-saver';
import * as AuthSelectors from 'src/app/modules/auth/state/auth.selectors';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import { State, } from '../../state';
import { select, Store } from '@ngrx/store';
import { AlertService } from 'src/app/core/services/alert.service';
import { ApiConfiguration } from 'src/app/api/api-configuration';
import { BooksService } from '../../services/books.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { map, skip, take } from 'rxjs/operators';
import { getDomains } from 'src/app/modules/admin/state';
import { loadDomains } from 'src/app/modules/admin/state/domains/domains.actions';
import { DomainService } from 'src/app/modules/admin/services/domain.service';
import { CategoryService } from 'src/app/modules/admin/services/category.service';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Output() save = new EventEmitter();
  @Output() goBack = new EventEmitter();

  public bookForm: FormGroup;

  public domains: Array<Domain>;
  public categories: Array<Category>;

  public uploading: boolean;
  public downloading: boolean;
  public saveRequestTrigger: boolean;
  public newAttachments: Array<Attachment> = [];
  public deleteAttachments: Array<Attachment> = [];
  public showAttachmentsPanel = false;
  public apiUrl: string;
  public editor = ClassicEditor;
  public erroredImage: boolean;
  public image: string;

  public subscriptions$: Array<Subscription> = [];
  private destroy$: Subject<boolean> = new Subject<boolean>();


  constructor(
    private fb: FormBuilder,
    private store: Store<State & AuthState.State>,
    private alertService: AlertService,
    private cdRef: ChangeDetectorRef,
    private authStore: Store<AuthState.State>,
    private apiConfiguration: ApiConfiguration,
    private bookService: BooksService,
    private domSanitizer: DomSanitizer,
    private dialog: MatDialog,
    private domainService: DomainService,
    private categoryService: CategoryService,
    private translateService: TranslateService,
  ) {
    this.apiUrl = this.apiConfiguration.rootUrl;
    this.erroredImage = true;
  }


  ngOnInit(): void {
    this.createForm();

    this.subscriptions$.push(
      forkJoin([
        this.domainService.getDomains(true),
        this.categoryService.getCategories(true)
      ])
        .pipe(take(1))
        .subscribe(async ([domains, categories]) => {
          this.domains = [...domains];
          this.categories = [...categories];
          this.cdRef.markForCheck();
        })
    );

  }

  private createForm() {
    this.bookForm = this.fb.group({
      id: this.fb.control(null),
      name: this.fb.control(null, Validators.required),
      authorName: this.fb.control(null, Validators.required),
      description: this.fb.control(null, Validators.required),
      numberOfPages: this.fb.control(null, Validators.required),
      domainId: this.fb.control(null, Validators.required),
      categoryId: this.fb.control(null, Validators.required),
      image: this.fb.control(null),
      attachment: this.fb.array([]),
      enabled: this.fb.control(true),
      from: this.fb.control(null),
      to: this.fb.control(null),
      authorRights: this.fb.control(true, Validators.required),
    });

    this.bookForm.get('from').disable();
    this.bookForm.get('to').disable();

    this.subscriptions$.push(
      this.bookForm.valueChanges.pipe(
        skip(1)
      ).subscribe(() => {
        this.bookForm.markAllAsTouched();
      })
    );
  }

  focusOut(input: string) {
    const controller = this.bookForm.get(input);
    if (typeof controller.value === 'string') {
      controller.setValue(null);
    }
  }

  isReadonly(input: string): boolean {
    const controller = this.bookForm.get(input);
    if (controller.value && typeof controller.value !== 'string') {
      return true;
    }
    return false;
  }

  compareObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id === object2.id;
  }

  displayDomains(domain?: Domain): string | undefined {
    return domain ? displayObject(domain, 'name') : undefined;
  }

  displayCategory(category?: Category): string | undefined {
    return category ? displayObject(category, 'name') : undefined;
  }

  uploadFile(e: any, fileInput: any) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    if (file) {
      if (file.size > 5242880) {
        this.alertService.error(this.translateService.instant('Fisierul este mai mare de 5MB!'));
        return;
      }
      const validExtension = 'pdf';
      const ext = file.type.substr(file.type.lastIndexOf('/') + 1);
      if (!ext || ext !== validExtension) {
        this.alertService.error(this.translateService.instant('Va rugam sa introduceti un fisier de tip pdf'));
        return;
      }
      this.uploading = true;
      this.subscriptions$.push(
        this.bookService.uploadAttachment(file).subscribe(
          (attachment: Attachment) => {
            if (attachment) {
              const formArray = this.bookForm.controls.attachment as FormArray;
              formArray.markAsTouched();
              formArray.push(new FormControl(attachment));
              this.newAttachments.push(attachment);
              this.bookForm.get('from').enable();
              this.bookForm.get('to').enable();
            }
            this.uploading = false;
            fileInput.value = '';
            this.cdRef.detectChanges();
          },
          (error) => {
            this.alertService.error(
              error && error.error && error.error.message ? error.error.message : 'Internal error!'
            );
            this.uploading = false;
            fileInput.value = '';
            this.cdRef.detectChanges();
          }
        )
      );
    }
  }

  onFileChanged(event) {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      return;
    }
    reader.onload = (data: any) => {
      this.image = data.target.result.split('base64,')[1];
      this.bookForm.get('image').patchValue(data.target.result);
      this.erroredImage = false;
      this.cdRef.detectChanges();
    };
    reader.readAsDataURL(file);
  }

  downloadAttachment(attachment: Attachment) {
    this.downloading = true;
    this.subscriptions$.push(
      this.bookService.downloadAttachment(attachment.id).subscribe(
        (data: any) => {
          let type = attachment.contentType;
          if (!type && attachment.metadata) {
            type = '' + attachment.metadata._contentType;
          }
          const blob = new Blob([data], { type });
          FileSaver.saveAs(blob, attachment.filename);
          const fileURL = URL.createObjectURL(blob);
          window.open(fileURL, '_blank');
          this.downloading = false;
          this.cdRef.detectChanges();
        },
        (error) => {
          this.alertService.error('Server internal error!');
          this.downloading = false;
          this.cdRef.detectChanges();
        }
      )
    );
  }

  removeAttachment(control: FormControl) {
    const confirmMessage = this.translateService.instant('Sigur doriți să ștergeți acest fișier?');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: { confirmMessage },
    });
    this.subscriptions$.push(
      dialogRef.afterClosed().subscribe((confirm) => {
        if (confirm) {
          const formArray = this.bookForm.controls.attachment as FormArray;
          formArray.markAllAsTouched();
          formArray.removeAt(formArray.value.findIndex((attach) => attach.id === control.value.id));
          this.deleteAttachments.push(control.value);
          this.cdRef.detectChanges();
        }
      })
    );
  }

  getThumb(image: string) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + image);
  }

  saveBook() {
    this.saveRequestTrigger = true;
    const book = this.bookForm.getRawValue();
    book.image = this.image;
    this.save.emit(book);
  }

  deleteNonNecesaryAttachments() {
    if (this.saveRequestTrigger) {
      if (this.deleteAttachments.length > 0) {
        const ids = this.deleteAttachments.map((e) => e.id);
        this.subscriptions$.push(this.bookService.deleteAttachments(ids).subscribe());
      }
    } else {
      if (this.deleteAttachments.length > 0) {
        const ids = this.deleteAttachments.map((e) => e.id);
        this.subscriptions$.push(this.bookService.deleteAttachments(ids).subscribe());
      }
    }
  }

  getCountAttachments(formValue: Book): number {
    return (formValue.attachment ? formValue.attachment.length : 0);
  }

  triggerUploadInput() {
    document.getElementById('upload_req_file_input').click();
  }


  ngOnDestroy(): void {
    this.deleteNonNecesaryAttachments();
    this.subscriptions$.forEach((e) => {
      if (e) {
        e.unsubscribe();
      }
    });
  }
}

function displayObject(object: any, prop?: string): string {
  let result = '';
  if (typeof object === 'string') {
    return object;
  }
  if (object.name) {
    result += object.name + ' ';
    return object.name;

  }
  return result;
}
