import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { select, Store } from '@ngrx/store';
import { forkJoin, of, Subscription } from 'rxjs';
import { Book, Category, Domain } from 'src/app/api/models';
import { MatPickRangeComponent } from 'src/app/modules/mat-pick-range/mat-pick-range/mat-pick-range.component';
import { Options, PresetItem, Range } from 'src/app/modules/mat-pick-range/model/model';
import { Filters } from '../../state/reducers/books.reducer';
import { State } from '../../state';
import { BooksService } from '../../services/books.service';
import { DomainService } from 'src/app/modules/admin/services/domain.service';
import { CategoryService } from 'src/app/modules/admin/services/category.service';
import { debounceTime, take } from 'rxjs/operators';

import * as moment from 'moment';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import { selectRoles } from 'src/app/modules/auth/state/auth.selectors';

@Component({
  selector: 'book-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() filterChanged: EventEmitter<Filters> = new EventEmitter<Filters>();
  @Input() filters: Filters;
  @ViewChild('accordion', { static: true }) accordion: MatAccordion;
  @ViewChild('dateRangePicker') dateRangePicker: MatPickRangeComponent;

  public options: Options;
  public expanded: boolean;
  public filterForm: FormGroup;

  public domainOptions: Array<Domain>;
  public categoryOptions: Array<Category>
  public isAdmin: boolean;

  public _subscriptions: Array<Subscription> = [];

  constructor(
    private store: Store<State>,
    private booksService: BooksService,
    private domainService: DomainService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private authStore: Store<AuthState.State>
  ) {
    this.domainOptions = [];
    this.categoryOptions = [];
  }


  ngOnInit(): void {
    this.options = this.setupOptions(this.filters);

    this.authStore.pipe(take(1), select(selectRoles)).subscribe(roles => {
      if (roles) {
        this.isAdmin = roles.includes('ROLE_ADMIN');
      }
    })

    this.filterForm = this.fb.group({
      name: this.filters.name,
      authorName: this.filters.authorName,
      numberOfPages: this.filters.numberOfPages,
      domainIds: [],
      categoryIds: [],
    });

    this.filterForm.patchValue({
      domainIds: this.filters.domainIds,
      categoryIds: this.filters.categoryIds,
    });

    this._subscriptions.push(
      forkJoin([
        this.domainService.getDomains(true),
        this.filters.domainIds && this.filters.domainIds.length > 0
          ? this.categoryService.getCategoriesForDomain(this.filters.domainIds)
          : of([]),
      ])
        .pipe(take(1))
        .subscribe(async ([domains, categories]) => {
          this.domainOptions = [...domains];
          this.categoryOptions = [...categories];
          this.cdr.markForCheck();
        }),

      this.filterForm.get('domainIds').valueChanges.subscribe((domains: string[]) => {
        if (domains && domains.length > 0) {
          forkJoin([
            this.categoryService.getCategoriesForDomain(domains),
          ])
            .pipe(take(1))
            .subscribe(([newCategories]) => {
              this.categoryOptions = [...newCategories];
              this.filterForm.get('categoryIds').patchValue([]);
            })
        } else {
          this.filterForm.get('categoryIds').patchValue([]);
        }
      }),

      this.filterForm.valueChanges.pipe(debounceTime(100)).subscribe((value) => {
        this.filterChanged.emit({
          ...this.filters,
          name: value.name,
          authorName: value.authorName,
          numberOfPages: value.numberOfPages,
          domainIds: value.domainIds,
          categoryIds: value.categoryIds,
          page: 0
        });
      })
    );

    // this.initClientFilters();
  }

  resetForm() {
    const to = new Date();
    const from = new Date(
      to.getFullYear(),
      to.getMonth() - 6,
      to.getDate(),
      0,
      -1 * to.getTimezoneOffset() + 59,
      59,
      0
    );
    this.filterForm.reset(undefined, { emitEvent: false });
    this.filterChanged.emit({
      ...this.filters,
      from,
      to,
      domainIds: [],
      categoryIds: [],
      name: undefined,
      authorName: undefined,
      numberOfPages: undefined,
      page: 0
    });

    this.resetDateRangePicker();
  }

  private resetDateRangePicker() {
    const today = new Date();
    const fromDate = new Date(
      today.getFullYear(),
      today.getMonth() - 6,
      today.getDate(),
      0,
      -1 * today.getTimezoneOffset() + 59,
      59,
      0
    );
    const resetRange = { fromDate, toDate: today };
    this.dateRangePicker.resetDates(resetRange);
  }

  private setupOptions(filters: Filters) {
    const today = new Date();

    const presets: Array<PresetItem> = [
      {
        presetLabel: 'Azi',
        range: {
          fromDate: moment().startOf('day').toDate(),
          toDate: moment().endOf('day').toDate(),
        },
      },
      {
        presetLabel: 'Ultimile 7 zile',
        range: {
          fromDate: moment().subtract(7, 'days').startOf('day').toDate(),
          toDate: moment().endOf('day').toDate(),
        },
      },
      {
        presetLabel: 'Luna aceasta',
        range: {
          fromDate: moment().startOf('month').toDate(),
          toDate: moment().endOf('month').toDate(),
        },
      },
      {
        presetLabel: 'Luna trecută',
        range: {
          fromDate: moment().subtract(1, 'month').startOf('month').toDate(),
          toDate: moment().subtract(1, 'month').endOf('month').toDate(),
        },
      },
      {
        presetLabel: 'Anul trecut',
        range: {
          fromDate: moment().subtract(1, 'year').startOf('year').toDate(),
          toDate: moment().subtract(1, 'year').endOf('year').toDate(),
        },
      },
      {
        presetLabel: 'Toată perioada',
        range: {
          fromDate: new Date(2021, 4, 2),
          toDate: moment().endOf('day').toDate(),
        },
      },
    ];
    const startDate = new Date(
      today.getFullYear(),
      today.getMonth() - 6,
      today.getDate(),
      0,
      -1 * today.getTimezoneOffset() + 59,
      59,
      0
    );
    return {
      presets,
      format: 'dd/MM/yyyy',
      locale: 'ro-RO',
      applyLabel: 'Ok',
      startDatePrefix: 'DIN',
      endDatePrefix: 'PANA IN',
      placeholder: 'Selecteaza perioada',
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: true,
        hasBackdrop: true,
      },
      cancelLabel: 'Cancel',
      range: {
        fromDate: filters.from ? moment(filters.from).subtract(1, 'days').toDate() : startDate,
        toDate: filters.to ? moment(filters.to).subtract(2, 'days').toDate() : today,
      },
    };
  }

  public updateRange(range: Range) {
    this.filterChanged.emit({
      from: moment(range.fromDate).add(1, 'days').toDate(),
      to: moment(range.toDate).add(2, 'days').toDate(),
    });
  }


  ngAfterViewInit(): void {
    this.accordion.openAll();
  }

  public parseListToString(list: Array<string>): string {
    let result = '';
    if (list && list.length > 0) {
      result += ' ';
      list.forEach((e) => (result += e + ' '));
    }
    return result;
  }


  ngOnDestroy(): void {
    this._subscriptions.forEach((it) => it.unsubscribe());
  }

}
