import { Component, OnDestroy } from '@angular/core';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { Book } from 'src/app/api/models';
import { Display } from '../../state/reducers/books-overview.reducer';
import { Filters } from '../../state/reducers/books.reducer';
import { State, selectAllBooks, selectDisplay, selectBookFilters } from '../../state';
import * as AuthSelectors from 'src/app/modules/auth/state/auth.selectors';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { BooksOverviewActions } from '../../state/actions';
import { PageEvent } from '@angular/material/paginator';
@Component({
  selector: 'book-overview',
  templateUrl: './book-overview.component.html',
  styleUrls: ['./book-overview.component.scss']
})
export class BookOverviewComponent implements OnDestroy {
  _subscriptions: Array<Subscription> = [];
  books$: Observable<Array<Book>>;
  selectedBook$: Observable<Book>;
  bookFilters$: Observable<Filters>;
  displayMode$: Observable<Display>;
  bookFilters: Filters;

  constructor(
    private store: Store<State>,
    private router: Router,
    private authStore: Store<AuthState.State>,
    private activatedRoute: ActivatedRoute,
  ) {
    this.books$ = store.pipe(select(selectAllBooks));
    this.displayMode$ = store.pipe(select(selectDisplay));
    this.bookFilters$ = store.pipe(select(selectBookFilters));
    this._subscriptions.push(
      this.bookFilters$.subscribe((bookFilters) => {
        this.bookFilters = bookFilters;
      })
    );
  }

  openBook(book) {
    this.router.navigate(['../edit', book.id], { relativeTo: this.activatedRoute });
  }

  deleteBook(book) {
    this.store.dispatch(BooksOverviewActions.deleteBookConfirmation({ id: book.id }));
  }

  readBook(book) {
    this.router.navigate(['../read', book.id], { relativeTo: this.activatedRoute });
  }

  enableBook(book) {
    this.store.dispatch(BooksOverviewActions.enableBookConfirmation({ id: book.id }));
  }

  disableBook(book) {
    this.store.dispatch(BooksOverviewActions.disableBookConfirmation({ id: book.id }));
  }

  changeDisplayMode(displayMode: Display) {
    this.store.dispatch(BooksOverviewActions.setDisplay({ display: displayMode }));
  }

  filterChanged(filters: Filters) {
    this.store.dispatch(BooksOverviewActions.updateFilters({ filters }));
  }

  pageChange(event: PageEvent) {
    this.store.dispatch(
      BooksOverviewActions.updateFilters({ filters: { page: event.pageIndex, size: event.pageSize } })
    );
  }

  closeSubscriptions(subscriptions: Array<Subscription>) {
    subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  ngOnDestroy(): void {
    this.closeSubscriptions(this._subscriptions);
  }



}
