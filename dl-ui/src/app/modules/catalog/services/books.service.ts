import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiConfiguration } from "src/app/api/api-configuration";
import { Attachment, Book, BookOverviewPage, Category, GridFsResource } from "src/app/api/models";
import { AppKeysControllerService, AttachmentControllerService, BookControllerService, CategoryControllerService, DomainControllerService } from "src/app/api/services";
import GetBooksUsingPOSTParams = BookControllerService.GetBooksUsingPOSTParams;
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable()
export class BooksService {
    private API_PATH = 'api_path';

    constructor(
        private http: HttpClient,
        config: ApiConfiguration,
        private bookService: BookControllerService,
        private domainService: DomainControllerService,
        private categoryService: CategoryControllerService,
        private attachmentService: AttachmentControllerService
    ) {
        this.API_PATH = config.rootUrl;
    }

    getCategories(includeDisabled: boolean = false): Observable<Array<Category>> {
        return this.categoryService.getCategoriesUsingGET(includeDisabled);
    }


    /** books */
    getBooks(params: GetBooksUsingPOSTParams): Observable<BookOverviewPage> {
        return this.bookService.getBooksUsingPOST(params);
    }

    getBook(bookId: string): Observable<Book> {
        return this.bookService.getBookUsingGET(bookId);
    }

    enablebook(bookId: string): Observable<null> {
        return this.bookService.enableBookUsingPUT(bookId);
    }

    disableBook(bookId: string): Observable<null> {
        return this.bookService.disableBookUsingPUT(bookId);
    }

    deleteBook(bookId: string): Observable<null> {
        return this.bookService.deleteBookUsingDELETE(bookId);
    }

    updateBook(book: Book): Observable<Book> {
        return this.bookService.updateBookUsingPUT(book);
    }

    getBookImageByBookId(bookId: string) {
        return this.API_PATH + '/book/' + bookId + '/image';
    }

    createBook(book: Book): Observable<Book> {
        return this.bookService.createBookUsingPOST(book);
    }

    downloadAttachment(id: string): Observable<GridFsResource> {
        return this.attachmentService.downloadUsingGET(id);
    }

    uploadAttachment(file: Blob): Observable<Attachment> {
        return this.attachmentService.uploadUsingPOST(file);
    }

    deleteAttachments(ids: Array<string>): Observable<Attachment> {
        return this.attachmentService.deleteAttachmentsUsingDELETE(ids);
    }

}