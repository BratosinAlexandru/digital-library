import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookOverviewComponent } from './containers/book-overview/book-overview.component';
import { FiltersComponent } from './containers/filters/filters.component';
import { CatalogRoutingModule } from './catalog-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateAoTLoaderFactory } from 'src/app/translate/factories/translate/translate.factory';
import { HttpClient } from '@angular/common/http';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CoreModule } from 'src/app/core';
import { AdminModule } from '../admin/admin.module';
import { DeleteConfirmationComponent } from './components/delete-confirmation/delete-confirmation.component';
import { MaterialModule } from 'src/app/material';
import { StoreModule } from '@ngrx/store';
import * as CatalogState from './state/'
import { EffectsModule } from '@ngrx/effects';
import { BooksOverviewEffects } from './state/effects/books-overview.effects';
import { BooksEffects } from './state/effects/books.effects';
import { BookListComponent } from './components/book-list/book-list.component';
import { BooksService } from './services/books.service';
import { BookComponent } from './components/book/book.component';
import { BookEffects } from './state/effects/book.effects';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AddBookComponent } from './components/add-book/add-book.component';
import { BookFormComponent } from './containers/book-form/book-form.component';
import { ConfirmDialogComponent } from './containers/confirm-dialog/confirm-dialog.component';
import { ReadBookComponent } from './components/read-book/read-book.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';



export const COMPONENTS = [
  BookOverviewComponent,
  FiltersComponent,
  DeleteConfirmationComponent,
  BookListComponent,
  BookComponent,
  AddBookComponent,
  BookFormComponent,
  ConfirmDialogComponent,
  ReadBookComponent,
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    PdfViewerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateAoTLoaderFactory,
        deps: [HttpClient],
      },
    }),
    StoreModule.forFeature(CatalogState.catalogFeatureKey, CatalogState.reducers),
    EffectsModule.forFeature([BooksOverviewEffects, BooksEffects, BookEffects]),
    MatTooltipModule,
    CoreModule,
    AdminModule,
    CKEditorModule,
  ],
  providers: [
    BooksService
  ],
})
export class CatalogModule { }
