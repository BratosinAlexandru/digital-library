import { createReducer, on } from "@ngrx/store";
import { Category, Domain } from "src/app/api/models";
import { BooksOverviewActions } from "../actions";


export enum Display {
    Grid,
    List
};

export const booksOverviewFeatureKey = 'booksOverview';

export interface State {
    selectedDomains: Array<Domain>;
    selectedCategories: Array<Category>;
    display: Display,
};

export const initialState: State = {
    selectedDomains: [],
    selectedCategories: [],
    display: Display.Grid
};

export const reducer = createReducer(
    initialState,
    on(BooksOverviewActions.resetState,
        () => initialState
    ),
    on(BooksOverviewActions.selectDomains,
        (state, { domains }) => ({ ...state, selectedDomains: domains })
    ),
    on(BooksOverviewActions.selectCategories,
        (state, { categories }) => ({ ...state, selectedCategories: categories })
    ),
    on(BooksOverviewActions.setDisplay,
        (state, { display }) => ({ ...state, display })
    ),
);

export const getSelectedDomains = (state: State) => state.selectedDomains;
export const getSelectedCategories = (state: State) => state.selectedCategories;
export const getDisplay = (state: State) => state.display;