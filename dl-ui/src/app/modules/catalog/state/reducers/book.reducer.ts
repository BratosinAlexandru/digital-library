import { createReducer, on } from "@ngrx/store";
import { Book } from "src/app/api/models";
import { BookActions } from "../actions";

export const bookFeatureKey = 'book';

export interface State {
    book: Book;
}

export const initialState: State = {
    book: null,
}

export const reducer = createReducer(
    initialState,
    on(BookActions.loadBookSuccess,
        (state, { book }) => ({ ...state, book })
    ),
    on(BookActions.updateBookSuccess,
        (state, { updatedBook }) => ({ ...state, book: updatedBook })
    ),
    on(BookActions.createBookSuccess,
        (state, { newBook }) => ({ ...state, book: newBook })
    ),
);

export const getBook = (state: State) => state.book;