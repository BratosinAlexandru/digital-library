import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { createReducer, on } from "@ngrx/store";
import { BookOverview } from "src/app/api/models";
import { BooksApiActions, BooksOverviewActions } from "../actions";

export interface Filters {
    from?: Date,
    to?: Date;
    disabledOnly?: boolean;
    page?: number;
    size?: number;
    totalElements?: number;
    totalPages?: number;
    domainIds?: string[];
    categoryIds?: string[];
    name?: string;
    authorName?: string;
    numberOfPages?: number;
    allBooks?: boolean;
    myBooks?: boolean;
}

export const booksFeatureKey = 'books';

export interface State extends EntityState<BookOverview> {
    loading: boolean;
    filters: Filters;
}

export const adapter: EntityAdapter<BookOverview> = createEntityAdapter<BookOverview>({
    sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
    loading: false,
    filters: {
        from: undefined,
        to: undefined,
        disabledOnly: false,
        page: 0,
        size: 25,
        totalElements: 100,
        allBooks: false,
        myBooks: false,
    }
});

export const reducer = createReducer(
    initialState,
    on(BooksApiActions.loadBooksSuccess,
        (state, {booksOverviewPage}) => {
            const {page, size, totalElements, totalPages} = booksOverviewPage;
            return {...adapter.setAll(booksOverviewPage.content, state), filters: {...state.filters, page, size, totalElements, totalPages}};
        }
    ),
    on(BooksOverviewActions.updateFilters,
        (state, {filters}) => ({...state, filters: Object.assign(Object.assign({}, state.filters), filters)})
    ),
);

export const getFilters = (state: State) => state.filters;