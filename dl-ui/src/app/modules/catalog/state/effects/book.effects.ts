import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { Book } from "src/app/api/models";
import { BooksService } from "../../services/books.service";
import { BookActions } from "../actions";

@Injectable()
export class BookEffects {

    loadBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BookActions.loadBook),
            switchMap((action) =>
                this.bookService.getBook(action.bookId).pipe(
                    map((book: Book) => BookActions.loadBookSuccess({ book })),
                    catchError((error) => of(BookActions.loadBookFailure({ error })))
                )
            )
        )
    );

    updateBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BookActions.updateBook),
            switchMap((action) =>
                this.bookService.updateBook(action.updatedBook).pipe(
                    map(() => BookActions.updateBookSuccess({ updatedBook: action.updatedBook })),
                    catchError((error) => of(BookActions.updateBookFailure({ error })))
                )
            )
        )
    )

    createBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BookActions.createBook),
            switchMap((action) =>
                this.bookService.createBook(action.newBook).pipe(
                    map((newBook) => {
                        if (action.redirectTo === 'overview') {
                            this.goBack();
                        }
                        return BookActions.createBookSuccess({ newBook });
                    }),
                    catchError((error) => of(BookActions.createBookFailure({ error })))
                )
            )
        )
    );

    private goBack() {
        this._router.navigate(['/catalog']);
    }

    constructor(
        private actions$: Actions,
        private _router: Router,
        private bookService: BooksService,
    ) { }
}