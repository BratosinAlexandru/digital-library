import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, exhaustMap, map, switchMap, withLatestFrom } from 'rxjs/operators';
import * as CatalogState from "../index";
import * as fromBooks from "../reducers/books.reducer";
import { BooksService } from "../../services/books.service";
import { BooksApiActions, BooksOverviewActions } from "../actions";
import { Action, Store } from '@ngrx/store';
import { BookControllerService } from "src/app/api/services";
import GetBooksUsingPOSTParams = BookControllerService.GetBooksUsingPOSTParams
import { BookOverviewPage } from "src/app/api/models";
import { of } from "rxjs";
import { DeleteConfirmationComponent, DeleteConfirmationTypeEnum } from "../../components/delete-confirmation/delete-confirmation.component";

@Injectable()
export class BooksEffects {

    loadBooks$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.loadBooks),
            withLatestFrom(this.store$.select(CatalogState.selectBooksState)),
            switchMap(([action, booksState]: [Action, fromBooks.State]) => {
                const params: GetBooksUsingPOSTParams = {
                    page: booksState.filters.page,
                    size: booksState.filters.size,
                    getBooksDto: {
                        name: booksState.filters.name || undefined,
                        authorName: booksState.filters.authorName || undefined,
                        numberOfPages: booksState.filters.numberOfPages || undefined,
                        from: booksState.filters.from.toISOString().split('T')[0] || undefined,
                        to: booksState.filters.to.toISOString().split('T')[0] || undefined,
                        allBooks: booksState.filters.allBooks || false,
                        disabledOnly: booksState.filters.disabledOnly || false,
                        domainIds:
                            booksState.filters.domainIds && booksState.filters.domainIds.length
                                ? booksState.filters.domainIds
                                : undefined,
                        categoryIds:
                            booksState.filters.categoryIds && booksState.filters.categoryIds.length
                                ? booksState.filters.categoryIds
                                : undefined,
                        myBooks: booksState.filters.myBooks
                    },
                };
                return this.booksService.getBooks(params).pipe(
                    map((booksOverviewPage: BookOverviewPage) => BooksApiActions.loadBooksSuccess({ booksOverviewPage })),
                    catchError((error) => of(BooksApiActions.loadBooksFailure({ error })))
                )
            })
        )
    );

    activateBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.enableBook),
            switchMap(({ id }) => {
                return this.booksService.enablebook(id).pipe(
                    map(() => BooksOverviewActions.loadBooks())
                );
            })
        )
    );

    activateBookConfirmation$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.enableBookConfirmation),
            exhaustMap(({ id }) => {
                const dialogRef = this.dialog.open<
                    DeleteConfirmationComponent,
                    { id: string; confirmationType: DeleteConfirmationTypeEnum },
                    string | false
                >(DeleteConfirmationComponent, {
                    data: {
                        id: id,
                        confirmationType: DeleteConfirmationTypeEnum.ACTIVATE
                    },
                });
                return dialogRef.afterClosed();
            }),
            map((result) =>
                result ? BooksOverviewActions.enableBook({ id: result })
                    : BooksOverviewActions.enableBookConfirmationDismiss()
            )
        )
    );

    disableBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.disableBook),
            switchMap(({ id }) => {
                return this.booksService.disableBook(id).pipe(
                    map(() => BooksOverviewActions.loadBooks())
                )
            })
        )
    );

    disableBookConfirmation$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.disableBookConfirmation),
            exhaustMap(({ id }) => {
                const dialogRef = this.dialog.open<
                    DeleteConfirmationComponent,
                    { id: string; confirmationType: DeleteConfirmationTypeEnum },
                    string | false
                >(DeleteConfirmationComponent, {
                    data: {
                        id: id,
                        confirmationType: DeleteConfirmationTypeEnum.DEACTIVATE
                    },
                });
                return dialogRef.afterClosed();
            }),
            map((result) =>
                result ? BooksOverviewActions.disableBook({ id: result })
                    : BooksOverviewActions.disableBookConfirmationDismiss()
            )
        )
    );

    loadBooksOnFilterUpdate$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.updateFilters),
            map(() => BooksOverviewActions.loadBooks())
        )
    );

    deleteBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.deleteBook),
            switchMap(({ id }) => {
                return this.booksService.deleteBook(id).pipe(
                    map(() => BooksOverviewActions.loadBooks()),
                    catchError((error) => of(BooksApiActions.deleteBookFailure({ error })))
                )
            })
        )
    );

    deleteBookConfirmation$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BooksOverviewActions.deleteBookConfirmation),
            exhaustMap(({ id }) => {
                const dialogRef = this.dialog.open<
                    DeleteConfirmationComponent,
                    { id: string; confirmationType: DeleteConfirmationTypeEnum },
                    string | false
                >(DeleteConfirmationComponent, {
                    data: {
                        id: id,
                        confirmationType: DeleteConfirmationTypeEnum.DELETE
                    },
                });
                return dialogRef.afterClosed();
            }),
            map((result) =>
                result ? BooksOverviewActions.deleteBook({ id: result })
                    : BooksOverviewActions.deleteBookConfirmationDismiss()
            )
        )
    );


    constructor(
        private actions$: Actions,
        private dialog: MatDialog,
        private booksService: BooksService,
        private store$: Store<CatalogState.State>
    ) { }
}