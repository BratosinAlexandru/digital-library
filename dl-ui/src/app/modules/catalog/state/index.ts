import * as fromBooksOverview from './reducers/books-overview.reducer';
import * as fromBooks from './reducers/books.reducer';
import * as fromBook from './reducers/book.reducer';
import * as AppState from '../../../state/app.state';
import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

export const catalogFeatureKey = 'batalog';

export interface CatalogState {
    [fromBooksOverview.booksOverviewFeatureKey]: fromBooksOverview.State;
    [fromBooks.booksFeatureKey]: fromBooks.State;
    [fromBook.bookFeatureKey]: fromBook.State;
}

export interface State extends AppState.State {
    [catalogFeatureKey]: CatalogState;
}

export function reducers(state: CatalogState | undefined, action: Action) {
    return combineReducers({
        [fromBooksOverview.booksOverviewFeatureKey]: fromBooksOverview.reducer,
        [fromBooks.booksFeatureKey]: fromBooks.reducer,
        [fromBook.bookFeatureKey]: fromBook.reducer,
    })(state, action);
}

export const selectCatalogState = createFeatureSelector<State, CatalogState>(catalogFeatureKey);

/** Books Overview State */

export const selectBooksOverviewState = createSelector(
    selectCatalogState,
    (state: CatalogState) => state[fromBooksOverview.booksOverviewFeatureKey]
);

export const selectDomains = createSelector(
    selectBooksOverviewState,
    fromBooksOverview.getSelectedDomains
);

export const selectCategories = createSelector(
    selectBooksOverviewState,
    fromBooksOverview.getSelectedCategories
);

export const selectDisplay = createSelector(
    selectBooksOverviewState,
    fromBooksOverview.getDisplay
);

/** Books State */

export const selectBooksState = createSelector(
    selectCatalogState,
    (state: CatalogState) => state[fromBooks.booksFeatureKey]
);

export const {
    selectIds: selectBookIds,
    selectEntities: selectBookEntities,
    selectAll: selectAllBooks,
    selectTotal: selectTotalBooks,
} = fromBooks.adapter.getSelectors(selectBooksState);

export const selectBookFilters = createSelector(
    selectBooksState,
    fromBooks.getFilters
);

/** Book State */

export const selectBookState = createSelector(
    selectCatalogState,
    (state: CatalogState) => state[fromBook.bookFeatureKey]
);

export const selectBookEntity = createSelector(
    selectBookState,
    fromBook.getBook
);