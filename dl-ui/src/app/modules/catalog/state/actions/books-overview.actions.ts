import { createAction, props } from "@ngrx/store";
import { Category, Domain } from "src/app/api/models";
import { Display } from "../reducers/books-overview.reducer";
import { Filters } from "../reducers/books.reducer";

/** Load books action */
export const loadBooks = createAction('[Book Overview Component] Load Books');

/** Select Domains action  */
export const selectDomains = createAction('[Book Overview Component / API] Select Domains', props<({ domains: Array<Domain> })>());

/** Select Categories action */
export const selectCategories = createAction('[Book Overview Component / API] Select Categories', props<({ categories: Array<Category> })>());

/** Update filters */
export const updateFilters = createAction('[Book Overview Component] Update filters', props<({ filters: Filters })>());

/** Clear selection action */
export const clearSelection = createAction('[Book Overview Component] Clear selection');

/** Enable book action */
export const enableBook = createAction('[Auth] Enable Book', props<({ id: string })>());
export const enableBookConfirmation = createAction('[Auth] Enable Book Confirmation', props<({ id: string })>());
export const enableBookConfirmationDismiss = createAction('[Auth] Enable Book Confirmation Dismiss');

/** Disable book action */
export const disableBook = createAction('[Auth] Disable Book', props<({ id: string })>());
export const disableBookConfirmation = createAction('[Auth] Disable Book Confirmation', props<({ id: string })>());
export const disableBookConfirmationDismiss = createAction('[Auth] Disable Book Confirmation Dismiss');

/** Delete book action */
export const deleteBook = createAction('[Book Overview Component] Delete Book', props<({ id: string })>());
export const deleteBookConfirmation = createAction('[Book Overview Component] Delete Book Confirmation', props<({ id: string })>());
export const deleteBookConfirmationDismiss = createAction('[Book Overview Component] Delete Book Confirmation Dismiss');

/** Set display action */
export const setDisplay = createAction('[Book Overview Component] Set Display', props<({ display: Display })>());

/** Reset State action */
export const resetState = createAction('[Book Overview Page] Reset State');
