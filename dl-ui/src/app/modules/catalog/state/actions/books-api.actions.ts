import { createAction, props } from "@ngrx/store";
import { BookOverviewPage, Category } from "src/app/api/models";

/** Load books actions */
export const loadBooksSuccess = createAction('[Books / API] Load Books Success', props<({ booksOverviewPage: BookOverviewPage })>());
export const loadBooksFailure = createAction('[Books / API] Load Books Failure', props<({ error: any })>());

/** Load Categories actions */
export const loadCategoriesSuccess = createAction('[Books / API] Load Categories Success', props<({ categories: Array<Category> })>());
export const loadCategoriesFailure = createAction('[Books / API] Load Categories Failure', props<({ error: any })>());


/** Delete book actions */
export const deleteBookSuccess = createAction('[Books / API] Delete Book Success', props<({ id: string })>());
export const deleteBookFailure = createAction('[Books / API] Delete Book Failure', props<({ error: any })>());

