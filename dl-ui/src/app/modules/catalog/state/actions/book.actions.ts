import { createAction, props } from "@ngrx/store";
import { Book } from "src/app/api/models";

/** Load book actions */
export const loadBook = createAction('[Book Component] Load Book', props<({ bookId: string })>());
export const loadBookSuccess = createAction('[Book Component] Load Book Success', props<({ book: Book })>());
export const loadBookFailure = createAction('[Book Component] Load Book Failure', props<({ error: any })>());

/** Update book actions */
export const updateBook = createAction('[Book Component] Update Book', props<({ updatedBook: Book })>());
export const updateBookSuccess = createAction('[Book Component] Update Book Success', props<({ updatedBook: Book })>());
export const updateBookFailure = createAction('[Book Component] Update Book Failure', props<({ error: any })>());

/** Create book action */
export const createBook = createAction('[Add Book Component] Create Book', props<({ newBook: Book, redirectTo?: string })>());
export const createBookSuccess = createAction('[Add Book Component] Create Book Success', props<({ newBook: Book })>());
export const createBookFailure = createAction('[Add Book Component] Create Book Failure', props<({ error: any })>());

