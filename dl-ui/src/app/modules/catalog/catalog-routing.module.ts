import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddBookComponent } from "./components/add-book/add-book.component";
import { BookComponent } from "./components/book/book.component";
import { ReadBookComponent } from "./components/read-book/read-book.component";
import { BookOverviewComponent } from "./containers/book-overview/book-overview.component";

const routes: Routes = [
    {
        path: 'overview',
        component: BookOverviewComponent,
    },
    {
        path: 'edit/:id',
        data: {
            title: 'Carte'
        },
        component: BookComponent,
    },
    {
        path: ':id',
        component: AddBookComponent,
        data: {
            title: 'Adauga carte'
        }
    },
    {
        path: 'read/:id',
        component: ReadBookComponent,
        data: {
            title: 'Citeste cartea'
        }
    },
    { path: '**', redirectTo: 'overview', pathMatch: 'full' },
    { path: '', redirectTo: 'overview', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CatalogRoutingModule { }