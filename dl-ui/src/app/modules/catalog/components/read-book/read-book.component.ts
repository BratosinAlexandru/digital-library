import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Attachment } from 'src/app/api/models';
import { BooksService } from '../../services/books.service';
import { State, selectBookEntity } from '../../state';
import { BookActions } from '../../state/actions';
import { AlertService } from 'src/app/core/services/alert.service';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

@Component({
  selector: 'read-book',
  templateUrl: './read-book.component.html',
  styleUrls: ['./read-book.component.scss']
})
export class ReadBookComponent implements OnInit, OnDestroy {
  
  @ViewChild(PdfViewerComponent) pdfComponent: PdfViewerComponent;

  public attachment: Attachment;
  public fileURL: string;
  public numberOfPages: number;
  public page = 1;
  private subscriptions$: Array<Subscription> = [];

  constructor(
    private activeRouter: ActivatedRoute,
    private store: Store<State>,
    private bookService: BooksService,
    private cdr: ChangeDetectorRef,
    private alertService: AlertService,

  ) {
    this.subscriptions$.push(this.activeRouter.params.subscribe(params => {
      const bookId = params.id;
      this.store.dispatch(BookActions.loadBook({ bookId }));
    }));
  }


  ngOnInit(): void {
    this.subscriptions$.push(
      this.store.pipe(select(selectBookEntity)).subscribe(book => {
        if (book) {
          this.numberOfPages = book.numberOfPages;
          this.downloadAttachment(book.attachment[0]);
        }
      })
    );
  }

  downloadAttachment(attachment: Attachment) {
    if (attachment) {
      this.subscriptions$.push(
        this.bookService.downloadAttachment(attachment.id).subscribe(
          (data: any) => {
            let type = data.contentType;
            if (!type && data.metadata) {
              type = '' + data.metadata._contentType;
            }
            const blob = new Blob([data], { type });
            this.fileURL = URL.createObjectURL(blob);
            this.cdr.detectChanges();
          },
          (error) => {
            this.alertService.error('Server internal error!');
          }
        )
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    })
  }

  goToPreviousPage() {
    --this.page;
  }

  gotoNextPage() {
    ++this.page;
  }

  pageInitialized(e: CustomEvent) {
    console.log('(pages-initialized)', e);
  }
}
