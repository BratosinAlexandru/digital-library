import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export enum DeleteConfirmationTypeEnum {
  DEACTIVATE = 'DEACTIVATE',
  ACTIVATE = 'ACTIVATE',
  DELETE = 'DELETE',
}

@Component({
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: string; confirmationType: DeleteConfirmationTypeEnum }
  ) { }


  get type() {
    return DeleteConfirmationTypeEnum;
  }

}
