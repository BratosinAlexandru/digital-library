import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Book, BookOverview } from 'src/app/api/models';
import { Display } from '../../state/reducers/books-overview.reducer';
import { Filters } from '../../state/reducers/books.reducer';
import { selectBookFilters, State } from '../../state';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import { ApiConfiguration } from 'src/app/api/api-configuration';
import { selectLoggedUser, selectRoles } from 'src/app/modules/auth/state/auth.selectors';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { BooksOverviewActions } from '../../state/actions';
import { take } from 'rxjs/operators';

@Component({
  selector: 'book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, AfterViewInit, OnDestroy {
  private _books: Array<BookOverview>;
  private _subscriptions: Array<Subscription> = [];


  @Input() set books(books: Array<BookOverview>) {
    this.erroredImages = new Array(books.length);
    this.erroredImages.fill(false);
    this._books = books;
  }

  get books() {
    return this._books;
  }

  @Input() displayMode: Display;
  @Input() totalElements: number;
  @Output() openBook: EventEmitter<Book> = new EventEmitter<Book>();
  @Output() deleteBook: EventEmitter<Book> = new EventEmitter<Book>();
  @Output() enableBook: EventEmitter<Book> = new EventEmitter<Book>();
  @Output() disableBook: EventEmitter<Book> = new EventEmitter<Book>();
  @Output() displayModeChange: EventEmitter<Display> = new EventEmitter<Display>();
  @Output() readBook: EventEmitter<Book> = new EventEmitter<Book>();
  Display = Display;

  public apiUrl: string;
  public openMenu: boolean;
  public erroredImages: boolean[];
  public bookFilters: Filters;
  public isAdmin = false;

  constructor(
    private apiConfiguration: ApiConfiguration,
    private sanitizer: DomSanitizer,
    private store: Store<State>,
    private authStore: Store<AuthState.State>
  ) {
    this.apiUrl = apiConfiguration.rootUrl;
  }


  ngOnInit(): void {
    this._subscriptions.push(
      this.store.pipe(select(selectBookFilters)).subscribe((filters) => {
        this.bookFilters = filters;
      }),

      this.authStore.pipe(take(1), select(selectRoles)).subscribe(
        (roles: any) => {
          if (roles) {
            this.isAdmin = roles.includes('ROLE_ADMIN');
          }
        }
      ));
  }

  changeDisplayMode() {
    const displayMode = this.displayMode === Display.Grid ? Display.List : Display.Grid;
    this.displayModeChange.emit(displayMode);
    localStorage.setItem('bookOverviewDisplayMode', displayMode.toString());
  }

  ngAfterViewInit(): void {
    this.displayModeChange.emit(parseInt(localStorage.getItem('bookOverviewDisplayMode'), 10) === 1 ? 1 : 0);
  }

  imageURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  ngOnDestroy(): void {
    this._subscriptions.forEach((it) => it.unsubscribe());
  }

  onDeactivateSwitchChange(toggleChange: MatSlideToggleChange) {
    this.store.dispatch(
      BooksOverviewActions.updateFilters({
        filters: {
          ...this.bookFilters,
          disabledOnly: toggleChange.checked,
          allBooks: false,
        }
      })
    );
  }

  onShowAllSwitchChange(toggleChange: MatSlideToggleChange) {
    this.store.dispatch(
      BooksOverviewActions.updateFilters({
        filters: {
          ...this.bookFilters,
          allBooks: toggleChange.checked,
        }
      })
    );
  }

  onShowMyBooks(toggleChange: MatSlideToggleChange) {
    this.store.dispatch(
      BooksOverviewActions.updateFilters({
        filters: {
          ...this.bookFilters,
          myBooks: toggleChange.checked,
        }
      })
    );
  }
}
