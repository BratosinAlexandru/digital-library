import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Book } from 'src/app/api/models';
import { State } from '../../state';
import { BookActions } from '../../state/actions';

@Component({
  selector: 'add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {

  public id: string;
  public book: Book;

  private submited: boolean;

  constructor(
    private _route: ActivatedRoute,
    private store: Store<State>,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.params.id;
  }

  saveBook(newBook: Book) {
    if (this.submited) {
      return;
    }
    this.submited = true;
    this.store.dispatch(BookActions.createBook({ newBook, redirectTo: 'overview' }));
    // this._router.navigate(['/catalog', 'overview']);
  }

  public goBack() {
    this._router.navigate(['/catalog']);
  }

}
