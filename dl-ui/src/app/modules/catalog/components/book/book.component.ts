import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Book } from 'src/app/api/models';
import { BooksService } from '../../services/books.service';
import { State, selectBookEntity } from '../../state';
import { BookActions } from '../../state/actions';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { map } from 'rxjs/operators';
@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit, OnDestroy {

  public book$: Observable<Book>;
  public bookForm: FormGroup;
  public erroredImage: boolean;
  public times = Date.now();
  public editor = ClassicEditor;

  public subscriptions$: Array<Subscription> = [];

  constructor(
    private activeRouter: ActivatedRoute,
    private store: Store<State>,
    private bookService: BooksService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
  ) {
    this.subscriptions$.push(this.activeRouter.params.subscribe(params => {
      const bookId = params.id;
      this.store.dispatch(BookActions.loadBook({ bookId }));
    }));
    this.erroredImage = false;
  }

  ngOnInit(): void {
    this.createForm();
    this.initDescription();
  }

  private createForm() {
    this.bookForm = this.fb.group({
      description: this.fb.control('')
    });
  }

  private initDescription() {
    this.book$ = this.book$ = this.store.pipe(
      select(selectBookEntity),
      map((book: Book) => {
        if (book && book.description) {
          this.bookForm.get('description').patchValue(book.description);
        } else {
          this.bookForm.get('description').patchValue('');
        }
        return book;
      })
    );
  }

  getCoverImage(bookId: string, times: number) {
    console.log(this.bookService.getBookImageByBookId(bookId));
    return this.bookService.getBookImageByBookId(bookId) + '?times=' + times;
  }

  updateBookCover(event: any, book: Book) {
    if (event && book) {
      const updatedBook = JSON.parse(JSON.stringify(book));
      updatedBook.image = event;
      this.store.dispatch(BookActions.updateBook({ updatedBook }));
      this.erroredImage = false;
      this.times = Date.now();
    }
  }

  onFileChanged(event, book: Book) {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      return;
    }
    reader.onload = (data: any) => {
      this.updateBookCover(data.target.result.split('base64,')[1], book);
    }
    reader.readAsDataURL(file);
  }

  updateBook(book) {
    const updatedBook = JSON.parse(JSON.stringify(book));
    updatedBook.description = this.bookForm.get('description').value;
    this.store.dispatch(BookActions.updateBook({ updatedBook }));
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
