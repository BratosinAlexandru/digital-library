import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Request } from 'src/app/api/models';
import { RequestsAppState } from '../../state';
import { createRequest, selectRequest, updateRequest } from '../../state/requests/requests.actions';

@Component({
  selector: 'request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {

  public id: string;
  public request: Request;

  private submitted: boolean;

  constructor(
    private _route: ActivatedRoute,
    private store: Store<RequestsAppState>,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.params.id;
  }

  saveRequest(request: Request) {
    if (this.submitted) {
      return;
    }
    this.submitted = true;
    if (request.customer) {
      const account = request.customer;
      if (account.name && typeof account.name === 'object') {
        account.name = account.name['name'];
      }
    }
    if (request.id) {
      this.store.dispatch(updateRequest({request, redirectTo: 'requests'}));
    } else {
      this.store.dispatch(createRequest({request}));
    }
  }

  public goBack() {
    this.store.dispatch(selectRequest({selectedRequest: null}));
    this._router.navigate(['/requests', 'pages']);
  }

}
