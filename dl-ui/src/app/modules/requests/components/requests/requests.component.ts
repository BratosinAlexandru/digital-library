import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Account, PaginationRequestDtoFiltersRequest, PaginationRequests, RequestPage, RequestStatusCount } from 'src/app/api/models';
import { RequestControllerService } from 'src/app/api/services';
import * as AuthSelectors from 'src/app/modules/auth/state/auth.selectors';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import { FilterEvent, FilterEventType } from '../../models/filter-event.model';
import { getPaginationRequest, getRequestsPage, getSelectedRequest, RequestsAppState } from '../../state';
import { deleteRequests, enableRequests, loadRequestsPage } from '../../state/requests/requests.actions';

@Component({
  selector: 'requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit, OnDestroy {

  public tableSource: PaginationRequests;
  public isLoading: boolean;
  public pagination: PaginationRequestDtoFiltersRequest;
  public requestStatus?: Array<RequestStatusCount>;
  public subscriptions$: Array<Subscription> = [];
  public isAdmin: boolean;

  constructor(
    private ref: ChangeDetectorRef,
    private store: Store<RequestsAppState>,
    private activeRouter: ActivatedRoute,
    private authStore: Store<AuthState.State>,
    private router: Router,
  ) { }


  ngOnInit(): void {
    this.subscriptions$.push(this.store.pipe(take(1), select(getSelectedRequest)).subscribe(request => {
      if (request) {
        this.router.navigate(['/requests', request.id]);
      }
    }));
    this.subscriptions$.push(this.authStore.pipe(take(1), select(AuthSelectors.selectRoles)).subscribe(
      (roles: any) => {
        if (roles) {
          this.isAdmin = roles.includes('ROLE_ADMIN');
        }
      }
    ))
    this.subscriptions$.push(this.activeRouter.queryParams.pipe().subscribe(params => {
      if (params.startDate && params.endDate && params.name) {
        this.pagination = this.getDefaultPage();
        this.pagination.filtersRequest = {
          endDate: params.endDate + 'T23:59:59.000Z',
          startDate: params.startDate + 'T00:00:00.000Z',
          enabled: true,
        }
        this.loadRequestsPage();
        this.ref.detectChanges();
      } else {
        this.subscriptions$.push(this.store.pipe(take(1), select(getPaginationRequest)).subscribe(
          (paginationRequest: PaginationRequestDtoFiltersRequest) => {
            if (paginationRequest) {
              this.pagination = paginationRequest;
            } else {
              this.pagination = this.getDefaultPage();
            }
            this.loadRequestsPage();
            this.ref.detectChanges();
          }
        ));
      }

      this.subscriptions$.push(this.store.pipe(select(getRequestsPage)).subscribe(
        (page: RequestPage) => {
          if (page) {
            this.isLoading = false;
            this.tableSource = JSON.parse(JSON.stringify(page.pagination)) as PaginationRequests;
            if (page.totalElements && page.totalElements[0] && page.totalElements[0].count) {
              this.tableSource.totalElements = page.totalElements[0].count;
            } else {
              this.tableSource.totalElements = 0;
            }
            this.requestStatus = page.requestStatus;
            this.ref.detectChanges();
          }
        },
        (error: any) => {
          this.isLoading = false;
        }
      ));
    }));
  }

  pageChanged(paginationRequest: PaginationRequestDtoFiltersRequest) {
    this.pagination = JSON.parse(JSON.stringify(this.pagination));
    this.pagination.pageNumber = paginationRequest.pageNumber;
    this.pagination.pageSize = paginationRequest.pageSize;
    this.pagination.sortDirection = paginationRequest.sortDirection;
    this.pagination.sortProperties = paginationRequest.sortProperties;
    this.loadRequestsPage();
  }

  private formatDate(date: Date): string {
    let response = '';
    response += date.getFullYear();
    response += '-';
    response += date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
    response += '-';
    response += date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
    return response;
  }

  filterChanged(event: FilterEvent) {
    if (!this.pagination) {
      this.pagination = this.getDefaultPage();
    } else {
      this.pagination = JSON.parse(JSON.stringify(this.pagination));
      this.pagination.pageNumber = 0;
    }
    let keepEnabled: boolean;
    switch (event.type) {
      case FilterEventType.RANGE:
        this.pagination.filtersRequest.startDate = this.formatDate(event.value.fromDate) + 'T00:00:00.000Z';
        this.pagination.filtersRequest.endDate = this.formatDate(event.value.toDate) + 'T23:59:59.000Z';
        break;
      case FilterEventType.CUSTOMER:
        keepEnabled = this.pagination.filtersRequest.enabled;
        this.pagination = this.getDefaultPage(true);
        this.pagination.filtersRequest.enabled = keepEnabled;
        this.pagination.filtersRequest.customer = event.value;
        break;
      case FilterEventType.REQUEST_TYPES:
        this.pagination.filtersRequest.requestType = event.value;
        break;
      case FilterEventType.IMPORTANT_REQUEST:
        this.pagination.filtersRequest.importantRequest = event.value;
        break;
      case FilterEventType.RESET:
        this.pagination = this.getDefaultPage();
        break;
      case FilterEventType.ENABLED:
        this.pagination.filtersRequest.enabled = event.value;
        break;
      case FilterEventType.REQUEST_STATUS:
        this.pagination.filtersRequest.requestStatus = event.value.status;
        break;
      default:
        break;
    }
    this.loadRequestsPage();
  }

  private loadRequestsPage() {
    this.isLoading = true;
    this.store.dispatch(loadRequestsPage({ paginationRequest: this.pagination }));
  }

  private getDefaultPage(excludePeriod?: boolean): PaginationRequestDtoFiltersRequest {
    const today = new Date();
    const endDate = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate(),
      23,
      59 - today.getTimezoneOffset(),
      59,
      0
    );
    const startDate = new Date(
      today.getFullYear(),
      today.getMonth() - 6,
      today.getDate(),
      0,
      0 - today.getTimezoneOffset(),
      0,
      0
    );

    const paginationRequest: PaginationRequestDtoFiltersRequest = {
      pageNumber: 0,
      pageSize: 50,
      sortDirection: 'desc',
      sortProperties: ['createdOn'],
      filtersRequest: {
        startDate: excludePeriod ? null : startDate.toISOString(),
        endDate: excludePeriod ? null : endDate.toISOString(),
        enabled: true,
      }
    };
    return paginationRequest;
  }

  actionRequests(event: any) {
    if (event.type === 'enable') {
      this.store.dispatch(enableRequests({ requestsIds: event.ids, enable: event.enable, paginationRequest: this.pagination }));
    } else if (event.type === 'delete') {
      this.store.dispatch(deleteRequests({ rqeuestIds: event.ids, paginationRequest: this.pagination }));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

}
