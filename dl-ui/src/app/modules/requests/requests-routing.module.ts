import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RequestComponent } from "./components/request/request.component";
import { RequestsComponent } from "./components/requests/requests.component";

const routes: Routes = [
    {
        path: 'pages',
        component: RequestsComponent,
        data: {
            title: 'Solicitari'
        }
    },
    {
        path: ':id',
        component: RequestComponent,
        data: {
            title: 'Solicitare'
        }
    },
    { path: '**', redirectTo: 'pages', pathMatch: 'full' },
    { path: '', redirectTo: 'pages', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RequestsRoutingModule {}