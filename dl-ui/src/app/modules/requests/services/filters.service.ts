import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account, BookOverview, CommunicationChannel, Customer, RequestStatus, RequestType } from 'src/app/api/models';
import { BookControllerService, CommunicationChannelControllerService, CustomerControllerService, RequestStatusControllerService, RequestTypeControllerService } from 'src/app/api/services';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  constructor(
    private customerControllerService: CustomerControllerService,
    private requestTypeControllerService: RequestTypeControllerService,
    private requestStatusControllerService: RequestStatusControllerService,
    private communicationChannelControllerService: CommunicationChannelControllerService,
    private bookControllerService: BookControllerService,
  ) { }

  getByEmailOrByName(query: string, hasRequestsEnabled?: boolean): Observable<Array<Account>> {
    return this.customerControllerService.getCustomersByQueryUsingGET({query, hasRequestsEnabled});
  }

  getByEmail(email: string): Observable<Array<Customer>> {
    return this.customerControllerService.getCustomerByEmailUsingGET(email);
  }


  getRequestTypes(includeDisabled?: boolean): Observable<Array<RequestType>> {
    return this.requestTypeControllerService.getRequestTypesUsingGET(includeDisabled);
  }

  getRequestStatues(includeDisabled?: boolean): Observable<Array<RequestStatus>> {
    return this.requestStatusControllerService.getRequestStatuesUsingGET(includeDisabled);
  }

  getCommunicationChannels(includeDisabled?: boolean): Observable<Array<CommunicationChannel>> {
    return this.communicationChannelControllerService.getCommunicationChannelsUsingGET(includeDisabled);
  }

  getBooks(includeDisabled?: boolean): Observable<Array<BookOverview>> {
    return this.bookControllerService.getBooksOverviewUsingGET(includeDisabled);
  }
}
