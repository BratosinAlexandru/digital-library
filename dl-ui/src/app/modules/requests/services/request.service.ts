import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginationRequestDtoFiltersRequest, Request, RequestHistoryDto, RequestPage } from 'src/app/api/models';
import { BookControllerService, RequestControllerService } from 'src/app/api/services';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private requestControllerService: RequestControllerService,
    private bookService: BookControllerService,
  ) { }

  getPagedRequests(pageRequest: PaginationRequestDtoFiltersRequest): Observable<RequestPage> {
    return this.requestControllerService.getPagedUsingPOST1(pageRequest);
  }

  getRequestById(requestId: string): Observable<Request> {
    return this.requestControllerService.getRequestUsingGET(requestId);
  }

  createRequest(request: Request): Observable<Request> {
    return this.requestControllerService.createRequestUsingPOST(request);
  }

  updateRequest(request: Request): Observable<null> {
    return this.requestControllerService.updateRequestUsingPUT(request);
  }

  getHistory(requestId: string): Observable<Array<RequestHistoryDto>> {
    return this.requestControllerService.getHistoryUsingGET(requestId);
  }

  deleteAllRequests(requestIds: Array<string>): Observable<null> {
    return this.requestControllerService.deleteAllUsingDELETE(requestIds);
  }

  enableAllRequests(requestIds: Array<string>, enable: boolean): Observable<null> {
    return this.requestControllerService.updateEnableUsingPUT({ requestIds, enable });
  }
  
  acceptRequests(requestIds: Array<string>): Observable<null> {
    return this.requestControllerService.acceptRequestUsingPUT(requestIds);
  }
}
