
export interface FilterEvent {
    type?: FilterEventType;
    value?: any;
}

export enum FilterEventType {
    RANGE,
    CUSTOMER,
    REQUEST_TYPES,
    IMPORTANT_REQUEST,
    RESET,
    ENABLED,
    REQUEST_STATUS,
    ACCEPT_REQUESTS
}