import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { LibraryTranslateService } from "src/app/translate/services/library-translate.service";

@Injectable()
export class RequestsTranslateLoader implements Resolve<any> {
    constructor (
        private translateService: LibraryTranslateService
    ) {}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.translateService.addTranslations('requests');
    }    
}