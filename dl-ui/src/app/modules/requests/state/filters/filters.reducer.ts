import { state } from "@angular/animations";
import { createReducer, on } from "@ngrx/store";
import { BookOverview, CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";
import { loadBooksSuccess, loadCommunicationChannelSuccess, loadRequestStatusSuccess, loadRequestTypeSuccess } from "./filters.actions";

export const requestFiltersFeatureKey = 'requestFilters';

export interface State {
    types: Array<RequestType>;
    requestStatus: Array<RequestStatus>;
    communicationChannel: Array<CommunicationChannel>;
    books: Array<BookOverview>;
};

export const initialState: State = {
    types: null,
    requestStatus: null,
    communicationChannel: null,
    books: null,
};

export const reducer = createReducer(
    initialState,
    on(loadRequestTypeSuccess,
        (state, {types}) => ({...state, types})
    ),
    on(loadRequestStatusSuccess,
        (state, {requestStatuses}) => ({...state, requestStatus: requestStatuses})
    ),
    on(loadCommunicationChannelSuccess,
        (state, {communicationChannels}) => ({...state, communicationChannel: communicationChannels})
    ),
    on(loadBooksSuccess,
        (state, {books}) => ({...state, books: books})
    ),
);

export const getRequestTypes = (state: State) => state.types;
export const getRequestStatues = (state: State) => state.requestStatus;
export const getCommunicationChannels = (state: State) => state.communicationChannel;
export const getBooks = (state: State) => state.books;