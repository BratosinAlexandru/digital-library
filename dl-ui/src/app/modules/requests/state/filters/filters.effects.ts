import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { BookOverview, CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";
import { FiltersService } from "../../services/filters.service";
import { loadCommunicationChannel, loadCommunicationChannelFailure, loadCommunicationChannelSuccess, loadRequestStatus, loadRequestStatusFailure, loadRequestStatusSuccess, loadRequestType, loadRequestTypeFailure, loadRequestTypeSuccess, loadBooks, loadBooksSuccess, loadBooksFailure } from "./filters.actions";

@Injectable()
export class FiltersEffects {

    loadRequestType$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestType),
            switchMap((action) =>
                this.filtersService.getRequestTypes(action.includeDisabled).pipe(
                    map((types: Array<RequestType>) => loadRequestTypeSuccess({ types })),
                    catchError((error) => of(loadRequestTypeFailure({ error })))
                )
            )
        )
    );

    loadRequestStatus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestStatus),
            switchMap((action) =>
                this.filtersService.getRequestStatues(action.includeDisabled).pipe(
                    map((requestStatuses: Array<RequestStatus>) => loadRequestStatusSuccess({ requestStatuses })),
                    catchError((error) => of(loadRequestStatusFailure({ error })))
                )
            )
        )
    );

    loadCommunicationChannel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadCommunicationChannel),
            switchMap((action) =>
                this.filtersService.getCommunicationChannels(action.includeDisabled).pipe(
                    map((communicationChannels: Array<CommunicationChannel>) => loadCommunicationChannelSuccess({ communicationChannels })),
                    catchError((error) => of(loadCommunicationChannelFailure({ error })))
                )
            )
        )
    );

    loadBooks$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadBooks),
            switchMap((action) =>
                this.filtersService.getBooks(action.includeDisabled).pipe(
                    map((books: Array<BookOverview>) => loadBooksSuccess({ books })),
                    catchError((error) => of(loadBooksFailure({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private filtersService: FiltersService
    ) { }

}