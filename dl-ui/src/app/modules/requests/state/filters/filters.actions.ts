import { createAction, props } from "@ngrx/store";
import { BookOverview, CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";

/** Load request type actions */
export const loadRequestType = createAction('[Request] Load Request Type', props<({ includeDisabled?: boolean })>());
export const loadRequestTypeSuccess = createAction('[Request] Load Request Type Success', props<({ types: Array<RequestType> })>());
export const loadRequestTypeFailure = createAction('[Request] Load Request Type Failure', props<({ error: any })>());

/** Load Request status actions */
export const loadRequestStatus = createAction('[Request] Load Request Status', props<({ includeDisabled?: boolean })>());
export const loadRequestStatusSuccess = createAction('[Request] Load Request Status', props<({ requestStatuses: Array<RequestStatus> })>());
export const loadRequestStatusFailure = createAction('[Request] Load Request Status', props<({ error: any })>());

/** Load Communication channels actions */
export const loadCommunicationChannel = createAction('[Request] Load Communication Channel', props<({ includeDisabled?: boolean })>());
export const loadCommunicationChannelSuccess = createAction('[Request] Load Communication Channel Success', props<({ communicationChannels: Array<CommunicationChannel> })>());
export const loadCommunicationChannelFailure = createAction('[Request] Load Communication Channel Failure', props<({ error: any })>());

/** Load Books actions */
export const loadBooks = createAction('[Request] Load Books', props<({ includeDisabled?: boolean })>());
export const loadBooksSuccess = createAction('[Request] Load Books Success', props<({ books: Array<BookOverview> })>());
export const loadBooksFailure = createAction('[Request] Load Books Failure', props<({ error: any })>());