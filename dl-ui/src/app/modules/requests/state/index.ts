import * as fromFilters from './filters/filters.reducer';
import * as fromRequests from './requests/requests.reducer';
import * as AppState from '../../../state/app.state';
import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

export const requestsFeatureKey = 'requests';

export interface RequestsState {
    [fromFilters.requestFiltersFeatureKey]: fromFilters.State;
    [fromRequests.requestsPageFeatureKey]: fromRequests.State;
}

export interface RequestsAppState extends AppState.State {
    [requestsFeatureKey]: RequestsState;
}

export function reducers(state: RequestsState | undefined, action: Action) {
    return combineReducers({
        [fromFilters.requestFiltersFeatureKey]: fromFilters.reducer,
        [fromRequests.requestsPageFeatureKey]: fromRequests.reducer,
    })(state, action);
}

export const selectRequestsState = createFeatureSelector<RequestsAppState, RequestsState> (
    requestsFeatureKey
);

/** Filters */
export const selectRequestFiltersState = createSelector(
    selectRequestsState,
    (state: RequestsState) => state[fromFilters.requestFiltersFeatureKey]
);

export const getRequestTypes = createSelector(
    selectRequestFiltersState,
    fromFilters.getRequestTypes
);

export const getRequestStatues = createSelector(
    selectRequestFiltersState,
    fromFilters.getRequestStatues
);

export const getCommunicationChannels = createSelector(
    selectRequestFiltersState,
    fromFilters.getCommunicationChannels
);

export const getBooks = createSelector(
    selectRequestFiltersState,
    fromFilters.getBooks
);

/** Requests page */
export const selectRequestPageState = createSelector(
    selectRequestsState,
    (state: RequestsState) => state[fromRequests.requestsPageFeatureKey]
);

export const getRequestsPage = createSelector(
    selectRequestPageState,
    fromRequests.getRequestsPage
);

export const getSelectedRequest = createSelector(
    selectRequestPageState,
    fromRequests.getSelectedRequest
);

export const getPaginationRequest = createSelector(
    selectRequestPageState,
    fromRequests.getPaginationRequest
);

export const getViewCollumns = createSelector(
    selectRequestPageState,
    fromRequests.getViewCollumns
);

export const getRequestHistory = createSelector(
    selectRequestPageState,
    fromRequests.getRequestHistory
);