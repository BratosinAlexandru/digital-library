import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { PaginationRequestDtoFiltersRequest, Request, RequestHistoryDto, RequestPage } from "src/app/api/models";
import { loadRequestByIdSuccess, loadRequestsPage, loadRequestsPageSuccess, selectRequest, setViewCollumns, updateRequestSuccess, loadRequestHistoryByIdSuccess, acceptRequestsSuccess } from "./requests.actions";


export const requestsPageFeatureKey = 'requestsPage';

export interface State {
    page: RequestPage;
    selectedRequest: Request;
    paginationRequest: PaginationRequestDtoFiltersRequest;
    viewCollumns: Array<string>;
    requestHistory: Array<RequestHistoryDto>;
};

export const initialState: State = {
    page: null,
    selectedRequest: null,
    paginationRequest: null,
    viewCollumns: null,
    requestHistory: null,
};

export const reducer = createReducer(
    initialState,
    on(loadRequestsPage,
        (state, { paginationRequest }) => ({ ...state, paginationRequest: JSON.parse(JSON.stringify(paginationRequest)) })
    ),
    on(loadRequestsPageSuccess,
        (state, { page }) => ({ ...state, page })
    ),
    on(loadRequestByIdSuccess,
        (state, { request }) => ({ ...state, selectedRequest: request, requestHistory: null })
    ),
    on(selectRequest,
        (state, { selectedRequest }) => ({ ...state, selectedRequest, requestHistory: null })
    ),
    on(updateRequestSuccess,
        (state, { request }) => produce(state, draftState => {
            if (request) {
                const page: RequestPage = JSON.parse(JSON.stringify(state.page));
                if (page && page.pagination && page.pagination.content) {
                    for (let i = 0; i < page.pagination.content.length; i++) {
                        if (page.pagination.content[i].id === request.id) {
                            page.pagination.content[i] = request;
                        }
                    }
                    // const index = page.pagination.content.findIndex(e => e.id === request.id);
                    // if (index !== -1) {
                    //     page.pagination.content[index] = request;
                    // }
                }

                return { ...draftState, page, selectedRequest: null, requestHistory: null };
            } else {
                return { ...draftState, selectedRequest: null, requestHistory: null };
            }
        })
    ),
    on(setViewCollumns,
        (state, { viewCollumns }) => ({ ...state, viewCollumns })
    ),
    on(loadRequestHistoryByIdSuccess,
        (state, { requestHistory }) => ({ ...state, requestHistory })
    ),
);

export const getRequestsPage = (state: State) => state.page;
export const getSelectedRequest = (state: State) => state.selectedRequest;
export const getPaginationRequest = (state: State) => state.paginationRequest;
export const getViewCollumns = (state: State) => state.viewCollumns;
export const getRequestHistory = (state: State) => state.requestHistory;