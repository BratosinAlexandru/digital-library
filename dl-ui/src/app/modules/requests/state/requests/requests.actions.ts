import { createAction, props } from "@ngrx/store";
import { PaginationRequestDtoFiltersRequest, Request, RequestHistoryDto, RequestPage } from "src/app/api/models";

/** Load requests page actions */
export const loadRequestsPage = createAction('[Requests] Load Request Page', props<({ paginationRequest: PaginationRequestDtoFiltersRequest })>());
export const loadRequestsPageSuccess = createAction('[Requests] Load Request Page Success', props<({ page: RequestPage })>());
export const loadRequestsPageFailure = createAction('[Requests] Load Request Page Failure', props<({ error: any })>());

/** Load request actions */
export const loadRequestById = createAction('[Requests] Load Request By Id', props<({ id: string })>());
export const loadRequestByIdSuccess = createAction('[Requests] Load Request By Id Success', props<({ request: Request })>());
export const loadRequestByIdFailure = createAction('[Requests] Load Request By Id Failure', props<({ error: any })>());

/** Load Request History actions */
export const loadRequestHistoryById = createAction('[Requests] Load Request History By Id', props<({ id: string })>());
export const loadRequestHistoryByIdSuccess = createAction('[Requests] Load Request History By Id Success', props<({ requestHistory: Array<RequestHistoryDto> })>());
export const loadRequestHistoryByIdFailure = createAction('[Requests] Load Request History By Id Failure', props<({ error: any })>());

/** Create request actions */
export const createRequest = createAction('[Requests] Create Request', props<({ request: Request })>());
export const createRequestSuccess = createAction('[Requests] Create Request Success', props<({ request: Request })>());
export const createRequestFailure = createAction('[Requests] Create Request Failure', props<({ error: any })>());

/** Update  request actions*/
export const updateRequest = createAction('[Requests] Update Request', props<({ request: Request, redirectTo?: string })>());
export const updateRequestSuccess = createAction('[Requests] Update Request Success', props<({ request: Request })>());
export const updateRequestFailure = createAction('[Requests] Update Request Failure', props<({ error: any })>());

/** Accept request actions */
export const acceptRequests = createAction('[Requests] Accept requests', props<({ requestIds: Array<string>, paginationRequest: PaginationRequestDtoFiltersRequest })>());
export const acceptRequestsSuccess = createAction('[Requests] Accept requests');
export const acceptRequestsFailure = createAction('[Requests] Accept requests');

/** Delete requests actions */
export const deleteRequests = createAction('[Requests] Delete Requests', props<({ rqeuestIds: Array<string>, paginationRequest: PaginationRequestDtoFiltersRequest })>());
export const deleteRequestsSuccess = createAction('[Requests] Delete Requests Success', props<({ requestIds: Array<string> })>());
export const deleteRequestsFailure = createAction('[Requests] Delete Requests Failure', props<({ error: any })>());

/** Enable Requests actions*/
export const enableRequests = createAction('[Requests] Enable Requests', props<({ requestsIds: Array<string>, enable: boolean, paginationRequest: PaginationRequestDtoFiltersRequest })>());
export const enableRequestsSuccess = createAction('[Requests] Enable Requests Success', props<({ requestIds: Array<string>, enable: boolean })>());
export const enableRequestsFailure = createAction('[Requests] Enable Requests Failure', props<({ error: any })>());

/** Other actions */
export const setViewCollumns = createAction('[Requests] Set view collumns', props<({ viewCollumns: Array<string> })>());
export const selectRequest = createAction('[Requests] Set select request', props<({ selectedRequest: Request })>());