import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { Request, RequestPage } from "src/app/api/models";
import { AlertService } from "src/app/core/services/alert.service";
import { RequestService } from "../../services/request.service";
import { loadRequestById, loadRequestByIdFailure, loadRequestByIdSuccess, loadRequestsPage, loadRequestsPageFailure, loadRequestsPageSuccess, createRequest, createRequestSuccess, createRequestFailure, updateRequest, updateRequestSuccess, updateRequestFailure, deleteRequests, loadRequestHistoryById, loadRequestHistoryByIdSuccess, loadRequestHistoryByIdFailure, deleteRequestsFailure, enableRequests, enableRequestsFailure, acceptRequests, acceptRequestsSuccess, acceptRequestsFailure } from "./requests.actions";

@Injectable()
export class RequestsEffects {

    loadRequestsPage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestsPage),
            switchMap((action) =>
                this.requestService.getPagedRequests(action.paginationRequest).pipe(
                    map((page: RequestPage) => loadRequestsPageSuccess({ page })),
                    catchError((error) => of(loadRequestsPageFailure({ error })))
                )
            )
        )
    );

    loadRequestById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestById),
            switchMap((action) =>
                this.requestService.getRequestById(action.id).pipe(
                    map((request: Request) => loadRequestByIdSuccess({ request })),
                    catchError((error) => of(loadRequestByIdFailure({ error })))
                )
            )
        )
    );

    createRequest$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createRequest),
            switchMap((action) =>
                this.requestService.createRequest(action.request).pipe(
                    map(() => {
                        this.goBack();
                        return createRequestSuccess({ request: action.request });
                    }),
                    catchError((error) => of(createRequestFailure({ error })))
                )
            )
        )
    );

    updateRequest$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateRequest),
            switchMap((action) =>
                this.requestService.updateRequest(action.request).pipe(
                    map(() => {
                        if (action.redirectTo === 'requests') {
                            this.goBack();
                        }
                        return updateRequestSuccess({ request: action.request });
                    }),
                    catchError((error) => of(updateRequestFailure({ error })))
                )
            )
        )
    );

    acceptRequests$ = createEffect(() =>
        this.actions$.pipe(
            ofType(acceptRequests),
            switchMap((action) => {
                return this.requestService.acceptRequests(action.requestIds).pipe(
                    switchMap(() => this.requestService.getPagedRequests(action.paginationRequest).pipe(
                        map((page: RequestPage) => loadRequestsPageSuccess({ page })),
                        catchError((error) => of(loadRequestsPageFailure({ error })))
                    )),
                )
            })
        )
    );

    deleteRequests$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteRequests),
            switchMap((action) => {
                return this.requestService.deleteAllRequests(action.rqeuestIds).pipe(
                    switchMap(() => this.requestService.getPagedRequests(action.paginationRequest).pipe(
                        map((page: RequestPage) => loadRequestsPageSuccess({ page })),
                        catchError((error) => of(loadRequestsPageFailure({ error })))
                    )),
                    catchError((error) => of(deleteRequestsFailure({ error })))
                )
            })
        )
    );

    enableRequests$ = createEffect(() =>
        this.actions$.pipe(
            ofType(enableRequests),
            switchMap((action) => {
                return this.requestService.enableAllRequests(action.requestsIds, action.enable).pipe(
                    switchMap(() => this.requestService.getPagedRequests(action.paginationRequest).pipe(
                        map((page: RequestPage) => loadRequestsPageSuccess({ page })),
                        catchError((error) => of(loadRequestsPageFailure({ error })))
                    )),
                    catchError((error) => of(enableRequestsFailure({ error })))
                )
            })
        )
    );

    loadRequestHistoryById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestHistoryById),
            switchMap((action) => {
                return this.requestService.getHistory(action.id).pipe(
                    map(requestHistory => loadRequestHistoryByIdSuccess({ requestHistory })),
                    catchError(error => of(loadRequestHistoryByIdFailure({ error })))
                );
            })
        )
    );

    constructor(
        private actions$: Actions,
        private requestService: RequestService,
        private _router: Router,
        private alertService: AlertService,
    ) { }

    private goBack() {
        this._router.navigate(['/requests', 'pages']);
    }
}