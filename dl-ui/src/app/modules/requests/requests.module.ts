import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestsRoutingModule } from './requests-routing.module';
import { MaterialModule } from 'src/app/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateAoTLoaderFactory } from 'src/app/translate/factories/translate/translate.factory';
import { HttpClient } from '@angular/common/http';
import { CoreModule } from 'src/app/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { ConfirmDialogComponent } from './containers/confirm-dialog/confirm-dialog.component';
import { FiltersComponent } from './containers/filters/filters.component';
import { RequestHistoryComponent } from './containers/request-history/request-history.component';
import { RequestTableComponent } from './containers/request-table/request-table.component';
import { RequestComponent } from './components/request/request.component';
import { RequestsComponent } from './components/requests/requests.component';
import { StoreModule } from '@ngrx/store';
import * as RequestsProjectState from './state';
import { EffectsModule } from '@ngrx/effects';
import { FiltersEffects } from './state/filters/filters.effects';
import { RequestsEffects } from './state/requests/requests.effects';
import { RequestFormComponent } from './containers/request-form/request-form.component';

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    FiltersComponent,
    RequestHistoryComponent,
    RequestTableComponent,
    RequestComponent,
    RequestsComponent,
    RequestFormComponent
  ],
  imports: [
    CommonModule,
    RequestsRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateAoTLoaderFactory),
        deps: [HttpClient]
      }
    }),
    StoreModule.forFeature(RequestsProjectState.requestsFeatureKey, RequestsProjectState.reducers),
    EffectsModule.forFeature([FiltersEffects, RequestsEffects]),
    CoreModule,
    MatTooltipModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ro-RO' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ]
})
export class RequestsModule { }
