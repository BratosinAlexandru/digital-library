import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { merge, of, Subscription } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { PaginationRequestDtoFiltersRequest, Request, RequestStatus, RequestStatusCount, RequestType } from 'src/app/api/models';
import * as AuthSelectors from '../../../auth/state/auth.selectors';
import * as AuthState from '../../../auth/state/auth.state';
import { FilterEvent, FilterEventType } from '../../models/filter-event.model';
import { RequestsAppState, getViewCollumns } from '../../state';
import { acceptRequests, loadRequestHistoryById, setViewCollumns, updateRequest } from '../../state/requests/requests.actions';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { getRequestHistory } from '../../state';
import { RequestHistoryComponent } from '../request-history/request-history.component';

@Component({
  selector: 'request-table',
  templateUrl: './request-table.component.html',
  styleUrls: ['./request-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestTableComponent implements OnInit, OnChanges, OnDestroy {

  @Input() tableSource: Array<Request>;
  @Input() isLoading: boolean;
  @Input() requestStatus: Array<RequestStatus>;
  @Input() pagination: PaginationRequestDtoFiltersRequest;
  @Input() enableFilter: boolean;
  @Input() isAdmin: boolean;

  @Output() pageChanged = new EventEmitter();
  @Output() filterChanged = new EventEmitter();
  @Output() actionRequests = new EventEmitter();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public dataSource: Array<Request> = [];
  private orderColumns: Array<string> = [
    'requestDate',
    'requestType',
    'importantRequest',
    'requestStatus',
  ];
  public collumnsControl = new FormControl();
  public subscriptions$: Array<Subscription> = [];
  public hasSelectAll: boolean;
  public selectedRequests: Map<string, Request> = new Map<string, Request>();
  public enableButton: boolean;
  public disableButton: boolean;
  public deleteButton: boolean;
  private isFilterStatus: boolean;

  constructor(
    private ref: ChangeDetectorRef,
    public dialog: MatDialog,
    private _router: Router,
    private store: Store<RequestsAppState & AuthState.State>,
    private translateService: TranslateService,
  ) {
    this.resetDisableActions();
  }



  ngOnInit(): void {
    this.subscriptions$.push(this.store.pipe(take(1), select(getViewCollumns)).subscribe(
      (columns: Array<string>) => {
        if (columns) {
          this.collumnsControl.setValue(columns);
        } else {
          this.collumnsControl.setValue(this.orderColumns);
        }
        this.ref.detectChanges();
      }
    ));
    this.subscriptions$.push(this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
    }));
    this.subscriptions$.push(merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        switchMap(() => {
          const paginationRequest: PaginationRequestDtoFiltersRequest = {
            pageNumber: this.paginator.pageIndex,
            pageSize: this.paginator.pageSize || 50,
            sortDirection: this.sort.direction,
            sortProperties: this.sort.active && this.sort.direction ? [this.sort.active] : ['createdOn']
          };
          this.pageChanged.emit(paginationRequest);
          return of([]);
        })
      ).subscribe()
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tableSource && changes.tableSource.currentValue) {
      const source = changes.tableSource.currentValue;
      this.dataSource = source.content;
      this.paginator.length = source.totalElements;
      this.paginator.pageIndex = source.pageNumber;
      this.paginator.pageSize = source.pageSize;
      this.hasSelectAll = false;
    }
    this.ref.detectChanges();
  }

  getRequestType(request: Request): string {
    if (request && request.requestType && request.requestType.length > 0) {
      const reducer = (accumulator: string, currentValue: RequestType) => accumulator + ' ' + currentValue.name;
      return request.requestType.reduce(reducer, '');
    } else {
      return '';
    }
  }

  selectDisabled(event: MatCheckboxChange) {
    this.filterChanged.emit({
      type: FilterEventType.ENABLED,
      value: !event.checked
    } as FilterEvent);
  }

  select(event: MatCheckboxChange, row?: Request): void {
    if (row) {
      this.hasSelectAll = false;
      if (event.checked) {
        this.selectedRequests.set(row.id, row);
      } else {
        this.selectedRequests.delete(row.id);
      }
    } else {
      if (event.checked) {
        this.hasSelectAll = true;
        this.dataSource.forEach(e => this.selectedRequests.set(e.id, e));
      } else {
        this.hasSelectAll = false;
        this.selectedRequests.clear();
      }
    }
    this.checkButtonEnabled();
  }

  checkboxLabel(row?: Request): string {
    if (!row) {
      return `${this.hasSelectAll ? 'select' : 'deselect'} all`;
    }
    return `${this.selectedRequests.has(row.id) ? 'deselect' : 'select'} row ${row.id}`;
  }

  filterRequestStatus(status?: RequestStatusCount, index?: number) {
    this.isFilterStatus = !this.isFilterStatus;
    this.filterChanged.emit({
      type: FilterEventType.REQUEST_STATUS,
      value: { status, index }
    } as FilterEvent);
  }

  enableAll() {
    const confirmMessage = this.translateService.instant('Sigur doriți să ' + (this.enableFilter ? 'dezactivați?' : 'activați?'));
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: { confirmMessage }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.actionRequests.emit({ type: 'enable', ids: this.getIds(), enable: !this.enableFilter });
        this.selectedRequests.clear();
      }
    }));
  }

  deleteAll() {
    const confirmMessage = this.translateService.instant('Sigur doriți să ștergeți?');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: { confirmMessage }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.actionRequests.emit({ type: 'delete', ids: this.getIds() });
      }
    }));
  }

  acceptRequest() {
    const confirmMessage = this.translateService.instant('Sigur doriți să acceptați?');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: { confirmMessage }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.store.dispatch(acceptRequests({ requestIds: this.getIds(), paginationRequest: this.pagination }));
        this.selectedRequests.clear();
      }
    }));
  }

  openHistory(row?: Request) {
    this.dialog.open(RequestHistoryComponent, {
      data: {
        dataSource: this.store.pipe(
          select(getRequestHistory),
          map((requestHistory) => {
            if (!requestHistory) {
              this.store.dispatch(loadRequestHistoryById({ id: row.id }));
            }
            return requestHistory;
          })
        )
      }
    })
  }

  resetDisableActions() {
    this.enableButton = true;
    this.disableButton = true;
    this.deleteButton = true;
  }

  checkButtonEnabled() {
    this.resetDisableActions();
    this.selectedRequests.forEach(v => {
      if (v.enabled) {
        this.disableButton = false;
      } else {
        this.enableButton = false;
      }
    });
  }

  getIds(): Array<string> {
    const ids: Array<string> = [];
    for (const [k, v] of this.selectedRequests) {
      if ((v.enabled && this.enableFilter) || (!v.enabled && !this.enableFilter)) {
        ids.push(k);
      }
    }

    return ids;
  }

  compareCollumns(object1: string, object2: string) {
    return object1 && object2 && object1 === object2;
  }

  setColumn(selected: MatSelectChange) {
    this.store.dispatch(setViewCollumns({ viewCollumns: this.orderColumns.filter(e => selected.value.includes(e)) }));
  }

  selectRequest(request: Request) {
    if (!this.isAdmin) {
      this.store.dispatch(updateRequest({ request }));
      this._router.navigate(['/requests', request.id]);
    } else {
      this.openHistory(request);
    }
  }

  getColumns(colls: Array<string>) {
    if (this.isAdmin) {
      return colls ? ['selected', 'customer', ...colls] : [];
    } else {
      return colls ? ['customer', ...colls] : [];
    }
  }


  ngOnDestroy(): void {
    this.subscriptions$.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

}
