import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { merge, Observable, of, Subject, Subscription } from 'rxjs';
import { Account, BookOverview, CommunicationChannel, Customer, Request, RequestType } from 'src/app/api/models';
import { FiltersService } from '../../services/filters.service';
import {
  getRequestTypes,
  getSelectedRequest,
  RequestsAppState,
  getCommunicationChannels,
  getBooks
} from '../../state';
import * as AuthState from '../../../../modules/auth/state/auth.state';
import * as AuthSelectors from 'src/app/modules/auth/state/auth.selectors';
import { select, Store } from '@ngrx/store';
import { ApiConfiguration } from 'src/app/api/api-configuration';
import { catchError, debounceTime, map, switchMap, take, takeUntil } from 'rxjs/operators';
import { loadRequestById } from '../../state/requests/requests.actions';
import { AlertService } from 'src/app/core/services/alert.service';
import { loadBooks, loadCommunicationChannel, loadRequestType } from '../../state/filters/filters.actions';
@Component({
  selector: 'request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.scss']
})
export class RequestFormComponent implements OnInit, OnDestroy {

  @Input() id: string;
  @Output() save = new EventEmitter();
  @Output() goBack = new EventEmitter();

  public requestForm: FormGroup;
  public customerSearch: FormControl = new FormControl();

  public customerOptions$: Observable<Array<Customer>>;

  public requestTypes$: Observable<Array<RequestType>>;
  public communicationChannels$: Observable<Array<CommunicationChannel>>
  public books$: Observable<Array<BookOverview>>;

  public request: Request;
  public apiUrl: string;
  public saveRequestTrigger: boolean;
  public isAdmin: boolean;

  public subscriptions$: Array<Subscription> = [];
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private accountId: string;

  constructor(
    private fb: FormBuilder,
    private filtersService: FiltersService,
    private store: Store<RequestsAppState & AuthState.State>,
    private cdRef: ChangeDetectorRef,
    private authStore: Store<AuthState.State>,
    private apiConfiguration: ApiConfiguration,
    private alertService: AlertService,
  ) {
    this.apiUrl = this.apiConfiguration.rootUrl;
  }


  ngOnInit(): void {
    this.createForm();
    if (this.id && this.id !== 'new') {
      this.loadRequest(this.id);
    }

    this.initCustomerOptions();
    this.getLoggedUser();
  }

  private getLoggedUser() {
    this.subscriptions$.push(
      this.authStore.pipe(
        take(1),
        select(AuthSelectors.selectLoggedUser))
        .subscribe((user: any) => {
          this.accountId = user.id;
          if (user && user.roles) {
            if (user.roles.includes('admin')) {
              this.isAdmin = true;
            } else {
              this.isAdmin = false;
            }
            this.cdRef.detectChanges();
          }
        })
    );
  }

  loadRequest(id: string) {
    this.subscriptions$.push(
      this.store.pipe(select(getSelectedRequest), takeUntil(this.destroy$)).subscribe(
        (request: Request) => {
          if (!request || request.id !== id) {
            this.store.dispatch(loadRequestById({ id }));
          } else if (request && !this.request && id === request.id) {
            request = JSON.parse(JSON.stringify(request));
            this.destroy$.next(true);
            if (!request.customer || !request.customer.emails || !request.customer.phones) {
              if (!request.customer.emails) {
                request.customer.emails = [];
              }
              if (!request.customer.phones) {
                request.customer.phones = [];
              }
            }
            this.request = request;
            if (request && request.customer) {
              this.customerSearch.setValue(request.customer);
            }
            this.resetCustomerCtrls(request.customer);
            this.requestForm.patchValue(request);
            this.cdRef.detectChanges();
          }
        },
        (error: any) => {
          this.alertService.error(error);
        }
      )
    );
  }

  private createForm() {
    this.requestForm = this.fb.group({
      id: this.fb.control(null),
      enabled: this.fb.control(true),
      customer: this.fb.group({
        id: this.fb.control(null),
        enabled: this.fb.control(true),
        name: this.fb.control(null, [Validators.required, Validators.minLength(3)]),
        emails: this.fb.array([]),
        phones: this.fb.array([
          this.fb.control('', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9 ()-+]*')]),
        ]),
      }),
      accountId: this.fb.control(null),
      requestType: this.fb.control(null),
      communicationChannels: this.fb.control(null),
      importantRequest: this.fb.control(null, Validators.required),
      requestDate: this.fb.control(null),
      createdOn: this.fb.control(null),
      updatedOn: this.fb.control(null),
      book: this.fb.control(null),
      otherMentions: this.fb.control(null),
      college: this.fb.control(null),
      requestStatus: this.fb.control(null),
    });
  }

  addFormCtrl(formName: string) {
    const formArray: FormArray = this.requestForm.get('customer').get(formName) as FormArray;
    switch (formName) {
      case 'emails':
        formArray.push(
          new FormControl(
            '',
            [
              Validators.required,
              Validators.email,
              Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
            ],
            this.forbiddenEmail.bind(this)
          )
        );
        break;
      case 'phones':
        formArray.push(
          new FormControl(
            '',
            [
              Validators.minLength(10),
              Validators.pattern('[0-9 ()-+]*')
            ]
          )
        );
        break;
      default:
        break;
    }
    this.initCustomerOptions();
  }

  forbiddenEmailPatern(control: FormGroup): { [s: string]: boolean } {
    if (control.value && control.value.length > 0) {
      const patt = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9_+-]+.([a-zA-Z]{2,})$', 'g');
      if (patt.test(control.value.trim())) {
        return null;
      } else {
        return { forbiddenEmail: true };
      }
    } else {
      return null;
    }
  }

  forbiddenEmail(control: AbstractControl): Observable<any> {
    if (control.touched && control.value && control.value.length > 0) {
      if (
        this.customerSearch &&
        this.customerSearch.value &&
        this.customerSearch.value.email &&
        this.customerSearch.value.email.trim().toLowerCase() === control.value.trim().toLowerCase()
      ) {
        return of(null);
      }
      return this.filtersService.getByEmailOrByName(control.value).pipe(
        map((accounts: Array<Account>) => {
          this.cdRef.markForCheck();
          if (accounts && accounts.length > 0) {
            if (accounts.length === 1 && this.requestForm) {
              const id = this.requestForm.get('customer').get('id').value;
              if (id === accounts[0].id) {
                return null;
              }
            }
            this.alertService.error('Email existent!');
            return { forbiddenEmail: true };
          } else {
            return null;
          }
        })
      );
    } else {
      return of(null);
    }
  }

  private initCustomerOptions() {
    this.customerOptions$ = merge(
      ...this.requestForm
        .get('customer')
        .get('emails')
      ['controls'].map((control) => control.valueChanges),
      ...this.requestForm
        .get('customer')
        .get('phones')
      ['controls'].map((control) => control.valueChanges),
      this.customerSearch.valueChanges
    ).pipe(
      debounceTime(300),
      switchMap((value) => {
        if (typeof value === 'string' && value.length > 2) {
          return this.filtersService.getByEmailOrByName(value).pipe(
            catchError((error) => {
              return of([]);
            })
          );
        } else {
          return of([]);
        }
      })
    );

    this.initRequestType();
    this.initCommunicationChannels();
    this.initBooks();
  }

  public removeFormCtrl(control?: string, index?: number) {
    if (control) {
      const array = this.requestForm.get('customer').get(control) as FormArray;
      array.removeAt(index);
      this.cdRef.detectChanges();
    }
  }

  public resetClient(event: MouseEvent, control?: string) {
    event.stopPropagation();
    event.preventDefault();

    if (event.detail) {
      if (control) {
        this.requestForm.get('customer').get(control).reset();
      } else {
        this.requestForm.get('customer').reset();
        this.customerSearch.reset();
        this.resetCustomerCtrls();
      }
    }
    this.cdRef.detectChanges();
  }

  private initRequestType() {
    this.requestTypes$ = this.store.pipe(
      select(getRequestTypes),
      map((types: Array<RequestType>) => {
        if (!types) {
          this.store.dispatch(loadRequestType({ includeDisabled: false }));
          types = [];
        } else if (this.id && this.id === 'new') {
          types.filter((e) => e.defaultValue)
            .forEach((type) => {
              this.requestForm.get('requestType').patchValue([type]);
            });
        }
        return types;
      })
    );
  }

  private initCommunicationChannels() {
    this.communicationChannels$ = this.store.pipe(
      select(getCommunicationChannels),
      map((comm: Array<CommunicationChannel>) => {
        if (!comm) {
          this.store.dispatch(loadCommunicationChannel({ includeDisabled: false }));
          comm = [];
        }
        return comm;
      })
    );
  }

  private initBooks() {
    this.books$ = this.store.pipe(
      select(getBooks),
      map((books: Array<BookOverview>) => {
        if (!books) {
          this.store.dispatch(loadBooks({ includeDisabled: false }));
          books = [];
        }
        return books;
      })
    )
  }

  focusOut(input: string) {
    const controller = this.requestForm.get(input);
    if (typeof controller.value === 'string') {
      controller.setValue(null);
    }
  }

  isReadonly(input: string): boolean {
    const controller = this.requestForm.get(input);
    if (controller.value && typeof controller.value !== 'string') {
      return true;
    }
    return false;
  }

  selectCustomer(customer: Customer) {
    this.customerSearch.setValue(customer);
    setTimeout(() => {
      this.resetCustomerCtrls(customer);
    }, 0);
  }

  private resetCustomerCtrls(customer?: Customer) {
    const customerCtrl: FormGroup = this.requestForm.get('customer') as FormGroup;
    customerCtrl.reset();
    const formArrayEmails: FormArray = this.requestForm.get('customer').get('emails') as FormArray;
    formArrayEmails.clear();
    const formArrayPhones: FormArray = this.requestForm.get('customer').get('phones') as FormArray;
    formArrayPhones.clear();


    if (customer && customer.emails && customer.emails.length > 0) {
      customer.emails.forEach((email) => {
        formArrayEmails.push(
          new FormControl(
            email,
            [
              Validators.required,
              Validators.email,
              Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
            ],
            this.forbiddenEmail.bind(this)
          )
        );
      });
    }

    if (customer && customer.phones && customer.phones.length > 0) {
      customer.phones.forEach((phone) => {
        formArrayPhones.push(
          new FormControl(phone, [Validators.required, Validators.minLength(10), Validators.pattern('[0-9 ()-+]*')])
        );
      });
    } else {
      formArrayPhones.push(
        new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9 ()-+]*')])
      );
    }
    if (customer) {
      customerCtrl.patchValue(customer);
    }
  }

  compareObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id === object2.id;
  }

  displayCustomerFn(customer?: Customer): string | undefined {
    return customer ? displayObject(customer) : undefined;
  }

  displayCustomerEmailFn(customer?: Customer): string | undefined {
    return customer ? displayObject(customer, 'email') : undefined;
  }

  displayCustomerPhoneFn(customer?: Customer): string | undefined {
    return customer ? displayObject(customer, 'phone') : undefined;
  }

  public resetControl(event: MouseEvent, control?: string) {
    event.stopPropagation();
    event.preventDefault();

    if (event.detail) {
      if (control) {
        this.requestForm.get(control).reset();
      } else {
        this.requestForm.reset();
      }
    }
    this.cdRef.detectChanges();
  }

  saveRequest() {
    this.saveRequestTrigger = true;
    this.requestForm.get('accountId').patchValue(this.accountId);
    const request = this.requestForm.getRawValue();
    this.save.emit(request);
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach((e) => {
      if (e) {
        e.unsubscribe();
      }
    });
  }
}

function displayObject(customer: Customer, prop?: string): string {
  let result = '';
  if (typeof customer === 'string') {
    return customer;
  }
  if (customer.name) {
    result += customer.name + ' ';
    if (prop === 'name') {
      return customer.name;
    }
  }
  if (customer.emails) {
    result += customer.emails + ' ';
    if (prop === 'email') {
      return '' + customer.emails;
    }
  }
  if (customer.phones) {
    result += customer.phones + ' ';
    if (prop === 'phone') {
      return '' + customer.phones;
    }
  }
  return result;
}
