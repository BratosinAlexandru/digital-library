import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { Request, RequestHistoryDto } from 'src/app/api/models';

@Component({
  selector: 'request-history',
  templateUrl: './request-history.component.html',
  styleUrls: ['./request-history.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class RequestHistoryComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns = ['author', 'updatedOn', 'type', 'version', 'changed'];
  public dataSource: Observable<MatTableDataSource<RequestHistoryDto>>;
  public isLoading: boolean;
  public isExpanded: boolean;

  constructor(
    private dialogRef: MatDialogRef<RequestHistoryComponent>,
    private translateService: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.isLoading = true;
    this.dataSource = data.dataSource.pipe(
      filter(e => e !== null),
      map((requestHistory: Array<RequestHistoryDto>) => {
        setTimeout(() => {
          this.isLoading = false;
        }, 0);
        const table = new MatTableDataSource(requestHistory);
        table.paginator = this.paginator;
        table.sort = this.sort;
        return table;
      }),
      catchError(e => {
        this.isLoading = false;
        return [];
      })
    );
  }

  ngOnInit(): void {
  }

  getDetails(change: string, request: Request): string {
    if (!request) {
      return '';
    }

    switch (change) {
      case 'customer':
        return this.showValue(request.customer, ['name', 'email', 'phone']);
      case 'communicationChannels':
        const channel = request.communicationChannels;
        return channel ? channel.map(e => e.name).join(', ') : '';
      case 'requestType':
        const requestType = request.requestType;
        return requestType ? requestType.map(e => e.name).join(', ') : '';
      case 'book':
        return this.showValue(request.book, ['name']);
      case 'requestDate':
        const requestDate = request.requestDate;
        return requestDate ? requestDate?.split("T")[0] : '';
      case 'createdOn':
        const createdOn = request.createdOn;
        return createdOn ? createdOn?.split("T")[0] : '';
      case 'updatedOn':
        const updatedOn = request.updatedOn;
        return updatedOn ? updatedOn?.split("T")[0] : '';
      case 'requestStatus':
        return this.showValue(request.requestStatus, ['name']);
      default:
        return request[change];
    }
  }

  getDetailsOfCustomer(change: string, request: Request): string {
    if (!request || !request.customer) {
      return '';
    }

    const customer = request.customer;
    switch (change) {
      case 'name':
        return this.showValue(customer, ['name']);
      case 'email':
        return this.showValue(customer, ['email']);
      case 'phone':
        return this.showValue(customer, ['phone']);
      default:
        return customer[change];
    }
  }

  getListOfProperties(changes: Array<string>): string {
    return changes ? changes.map(e => this.getProperty(e)).join(', ') : '';
  }

  getProperty(change: string): string {

    if (!change) {
      return '';
    }

    const possibleChanges = {
      customer: 'Autor',
      communicationChannels: 'Canal de comunicare',
      college: 'Facultate',
      otherMentions: 'Alte mentiuni',
      requestType: 'Tip solicitare',
      importantRequest: 'Cerere importanta',
      book: 'Carte',
      requestDate: 'Data cerere',
      createdOn: 'Data creare',
      updatedOn: 'Data modificare',
      name: 'Nume',
      email: 'Email',
      phone: 'Telefon',
      accountId: 'Id-ul contului',
      enabled: 'Activata',
      requestStatus: 'Status solicitare',
    }
    return possibleChanges[change] ? possibleChanges[change] : change;
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

  private showValue(object: any, properties: Array<string>): string {
    if (object) {
      return properties.map(prop => object[prop]).filter(value => value).join(', ');
    } else {
      return '';
    }
  }

}
