import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatRadioButton } from '@angular/material/radio';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, debounceTime, map, skip, switchMap } from 'rxjs/operators';
import { Account, Customer, FiltersRequest, RequestType } from 'src/app/api/models';
import { MatPickRangeDefaultsService } from 'src/app/modules/mat-pick-range';
import { MatPickRangeComponent } from 'src/app/modules/mat-pick-range/mat-pick-range/mat-pick-range.component';
import { Options, Range } from 'src/app/modules/mat-pick-range/model/model';
import { FilterEvent, FilterEventType } from '../../models/filter-event.model';
import { FiltersService } from '../../services/filters.service';
import { getRequestTypes, RequestsAppState } from '../../state';
import { loadRequestType } from '../../state/filters/filters.actions';

@Component({
  selector: 'filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy {

  @Input() filters: FiltersRequest;
  @Input() isAdmin: boolean;
  @Output() filterChanged = new EventEmitter();
  @ViewChild('dateRangePicker') dateRangePicker: MatPickRangeComponent;

  public filterForm: FormGroup;
  public requestTypes$: Observable<Array<RequestType>>;
  public clientOptions$: Observable<Array<Customer>>;
  public options: Options;
  public expanded: boolean;

  private resetDateTrigger = true;
  private subscriptions$: Array<Subscription> = [];

  constructor(
    private fb: FormBuilder,
    private filtersService: FiltersService,
    private store: Store<RequestsAppState>,
    private cd: ChangeDetectorRef,
    private matPickRangeDefaultsService: MatPickRangeDefaultsService,
  ) { }


  ngOnInit(): void {
    this.createForm();
    this.initClientFilters();
    this.options = this.matPickRangeDefaultsService.getDefaultOptions(this.filters);
    this.initRequestTypeFilters();
  }

  private createForm() {
    this.filterForm = this.fb.group({
      client: this.filters.customer ? this.filters.customer : '',
      requestType: new FormArray([]),
      importantRequest: this.filters.importantRequest !== undefined ? '' + this.filters.importantRequest : null,
    });
  }

  private specialCharachers(text: string): string {
    text = text.toLowerCase();
    text = text.split(/[țtȚT]+/).join('t');
    text = text.split(/[ăaâAĂÂ]+/).join('a');
    text = text.split(/[iîÎI]+/).join('i');
    text = text.split(/[șsSȘ]+/).join('s');
    return text;
  }

  public updateRange(range: Range) {
    if (this.resetDateTrigger) {
      this.resetDateTrigger = false;
    } else {
      this.filterChanged.emit({ type: FilterEventType.RANGE, value: range } as FilterEvent);
    }
  }

  private initClientFilters() {
    this.clientOptions$ = this.filterForm.get('client').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => {
          if (typeof value === 'string' && value.length > 0) {
            if (typeof this.filterForm.get('client').value === 'object') {
              this.selectClient(null);
            }
            return this.filtersService.getByEmailOrByName(value, this.filters.enabled).pipe(
              catchError(error => {
                return of([]);
              })
            );
          } else {
            return of([]);
          }
        })
      )
  }

  public selectClient(client: Customer) {
    this.filterForm.reset();
    this.setEntirePeriod();
    this.filterForm.patchValue({ client });
    this.filterChanged.emit({ type: FilterEventType.CUSTOMER, value: client } as FilterEvent);
  }

  public searchClientName(name: string) {
    if (typeof name === 'string') {
      this.selectClient({ name });
    }
  }

  private initRequestTypeFilters() {
    this.store.dispatch(loadRequestType({ includeDisabled: false }));
    this.requestTypes$ = this.store.pipe(
      select(getRequestTypes),
      skip(1),
      map((types: Array<RequestType>) => {
        if (types) {
          const formArray = (this.filterForm.controls.requestType as FormArray);
          types.forEach(type => {
            formArray.push(
              new FormControl(
                this.filters.requestType && this.filters.requestType.length > 0
                  ? this.filters.requestType.find(e => type.id === e.id)
                  : false
              ));
          });
        }
        return types;
      })
    );
  }

  changedRequestTypes(selected: Array<boolean>, requestTypes: Array<RequestType>) {
    const types: Array<RequestType> = requestTypes.filter((element, index) => {
      return selected[index];
    });
    this.filterChanged.emit({ type: FilterEventType.REQUEST_TYPES, value: types } as FilterEvent);
  }

  changedRadio(el: MatRadioButton, controlName: string) {
    let value = null;
    if (el.checked) {
      setTimeout(() => {
        this.filterForm.get(controlName).reset();
      }, 0);
    } else {
      value = (el.value === 'true');
    }
    if (controlName === 'importantRequest') {
      this.filterChanged.emit({ type: FilterEventType.IMPORTANT_REQUEST, value } as FilterEvent);
    }
  }

  resetForm(event: MouseEvent) {
    this.filterForm.reset();
    this.resetDateRangePicker();
    this.filterChanged.emit({ type: FilterEventType.RESET } as FilterEvent);
  }

  private resetDateRangePicker() {
    const today = new Date();
    const fromDate = new Date(
      today.getFullYear(),
      today.getMonth() - 6,
      today.getDate(),
      0,
      -1 * today.getTimezoneOffset() + 59,
      0,
    );
    const resetRange = { fromDate, toDate: today };
    this.resetDateTrigger = true;
    this.dateRangePicker.resetDates(resetRange);
  }

  private setEntirePeriod() {
    const toDate = new Date();
    const resetRange = { fromDate: this.matPickRangeDefaultsService.startProjectDate, toDate };
    this.resetDateTrigger = true;
    this.dateRangePicker.resetDates(resetRange);
  }

  focusOut(input: string) {
    setTimeout(() => {
      const controller = this.filterForm.get(input);
      if (typeof controller.value === 'string') {
        controller.setValue(null);
        if (input === 'client') {
          this.filterChanged.emit({ type: FilterEventType.CUSTOMER, value: null } as FilterEvent);
        }
      }
    }, 500);
  }

  clearControl(input: string) {
    const controller = this.filterForm.get(input);
    controller.setValue(null);
    if (input === 'client') {
      this.filterChanged.emit({ type: FilterEventType.CUSTOMER, value: null } as FilterEvent);
    }
  }

  displayClientFn(object?: Customer): string | undefined {
    return object ? displayObjectClient(object) : undefined;
  }

  public parseListToString(list: Array<string>): string {
    let result = '';
    if (list && list.length > 0) {
      result += ' ';
      list.forEach(e => result += e + ' ');
    }
    return result;
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }
}

function displayObjectClient(product: Customer): string {
  let result = '';
  if (product.name) {
    result += product.name + ' ';
  }
  if (product.emails) {
    result += product.emails + ' ';
  }
  if (product.phones) {
    result += product.phones + ' ';
  }
  return result;
}
