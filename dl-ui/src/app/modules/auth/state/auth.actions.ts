import { createAction, props } from "@ngrx/store";
import { Credentials } from "../models";

/** Login actions */
export const login = createAction('[Login Page] Login', props<{ credentials: Credentials }>());
export const loginSuccess = createAction('[Auth/API] Login Success', props<{ credentials: Credentials }>());
export const loginFailure = createAction('[Auth/API] Login Failure', props<{ error: any }>());


/** Token actions */
export const refreshToken = createAction('[Interceptor] Refresh Token');
export const refreshTokenSuccess = createAction('[Auth/API] Refresh Token Success');
export const reloadToken = createAction('[Auth] Reload token');


/** Login redirect actions */
export const loginRedirect = createAction('[Auth/API] Login Redirect');

/** Logout actions */
export const logout = createAction('[Auth] Logout');
export const logoutConfirmation = createAction('[Auth] Logout Confirmation');
export const logoutConfirmationDismiss = createAction('[Auth] Logout Confirmation Dismiss');

/** Logged user actions */
export const getLoggedUser = createAction('[Auth] Get Logged User');
export const getLoggedUserRoles = createAction('[Auth] Get Logged User Roles');
export const getLoggedUserRolesSuccess = createAction(
    '[Auth] Get Logged User Roles Success',
    props<{ roles: string[] }>()
);
export const getLoggedUserSuccess = createAction(
    '[Auth] Get Logged User Success',
    props<{ user: Account }>()
);
export const getLoggedUserFailure = createAction('[Auth] Get Logged User Failure', props<{ error: any }>());

/** User actions */
export const updateAccount = createAction('[Login Page] Update Account', props<{ account: Account }>());
export const updateAccountSuccess = createAction(
    '[Login Page] Update Account Success',
    props<{ account: Account }>()
);
export const updateAccountFailure = createAction('[Login Page] Update Account Failure', props<{ error: any }>());


/** Reset password actions */
export const resetPassword = createAction('[Login Page] Reset Password', props<{email: string}>());
export const resetPasswordSuccess = createAction('[Login Page] Reset Password Success');
export const resetPasswordFailure = createAction('[Login Page] Reset Password Failure');