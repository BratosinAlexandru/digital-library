import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Credentials, User } from '../models';
import {
  getLoggedUser,
  getLoggedUserFailure,
  getLoggedUserSuccess,
  login,
  loginFailure,
  loginRedirect,
  loginSuccess,
  logout,
  logoutConfirmation,
  logoutConfirmationDismiss,
  refreshToken,
  refreshTokenSuccess,
  reloadToken,
  updateAccount,
  updateAccountSuccess,
  updateAccountFailure,
  getLoggedUserRoles,
  getLoggedUserRolesSuccess,
  resetPassword,
  resetPasswordSuccess,
  resetPasswordFailure,
} from './auth.actions';
import * as AuthSelectors from './auth.selectors';
import * as AuthState from './auth.state';
import { AccountService } from '../services/account.service';
import { AuthService } from '../services/auth.service';
import { catchError, exhaustMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserActions } from 'src/app/core/state';
import { Injectable } from '@angular/core';
import { LogoutConfirmationDialogComponent } from '../components/logout-confirmation-dialog/logout-confirmation-dialog.component';

@Injectable()
export class AuthEffects {

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      map((action) => action.credentials),
      exhaustMap((auth: Credentials) =>
        this.authService.login(auth).pipe(
          map(() => loginSuccess({ credentials: auth })),
          catchError((error) => of(loginFailure({ error })))
        )
      )
    )
  );

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginSuccess),
      map((action) => getLoggedUserRoles())
    )
  );

  getLoggedUserRolesSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getLoggedUserRolesSuccess),
      map((action) => getLoggedUser())
    )
  );

  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginRedirect, logout),
        tap((authed) => {
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  logoutConfirmation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(logoutConfirmation),
      exhaustMap(() => {
        const dialogRef = this.dialog.open<LogoutConfirmationDialogComponent, undefined, boolean>(
          LogoutConfirmationDialogComponent
        );
        return dialogRef.afterClosed();
      }),
      map((result) => (result ? logout() : logoutConfirmationDismiss()))
    )
  );

  logoutIdleUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.idleTimeout),
      map(() => logout())
    )
  );

  refreshToken$ = createEffect(() =>
    this.actions$.pipe(
      ofType(refreshToken),
      withLatestFrom(this.store$.select(AuthSelectors.selectUser)),
      switchMap(([action, user]: [Action, User]) => {
        if (!user) {
          return of(logout());
        }
        return this.authService.refreshToken().pipe(
          map(() => refreshTokenSuccess()),
          catchError((error) => of(reloadToken()))
        );
      })
    )
  );

  reloadToken$ = createEffect(() =>
    this.actions$.pipe(
      ofType(reloadToken),
      withLatestFrom(this.store$.select(AuthSelectors.selectRememberMe)),
      withLatestFrom(this.store$.select(AuthSelectors.selectUser)),
      switchMap(([[action, rememberMe], user]: [[Action, boolean], User]) =>
        rememberMe
          ? this.authService.login({ username: user.name, password: user.password, rememberMe: true }).pipe(
            map(() => refreshTokenSuccess()),
            catchError((error) => of(logout()))
          )
          : of(logout())
      )
    )
  );

  getLoggedUserRoles$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getLoggedUserRoles),
      exhaustMap(() => {
        return this.authService.getRolesForLoggedUser()
          .pipe(
            map((value) => getLoggedUserRolesSuccess({ roles: value }))
          );
      })
    )
  );

  getLoggedUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getLoggedUser),
      withLatestFrom(this.store$.select(AuthSelectors.selectRoles)),
      withLatestFrom(this.store$.select(AuthSelectors.selectUser)),
      exhaustMap(([[action, userRoles], user]: [[Action, string[]], User]) => {
        if (userRoles == null) {
          return of(getLoggedUserFailure({ error: 'User not logged' }));
        }
        if (userRoles.includes('ROLE_ADMIN')) {
          return this.authService.getAccountByEmail(user.name).pipe(
            map((account: Account) => {
              this.router.navigate(['/admin/accounts']);
              return getLoggedUserSuccess({ user: account });
            }),
            catchError((error) => of(getLoggedUserFailure({ error })))
          );
        } else {
          return this.authService.getAccountByEmail(user.name).pipe(
            map((account: Account) => {
              this.router.navigate(['/catalog']);
              return getLoggedUserSuccess({ user: account });
            }),
            catchError((error) => of(getLoggedUserFailure({ error })))
          );
        }
      })
    )
  );

  updateAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateAccount),
      switchMap((action) =>
        this.accountService.updateAccount(action.account).pipe(
          map(() => updateAccountSuccess({ account: action.account })),
          catchError((error) => of(updateAccountFailure({ error })))
        )
      )
    )
  );

  resetPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(resetPassword),
      map((action) => action.email),
      switchMap((email: string) =>
        this.authService.resetPassword(email).pipe(
          map(() => loginRedirect()),
          catchError((error) => of(resetPasswordFailure()))
        )
      )
    )
  );


  constructor(
    private actions$: Actions,
    private router: Router,
    private authService: AuthService,
    private accountService: AccountService,
    private dialog: MatDialog,
    private store$: Store<AuthState.State>
  ) { }
}