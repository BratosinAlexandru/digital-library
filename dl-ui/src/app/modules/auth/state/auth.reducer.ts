
import { Action, createReducer, on } from '@ngrx/store';
import { AuthState, initialState } from './auth.state';
import {
    login,
    loginFailure,
    loginSuccess,
    logout,
    refreshToken,
    refreshTokenSuccess,
    getLoggedUserSuccess,
    updateAccountSuccess,
    getLoggedUserRolesSuccess,
} from './auth.actions';

const _authReducer = createReducer(
    initialState,
    on(loginSuccess, (state, { credentials }) => ({
        ...state,
        rememberMe: credentials.rememberMe,
        user: { name: credentials.username, password: credentials.password },
        isLoggedIn: true,
        error: null,
        pending: false,
    })),
    on(getLoggedUserSuccess, (state, { user }) => ({
        ...state,
        loggedUser: user,
    })),
    on(getLoggedUserRolesSuccess, (state, { roles }) => ({
        ...state,
        roles: roles,
    })),
    on(logout, () => initialState),
    on(login, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(refreshToken, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(refreshTokenSuccess, (state) => ({
        ...state,
        isLoggedIn: true,
        error: null,
        pending: false,
    })),
    on(loginFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        isLoggedIn: false,
    })),
    on(updateAccountSuccess, (state, { account }) => ({
        ...state,
        loggedUser: account,
    }))
);

export function reducer(state: AuthState | undefined, action: Action) {
    return _authReducer(state, action);
}