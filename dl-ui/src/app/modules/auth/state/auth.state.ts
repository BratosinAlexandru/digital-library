import { User } from "../models";
import * as AppState from '../../../state/app.state';


export const authFeatureKey = 'auth';

export interface AuthState {
    user: User | undefined | null;
    error: string | undefined | null;
    pending: boolean;
    loggedUser: Account | null;
    rememberMe: boolean;
    roles: string[] | null;
    isLoggedIn: boolean;
}

export const initialState: AuthState = {
    user: null,
    error: null,
    pending: false,
    loggedUser: null,
    rememberMe: false,
    roles: null,
    isLoggedIn: false,
};

export interface State extends AppState.State {
    [authFeatureKey]: AuthState;
  }