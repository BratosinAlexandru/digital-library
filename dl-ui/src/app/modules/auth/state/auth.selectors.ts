import { createFeatureSelector, createSelector } from "@ngrx/store";
import { authFeatureKey, AuthState, State } from './auth.state';

export const getUser = (state: AuthState) => state.user;
export const getRoles = (state: AuthState) => state.roles;
export const getError = (state: AuthState) => state.error;
export const getPending = (state: AuthState) => state.pending;
export const getLoggedUser = (state: AuthState) => state.loggedUser;
export const getRememberMe = (state: AuthState) => state.rememberMe;
export const getIsLoggedIn = (state: AuthState) => state.isLoggedIn;

export const selectAuthState = createFeatureSelector<State, AuthState>(authFeatureKey);

export const selectUser = createSelector(selectAuthState, getUser);
export const selectRoles = createSelector(selectAuthState, getRoles);
export const selectLoggedUser = createSelector(selectAuthState, getLoggedUser);
export const selectIsLoggedIn = createSelector(selectAuthState, getIsLoggedIn);
export const selectLoggedIn = createSelector(selectLoggedUser, (loggedUser) => !!loggedUser);
export const selectLoginPageError = createSelector(selectAuthState, getError);
export const selectLoginPagePending = createSelector(selectAuthState, getPending);
export const selectRememberMe = createSelector(selectAuthState, getRememberMe);