import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as AuthReducers from './state/auth.reducer';
import * as AuthState from './state/auth.state';
import { CoreModule } from 'src/app/core/core.module';
import { LogoutConfirmationDialogComponent } from './components/logout-confirmation-dialog/logout-confirmation-dialog.component';
import { MaterialModule } from 'src/app/material';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LoginPageComponent } from './containers';
import { AuthEffects } from './state/auth.effects';
import { EditUserProfileDialogComponent } from './components/edit-user-profile-dialog/edit-user-profile-dialog.component';

export const COMPONENTS = [
  LogoutConfirmationDialogComponent,
  LoginFormComponent,
  LoginPageComponent,
  EditUserProfileDialogComponent,
];


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthRoutingModule,
    CoreModule,
    StoreModule.forFeature(AuthState.authFeatureKey, AuthReducers.reducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: COMPONENTS,
})
export class AuthModule { }
