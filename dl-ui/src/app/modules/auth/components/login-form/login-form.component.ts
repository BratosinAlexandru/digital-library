import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { User } from '../../models';
import { AuthService } from '../../services';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Input()
  set pending(isPending: boolean) {
    if (isPending) {
      this.forgotPasswordForm.disable();
      this.loginForm.disable();
    } else {
      this.forgotPasswordForm.enable();
      this.loginForm.enable();
    }
  }

  @Input() user: User;
  @Input() errorMessage: any;
  @Output() submitted = new EventEmitter<Credential>();
  @Output() resetPasswordOutput = new EventEmitter<string>();

  public showResetPassword: boolean;
  public recoveryEmailSent: boolean = false;
  private subscriptions$: Array<Subscription> = [];


  loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    rememberMe: new FormControl(false),
  });

  forgotPasswordForm: FormGroup = new FormGroup({
    email: this.fb.control('', [
      Validators.required,
      Validators.email,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
    ]),
  });


  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private alertService: AlertService,
    public translateService: TranslateService,
  ) { }


  ngOnInit(): void {
    if (this.user) {
      this.loginForm.patchValue({
        username: this.user.name,
        password: this.user.password,
      });
    }
  }

  removeWhitespacesUsername() {
    const valueUsername = this.loginForm.controls.username.value.replace(/\s/g, '');
    this.loginForm.controls.username.setValue(valueUsername.trim(), { onlySelf: true });
  }

  removeWhitespaces() {
    const valuePassword = this.loginForm.controls.password.value.replace(/\s/g, '');
    this.loginForm.controls.password.setValue(valuePassword.trim(), { onlySelf: true });
  }

  submit() {
    if (this.loginForm.valid) {
      this.submitted.emit(this.loginForm.value);
    }
  }

  backToLogin() {
    this.showResetPassword = !this.showResetPassword;
    this.recoveryEmailSent = false;
  }

  resetPassword() {
    if (this.forgotPasswordForm.valid) {
      const email = this.forgotPasswordForm.value.email;
      this.subscriptions$.push(
        this.authService.isEmailUnique(email).subscribe((unique) => {
          if (unique) {
            this.alertService.error(this.translateService.instant('Email inexistent!'));
          } else {
            this.resetPasswordOutput.emit(email);
            this.recoveryEmailSent = true;
            this.loginForm.patchValue({username: email, password: ''});
            this.forgotPasswordForm.reset();
          }
        })
      );
    }
  }
}
