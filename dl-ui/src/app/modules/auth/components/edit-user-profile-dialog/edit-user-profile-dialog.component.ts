import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'edit-user-profile-dialog',
  templateUrl: './edit-user-profile-dialog.component.html',
  styleUrls: ['./edit-user-profile-dialog.component.scss']
})
export class EditUserProfileDialogComponent implements OnInit {
  public accountForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditUserProfileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit(): void {
    this.accountForm = new FormGroup(
      {
        email: new FormControl(this.data.email),
        id: new FormControl(this.data.id),
        name: new FormControl(this.data.name, [Validators.required]),
        password: new FormControl(this.data.password),
        passwordConfirm: new FormControl(this.data.password),
        profilePicture: new FormControl(this.data.profilePicture),
        roles: new FormControl(this.data.roles),
      },
      this.groupValidation.bind(this)
    );
  }

  onClick(formValue?: Account): void {
    if (formValue) {
      const accountForm = JSON.parse(JSON.stringify(this.accountForm.getRawValue()));
      this.dialogRef.close(accountForm);
    } else {
      this.dialogRef.close();
    }
  }

  handleInputChanges(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    const reader = e.target;
    this.accountForm.get('profilePicture').setValue(reader.result);
    this.accountForm.markAsDirty();
  }

  groupValidation(control: FormGroup): { [s: string]: boolean } {
    const value = control.value;
    control.controls.passwordConfirm.setErrors(value.password === value.passwordConfirm ? null : { incorrect: true });

    return null;
  }

}
