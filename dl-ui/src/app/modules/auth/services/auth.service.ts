import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiConfiguration } from 'src/app/api/api-configuration';
import { AccountControllerService } from 'src/app/api/services';
import { Account } from 'src/app/api/models';
import { environment } from 'src/environments/environment';
import { Credentials } from '../models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_PATH = 'api_path';

  constructor(
    private http: HttpClient,
    config: ApiConfiguration,
    private accountControllerService: AccountControllerService,
  ) {
    this.API_PATH = config.rootUrl;
  }

  login({ username, password }: Credentials): Observable<any> {
    const input = new FormData();
    input.append('username', username);
    input.append('password', password);
    input.append('grant_type', 'password');

    const header = new HttpHeaders({
      Authorization: 'Basic ' + btoa(environment.clientId + ':' + environment.clientSecret),
    });
    return this.http.post<any>(`${this.API_PATH}/oauth/token`, input, { headers: header });
  }

  refreshToken(): Observable<any> {
    const input = new FormData();
    input.append('grant_type', 'refresh_token');
    const header = new HttpHeaders({
      Authorization: 'Basic ' + btoa(environment.clientId + ':' + environment.clientSecret),
    });

    return this.http.post<any>(`${this.API_PATH}/oauth/token`, input, { headers: header });
  }

  getAccountByEmail(email: string): Observable<Account> {
    return this.accountControllerService.getByEmailUsingGET(email);
  }

  getRolesForLoggedUser(): Observable<string[]> {
    return this.accountControllerService.getRolesUsingGET();
  }

  isEmailUnique(email: string): Observable<boolean> {
    return this.accountControllerService.isUniqueUsingGET(email);
  }

  resetPassword(email: string) {
    return this.accountControllerService.resetPasswordUsingPUT(email);
  }
}
