import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as AuthActions from '../state/auth.actions';
import * as AuthState from '../state/auth.state';
import * as AuthSelectors from '../state/auth.selectors';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private store: Store<AuthState.State>) { }
  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(AuthSelectors.selectLoggedIn),
      map(authed => {
        if (!authed) {
          this.store.dispatch(AuthActions.loginRedirect());
          return false;
        }

        return true;
      }),
      take(1)
    );
  }
}
