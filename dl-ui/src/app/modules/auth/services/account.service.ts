import { Injectable } from '@angular/core';
import { AccountControllerService } from 'src/app/api/services';
import { Account, PageAccount, PaginationRequestDtoFiltersRequest } from 'src/app/api/models';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(
    private serviceAccount: AccountControllerService,
  ) { }

  updateAccount(account: Account): Observable<null> {
    return this.serviceAccount.updateAccountUsingPUT(account);
  }

  getPagedAccounts(page: PaginationRequestDtoFiltersRequest): Observable<PageAccount> {
    return this.serviceAccount.getPagedUsingPOST(page);
  }
}
