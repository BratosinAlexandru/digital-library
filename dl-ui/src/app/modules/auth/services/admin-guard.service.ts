import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import * as AuthSelectors from '../state/auth.selectors';
import * as AuthState from '../state/auth.state';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  constructor(
    private store: Store<AuthState.State>,
    private router: Router,) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.store.pipe(
      select(AuthSelectors.selectRoles),
      map(token => {
        if (token && token.includes('ROLE_ADMIN')) {
          return true;
        } else {
          this.router.navigate(['admin']);
          return false;
        }
      }),
      take(1)
    );
  }
}
