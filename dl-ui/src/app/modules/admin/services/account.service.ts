import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account, PageAccount, PaginationRequestDtoFiltersRequest } from 'src/app/api/models';
import { AccountControllerService } from 'src/app/api/services';

@Injectable()
export class AccountService {

  constructor(
    private accountService: AccountControllerService,
  ) { }

  createAccount(account: Account): Observable<Account> {
    return this.accountService.createAccountUsingPOST(account);
  }

  updateAccount(account: Account): Observable<null> {
    return this.accountService.updateAccountUsingPUT(account);
  }

  getPagedAccounts(page: PaginationRequestDtoFiltersRequest): Observable<PageAccount> {
    return this.accountService.getPagedUsingPOST(page);
  }

  deleteAccounts(ids: Array<string>): Observable<null> {
    return this.accountService.deleteAccountsUsingDELETE(ids);
  }

  getAccountByEmail(email: string): Observable<Account> {
    return this.accountService.getByEmailUsingGET(email);
  }

  isEmailUnique(email: string): Observable<boolean> {
    return this.accountService.isUniqueUsingGET(email);
  }
}
