import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/api/models';
import { CategoryControllerService } from 'src/app/api/services';

@Injectable()
export class CategoryService {

  constructor(
    private categoryService: CategoryControllerService
  ) { }

  getCategories(includeDisabled?: boolean): Observable<Array<Category>> {
    return this.categoryService.getCategoriesUsingGET(includeDisabled);
  }

  getCategory(categoryId: string): Observable<Category> {
    return this.categoryService.getCategoryUsingGET(categoryId);
  }

  getCategoriesForDomain(ids: Array<string>, includeDisabled?: boolean): Observable<Array<Category>> {
    return this.categoryService.getCategoriesByDomainUsingGET({domainIds: ids, includeDisabled: includeDisabled});
  }

  createCategory(category: Category): Observable<Category> {
    return this.categoryService.createCategoryUsingPOST(category);
  }

  updateCategory(category: Category): Observable<Category> {
    return this.categoryService.updateCategoryUsingPUT(category);
  }

  deleteCategory(categoryId: string): Observable<Category> {
    return this.categoryService.deleteCategoryUsingDELETE(categoryId);
  }
}
