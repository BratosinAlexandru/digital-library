import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppKeysControllerService, NotificationControllerService, NotificationTemplateControllerService } from 'src/app/api/services';
import { NotificationTemplate, Notification } from 'src/app/api/models';
import { Language } from '../models/notification.model';
import { map } from 'rxjs/operators';
@Injectable()
export class NotificationTemplateService {

  constructor(
    private templatesControllerService: NotificationTemplateControllerService,
    private notificationControllerService: NotificationControllerService,
    private appKeyControllerService: AppKeysControllerService
  ) { }

  getNotifications(includeDisabled?: boolean): Observable<Array<Notification>> {
    return this.notificationControllerService.getNotificationsUsingGET(includeDisabled);
  }

  updateNotification(notification: Notification): Observable<null> {
    return this.notificationControllerService.updateNotificationUsingPUT(notification);
  }

  getTemplatesByNotificationId(id: string): Observable<{ [key: string]: Array<NotificationTemplate> }> {
    return this.templatesControllerService.getTemplatesByNotificationIdUsingGET(id);
  }

  updateTemplate(template: NotificationTemplate): Observable<null> {
    return this.templatesControllerService.updateNotificationTemplateUsingPUT(template);
  }

  createTemplate(template: NotificationTemplate): Observable<NotificationTemplate> {
    return this.templatesControllerService.createTemplateUsingPOST(template);
  }

  getTemplateTypes(): Observable<Array<string>> {
    return this.templatesControllerService.getTypesUsingGET();
  }

  deleteTemplate(id: string): Observable<NotificationTemplate> {
    return this.templatesControllerService.deleteTemplateUsingDELETE(id);
  }

  getTemplateLanguages(): Observable<Array<Language>> {
    return this.appKeyControllerService
      .findByKeyUsingGET('notification_languages')
      .pipe(map((e) => e.value as Array<Language>));
  }

}
