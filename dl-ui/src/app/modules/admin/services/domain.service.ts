import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Domain } from 'src/app/api/models';
import { DomainControllerService } from 'src/app/api/services';

@Injectable()
export class DomainService {

  constructor(
    private domainService: DomainControllerService
  ) { }

  getDomains(includeDisabled: boolean): Observable<Array<Domain>> {
    return this.domainService.getDomainsUsingGET(includeDisabled);
  }

  createDomain(domain: Domain): Observable<Domain> {
    return this.domainService.createDomainUsingPOST(domain);
  }

  updateDomain(domain: Domain): Observable<Domain> {
    return this.domainService.updateDomainUsingPUT(domain);
  }

  deleteDomain(domainId: string): Observable<Domain> {
    return this.domainService.deleteDomainUsingDELETE(domainId);
  }
}
