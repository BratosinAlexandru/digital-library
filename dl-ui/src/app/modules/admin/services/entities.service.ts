import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationChannel, RequestStatus, RequestType } from 'src/app/api/models';
import { CommunicationChannelControllerService, RequestStatusControllerService, RequestTypeControllerService } from 'src/app/api/services';

@Injectable({
  providedIn: 'root'
})
export class EntitiesService {

  constructor(
    private communicationChannelControllerService: CommunicationChannelControllerService,
    private requestTypeControllerService: RequestTypeControllerService,
    private requestStatusControllerService: RequestStatusControllerService,
  ) { }
  
  /** Communication channels */

  public loadCommunicationChannels(includeDisabled?: boolean): Observable<Array<CommunicationChannel>> {
    return this.communicationChannelControllerService.getCommunicationChannelsUsingGET(includeDisabled);
  }

  public createCommunicationChannel(communicationChannel: CommunicationChannel): Observable<CommunicationChannel> {
    return this.communicationChannelControllerService.createCommunicationChannelUsingPOST(communicationChannel);
  }

  public updateCommunicationChannel(communicationChannel: CommunicationChannel): Observable<null> {
    return this.communicationChannelControllerService.updateCommunicationChannelUsingPUT(communicationChannel);
  }

  public deleteCommunicationChannel(communicationChannelId: string): Observable<null> {
    return this.communicationChannelControllerService.deleteCommunicationChannelUsingDELETE(communicationChannelId);
  }

  /** Request types */

  public loadRequestTypes(includeDisabled?: boolean): Observable<Array<RequestType>> {
    return this.requestTypeControllerService.getRequestTypesUsingGET(includeDisabled);
  }

  public createRequestType(requestType: RequestType): Observable<RequestType> {
    return this.requestTypeControllerService.createRequestTypeUsingPOST(requestType);
  }

  public updateRequestType(requestType: RequestType): Observable<null> {
    return this.requestTypeControllerService.updateRequestTypeUsingPUT(requestType);
  }

  public deleteRequestType(requestTypeId: string): Observable<null> {
    return this.requestTypeControllerService.deleteRequestTypeUsingDELETE(requestTypeId);
  }

  /** Request statuses */

  public loadRequestStatuses(includeDisabled?: boolean): Observable<Array<RequestStatus>> {
    return this.requestStatusControllerService.getRequestStatuesUsingGET(includeDisabled);
  }

  public createRequestStatus(requestStatus: RequestStatus): Observable<RequestStatus> {
    return this.requestStatusControllerService.createRequestStatusUsingPOST(requestStatus);
  }

  public updateRequestStatus(requestStatus: RequestStatus): Observable<null> {
    return this.requestStatusControllerService.updateRequestStatusUsingPUT(requestStatus);
  }

  public deleteReqeustStatus(requestStatusId: string): Observable<null> {
    return this.requestStatusControllerService.deleteRequestStatusUsingDELETE(requestStatusId);
  }

}
