import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AccountsTableComponent } from './containers/accounts-table/accounts-table.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminTabsComponent } from './containers/admin-tabs/admin-tabs.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateAoTLoaderFactory } from 'src/app/translate/factories/translate/translate.factory';
import { HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import * as AdminProjectState from './state/';
import { EffectsModule } from '@ngrx/effects';
import { AccountEffects } from './state/account/account.effects';
import { AccountDialogComponent } from './containers/account-dialog/account-dialog.component';
import { ConfirmationDialogComponent } from './containers/confirmation-dialog/confirmation-dialog.component';
import { AccountService } from './services/account.service';
import { NotificationComponent } from './components/notification/notification.component';
import { NotificationElementComponent } from './containers/notification-element/notification-element.component';
import { NotificationTemplateComponent } from './containers/notification-template/notification-template.component';
import { NotificationTemplateService } from './services/notification-template.service';
import { NotificationEffects } from './state/notification/notification.effects';
import { NotificationTemplateDialogComponent } from './containers/notification-template-dialog/notification-template-dialog.component';
import { DomainEffects } from './state/domains/domains.effects';
import { CategoryEffects } from './state/category/category.effects';
import { GeneralComponent } from './components/general/general.component';
import { DomainsComponent } from './components/domains/domains.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { DomainService } from './services/domain.service';
import { CategoryService } from './services/category.service';
import { DomainsDialogComponent } from './containers/domains-dialog/domains-dialog.component';
import { CategoryDialogComponent } from './containers/category-dialog/category-dialog.component';
import { EntitiesEffects } from './state/entities/entities.effects';
import { CommunicationChannelsComponent } from './components/communication-channels/communication-channels.component';
import { ElementDialogComponent } from './containers/element-dialog/element-dialog.component';
import { RequestTypeComponent } from './components/request-type/request-type.component';
import { EntitiesService } from './services/entities.service';
import { RequestStatusComponent } from './components/request-status/request-status.component';
import { RequestStatusDialogComponent } from './containers/request-status-dialog/request-status-dialog.component';
import { StatusesEffects } from './state/statuses/statuses.effects';
import { ColorPickerModule } from 'ngx-color-picker';



@NgModule({
  declarations: [
    AccountsTableComponent,
    AccountsComponent,
    AdminTabsComponent,
    AccountDialogComponent,
    ConfirmationDialogComponent,
    NotificationComponent,
    NotificationElementComponent,
    NotificationTemplateComponent,
    NotificationTemplateDialogComponent,
    GeneralComponent,
    DomainsComponent,
    CategoriesComponent,
    DomainsDialogComponent,
    CategoryDialogComponent,
    CommunicationChannelsComponent,
    ElementDialogComponent,
    RequestTypeComponent,
    RequestStatusComponent,
    RequestStatusDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    ColorPickerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateAoTLoaderFactory),
        deps: [HttpClient]
      }
    }),
    StoreModule.forFeature(AdminProjectState.adminFeatureKey, AdminProjectState.reducers),
    EffectsModule.forFeature([
      DomainEffects,
      CategoryEffects,
      AccountEffects,
      NotificationEffects,
      EntitiesEffects,
      StatusesEffects,
    ]),
    MatTooltipModule,
    AdminRoutingModule,
    CKEditorModule,
  ],
  providers: [
    AccountService,
    NotificationTemplateService,
    DomainService,
    CategoryService,
    EntitiesService,
  ]
})
export class AdminModule { }
