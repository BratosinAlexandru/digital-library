import { Category, Domain } from "src/app/api/models";

export interface DomainsWithCategories extends Domain {
    categories?: Array<Category>
}