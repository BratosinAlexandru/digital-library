import { NotificationTemplate, Notification } from 'src/app/api/models';

export interface NotificationWithTemplates {
    notification: Notification;
    templates?: { [key: string]: Array<NotificationTemplate> };
}

export interface Language {
    code: string;
    name: string;
}