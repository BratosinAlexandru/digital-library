import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminAppState, getNotifications, getSelectedNotification, getTemplateTypes, getTemplateLanguages } from '../../state';
import { changeSelectedNotification, loadNotification, loadTemplatesForNotification, updateNotification, updateTemplate, deleteTemplate, createTemplate, loadTemplateTypes, loadTemplateLanguages, loadTemplateLanguagesFailure } from '../../state/notification/notification.actions';
import { NotificationTemplate } from 'src/app/api/models';
import { NotificationWithTemplates, Language } from '../../models/notification.model';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { NotificationTemplateDialogComponent } from '../../containers/notification-template-dialog/notification-template-dialog.component';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  public notifications$: Observable<Array<NotificationWithTemplates>>;
  public selectedNotification$: Observable<NotificationWithTemplates>;
  public types$: Observable<Array<string>>;
  public languages$: Observable<Array<Language>>;
  public selectedNotification: NotificationWithTemplates;

  public notificationForm: FormGroup;

  constructor(
    private store: Store<AdminAppState>,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.initNotifications();
    this.initForm();
  }

  private initNotifications() {
    this.notifications$ = this.store.pipe(
      select(getNotifications),
      map(notifications => {
        if (notifications) {
          return notifications;
        } else {
          this.store.dispatch(loadNotification({includeDisabled: true}));
          return [];
        }
      })
    );
    this.selectedNotification$ = this.store.pipe(
      select(getSelectedNotification),
      map(notification => {
        if (notification) {
          if (!notification.templates) {
            this.selectNotification(notification);
          } else {
            this.notificationForm.patchValue(notification);
            const notif = notification.notification;
            if (notif && notif.emails) {
              this.initEmailsArray(notif.emails);
            }
            if (notification.templates) {
              this.initTemplatesGroup(notification.templates);
            }
          }
        } else {
          this.notificationForm.reset();
        }
        return notification;
      })
    );
    this.types$ = this.store.pipe(
      select(getTemplateTypes),
      map(types => {
        if (types) {
          return types;
        } else {
          this.store.dispatch(loadTemplateTypes());
          return [];
        }
      })
    );
    this.languages$ = this.store.pipe(
      select(getTemplateLanguages),
      map(languages => {
        if (languages) {
          return languages;
        } else {
          this.store.dispatch(loadTemplateLanguages());
          return [];
        }
      })
    );
  }

  private initForm() {
    this.notificationForm = this.fb.group({
      notification: this.fb.group({
        id: this.fb.control(null),
        key: this.fb.control({ value: null, disabled: true }),
        name: this.fb.control(null),
        emails: this.fb.array([]),
        variables: this.fb.control({ value: null, disabled: true }),
        enableToMain: this.fb.control(null),
        enabled: this.fb.control(null),
      }),
      templates: this.fb.group({
        EMAIL: this.fb.array([]),
      })
    });
  }

  private initEmailsArray(emails: Array<string>) {
    const formArray: FormArray = this.notificationForm.get('notification').get('emails') as FormArray;
    formArray.clear();
    emails.forEach(email => formArray.push(new FormControl(
      email,
      [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]
    )));
  }

  private initTemplatesGroup(templates: { [key: string]: Array<NotificationTemplate> }) {
    if (templates['EMAIL']) {
      const formArray: FormArray = this.notificationForm.get('templates').get('EMAIL') as FormArray;
      formArray.clear()
      templates['EMAIL'].forEach(template => formArray.push(this.generateTemplateFormGroup(template)));
    }
  }

  private generateTemplateFormGroup(template: NotificationTemplate): FormGroup {
    return this.fb.group({
      id: this.fb.control(template.id),
      language: this.fb.control(template.language),
      enabled: this.fb.control(template.enabled),
      notification: this.fb.control(template.notification),
      type: this.fb.control(template.type),
      default: this.fb.control(template.default ? { value: true, disabled: true } : false),
      subject: this.fb.control(template.subject),
      content: this.fb.control(template.content),
    });
  }

  selectNotification(selectedNotification: NotificationWithTemplates) {
    this.selectedNotification = selectedNotification;
    if (selectedNotification.templates) {
      this.store.dispatch(changeSelectedNotification({ selectedNotification }));
    } else {
      const notification = selectedNotification.notification;
      this.store.dispatch(loadTemplatesForNotification({ notification }));
    }
  }

  onSaveClick(selected: NotificationWithTemplates) {
    if (this.notificationForm) {
      const templatesCtrl = this.notificationForm.get('templates');
      if (templatesCtrl.touched) {
        const mailCtrl = templatesCtrl.get('EMAIL') as FormArray;
        mailCtrl.controls.forEach(control => {
          if (control.touched) {
            const template = control.value;
            this.store.dispatch(updateTemplate({ template }));
          }
        })
      }
      const notificationCtrl = this.notificationForm.get('notification');
      const notification = JSON.parse(JSON.stringify(notificationCtrl.value));
      notification.key = selected.notification.key;
      notification.variables = selected.notification.variables;
      this.store.dispatch(updateNotification({ notification }));
    }
  }

  createTemplate(selected: NotificationWithTemplates) {
    const selectedLanguages = {};
    Object.keys(selected.templates).forEach(key => {
      selectedLanguages[key] = selected.templates[key].map(notification => notification.language);
    });
    const dialogRef = this.dialog.open(NotificationTemplateDialogComponent, {
      width: '300px',
      data: {
        types: this.types$,
        languages: this.languages$,
        selectedLanguages,
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        const template: NotificationTemplate = {
          content: '',
          subject: '',
          default: false,
          notification: selected.notification,
          enabled: response.enabled,
          type: response.type,
          language: response.language.code
        };
        this.store.dispatch(createTemplate({ template }));
      }
    });
  }

  deleteTemplate(template: NotificationTemplate) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        question: this.translateService.instant('Sunteti sigur ca doriti sa stergeti acest șablon?'),
        yesButton: this.translateService.instant('Da'),
        noButton:  this.translateService.instant('Nu')
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteTemplate({ template }));
      }
    });
  }

}
