import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { CommunicationChannel } from 'src/app/api/models';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';
import { ElementDialogComponent } from '../../containers/element-dialog/element-dialog.component';
import { AdminAppState, getCommunicationChannels } from '../../state';
import { createCommunicationChannel, deleteCommunicationChannel, loadCommunicationChannel, updateCommunicationChannel } from '../../state/entities/entities.actions';

@Component({
  selector: 'communication-channels',
  templateUrl: './communication-channels.component.html'
})
export class CommunicationChannelsComponent implements OnInit, OnDestroy {

  public communicationChannels$: Observable<Array<CommunicationChannel>>;

  private subscriptions$: Array<Subscription> = [];

  constructor(
    private store: Store<AdminAppState>,
    public dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    this.store.dispatch(loadCommunicationChannel());
  }

  ngOnInit(): void {
    this.communicationChannels$ = this.store.pipe(select(getCommunicationChannels));
  }

  addCommunicationChannel() {
    const dialogRef = this.dialog.open(ElementDialogComponent, {
      width: '500px',
      data: {
        enabled: true,
        name: '',
        header: this.translateService.instant('Adaugă canal de comunicare')
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(response => {
      if (response) {
        const communicationChannel = JSON.parse(JSON.stringify(response)) as CommunicationChannel;
        this.store.dispatch(createCommunicationChannel({ communicationChannel }));
      }
    }))
  }

  editCommunicationChannel(cc: CommunicationChannel) {
    const dialogRef = this.dialog.open(ElementDialogComponent, {
      width: '500px',
      data: {
        enabled: cc.enabled,
        name: cc.name,
        header: this.translateService.instant('Editează canal de comunicare'),
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(response => {
      if (response) {
        const communicationChannel = JSON.parse(JSON.stringify(cc)) as CommunicationChannel;
        communicationChannel.enabled = response.enabled;
        communicationChannel.name = response.name;
        this.store.dispatch(updateCommunicationChannel({ communicationChannel }));
      }
    }))
  }

  deleteCommunicationChannel(event: MouseEvent, communicationChannelId: string) {
    event.stopPropagation();
    event.preventDefault();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        question: this.translateService.instant('Sunteti sigur ca doriti sa stergeti acest canal de comunicatii?'),
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.store.dispatch(deleteCommunicationChannel({ communicationChannelId }));
      }
    }))
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach((subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

}
