import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { RequestStatus } from 'src/app/api/models';
import { RequestStatusControllerService } from 'src/app/api/services';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';
import { RequestStatusDialogComponent } from '../../containers/request-status-dialog/request-status-dialog.component';
import { AdminAppState, getStatuses } from '../../state';
import { createRequestStatus, updateRequestStatus, deleteRequestStatus, loadStatuses } from '../../state/statuses/statuses.actions';


@Component({
  selector: 'request-status',
  templateUrl: './request-status.component.html',
  styleUrls: ['./request-status.component.scss']
})
export class RequestStatusComponent implements OnInit, OnDestroy {

  public requestStatuses$: Observable<Array<RequestStatus>>;

  private subscriptions$: Array<Subscription> = [];

  constructor(
    private store: Store<AdminAppState>,
    private dialog: MatDialog,
    public translateService: TranslateService,
    private entitiesService: RequestStatusControllerService,
  ) {
    this.store.dispatch(loadStatuses());
  }

  ngOnInit(): void {
    this.requestStatuses$ = this.store.pipe(select(getStatuses));
  }

  addRequestStatus() {
    const dialogRef = this.dialog.open(RequestStatusDialogComponent, {
      width: '500px',
      data: {
        enabled: true,
        name: '',
        color: '',
        header: this.translateService.instant('Creaza un status pentru solicitari'),
        defaultValue: false,
      } as RequestStatus
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        const requestStatus = JSON.parse(JSON.stringify(response)) as RequestStatus;
        this.store.dispatch(createRequestStatus({ requestStatus }));
      }
    }));
  }

  editRequestStatus(selectedRequestStatus: RequestStatus) {
    const dialogRef = this.dialog.open(RequestStatusDialogComponent, {
      width: '500px',
      data: {
        enabled: selectedRequestStatus.enabled,
        name: selectedRequestStatus.name,
        header: this.translateService.instant('Editeaza statusul cererii'),
        defaultValue: selectedRequestStatus.defaultValue,
        color: selectedRequestStatus.color
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(response => {
      if (response) {
        const updatedRequestStatus = JSON.parse(JSON.stringify(selectedRequestStatus)) as RequestStatus;
        updatedRequestStatus.enabled = response.enabled;
        updatedRequestStatus.name = response.name;
        updatedRequestStatus.color = response.color;
        updatedRequestStatus.defaultValue = response.defaultValue;
        this.store.dispatch(updateRequestStatus({ updatedRequestStatus }));
      }
    }));
  }

  deleteRequestStatus(event: MouseEvent, requestStatusId: string) {
    event.stopPropagation();
    event.preventDefault();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        question: this.translateService.instant('Sunteti sigur ca doriti sa stergeti acest status de solicitare?'),
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteRequestStatus({ requestStatusId }));
      }
    }))
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach((subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
