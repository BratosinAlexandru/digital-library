import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Domain } from 'src/app/api/models';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';
import { DomainsDialogComponent } from '../../containers/domains-dialog/domains-dialog.component';
import { AdminAppState, getDomains } from '../../state';
import { createDomain, deleteDomain, loadDomains, updateDomain } from '../../state/domains/domains.actions';
@Component({
  selector: 'domains',
  templateUrl: './domains.component.html'
})
export class DomainsComponent implements OnInit {

  public domains$: Observable<Array<Domain>>;

  constructor(
    private store: Store<AdminAppState>,
    public dialog: MatDialog,
    public translateService: TranslateService
  ) {
    this.store.dispatch(loadDomains())
  }

  ngOnInit(): void {
    this.domains$ = this.store.pipe(select(getDomains));
  }

  addDomain() {
    const dialogRef = this.dialog.open(DomainsDialogComponent, {
      width: '500px',
      data: {
        enabled: true,
        name: '',
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        const domain = JSON.parse(JSON.stringify(response)) as Domain;
        this.store.dispatch(createDomain({ domain }));
      }
    });
  }

  editDomain(input: Domain) {
    const dialogRef = this.dialog.open(DomainsDialogComponent, {
      width: '500px',
      data: JSON.parse(JSON.stringify(input))
    });
    dialogRef.afterClosed().subscribe((domain: Domain) => {
      if (domain) {
        this.store.dispatch(updateDomain({ domain }))
      }
    });


  }

  delete(event: MouseEvent, domainId: string) {
    event.stopPropagation();
    event.preventDefault();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {
        question: this.translateService.instant('Esti sigur ca vrei sa stergi acest domeniu?'),
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteDomain({ domainId }));
      }
    });
  }

}
