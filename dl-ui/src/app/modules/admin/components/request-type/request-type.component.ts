import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { RequestType } from 'src/app/api/models';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';
import { ElementDialogComponent } from '../../containers/element-dialog/element-dialog.component';
import { AdminAppState, getRequestTypes } from '../../state';
import { createRequestType, deleteRequestType, loadRequestTypes, updateRequestType } from '../../state/entities/entities.actions';

@Component({
  selector: 'request-type',
  templateUrl: './request-type.component.html'
})
export class RequestTypeComponent implements OnInit, OnDestroy {

  public requestTypes$: Observable<Array<RequestType>>;

  private subscriptions$: Array<Subscription> = [];

  constructor(
    private store: Store<AdminAppState>,
    public dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    this.store.dispatch(loadRequestTypes());
  }

  ngOnInit(): void {
    this.requestTypes$ = this.store.pipe(select(getRequestTypes));
  }

  addRequestType() {
    const dialogRef = this.dialog.open(ElementDialogComponent, {
      width: '500px',
      data: {
        enabled: true,
        name: '',
        header: this.translateService.instant('Adauga un nou tip de solicitare'),
        defaultValue: false,
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(requestType => {
      if (requestType) {
        this.store.dispatch(createRequestType({ requestType }));
      }
    }));
  }

  editRequestType(selectedRequestType: RequestType) {
    const dialogRef = this.dialog.open(ElementDialogComponent, {
      width: '500px',
      data: {
        id: selectedRequestType.id,
        enabled: selectedRequestType.enabled,
        name: selectedRequestType.name,
        defaultValue: selectedRequestType.defaultValue,
        header: this.translateService.instant('Editeaza tipul de solicitare'),
        delete: false
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe(requestType => {
      if (requestType) {
        this.store.dispatch(updateRequestType({ requestType }));
      }
    }));
  }

  deleteRequestType(event: MouseEvent, requestTypeId: string) {
    event.stopPropagation();
    event.preventDefault();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        question: this.translateService.instant('Sunteti sigur ca doriti sa stergeti acest tip de solicitare?'),
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    this.subscriptions$.push(dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteRequestType({ requestTypeId }));
      }
    }))
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach((subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

}
