import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Category, Domain } from 'src/app/api/models';
import { CategoryDialogComponent } from '../../containers/category-dialog/category-dialog.component';
import { ConfirmationDialogComponent } from '../../containers/confirmation-dialog/confirmation-dialog.component';
import { AdminAppState, getCategories, getDomains } from '../../state';
import { createCategory, deleteCategory, loadCategories, updateCategory } from '../../state/category/category.actions';


@Component({
  selector: 'categories',
  templateUrl: './categories.component.html'
})
export class CategoriesComponent implements OnInit {

  public categories$: Observable<Array<Category>>;
  public domains$: Observable<Array<Domain>>;

  constructor(
    private store: Store<AdminAppState>,
    public dialog: MatDialog,
    public translateService: TranslateService,
  ) {
    this.store.dispatch(loadCategories({ includeDisabled: true }));
  }

  ngOnInit(): void {
    this.categories$ = this.store.pipe(select(getCategories));
    this.domains$ = this.store.pipe(select(getDomains));
  }

  addCategory() {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '500px',
      data: {
        edit: false,
        domains: this.domains$
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        const category: Category = {
          domain: response.domain,
          name: response.name,
          orderId: response.orderId,
          enabled: response.enabled
        }
        this.store.dispatch(createCategory({ category }));
      }
    });

  }

  editCategory(input: Category) {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '500px',
      data: {
        edit: true,
        category: JSON.parse(JSON.stringify(input)),
        domains: this.domains$
      },
    });
    dialogRef.afterClosed().subscribe((category) => {
      if (category) {
        if (category.domain === input.domain.name) {
          category.domain = input.domain;
        }
        this.store.dispatch(updateCategory({ updatedCategory: category }));
      }
    });
  }

  delete(event: MouseEvent, categoryId: string) {
    event.stopPropagation();
    event.preventDefault();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {
        question: this.translateService.instant('Esti sigur ca vrei sa stergi acesta categorie?'),
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteCategory({ categoryId }));
      }
    })
  }


}
