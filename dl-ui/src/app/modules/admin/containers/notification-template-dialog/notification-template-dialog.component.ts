import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';
import { Language } from '../../models/notification.model';


@Component({
  selector: 'notification-template-dialog',
  templateUrl: './notification-template-dialog.component.html',
  styleUrls: ['./notification-template-dialog.component.scss']
})
export class NotificationTemplateDialogComponent implements OnInit {

  public templateForm: FormGroup;
  public filtredLanguages: Observable<Array<Language>>;

  constructor(
    public dialogRef: MatDialogRef<NotificationTemplateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.templateForm = this.fb.group({
      enabled: this.fb.control(true),
      type: this.fb.control('', Validators.required),
      language: this.fb.control('', Validators.required),
    });
    this.filtredLanguages = this.templateForm.valueChanges.pipe(
      startWith(''),
      debounceTime(500),
      // distinctUntilChanged(),
      switchMap(form => this.data.languages.pipe(
        map((languages: Array<Language>) => {
          const type = this.templateForm.get('type').value;
          const text = form && form.language ? form.language : '';
          if (type) {
            // return languages.filter(lang => new RegExp(text, 'gi').test(lang.name) && (!this.data.selectedLanguages[type] || !this.data.selectedLanguages[type].includes(lang.code)));
            return languages;
          } else {
            // return languages.filter(lang => new RegExp(text, 'gi').test(lang.name));
            return languages;
          }
        })
      )),
      map((data: Array<Language>) => data)
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  typeChanged() {
    this.templateForm.get('language').reset();
  }

  displayFn(language?: Language): string | undefined {
    return language ? displayObject(language) : undefined;
  }
}

function displayObject(language: Language): string {
  let result = '';
  if (language.name) {
    result += language.name + ' ';
  }
  return result;
}