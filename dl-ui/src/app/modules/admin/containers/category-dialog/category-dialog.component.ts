import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';
import { Domain } from 'src/app/api/models';

@Component({
  selector: 'category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {

  public categoryForm: FormGroup;
  public filteredDomains: Observable<Array<Domain>>;
  public title: string;

  constructor(
    public dialogRef: MatDialogRef<CategoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.title = this.data.edit ? 'Editeaza categorie' : 'Creaza categorie';
    this.categoryForm = this.fb.group({
      enabled: this.fb.control(this.data.edit ? this.data.category.enabled : true),
      domain: this.fb.control(this.data.edit ? this.data.category.domain.name : '', Validators.required),
      name: this.fb.control(this.data.edit ? this.data.category.name : '', Validators.required),
      orderId: this.fb.control(this.data.edit ? this.data.category.orderId : '', Validators.required),
      id: this.data.edit ? this.data.category.id : null
    });
    this.filteredDomains = this.categoryForm.valueChanges.pipe(
      startWith(''),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(form => this.data.domains.pipe(
        map((domains: Array<Domain>) => {
          const text = form && form.domain ? form.domain : '';
          return domains.filter(domain => new RegExp(text, 'gi').test(domain.name));
        })
      )),
      map((data: Array<Domain>) => data)
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  displayFn(domain?: Domain): string | undefined {
    return domain ? displayObject(domain) : undefined;
  }
}

function displayObject(domain: Domain): string {
  let result = '';
  if (typeof domain === 'string') {
    return domain;
  }
  
  if (domain.name) {
    result += domain.name + ' ';
  }
  return result;
}
