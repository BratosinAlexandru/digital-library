import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Domain } from 'src/app/api/models';

@Component({
  selector: 'domains-dialog',
  templateUrl: './domains-dialog.component.html'
})
export class DomainsDialogComponent  {

  title: string = '';

  constructor(
    public dialogRef: MatDialogRef<DomainsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Domain,
  ) {
    if (this.data.name === '') {
      this.title = 'Adauga domeniu de interes';
    } else {
      this.title = 'Editare domeniu';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
