import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { merge, of, Subscription } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, startWith, switchMap } from 'rxjs/operators';
import { Account, PageAccount, PaginationRequestDtoFiltersRequest } from 'src/app/api/models';
import { AdminAppState, getAccountsPage } from '../../state';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { AccountDialogComponent } from '../account-dialog/account-dialog.component';
import { loadAccountsPage, deleteAccounts, createAccount, updateAccount } from '../../state/account/account.actions';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'accounts-table',
  templateUrl: './accounts-table.component.html',
  styleUrls: ['./accounts-table.component.scss']
})
export class AccountsTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public dataSource = [];
  public selectionForDelete = new Set<string>();
  public displayedColumns: string[] = ['selected', 'name', 'email', 'enabled', 'roles'];
  public isLoadingResults = true;
  public resultsLength = 0;

  public searchControl: FormControl = new FormControl('');

  public searchSubscription$: Subscription;
  public getAccountsPageSubscription$: Subscription;
  public sortSubscription$: Subscription;

  constructor(
    private ref: ChangeDetectorRef,
    private store: Store<AdminAppState>,
    public dialog: MatDialog,
    private translateService: TranslateService,
  ) { }


  ngOnInit(): void {
    this.initSearch();
  }

  private initSearch() {
    this.searchSubscription$ = this.searchControl.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
    ).subscribe(name => {
      this.isLoadingResults = true;
      const pageRequest: PaginationRequestDtoFiltersRequest = {
        pageNumber: 0,
        pageSize: this.paginator.pageSize,
        sortDirection: this.sort.direction,
        sortProperties: this.sort.active ? [this.sort.active] : ['name'],
        query: name
      };
      this.store.dispatch(loadAccountsPage({ pageRequest }));
    });
  }

  ngAfterViewInit(): void {
    this.sortSubscription$ = this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    this.getAccountsPageSubscription$ = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          const pageRequest: PaginationRequestDtoFiltersRequest = {
            pageNumber: this.paginator.pageIndex,
            pageSize: this.paginator.pageSize,
            sortDirection: this.sort.direction,
            sortProperties: this.sort.active ? [this.sort.active] : ['name'],
            query: this.searchControl.value
          };
          this.store.dispatch(loadAccountsPage({ pageRequest }));
          return this.store.pipe(select(getAccountsPage));
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return of([]);
        })
      ).subscribe((page: PageAccount) => {
        if (page) {
          this.dataSource = page.content;
          this.isLoadingResults = false;
          this.paginator.length = page.totalElements;
          this.ref.detectChanges();
        }
      });
  }

  select(event: MatCheckboxChange, row: Account): void {
    const checked = event.checked;
    if (checked) {
      this.selectionForDelete.add(row.id);
    } else {
      this.selectionForDelete.delete(row.id);
    }
  }

  editAccount(editAccount: Account) {
    const dialogRef = this.dialog.open(AccountDialogComponent, {
      width: '500px',
      data: {
        editMode: true,
        account: {
          name: editAccount.name || '',
          email: editAccount.email || '',
          roles: editAccount.roles || [],
          password: editAccount.password || '',
          enabled: editAccount.enabled,
          id: editAccount.id,
        } as Account
      }
    });
    dialogRef.afterClosed().subscribe((account) => {
      if (account) {
        account.id = editAccount.id;
        this.store.dispatch(updateAccount({ account }));
      }
    });
  }

  createAccount() {
    const dialogRef = this.dialog.open(AccountDialogComponent, {
      width: '500px',
      data: {
        editMode: false,
        account: {
          name: '',
          email: '',
          roles: [],
          password: '',
          enabled: true,
        } as Account
      }
    });
    dialogRef.afterClosed().subscribe((account) => {
      if (account) {
        this.store.dispatch(createAccount({ account }));
        this.reloadPage();
      }
    })
  }

  deleteAccounts() {
    const question = this.translateService.instant('Sunteti sigur ca doriti sa stergeti ' + (this.selectionForDelete.size > 1 ? 'acesti membri?' : 'acest membru?'));
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        question,
        yesButton: this.translateService.instant('Da'),
        noButton: this.translateService.instant('Nu')
      }
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.store.dispatch(deleteAccounts({ ids: [...this.selectionForDelete] }));
        this.reloadPage();
      }
    });
  }

  getToString(entity: Array<any>): string {
    return entity && entity.length > 0 ? entity.map(e => e.name).join(', ') : '';
  }

  reloadPage() {
    this.isLoadingResults = true;
    const query = this.searchControl.value;
    const pageRequest: PaginationRequestDtoFiltersRequest = {
      pageNumber: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
      sortDirection: this.sort.direction,
      sortProperties: this.sort.active ? [this.sort.active] : ['name'],
      query
    };
    this.store.dispatch(loadAccountsPage({ pageRequest }));
  }


  ngOnDestroy() {
    this.sortSubscription$.unsubscribe();
    this.getAccountsPageSubscription$.unsubscribe();
    this.searchSubscription$.unsubscribe();
  }
}
