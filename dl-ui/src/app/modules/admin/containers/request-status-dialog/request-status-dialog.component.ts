import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'request-status-dialog',
  templateUrl: './request-status-dialog.component.html',
  styleUrls: ['./request-status-dialog.component.scss']
})
export class RequestStatusDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RequestStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
