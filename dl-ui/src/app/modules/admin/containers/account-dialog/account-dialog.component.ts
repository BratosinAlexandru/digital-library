import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { merge, Observable, of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Account } from 'src/app/api/models';
import { AlertService } from 'src/app/core/services/alert.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'account-dialog',
  templateUrl: './account-dialog.component.html',
  styleUrls: ['./account-dialog.component.scss']
})
export class AccountDialogComponent implements OnInit, OnDestroy {

  public accountForm: FormGroup;

  private subscriptions$: Subscription[] = [];

  constructor(
    public dialogRef: MatDialogRef<AccountDialogComponent>,
    private accountService: AccountService,
    private alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    const passwordValidators = [Validators.minLength(5)];
    if (!this.data.account.id) {
      passwordValidators.push(Validators.required);
    }
    this.accountForm = new FormGroup(
      {
        name: new FormControl(this.data.account.name, [Validators.required]),
        email: new FormControl(
          { value: this.data.account.email, disabled: this.data.editMode },
          [
            Validators.required,
            Validators.email,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
          ],
          this.forbiddenEmail.bind(this)
        ),
        emailConfirm: new FormControl(this.data.account.email),
        roles: new FormGroup({
          admin: new FormControl(this.data.account.roles.includes('admin')),
          customer: new FormControl(this.data.account.roles.includes('customer')),
        }),
        password: new FormControl(this.data.account.password, passwordValidators),
        passwordConfirm: new FormControl(this.data.account.password),
        enabled: new FormControl(this.data.account.enabled),
      },
      this.groupValidation.bind(this)
    );
  }

  onClick(formValue?: Account): void {
    if (formValue) {
      const accountForm = JSON.parse(JSON.stringify(this.accountForm.getRawValue()));
      accountForm.roles = this.mapObjectToArray(accountForm, 'roles');
      this.dialogRef.close(accountForm);
    } else {
      this.dialogRef.close();
    }
  }

  groupValidation(control: FormGroup): { [s: string]: boolean } {
    const value = control.value;
    control.controls.passwordConfirm.setErrors(
      !value.password || value.password === value.passwordConfirm ? null : { incorrect: true }
    );
    return null;
  }

  forbiddenEmail(control: AbstractControl): Observable<any> {
    return this.accountService.isEmailUnique(control.value).pipe(
      map((unique: boolean) => {
        if (unique) {
          return null;
        } else {
          this.alertService.error('Email existent!');
          return { forbiddenEmail: true };
        }
      }),
      catchError((val) => {
        return of(val);
      })
    );
  }

  mapObjectToArray(accountForm: Account, arrayName: string): Array<string> {
    const data = [];
    Object.keys(accountForm[arrayName]).forEach((key, index) => {
      if (accountForm[arrayName][key]) {
        data.push(key);
      }
    });
    return data;
  }

  compareObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id === object2.id;
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach((subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
