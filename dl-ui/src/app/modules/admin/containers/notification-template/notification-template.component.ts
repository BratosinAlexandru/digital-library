import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'notification-template',
  templateUrl: './notification-template.component.html',
  styleUrls: ['./notification-template.component.scss']
})
export class NotificationTemplateComponent implements OnInit {

  @Input() templateForm: FormGroup;
  @Output() delete: EventEmitter<string> = new EventEmitter<string>();

  public editor = ClassicEditor;
  public type: string;

  constructor() { }

  ngOnInit(): void {
    this.type = this.templateForm.get('type').value;
  }

  deleteTemplate() {
    this.delete.emit(this.templateForm.value);
  }

}
