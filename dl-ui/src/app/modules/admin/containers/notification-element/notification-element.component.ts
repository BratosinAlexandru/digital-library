import { Component, Input } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'notification-element',
  templateUrl: './notification-element.component.html',
  styleUrls: ['./notification-element.component.scss']
})
export class NotificationElementComponent {

  @Input() notificationForm: FormGroup;

  addContact(controlName: string) {
    const formArray: FormArray = this.notificationForm.get(controlName) as FormArray;
    if (controlName === 'emails') {
      formArray.push(
        new FormControl(
          '',
          [
            Validators.required,
            Validators.email,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ]
        )
      );
    }
  }

  removeContact(index: number, controlName: string) {
    const emails: FormArray = this.notificationForm.get(controlName) as FormArray;
    emails.removeAt(index);
  }
}
