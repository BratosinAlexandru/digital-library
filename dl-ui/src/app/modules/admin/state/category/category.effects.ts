import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { Category } from "src/app/api/models";
import { CategoryService } from "../../services/category.service";
import { createCategory, createCategoryFailure, createCategorySuccess, deleteCategory, deleteCategoryFailure, deleteCategorySuccess, loadCategories, loadCategoriesFailure, loadCategoriesSuccess, loadCategory, loadCategoryFailure, loadCategorySuccess, updateCategory, updateCategoryFailure, updateCategorySuccess } from "./category.actions";

@Injectable()
export class CategoryEffects {

    loadCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadCategory),
            switchMap((action) =>
                this.categoryService.getCategory(action.categoryId).pipe(
                    map((category: Category) => loadCategorySuccess({ category })),
                    catchError((error) => of(loadCategoryFailure({ error })))
                )
            )
        )
    );

    loadCategories$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadCategories),
            switchMap((action) =>
                this.categoryService.getCategories(action.includeDisabled).pipe(
                    map((categories) => loadCategoriesSuccess({ categories })),
                    catchError((error) => of(loadCategoriesFailure({ error })))
                )
            )
        )
    );

    createCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createCategory),
            switchMap((action) =>
                this.categoryService.createCategory(action.category).pipe(
                    map((category) => createCategorySuccess({ category })),
                    catchError((error) => of(createCategoryFailure({ error })))
                )
            )
        )
    );

    updateCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateCategory),
            switchMap((action) =>
                this.categoryService.updateCategory(action.updatedCategory).pipe(
                    map((category) => updateCategorySuccess({ updatedCategory: action.updatedCategory })),
                    catchError((error) => of(updateCategoryFailure({ error })))
                )
            )
        )
    )

    deleteCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteCategory),
            switchMap((action) =>
                this.categoryService.deleteCategory(action.categoryId).pipe(
                    map(() => deleteCategorySuccess({ categoryId: action.categoryId })),
                    catchError((error) => of(deleteCategoryFailure({ error })))
                )
            )
        )
    )

    constructor(
        private actions$: Actions,
        private categoryService: CategoryService,
    ) { }
}