import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { Category } from "src/app/api/models";
import { createCategorySuccess, deleteCategorySuccess, loadCategoriesSuccess, loadCategorySuccess, updateCategorySuccess } from "./category.actions";

export const categoryFeatureKey = 'category';

export interface State {
    category: Category;
    categories: Array<Category>;
};

export const initialState: State = {
    category: null,
    categories: [],
};

export const reducer = createReducer(
    initialState,
    on(loadCategorySuccess,
        (state, { category }) => ({ ...state, category })
    ),
    on(loadCategoriesSuccess,
        (state, { categories }) => ({ category: state.category, categories })
    ),
    on(createCategorySuccess,
        (state, { category }) => ({ category: category, categories: [...state.categories, category] })
    ),
    on(updateCategorySuccess,
        (state, { updatedCategory }) => produce(state, draftState => {
            const categories: Array<Category> = JSON.parse(JSON.stringify(state.categories))
            let entity: Category = JSON.parse(JSON.stringify(state.category))
            let index = categories.findIndex((e: Category) => e.id === updatedCategory.id);

            if (index !== -1) {
                entity = updatedCategory;
                categories[index] = updatedCategory;
            }
            return { ...draftState, category: entity, categories: categories };
        })
    ),
    on(deleteCategorySuccess,
        (state, { categoryId }) => produce(state, draftState => {

            if (categoryId) {
                let categories: Array<Category> = JSON.parse(JSON.stringify(state.categories));
                categories = categories.filter(entity => entity.id !== categoryId);
                return { ...draftState, categories }
            } else {
                return { ...draftState }
            }
        })
    ),
);

export const getCategory = (state: State) => state.category;
export const getCategories = (state: State) => state.categories;