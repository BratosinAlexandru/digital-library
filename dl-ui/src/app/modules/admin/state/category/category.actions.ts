import { createAction, props } from "@ngrx/store";
import { Category } from "src/app/api/models";

/** Load category actions */
export const loadCategory = createAction('[Admin] Load Category', props<({ categoryId: string })>());
export const loadCategorySuccess = createAction('[Admin] Load Category Success', props<({ category: Category })>());
export const loadCategoryFailure = createAction('[Admin] Load Category Failure', props<({ error: any })>());

/** Load Categories actions */
export const loadCategories = createAction('[Admin] Load Categories', props<({ includeDisabled: boolean })>());
export const loadCategoriesSuccess = createAction('[Admin] Load Categories Success', props<({ categories: Array<Category> })>());
export const loadCategoriesFailure = createAction('[Admin] Load Categories Failure', props<({ error: any })>());

/** Create category actions */
export const createCategory = createAction('[Admin] Create Category', props<({ category: Category })>());
export const createCategorySuccess = createAction('[Admin] Create Category Success', props<({ category: Category })>());
export const createCategoryFailure = createAction('[Admin] Create Category Failure', props<({ error: any })>());

/** Update category actions */
export const updateCategory = createAction('[Admin] Update Category', props<({ updatedCategory: Category })>());
export const updateCategorySuccess = createAction('[Admin] Update Category Success', props<({ updatedCategory: Category })>());
export const updateCategoryFailure = createAction('[Admin] Update Category Failure', props<({ error: any })>());

/** Delete category actions */
export const deleteCategory = createAction('[Admin] Delete category', props<({ categoryId: string })>());
export const deleteCategorySuccess = createAction('[Admin] Delete category Success', props<({ categoryId: string })>());
export const deleteCategoryFailure = createAction('[Admin] Delete category Failure', props<({ error: any })>());