import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from "@ngx-translate/core";
import { EMPTY, of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { Category, Domain } from "src/app/api/models";
import { AlertService } from "src/app/core/services/alert.service";
import { CategoryService } from "../../services/category.service";
import { DomainService } from "../../services/domain.service";
import { createCategoryForDomain, createCategoryForDomainFailure, createDomain, createDomainFailure, createDomainSuccess, deleteCategory, deleteCategoryFailure, deleteCategorySuccess, deleteDomain, deleteDomainFailure, deleteDomainSuccess, loadCategoriesForDomain, loadCategoriesForDomainFailure, loadCategoriesForDomainSuccess, loadDomains, loadDomainsFailure, loadDomainsSuccess, updateDomain, updateDomainFailure, updateDomainSuccess } from "./domains.actions";

@Injectable()
export class DomainEffects {

    loadDomains$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadDomains),
            switchMap(() =>
                this.domainService.getDomains(true).pipe(
                    map((domains: Array<Domain>) => loadDomainsSuccess({
                        domains: domains ? this.sortEntities(domains) : []
                    })),
                    catchError((error) => of(loadDomainsFailure({ error })))
                )
            )
        )
    );

    createDomain$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createDomain),
            switchMap((action) =>
                this.domainService.createDomain(action.domain).pipe(
                    map((domain: Domain) => createDomainSuccess({ domain })),
                    catchError((error) => of(createDomainFailure({ error })))
                )
            )
        )
    );

    updateDomain$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateDomain),
            switchMap((action) =>
                this.domainService.updateDomain(action.domain).pipe(
                    map((domain) => updateDomainSuccess({ domain })),
                    catchError((error) => of(updateDomainFailure({ error })))
                )
            )
        )
    );

    loadCategoriesForDomain$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadCategoriesForDomain),
            switchMap((action) => {
                return this.categoryService.getCategoriesForDomain(action.domainIds, true).pipe(
                    map((categories: Array<Category>) => loadCategoriesForDomainSuccess({ categories, domainIds: action.domainIds })),
                    catchError((error) => of(loadCategoriesForDomainFailure({ error })))
                );
            })
        )
    );

    createCategoryForDomain$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createCategoryForDomain),
            switchMap((action) =>
                this.categoryService.createCategory(action.category).pipe(
                    map((category: Category) => createCategoryForDomain({ category })),
                    catchError((error) => of(createCategoryForDomainFailure({ error })))
                )
            )
        )
    );

    deleteCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteCategory),
            switchMap((action) => {
                return this.categoryService.deleteCategory(action.categoryId).pipe(
                    map(() => deleteCategorySuccess({ categoryId: action.categoryId })),
                    catchError((error) => of(deleteCategoryFailure({ error })))
                )
            })
        )
    );

    deleteCategoryFailureError$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteCategoryFailure),
            switchMap(({ error }) => {
                if (error.status === 400) {
                    this.alertService.error(this.translateService.instant('Categoria este inca folosita!'));
                }
                return EMPTY;
            })
        ),
        { dispatch: false }
    );

    deleteDomain$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteDomain),
            switchMap((action) => {
                return this.domainService.deleteDomain(action.domainId).pipe(
                    map(() => deleteDomainSuccess({ domainId: action.domainId })),
                    catchError((error) => of(deleteDomainFailure({ error })))
                )
            })
        )
    );

    deleteDomainFailureError$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteDomainFailure),
            switchMap(({ error }) => {
                if (error.status === 400) {
                    this.alertService.error(this.translateService.instant('Domeniul este inca folosit!'));
                }
                return EMPTY;
            })
        ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private domainService: DomainService,
        private categoryService: CategoryService,
        private alertService: AlertService,
        private translateService: TranslateService,
    ) { }

    private sortEntities(entities: Array<any>) {
        return entities.sort((e1, e2) => e1.orderId > e2.orderId ? 1 : -1);
    }
}