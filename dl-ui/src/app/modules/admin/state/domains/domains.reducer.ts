import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { Domain } from "src/app/api/models";
import { DomainsWithCategories } from "../../models/domains";
import { createCategoryForDomainSuccess, createDomainSuccess, deleteCategorySuccess, deleteDomainSuccess, loadCategoriesForDomainSuccess, loadDomainsSuccess, updateDomainSuccess } from "./domains.actions";

export const domainFeatureKey = 'domain';

export interface State {
    domains: Array<Domain>;
};

export const initialState: State = {
    domains: [],
};

const sortCategories = (categories) => {
    return categories ? categories.sort((c1, c2) => c1.orderId > c2.orderId ? 1 : -1) : [];
}

export const reducer = createReducer(
    initialState,
    on(loadDomainsSuccess,
        (state, { domains }) => ({ ...state, domains })
    ),
    on(createDomainSuccess,
        (state, { domain }) => ({ ...state, domains: [...state.domains, domain] }),
    ),
    on(updateDomainSuccess,
        (state, { domain }) => produce(state, draftState => {
            const domains: Array<Domain> = JSON.parse(JSON.stringify(state.domains));
            // for (let index = 0; index < domains.length; index++) {
            //     const element = state.domains[index];
            //     if (element.id === domain.id) {
            //         domains[index] = domain;
            //     }
            // }

            let index = domains.findIndex((e: Domain) => e.id === domain.id);
            if (index !== -1) {
                domains[index] = domain;
            }
            return { ...draftState, domains };
        })
    ),
    on(loadCategoriesForDomainSuccess,
        (state, { categories, domainIds }) => produce(state, draftState => {
            if (categories && categories.length > 0) {
                const domains: Array<DomainsWithCategories> = JSON.parse(JSON.stringify(state.domains));
                for (let index = 0; index < domains.length; index++) {
                    const element = state.domains[index];
                    if (domainIds.indexOf(element.id) !== -1) {
                        domains[index].categories = sortCategories(categories);
                    }
                }
                return { ...draftState, domains };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteCategorySuccess,
        (state, { categoryId }) => produce(state, draftState => {
            if (categoryId) {
                const entities: Array<DomainsWithCategories> = JSON.parse(JSON.stringify(state.domains));

                entities.forEach(entity => {
                    if (entity.categories) {
                        entity.categories = entity.categories.filter(category => category.id !== category);
                    }
                });
                return { ...draftState, entities };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteDomainSuccess,
        (state, { domainId }) => produce(state, draftState => {
            if (domainId) {
                let domains: Array<Domain> = JSON.parse(JSON.stringify(state.domains));
                domains = domains.filter(entity => entity.id !== domainId);
                return { ...draftState, domains };
            } else {
                return { ...draftState };
            }
        }),
    ),
    on(createCategoryForDomainSuccess,
        (state, { category }) => produce(state, draftState => {
            const entities = JSON.parse(JSON.stringify(state.domains));

            if (category && category.domain && category.domain.id) {
                for (let index = 0; index < state.domains.length; index++) {
                    const element = entities[index];
                    if (element.id === category.domain.id) {
                        if (!element.categories) {
                            element.categories = [category];
                        } else {
                            element.categories = [...element.categories, category];
                        }
                    }
                }
            }
            return { ...draftState, entities };
        })
    ),
);

export const getDomains = (state: State) => state.domains;