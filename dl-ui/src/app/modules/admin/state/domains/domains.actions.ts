import { createAction, props } from "@ngrx/store";
import { Category, Domain } from "src/app/api/models";

/** Load Domains actions */
export const loadDomains = createAction('[Admin] Load Domains');
export const loadDomainsSuccess = createAction('[Admin] Load Domains Success', props<({ domains: Array<Domain> })>());
export const loadDomainsFailure = createAction('[Admin] Load Domains Failure', props<({ error: any })>());

/** Load Categories for Domain actions */
export const loadCategoriesForDomain = createAction('[Admin] Load Categories for Domain', props<({ domainIds: Array<string> })>());
export const loadCategoriesForDomainSuccess = createAction('[Admin] Load Categories for Domain Success', props<({ categories: Array<Category>, domainIds: Array<string> })>());
export const loadCategoriesForDomainFailure = createAction('[Admin] Load Categories for Domain Failure', props<({ error: any })>());

/** Create Domains actions */
export const createDomain = createAction('[Admin] Create Domain', props<({ domain: Domain })>());
export const createDomainSuccess = createAction('[Admin] Create Domain Success', props<({ domain: Domain })>());
export const createDomainFailure = createAction('[Admin] Create Domain Failure', props<({ error: any })>());

/** Create Category for Domain actions */
export const createCategoryForDomain = createAction('[Admin] Create Category for Domain', props<({ category: Category })>());
export const createCategoryForDomainSuccess = createAction('[Admin] Create Category for Domain', props<({ category: Category })>());
export const createCategoryForDomainFailure = createAction('[Admin] Create Category for Domain', props<({ error: any })>());

/** Update Domains actions */
export const updateDomain = createAction('[Admin] Update Domain', props<({ domain: Domain })>());
export const updateDomainSuccess = createAction('[Admin] Update Domain Success', props<({ domain: Domain })>());
export const updateDomainFailure = createAction('[Admin] Update Domain Failure', props<({ error: any })>());


/** Delete Category */
export const deleteCategory = createAction('[Admin] Delete Category', props<({ categoryId: string })>());
export const deleteCategorySuccess = createAction('[Admin] Delete Category', props<({ categoryId: string })>());
export const deleteCategoryFailure = createAction('[Admin] Delete Category', props<({ error: any })>());

/** Delete Domain actions */
export const deleteDomain = createAction('[Admin] Delete Domain', props<({ domainId: string })>());
export const deleteDomainSuccess = createAction('[Admin] Delete Domain Success', props<({ domainId: string })>());
export const deleteDomainFailure = createAction('[Admin] Delete Domain Failure', props<({ error: any })>());