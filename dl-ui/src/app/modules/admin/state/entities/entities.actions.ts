import { createAction, props } from "@ngrx/store";
import { CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";

/** Communication Channels */

/** Load communication channel actions */
export const loadCommunicationChannel = createAction('[Admin] Load Communication Channel');
export const loadCommunicationChannelSuccess = createAction('[Admin] Load Communication Channel Success', props<({ communicationChannels: Array<CommunicationChannel> })>());
export const loadCommunicationChannelFailure = createAction('[Admin] Load Communication Channel Failure', props<({ error: any })>());

/** Create communication channel actions */
export const createCommunicationChannel = createAction('[Admin] Create Communication Channel', props<({ communicationChannel: CommunicationChannel })>());
export const createCommunicationChannelSuccess = createAction('[Admin] Create Communication Channel Success', props<({ communicationChannel: CommunicationChannel })>());
export const createCommunicationChannelFailure = createAction('[Admin] Create Communication Channel Failure', props<({ error: any })>());

/** Update communicaton channel actions */
export const updateCommunicationChannel = createAction('[Admin] Update Communication Channel', props<({ communicationChannel: CommunicationChannel })>());
export const updateCommunicationChannelSuccess = createAction('[Admin] Update Communication Channel Success', props<({ communicationChannel: CommunicationChannel })>());
export const updateCommunicationChannelFailure = createAction('[Admin] Update Communication Channel Failure', props<({ error: any })>());

/** Delete communication channel actions */
export const deleteCommunicationChannel = createAction('[Admin] Delete Communication Channel', props<({ communicationChannelId: string })>());
export const deleteCommunicationChannelSuccess = createAction('[Admin] Delete Communication Channel Success', props<({ communicationChannelId: string  })>());
export const deleteCommunicationChannelFailure = createAction('[Admin] Delete Communication Channel Failure', props<({ error: any })>());



/** Request Types */

/** Load request type actions */
export const loadRequestTypes = createAction('[Admin] Load Request Types');
export const loadRequestTypesSuccess = createAction('[Admin] Load Request Types Success', props<({ requestTypes: Array<RequestType> })>());
export const loadRequestTypesFailure = createAction('[Admin] Load Request Types Failure', props<({ error: any })>());

/** Create request type action */
export const createRequestType = createAction('[Admin] Create Request Type', props<({ requestType: RequestType })>());
export const createRequestTypeSuccess = createAction('[Admin] Create Request Type Success', props<({ requestType: RequestType })>());
export const createRequestTypeFailure = createAction('[Admin] Create Request Type Failure', props<({ error: any })>());

/** Update request type actions */
export const updateRequestType = createAction('[Admin] Update Request Type', props<({ requestType: RequestType })>());
export const updateRequestTypeSuccess = createAction('[Admin] Update Request Type Success', props<({ requestType: RequestType })>());
export const updateRequestTypeFailure = createAction('[Admin] Update Request Type Failure', props<({ error: any })>());

/** Delete request type actions */
export const deleteRequestType = createAction('[Admin] Delete Request Type', props<({ requestTypeId: string })>());
export const deleteRequestTypeSuccess = createAction('[Admin] Delete Request Type Success', props<({ requestTypeId: string })>());
export const deleteRequestTypeFailure = createAction('[Admin] Delete Request Type Failure', props<({ error })>());