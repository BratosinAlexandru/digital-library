import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";
import { createCommunicationChannelSuccess, createRequestTypeSuccess, deleteCommunicationChannelSuccess, deleteRequestTypeSuccess, loadCommunicationChannelSuccess, loadRequestTypesSuccess, updateCommunicationChannelSuccess, updateRequestTypeSuccess } from "./entities.actions";

export const entitiesFeatureKey = 'entities';

export interface State {
    communicationChannels: Array<CommunicationChannel>;
    requestTypes: Array<RequestType>;
    // requstStatus: Array<RequestStatus>;
}

export const initialState: State = {
    communicationChannels: [],
    requestTypes: [],
    // requstStatus: []
}

export const reducer = createReducer(
    initialState,
    on(loadCommunicationChannelSuccess,
        (state, { communicationChannels }) => produce(state, draftState => {
            return { ...draftState, communicationChannels };
        })
    ),
    on(createCommunicationChannelSuccess,
        (state, { communicationChannel }) => produce(state, draftState => {
            const communicationChannels: Array<CommunicationChannel> = JSON.parse(JSON.stringify(state.communicationChannels));
            communicationChannels.push(communicationChannel);
            return { ...draftState, communicationChannels };
        })
    ),
    on(updateCommunicationChannelSuccess,
        (state, { communicationChannel }) => produce(state, draftState => {
            if (communicationChannel) {
                const communicationChannels: Array<CommunicationChannel> = JSON.parse(JSON.stringify(state.communicationChannels));
                const index = communicationChannels.findIndex(e => e.id === communicationChannel.id);

                if (index !== -1) {
                    communicationChannels[index] = communicationChannel;
                    return { ...draftState, communicationChannels };
                } else {
                    return { ...draftState };
                }
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteCommunicationChannelSuccess,
        (state, { communicationChannelId }) => produce(state, draftState => {
            if (communicationChannelId) {
                let communicationChannels: Array<CommunicationChannel> = JSON.parse(JSON.stringify(state.communicationChannels));
                communicationChannels = communicationChannels.filter(e => e.id !== communicationChannelId);
                return { ...draftState, communicationChannels };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(loadRequestTypesSuccess,
        (state, { requestTypes }) => produce(state, draftState => {
            return { ...state, requestTypes };
        })
    ),
    on(createRequestTypeSuccess,
        (state, { requestType }) => produce(state, draftState => {
            const requestTypes: Array<RequestType> = JSON.parse(JSON.stringify(state.requestTypes));
            if (requestType.defaultValue) {
                const findRequestTypeIndex = requestTypes.findIndex(request => request.defaultValue === true);
                if (findRequestTypeIndex !== -1) {
                    requestTypes[findRequestTypeIndex].defaultValue = false;
                }
            }
            requestTypes.push(requestType);
            return { ...draftState, requestTypes };
        })
    ),
    on(updateRequestTypeSuccess,
        (state, { requestType }) => produce(state, draftState => {
            if (requestType) {
                const requestTypes: Array<RequestType> = JSON.parse(JSON.stringify(state.requestTypes));
                const index = requestTypes.findIndex(e => e.id === requestType.id);

                if (index !== -1) {
                    requestTypes[index] = requestType;
                    return { ...draftState, requestTypes };
                } else {
                    return { ...draftState };
                }
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteRequestTypeSuccess,
        (state, { requestTypeId }) => produce(state, draftState => {
            if (requestTypeId) {
                let requestTypes: Array<RequestType> = JSON.parse(JSON.stringify(state.requestTypes));
                requestTypes = requestTypes.filter(e => e.id !== requestTypeId);
                return { ...draftState, requestTypes };
            } else {
                return { ...draftState }
            }
        })
    ),
    // on(loadRequestStatusesSuccess,
    //     (state, { requestStatuses }) => produce(state, draftState => {
    //         return { ...draftState, requestStatuses };
    //     })
    // ),
    // on(createRequestStatusSuccess,
    //     (state, { requestStatus }) => produce(state, draftState => {
    //         const requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.requstStatus));
    //         requestStatuses.push(requestStatus);
    //         return { ...draftState, requestStatuses };
    //     })
    // ),
    // on(updateRequestStatusSuccess,
    //     (state, { updatedRequestStatus }) => produce(state, draftState => {
    //         if (updatedRequestStatus) {
    //             console.log('updatedRequest: ' + updateRequestStatus);
    //             const requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.requstStatus));
    //             console.log('statuses: ' + requestStatuses);
    //             const index = requestStatuses.findIndex(e => e.id === updatedRequestStatus.id);
    //             console.log('index ', index);
    //             if (index !== -1) {
    //                 requestStatuses[index] = updatedRequestStatus;
    //                 return { ...draftState, requestStatuses };
    //             } else {
    //                 return { ...draftState };
    //             }
    //         } else {
    //             return { ...draftState };
    //         }
    //     })
    // ),
    // on(deleteRequestStatusSuccess,
    //     (state, {requestStatusId}) => produce(state, draftState => {
    //         let requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.requstStatus));
    //         requestStatuses = requestStatuses.filter(e => e.id !== requestStatusId);
    //         return {...draftState, requestStatuses};
    //     })
    // ),
);

export const getCommunicationChannels = (state: State) => state.communicationChannels;
export const getRequestTypes = (state: State) => state.requestTypes;
// export const getRequestStatuses = (state: State) => state.requstStatus;