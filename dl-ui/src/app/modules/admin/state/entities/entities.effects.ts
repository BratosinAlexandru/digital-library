import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { TranslateService } from "@ngx-translate/core";
import { EMPTY, of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { CommunicationChannel, RequestStatus, RequestType } from "src/app/api/models";
import { AlertService } from "src/app/core/services/alert.service";
import { EntitiesService } from "../../services/entities.service";
import { createCommunicationChannel, createCommunicationChannelFailure, createCommunicationChannelSuccess, createRequestType, createRequestTypeFailure, createRequestTypeSuccess, deleteCommunicationChannel, deleteCommunicationChannelFailure, deleteCommunicationChannelSuccess, deleteRequestType, deleteRequestTypeFailure, deleteRequestTypeSuccess, loadCommunicationChannel, loadCommunicationChannelFailure, loadCommunicationChannelSuccess, loadRequestTypes, loadRequestTypesFailure, loadRequestTypesSuccess, updateCommunicationChannel, updateCommunicationChannelFailure, updateCommunicationChannelSuccess, updateRequestType, updateRequestTypeFailure, updateRequestTypeSuccess } from "./entities.actions";

@Injectable()
export class EntitiesEffects {

    /** Communication channels */

    loadCommunicationChannels$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadCommunicationChannel),
            switchMap((action) =>
                this.entitiesService.loadCommunicationChannels(true).pipe(
                    map((communicationChannels: Array<CommunicationChannel>) => loadCommunicationChannelSuccess({ communicationChannels })),
                    catchError((error) => of(loadCommunicationChannelFailure({ error })))
                )
            )
        )
    );

    createCommunicationChannel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createCommunicationChannel),
            switchMap((action) =>
                this.entitiesService.createCommunicationChannel(action.communicationChannel).pipe(
                    map((communicationChannel: CommunicationChannel) => createCommunicationChannelSuccess({ communicationChannel })),
                    catchError((error) => of(createCommunicationChannelFailure({ error })))
                )
            )
        )
    );

    updateCommunicationChannel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateCommunicationChannel),
            switchMap((action) =>
                this.entitiesService.updateCommunicationChannel(action.communicationChannel).pipe(
                    map(() => updateCommunicationChannelSuccess({ communicationChannel: action.communicationChannel })),
                    catchError((error) => of(updateCommunicationChannelFailure({ error })))
                )
            )
        )
    );

    deleteCommunicationChannel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteCommunicationChannel),
            switchMap((action) =>
                this.entitiesService.deleteCommunicationChannel(action.communicationChannelId).pipe(
                    map(() => deleteCommunicationChannelSuccess({ communicationChannelId: action.communicationChannelId })),
                    catchError((error) => of(deleteCommunicationChannelFailure({ error })))
                )
            )
        )
    );

    deleteCommunicationChannelFailureError$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteCommunicationChannelFailure),
            switchMap(({ error }) => {
                if (error.status === 400) {
                    this.alertService.error('Canalul de comunicatii este utilizat intr-o solicitare');
                }
                return EMPTY;
            })
        ), { dispatch: false }
    );


    /** Request types */

    loadRequestTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRequestTypes),
            switchMap((action) =>
                this.entitiesService.loadRequestTypes(true).pipe(
                    map((requestTypes: Array<RequestType>) => loadRequestTypesSuccess({ requestTypes })),
                    catchError((error) => of(loadRequestTypesFailure({ error })))
                )
            )
        )
    );

    createRequestTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createRequestType),
            switchMap((action) =>
                this.entitiesService.createRequestType(action.requestType).pipe(
                    map((requestType: RequestType) => createRequestTypeSuccess({ requestType })),
                    catchError((error) => of(createRequestTypeFailure({ error })))
                )
            )
        )
    );

    updateRequestType$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateRequestType),
            switchMap((action) =>
                this.entitiesService.updateRequestType(action.requestType).pipe(
                    map(() => updateRequestTypeSuccess({ requestType: action.requestType })),
                    catchError((error) => of(updateRequestTypeFailure({ error })))
                )
            )
        )
    );

    deleteRequestType$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteRequestType),
            switchMap((action) =>
                this.entitiesService.deleteRequestType(action.requestTypeId).pipe(
                    map(() => deleteRequestTypeSuccess({ requestTypeId: action.requestTypeId })),
                    catchError((error) => of(deleteRequestTypeFailure({ error })))
                )
            )
        )
    )

    deleteRequestTypeFailureError$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteRequestTypeFailure),
            switchMap(({ error }) => {
                if (error.status === 400) {
                    this.alertService.error(this.translateService.instant('Tipul de solicitare este utilizata intr-o solicitare'));
                }
                return EMPTY;
            })
        ), { dispatch: false }
    );

    /** Request statuses */

    // loadRequestStatuses$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(loadRequestStatuses),
    //         switchMap((action) =>
    //             this.entitiesService.loadRequestStatuses(true).pipe(
    //                 map((requestStatuses) => loadRequestStatusesSuccess({ requestStatuses })),
    //                 catchError((error) => of(loadRequestStatusesFailure({ error })))
    //             )
    //         )
    //     )
    // );

    // createRequestStatus$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(createRequestStatus),
    //         switchMap((action) =>
    //             this.entitiesService.createRequestStatus(action.requestStatus).pipe(
    //                 map((requestStatus: RequestStatus) => createRequestStatus({ requestStatus })),
    //                 catchError((error) => of(createRequestStatusFailure({ error })))
    //             )
    //         )
    //     )
    // );

    // updateRequestStatus$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(updateRequestStatus),
    //         switchMap((action) =>
    //             this.entitiesService.updateRequestStatus(action.updatedRequestStatus).pipe(
    //                 map(() => updateRequestStatusSuccess({ updatedRequestStatus: action.updatedRequestStatus })),
    //                 catchError((error) => of(updateRequestStatusFailure({ error })))
    //             )
    //         )
    //     )
    // );

    // deleteRequestStatus$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(deleteRequestStatus),
    //         switchMap((action) =>
    //             this.entitiesService.deleteReqeustStatus(action.requestStatusId).pipe(
    //                 map(() => deleteRequestStatusSuccess({ requestStatusId: action.requestStatusId })),
    //                 catchError((error) => of(deleteRequestStatusFailure({ error })))
    //             )
    //         )
    //     )
    // );

    // deleteRequestStatusFailure$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(deleteRequestStatusFailure),
    //         switchMap(({ error }) => {
    //             if (error.status === 400) {
    //                 this.alertService.error(this.translateService.instant('Statusul solicitarii este utilizata'));
    //             }
    //             return EMPTY;
    //         })
    //     ), { dispatch: false }
    // );


    constructor(
        private actions$: Actions,
        private entitiesService: EntitiesService,
        private translateService: TranslateService,
        private alertService: AlertService
    ) { }
}