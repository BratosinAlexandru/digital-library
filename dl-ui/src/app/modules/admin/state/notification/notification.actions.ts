import { createAction, props } from "@ngrx/store";
import { Notification, NotificationTemplate } from 'src/app/api/models';
import { NotificationWithTemplates, Language } from '../../models/notification.model';

/** Load notification actions */
export const loadNotification = createAction('[Admin] Load Notifications', props<({ includeDisabled?: boolean })>());
export const loadNotificationSuccess = createAction('[Admin] Load Notifications Success', props<({ notifications: Array<Notification> })>());
export const loadNotificationFailure = createAction('[Admin] Load Notifications Failure', props<({ error: any })>());

/** Load templates actions  */
export const loadTemplatesForNotification = createAction('[Admin] Load Templates for Notification', props<({ notification: Notification })>());
export const loadTemplatesForNotificationSuccess = createAction('[Admin] Load Templates for Notification Success', props<({ notification: Notification, templates: { [key: string]: Array<NotificationTemplate> } })>());
export const loadTemplatesForNotificationFailure = createAction('[Admin] Load Templates for Notification Failure', props<({ error: any })>());

/** Load template types  */
export const loadTemplateTypes = createAction('[Admin] Load Template Types');
export const loadTemplateTypesSuccess = createAction('[Admin] Load Template Types Success', props<({ templateTypes: Array<string> })>());
export const loadTemplateTypesFailure = createAction('[Admin] Load Template Types Failure', props<({ error: any })>());

/** Load template languages */
export const loadTemplateLanguages = createAction('[Admin] Load Template Languages');
export const loadTemplateLanguagesSuccess = createAction('[Admin] Load Template Languages Success', props<({ templateLanguages: Array<Language> })>());
export const loadTemplateLanguagesFailure = createAction('[Admin] Load Template Languages', props<({ error: any })>());

/** Update notification actions */
export const updateNotification = createAction('[Admin] Update Notification', props<({ notification: Notification })>());
export const updateNotificationSuccess = createAction('[Admin] Update Notification Success', props<({ notification: Notification })>());
export const updateNotificationFailure = createAction('[Admin] Update Notification Failure', props<({ error: any })>());

/** Update templates actions  */
export const updateTemplate = createAction('[Admin] Update Template', props<({ template: NotificationTemplate })>());
export const updateTemplateSuccess = createAction('[Admin] Update Template Success', props<({ template: NotificationTemplate })>());
export const updateTemplateFailure = createAction('[Admin] Update Template Failure', props<({ error: any })>());

/** Create template actions */
export const createTemplate = createAction('[Admin] Create Template', props<({ template: NotificationTemplate })>());
export const createTemplateSuccess = createAction('[Admin] Create Template Success', props<({ template: NotificationTemplate })>());
export const createTemplateFailure = createAction('[Admin] Create Template Failure', props<({ error: any })>());

/** Delete template */
export const deleteTemplate = createAction('[Admin] Delete Template', props<({ template: NotificationTemplate })>());
export const deleteTemplateSuccess = createAction('[Admin] Delete Template Success', props<({ template: NotificationTemplate })>());
export const deleteTemplateFailure = createAction('[Admin] Delete Template Failure', props<({ error: any })>());

/** Change selected notification actions */
export const changeSelectedNotification = createAction('[Admin] Change selecte Notification', props<({ selectedNotification: NotificationWithTemplates })>());