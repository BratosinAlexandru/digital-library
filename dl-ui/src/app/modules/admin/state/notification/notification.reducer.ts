import { createReducer, on } from '@ngrx/store';
import produce from 'immer';
import { Language, NotificationWithTemplates } from '../../models/notification.model';
import { changeSelectedNotification, createTemplateSuccess, deleteTemplateSuccess, loadNotificationSuccess, loadTemplateLanguagesSuccess, loadTemplatesForNotificationSuccess, loadTemplateTypesSuccess, updateNotificationSuccess, updateTemplateSuccess } from './notification.actions';
export const notificationFeatureKey = 'notification';

export interface State {
    notifications: Array<NotificationWithTemplates>;
    selectedNotification: NotificationWithTemplates;
    templateTypes: Array<string>;
    templateLanguages: Array<Language>
}

export const initialState: State = {
    notifications: null,
    selectedNotification: null,
    templateTypes: null,
    templateLanguages: null,
}

export const reducer = createReducer(
    initialState,
    on(loadNotificationSuccess,
        (state, { notifications }) => produce(state, draftState => {
            const notificationWithTemplates: Array<NotificationWithTemplates> = notifications.map(notification => {
                return { notification };
            });
            let selectedNotification: NotificationWithTemplates = JSON.parse(JSON.stringify(state.selectedNotification));
            if (!selectedNotification && notificationWithTemplates && notificationWithTemplates.length > 0) {
                selectedNotification = notificationWithTemplates[0];
                return { ...draftState, notifications: notificationWithTemplates, selectedNotification };
            }
            return { ...draftState, notifications: notificationWithTemplates };
        })
    ),
    on(loadTemplatesForNotificationSuccess,
        (state, { notification, templates }) => produce(state, draftState => {
            if (notification) {
                const notifications: Array<NotificationWithTemplates> = JSON.parse(JSON.stringify(state.notifications));
                for (let index = 0; index < notifications.length; index++) {
                    const element = notifications[index];
                    if (element.notification.id === notification.id) {
                        notifications[index].templates = templates;
                        const selectedNotification = notifications[index];
                        return { ...draftState, notifications, selectedNotification };
                    }
                }
                return { ...draftState, notifications };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(loadTemplateTypesSuccess,
        (state, { templateTypes }) => produce(state, draftState => {
            return { ...draftState, templateTypes };
        })
    ),
    on(loadTemplateLanguagesSuccess,
        (state, { templateLanguages }) => produce(state, draftState => {
            return { ...draftState, templateLanguages };
        })
    ),
    on(createTemplateSuccess,
        (state, { template }) => produce(state, draftState => {
            if (template) {
                const notifications: Array<NotificationWithTemplates> = JSON.parse(JSON.stringify(state.notifications));
                let selectedNotification: NotificationWithTemplates;
                for (let index = 0; index < notifications.length; index++) {
                    const element = notifications[index];
                    if (element.notification.id === template.notification.id) {
                        const templates = notifications[index].templates;
                        if (templates[template.type]) {
                            templates[template.type].push(template);
                        } else {
                            templates[template.type] = [template]
                        }
                        selectedNotification = element;
                    }
                }
                return { ...draftState, notifications, selectedNotification };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(updateNotificationSuccess,
        (state, { notification }) => produce(state, draftState => {
            if (notification) {
                const notifications: Array<NotificationWithTemplates> = JSON.parse(JSON.stringify(state.notifications));
                for (let index = 0; index < notifications.length; index++) {
                    const element = notifications[index];
                    if (element.notification.id === notification.id) {
                        element.notification = notification;
                        return { ...draftState, notifications };
                    }
                }
                return { ...draftState, notifications };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(updateTemplateSuccess,
        (state, { template }) => produce(state, draftState => {
            if (template) {
                const notifications: Array<NotificationWithTemplates> = JSON.parse(JSON.stringify(state.notifications));
                for (let index = 0; index < notifications.length; index++) {
                    const element = notifications[index];
                    if (element.notification.id === template.notification.id) {
                        const templates = notifications[index].templates;

                        for (let i = 0; i < templates[template.type].length; i++) {
                            const elem = templates[template.type][i];
                            if (elem.id === template.id) {
                                templates[template.type][i] = template;
                            } else if (template.default) {
                                templates[template.type][i].default = false;
                            }
                        }
                    }
                }
                return { ...draftState, notifications };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteTemplateSuccess,
        (state, { template }) => produce(state, draftState => {
            if (template) {
                const notifications: Array<NotificationWithTemplates> = JSON.parse(JSON.stringify(state.notifications));
                let selectedNotification: NotificationWithTemplates;
                for (let index = 0; index < notifications.length; index++) {
                    const element = notifications[index];
                    if (element.notification.id === template.notification.id) {
                        const templates = notifications[index].templates;
                        Object.keys(templates).forEach(key => {
                            for (let i = 0; i < templates[key].length; i++) {
                                const elem = templates[key][i];
                                if (elem.id === template.id) {
                                    templates[key].splice(i, 1);
                                }
                            }
                        });
                        selectedNotification = notifications[index];
                    }
                }
                return { ...draftState, notifications, selectedNotification };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(changeSelectedNotification,
        (state, { selectedNotification }) => produce(state, draftState => {
            return { ...draftState, selectedNotification };
        })
    ),
);

export const getNotifications = (state: State) => state.notifications;
export const getSelectedNotification = (state: State) => state.selectedNotification;
export const getTemplateTypes = (state: State) => state.templateTypes;
export const getTemplateLanguages = (state: State) => state.templateLanguages;