import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { TranslateService } from "@ngx-translate/core";
import { catchError, map, switchMap } from "rxjs/operators";
import { AlertService } from "src/app/core/services/alert.service";
import { NotificationTemplateService } from "../../services/notification-template.service";
import { Notification, NotificationTemplate } from 'src/app/api/models';
import { Language } from '../../models/notification.model';
import { createTemplate, createTemplateFailure, createTemplateSuccess, loadNotification, loadNotificationFailure, loadNotificationSuccess, updateNotification, updateNotificationFailure, updateNotificationSuccess, updateTemplate, updateTemplateFailure, updateTemplateSuccess, loadTemplatesForNotification, loadTemplatesForNotificationFailure, loadTemplatesForNotificationSuccess, deleteTemplateFailure, deleteTemplateSuccess, deleteTemplate, loadTemplateTypesFailure, loadTemplateTypesSuccess, loadTemplateTypes, loadTemplateLanguagesFailure, loadTemplateLanguagesSuccess, loadTemplateLanguages } from './notification.actions';
import { of } from "rxjs";


@Injectable()
export class NotificationEffects {

    loadNotifications$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadNotification),
            switchMap((action) =>
                this.notification.getNotifications(action.includeDisabled).pipe(
                    map((notifications: Array<Notification>) => loadNotificationSuccess({ notifications })),
                    catchError((error) => of(loadNotificationFailure({ error })))
                )
            )
        )
    );

    updateNotification$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateNotification),
            switchMap((action) =>
                this.notification.updateNotification(action.notification).pipe(
                    map(() => {
                        this.alertService.success(this.translateService.instant('Notificarea a fost updatata cu succes!'));
                        return updateNotificationSuccess({ notification: action.notification });
                    }),
                    catchError((error) => of(updateTemplateFailure({ error })))
                )
            )
        )
    );

    loadTemplatesForNotification$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadTemplatesForNotification),
            switchMap((action) =>
                this.notification.getTemplatesByNotificationId(action.notification.id).pipe(
                    map((templates: { [key: string]: Array<NotificationTemplate> }) => loadTemplatesForNotificationSuccess({ templates, notification: action.notification })),
                    catchError((error) => of(loadTemplatesForNotificationFailure({ error })))
                )
            )
        )
    );

    createTemplate$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createTemplate),
            switchMap((action) =>
                this.notification.createTemplate(action.template).pipe(
                    map((template: NotificationTemplate) => createTemplateSuccess({ template })),
                    catchError((error) => of(createTemplateFailure({ error })))
                )
            )
        )
    );

    updateTemplate$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateTemplate),
            switchMap((action) =>
                this.notification.updateTemplate(action.template).pipe(
                    map(() => updateTemplateSuccess({ template: action.template })),
                    catchError((error) => of(updateTemplateFailure({ error })))
                )
            )
        )
    );

    deleteTemplate$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteTemplate),
            switchMap((action) =>
                this.notification.deleteTemplate(action.template.id).pipe(
                    map(() => deleteTemplateSuccess({ template: action.template })),
                    catchError((error) => of(deleteTemplateFailure({ error })))
                )
            )
        )
    );

    loadTemplateTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadTemplateTypes),
            switchMap((action) =>
                this.notification.getTemplateTypes().pipe(
                    map((templateTypes: Array<string>) => loadTemplateTypesSuccess({ templateTypes })),
                    catchError((error) => of(loadTemplateTypesFailure({ error })))
                )
            )
        )
    );

    loadTemplateLanguages$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadTemplateLanguages),
            switchMap((action) =>
                this.notification.getTemplateLanguages().pipe(
                    map((templateLanguages: Array<Language>) => loadTemplateLanguagesSuccess({ templateLanguages })),
                    catchError((error) => of(loadTemplateLanguagesFailure({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private notification: NotificationTemplateService,
        private alertService: AlertService,
        private translateService: TranslateService
    ) { }

}