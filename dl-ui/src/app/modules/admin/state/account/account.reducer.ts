import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { PageAccount } from "src/app/api/models";
import { createAccountSuccess, deleteAccountsSuccess, loadAccountsPageSuccess, updateAccountSuccess } from "./account.actions";
export const accountFeatureKey = 'account';

export interface State {
    page: PageAccount;
}

export const initialState: State = {
    page: null,
}

export const reducer = createReducer(
    initialState,
    on(loadAccountsPageSuccess,
        (state, { page }) => ({ ...state, page })
    ),
    on(createAccountSuccess,
        (state, { account }) => produce(state, draftState => {
            if (account) {
                const page: PageAccount = JSON.parse(JSON.stringify(state.page));
                page.content = [account, ...page.content];
                return { ...draftState, page };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(updateAccountSuccess,
        (state, { account }) => produce(state, draftState => {
            if (account) {
                const page: PageAccount = JSON.parse(JSON.stringify(state.page));
                const array = page.content;

                for (let index = 0; index < array.length; index++) {
                    const element = array[index];
                    if (element.id == account.id) {
                        array[index] = account;
                        return { ...draftState, page };
                    }
                }
                return { ...draftState, page };
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteAccountsSuccess,
        (state, { ids }) => produce(state, draftState => {
            if (ids && ids.length > 0) {
                const page: PageAccount = JSON.parse(JSON.stringify(state.page));
                ids.forEach(id => {
                    const hasId = (element: Account) => {
                        console.log(element.id);
                        console.log(id);
                        return element.id === id
                    };
                    page.content.splice(page.content.findIndex(hasId), 1);
                })
                return { ...draftState, page };
            } else {
                return { ...draftState };
            }
        })
    )

);

export const getAccountsPage = (state: State) => state.page;