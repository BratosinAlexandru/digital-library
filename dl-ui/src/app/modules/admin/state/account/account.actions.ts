import { createAction, props } from "@ngrx/store";
import { Account, PageAccount, PaginationRequestDtoFiltersRequest } from "src/app/api/models";

/** Load accounts actions */
export const loadAccountsPage = createAction('[Admin] Load Accounts Page', props<{ pageRequest: PaginationRequestDtoFiltersRequest }>());
export const loadAccountsPageSuccess = createAction('[Admin] Load Accounts Page Success', props<{ page: PageAccount }>());
export const loadAccountsPageFailure = createAction('[Admin] Load Accounts Page Failure', props<{ error: any }>());

/** Create acount actions */
export const createAccount = createAction('[Admin] Create Account', props<{ account: Account }>());
export const createAccountSuccess = createAction('[Admin] Create Account Success', props<{ account: Account }>());
export const createAccountFailure = createAction('[Admin] Create Account Failure', props<{ error: any }>());

/** Update account actions */
export const updateAccount = createAction('[Admin] Update Account', props<{ account: Account }>());
export const updateAccountSuccess = createAction('[Admin] Update Account Success', props<{ account: Account }>());
export const updateAccountFailure = createAction('[Admin] Update Account Failure', props<{ error: any }>());

/** Delete account actions */
export const deleteAccounts = createAction('[Admin] Delete Accounts', props<{ ids: Array<string> }>());
export const deleteAccountsSuccess = createAction('[Admin] Delete Accounts Success', props<{ ids: Array<string> }>());
export const deleteAccountsFailure = createAction('[Admin] Delete Accounts Failure', props<{ error: any }>());