import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
    loadAccountsPage,
    loadAccountsPageFailure,
    loadAccountsPageSuccess,
    deleteAccounts,
    deleteAccountsSuccess,
    deleteAccountsFailure,
    createAccountSuccess,
    createAccountFailure,
    createAccount,
    updateAccount,
    updateAccountSuccess,
    updateAccountFailure
} from './account.actions';
import { PageAccount } from 'src/app/api/models/page-account';
import { AccountService } from '../../services/account.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Injectable()
export class AccountEffects {

    loadAccountsPage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadAccountsPage),
            switchMap((action) =>
                this.accountService.getPagedAccounts(action.pageRequest).pipe(
                    map((page: PageAccount) => loadAccountsPageSuccess({ page })),
                    catchError((error) => of(loadAccountsPageFailure({ error })))
                )
            )
        )
    );

    createAccount$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createAccount),
            switchMap((action) =>
                this.accountService.createAccount(action.account).pipe(
                    map((account: Account) => createAccountSuccess({ account })),
                    catchError((error) => {
                        if (error.status === 409) {
                            this.alertService.error(this.translateService.instant('Exista un cont cu adresa de email introdusa: ') + action.account.email);
                        } else {
                            this.alertService.error(error.message);
                        }
                        return of(createAccountFailure({ error }))
                    })
                )
            )
        )
    );

    updateAccount$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateAccount),
            switchMap((action) =>
                this.accountService.updateAccount(action.account).pipe(
                    map(() => updateAccountSuccess({ account: action.account })),
                    catchError((error) => of(updateAccountFailure({ error })))
                )
            )
        )
    );

    deleteAccounts$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteAccounts),
            switchMap((action) =>
                this.accountService.deleteAccounts(action.ids).pipe(
                    map(() => deleteAccountsSuccess({ ids: action.ids })),
                    catchError((error) => of(deleteAccountsFailure({ error })))
                )
            )
        )
    );


    constructor(
        private actions$: Actions,
        private accountService: AccountService,
        private alertService: AlertService,
        private translateService: TranslateService
    ) { }
}