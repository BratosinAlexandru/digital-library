import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import * as AppState from '../../../state/app.state';
import * as fromAccount from './account/account.reducer';
import * as fromNotification from './notification/notification.reducer';
import * as fromDomains from './domains/domains.reducer';
import * as fromCategory from './category/category.reducer';
import * as fromEntities from './entities/entities.reducer';
import * as fromStatuses from './statuses/statuses.reducer';
export const adminFeatureKey = 'admin';

export interface AdminState {
    [fromAccount.accountFeatureKey]: fromAccount.State;
    [fromNotification.notificationFeatureKey]: fromNotification.State;
    [fromDomains.domainFeatureKey]: fromDomains.State;
    [fromCategory.categoryFeatureKey]: fromCategory.State;
    [fromEntities.entitiesFeatureKey]: fromEntities.State;
    [fromStatuses.statusesFeatureKey]: fromStatuses.State;
}

export interface AdminAppState extends AppState.State {
    [adminFeatureKey]: AdminState;
}

export function reducers(state: AdminState | undefined, action: Action) {
    return combineReducers({
        [fromAccount.accountFeatureKey]: fromAccount.reducer,
        [fromNotification.notificationFeatureKey]: fromNotification.reducer,
        [fromDomains.domainFeatureKey]: fromDomains.reducer,
        [fromCategory.categoryFeatureKey]: fromCategory.reducer,
        [fromEntities.entitiesFeatureKey]: fromEntities.reducer,
        [fromStatuses.statusesFeatureKey]: fromStatuses.reducer,
    })(state, action);
}

export const selectAdminState = createFeatureSelector<AdminAppState, AdminState>(
    adminFeatureKey
);

/** Accounts */
export const selectAccountEntityState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromAccount.accountFeatureKey]
);

export const getAccountsPage = createSelector(
    selectAccountEntityState,
    fromAccount.getAccountsPage
);

/** Notifications  */
export const selectNotificationState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromNotification.notificationFeatureKey]
);

export const getNotifications = createSelector(
    selectNotificationState,
    fromNotification.getNotifications
);

export const getSelectedNotification = createSelector(
    selectNotificationState,
    fromNotification.getSelectedNotification
);

export const getTemplateTypes = createSelector(
    selectNotificationState,
    fromNotification.getTemplateTypes
);

export const getTemplateLanguages = createSelector(
    selectNotificationState,
    fromNotification.getTemplateLanguages
);

/** Domains */
export const selectDomainsState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromDomains.domainFeatureKey]
);

export const getDomains = createSelector(
    selectDomainsState,
    fromDomains.getDomains
);

/** Categories */
export const selectCategoriesState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromCategory.categoryFeatureKey]
);

export const getCategory = createSelector(
    selectCategoriesState,
    fromCategory.getCategory
);

export const getCategories = createSelector(
    selectCategoriesState,
    fromCategory.getCategories
);

/** Entities */
export const selectEntitiesState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromEntities.entitiesFeatureKey]
);

export const getCommunicationChannels = createSelector(
    selectEntitiesState,
    fromEntities.getCommunicationChannels
)

export const getRequestTypes = createSelector(
    selectEntitiesState,
    fromEntities.getRequestTypes
)

// export const getRequestStatuses = createSelector(
//     selectEntitiesState,
//     fromEntities.getRequestStatuses
// ); 

export const selectStatusesState = createSelector(
    selectAdminState,
    (state: AdminState) => state[fromStatuses.statusesFeatureKey]
);

export const getStatuses = createSelector(
    selectStatusesState,
    fromStatuses.getStatuses
);