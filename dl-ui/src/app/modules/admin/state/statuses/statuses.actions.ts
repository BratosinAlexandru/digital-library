import { createAction, props } from "@ngrx/store";
import { RequestStatus } from "src/app/api/models";

/** Load request statuses actions */
export const loadStatuses = createAction('[TEST] loadStatuses');
export const loadStatusesSuccess = createAction('[Admin] loadStatuses Success', props<({ statuses: Array<RequestStatus> })>());
export const loadStatusesFailure = createAction('[Admin] loadStatuses Failure', props<({ error: any })>());

/** Create request status actions */
export const createRequestStatus = createAction('[Admin] Create Request Status', props<({ requestStatus: RequestStatus })>());
export const createRequestStatusSuccess = createAction('[Admin] Create Request Status Success', props<({ requestStatus: RequestStatus })>());
export const createRequestStatusFailure = createAction('[Admin] Create Request Status Failure', props<({ error: any })>());

/** Update request status actions  */
export const updateRequestStatus = createAction('[Admin] Update Request Status', props<({ updatedRequestStatus: RequestStatus })>());
export const updateRequestStatusSuccess = createAction('[Admin] Update Request Status Success', props<({ updatedRequestStatus: RequestStatus })>());
export const updateRequestStatusFailure = createAction('[Admin] Update Request Status Failure', props<({ error: any })>());

/** Delete request status actions */
export const deleteRequestStatus = createAction('[Admin] Delete Request Status', props<({ requestStatusId: string })>());
export const deleteRequestStatusSuccess = createAction('[Admin] Delete Request Status Success', props<({ requestStatusId: string })>());
export const deleteRequestStatusFailure = createAction('[Admin] Delete Request Status Failure', props<({ error: any })>());