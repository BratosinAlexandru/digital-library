import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { TranslateService } from "@ngx-translate/core";
import { EMPTY, of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { RequestStatus } from "src/app/api/models";
import { AlertService } from "src/app/core/services/alert.service";
import { EntitiesService } from "../../services/entities.service";
import { createRequestStatus, createRequestStatusFailure, createRequestStatusSuccess, deleteRequestStatus, deleteRequestStatusFailure, deleteRequestStatusSuccess, loadStatuses, loadStatusesFailure, loadStatusesSuccess, updateRequestStatus, updateRequestStatusFailure, updateRequestStatusSuccess } from "./statuses.actions";

@Injectable()
export class StatusesEffects {

    loadRequestStatuses$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadStatuses),
            switchMap((action) =>
                this.entitiesService.loadRequestStatuses(true).pipe(
                    map((requestStatuses) => loadStatusesSuccess({ statuses:  requestStatuses})),
                    catchError((error) => of(loadStatusesFailure({ error })))
                )
            )
        )
    );

    createRequestStatus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createRequestStatus),
            switchMap((action) =>
                this.entitiesService.createRequestStatus(action.requestStatus).pipe(
                    map((requestStatus: RequestStatus) => createRequestStatusSuccess({ requestStatus })),
                    catchError((error) => of(createRequestStatusFailure({ error })))
                )
            )
        )
    );

    updateRequestStatus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateRequestStatus),
            switchMap((action) =>
                this.entitiesService.updateRequestStatus(action.updatedRequestStatus).pipe(
                    map(() => updateRequestStatusSuccess({ updatedRequestStatus: action.updatedRequestStatus })),
                    catchError((error) => of(updateRequestStatusFailure({ error })))
                )
            )
        )
    );

    deleteRequestStatus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteRequestStatus),
            switchMap((action) =>
                this.entitiesService.deleteReqeustStatus(action.requestStatusId).pipe(
                    map(() => deleteRequestStatusSuccess({ requestStatusId: action.requestStatusId })),
                    catchError((error) => of(deleteRequestStatusFailure({ error })))
                )
            )
        )
    );

    deleteRequestStatusFailure$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteRequestStatusFailure),
            switchMap(({ error }) => {
                if (error.status === 400) {
                    this.alertService.error(this.translateService.instant('Statusul solicitarii este utilizata'));
                }
                return EMPTY;
            })
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private entitiesService: EntitiesService,
        private translateService: TranslateService,
        private alertService: AlertService
    ) { }

}