import { createReducer, on } from "@ngrx/store";
import produce from "immer";
import { RequestStatus } from "src/app/api/models";
import { createRequestStatusSuccess, deleteRequestStatusSuccess, loadStatusesSuccess, updateRequestStatusSuccess } from "./statuses.actions";

export const statusesFeatureKey = 'statuses';

export interface State {
    statuses: RequestStatus[];
};

export const initialState: State = {
    statuses: null
}

export const reducer = createReducer(
    initialState,
    on(loadStatusesSuccess,
        (state, {statuses}) => ({...state, statuses})
    ),
    on(createRequestStatusSuccess,
        (state, { requestStatus }) => produce(state, draftState => {
            const requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.statuses));

            const findDefaultValueIndex = requestStatuses.findIndex(requestStatus => requestStatus.defaultValue === true);
            if (findDefaultValueIndex !== -1) {
                requestStatuses[findDefaultValueIndex].defaultValue = false;
            }

            requestStatuses.push(requestStatus);
            return { ...draftState, statuses: requestStatuses };
        })
    ),
    on(updateRequestStatusSuccess,
        (state, { updatedRequestStatus }) => produce(state, draftState => {
            if (updatedRequestStatus) {
                const requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.statuses));
                const findDefaultValueIndex = requestStatuses.findIndex(requestStatus => requestStatus.defaultValue === true);
                if (findDefaultValueIndex !== -1) {
                    requestStatuses[findDefaultValueIndex].defaultValue = false;
                }

                const index = requestStatuses.findIndex(e => e.id === updatedRequestStatus.id);
                if (index !== -1) {
                    requestStatuses[index] = updatedRequestStatus;
                    return { ...draftState, statuses: requestStatuses };
                } else {
                    return { ...draftState };
                }
            } else {
                return { ...draftState };
            }
        })
    ),
    on(deleteRequestStatusSuccess,
        (state, {requestStatusId}) => produce(state, draftState => {
            let requestStatuses: Array<RequestStatus> = JSON.parse(JSON.stringify(state.statuses));
            requestStatuses = requestStatuses.filter(e => e.id !== requestStatusId);
            return {...draftState, statuses: requestStatuses};
        })
    ),
);

export const getStatuses = (state: State) => state.statuses;