import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { LibraryTranslateService } from "src/app/translate/services/library-translate.service";

@Injectable()
export class AdminTranslateLoader implements Resolve<any> {
    
    constructor(private translateService: LibraryTranslateService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.translateService.addTranslations('admin');
    }
}