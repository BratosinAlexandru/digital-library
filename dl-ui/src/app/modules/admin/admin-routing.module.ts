import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AccountsComponent } from "./components/accounts/accounts.component";
import { CategoriesComponent } from "./components/categories/categories.component";
import { CommunicationChannelsComponent } from "./components/communication-channels/communication-channels.component";
import { DomainsComponent } from "./components/domains/domains.component";
import { GeneralComponent } from "./components/general/general.component";
import { NotificationComponent } from "./components/notification/notification.component";
import { RequestStatusComponent } from "./components/request-status/request-status.component";
import { RequestTypeComponent } from "./components/request-type/request-type.component";

const routes: Routes = [
  {
    path: 'accounts',
    component: AccountsComponent,
    data: {
      title: 'Membri'
    },
  },
  {
    path: 'notifications',
    component: NotificationComponent,
    data: {
      title: 'Notificari'
    }
  },
  {
    path: 'general',
    component: GeneralComponent,
    data: {
      title: 'Setari generale'
    },
    children: [
      {
        path: '',
        redirectTo: 'domains',
        pathMatch: 'full'
      },
      {
        path: 'domains',
        component: DomainsComponent,
        data: {
          title: 'Domenii de interes'
        }
      },
      {
        path: 'categories',
        component: CategoriesComponent,
        data: {
          title: 'Categorii'
        }
      },
      {
        path: 'statuses',
        component: RequestStatusComponent,
        data: {
          title: 'Status solicitare'
        }
      },
      {
        path: 'communication',
        component: CommunicationChannelsComponent,
        data: {
          title: 'Canale de comunicatii'
        }
      },
      {
        path: 'types',
        component: RequestTypeComponent,
        data: {
          title: 'Tip solicitare',
        }
      },
    ]
  },
  { path: '**', redirectTo: 'accounts', pathMatch: 'full' },
  { path: '', redirectTo: 'accounts', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }