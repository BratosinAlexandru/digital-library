import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppStateModule } from './state/app-state.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AuthModule } from './modules/auth/auth.module';
import { ModulePreloadingStrategy } from './core/services/module-preloading-strategy';
import { AppComponent } from './core/containers/app/app.component';
import { LibraryTranslateModule } from './translate/library-translate.module';




@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    LibraryTranslateModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    NoopAnimationsModule,
    AppStateModule,
    CoreModule,
  ],
  providers: [ModulePreloadingStrategy],
  bootstrap: [AppComponent]
})
export class AppModule { }
