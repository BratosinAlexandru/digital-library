import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { interceptorProviders } from './core/interceptors/interceptors';
import { ModulePreloadingStrategy } from './core/services/module-preloading-strategy';
import { AdminTranslateLoader } from './modules/admin/admin-translate.loader';
import { AuthGuardService } from './modules/auth/services';
import { AdminGuardService } from './modules/auth/services/admin-guard.service';
import { CatalogTranslateLoader } from './modules/catalog/catalog-translate.module';
import { RequestsTranslateLoader } from './modules/requests/requests-translate.loader';

const routes: Routes = [
  { path: '', redirectTo: 'admin', pathMatch: 'full' },
  {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule),
    canActivate: [AuthGuardService],
    resolve: { translate: AdminTranslateLoader },
    data: { title: 'Administrare' },
  },
  {
    path: 'catalog',
    loadChildren: () => import('./modules/catalog/catalog.module').then((m) => m.CatalogModule),
    resolve: {
      translate: CatalogTranslateLoader,
    },
    data: { title: 'Panotaje' },
  },
  {
    path: 'requests',
    loadChildren: () => import('./modules/requests/requests.module').then((m) => m.RequestsModule),
    resolve: {
      translate: RequestsTranslateLoader,
    },
    data: { title: 'Solicitari' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: ModulePreloadingStrategy,
    useHash: true,
    // relativeLinkResolution: 'legacy'
  }),
  ],
  exports: [RouterModule],
  providers: [interceptorProviders],
})
export class AppRoutingModule { }
