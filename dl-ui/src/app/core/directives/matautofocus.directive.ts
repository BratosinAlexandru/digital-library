import { Directive, ElementRef, Input } from '@angular/core';
import { MatInput } from '@angular/material/input';

@Directive({
  selector: '[matInputAutofocus]'
})
export class MatautofocusDirective {
  @Input() matInputAutofocus: boolean;

  constructor(
    private host: MatInput,
    private elRef: ElementRef<HTMLInputElement>
  ) { }

  ngOnInit() {
    if (this.matInputAutofocus) {
      setTimeout(() => {
        this.host.focus();
        this.elRef.nativeElement.select();
      }, 0);
    }
  }
}
