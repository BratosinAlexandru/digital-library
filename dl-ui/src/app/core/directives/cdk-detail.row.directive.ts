import { Directive, HostBinding, HostListener, Input, TemplateRef, ViewContainerRef } from "@angular/core";

@Directive({
    selector: '[cdkDetailRow]'
})
export class CdkDetailRowDirective {
    private row: any;
    private templateRef: TemplateRef<any>;
    private opened: boolean;

    @HostBinding('class.expanded')
    get expended(): boolean {
        return this.opened;
    }

    @Input()
    set isExpanded(value: boolean) {
        if (value !== null && value !== undefined) {
            this.opened = value;
            this.toggle();
        }
    }

    @Input()
    set cdkDetailRow(value: any) {
        if (value !== this.row) {
            this.row = value;
        }
    }

    @Input('cdkDetailRowTpl')
    set template(value: TemplateRef<any>) {
        if (value !== this.templateRef) {
            this.templateRef = value;
        }
    }

    constructor(public vcRef: ViewContainerRef) { }

    @HostListener('click')
    onClick(): void {
        this.toggle();
    }

    private toggle(): void {
        if (this.opened) {
            this.vcRef.clear();
        } else {
            this.render();
        }
        this.opened = this.vcRef.length > 0;
    }

    private render(): void {
        this.vcRef.clear();
        if (this.templateRef && this.row) {
            this.vcRef.createEmbeddedView(this.templateRef, { $implicit: this.row });
        }
    }
}