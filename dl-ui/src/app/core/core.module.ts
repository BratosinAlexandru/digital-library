import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './containers/app/app.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertService } from './services/alert.service';
import { LayoutComponent, NavItemComponent, ToolbarComponent, UserControlsComponent, VarticalNavigationComponent } from './components';
import { MatautofocusDirective } from './directives/matautofocus.directive';
import { LanguageComponent } from './components/language/language.component';
import { HtmlSafePipe } from './pipes/html-safe.pipe';
import { CdkDetailRowDirective } from './directives/cdk-detail.row.directive';

export const COMPONENTS = [
  AppComponent,
  LayoutComponent,
  NavItemComponent,
  ToolbarComponent,
  UserControlsComponent,
  MatautofocusDirective,
  VarticalNavigationComponent,
  LanguageComponent,
  HtmlSafePipe,
  CdkDetailRowDirective,
]

export const SERVICES = [AlertService];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: SERVICES,
  exports: COMPONENTS,
})
export class CoreModule { }
