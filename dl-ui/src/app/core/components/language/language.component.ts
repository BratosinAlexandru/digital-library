import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent {
  languages = ['ro', 'en'];
  selectedLanguage: string = localStorage.getItem('language') !== null ? localStorage.getItem('language') : 'ro';

  constructor(private translateService: TranslateService) { }

  onLanguageSelect({ value: language }) {
    this.translateService.use(language).subscribe();
    localStorage.setItem('language', language);
  }

}
