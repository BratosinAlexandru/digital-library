import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'vartical-navigation',
  templateUrl: './vartical-navigation.component.html',
  styleUrls: ['./vartical-navigation.component.scss']
})
export class VarticalNavigationComponent {
  @ViewChild('scrollController', { static: true }) scrollContainer: ElementRef;

  isScrollable(element) {
    return element.scrollHeight > element.clientHeight;
  }

  scrollTopContainer(element) {
    element.scrollTop -= 50;
  }

  scrollBottomContainer(element) {
    element.scrollTop += 50;
  }

  onClick(event: any) {
    const path = event.composedPath();
    const indexOfContent = path.findIndex(
      (p) => p.localName === 'div' && p.className.indexOf('navigation-content') !== -1
    );
    const mainElement = path[indexOfContent - 1];
    const mOffsetTop = mainElement.offsetTop;
    const mOffsetHeight = mainElement.offsetHeight;
    const scrollValue =
      mOffsetTop -
      this.scrollContainer.nativeElement.offsetTop -
      this.scrollContainer.nativeElement.offsetHeight / 2 +
      mOffsetHeight / 2;

    this.scrollContainer.nativeElement.scrollTo({ left: 0, top: scrollValue, behavior: 'smooth' });
  }

}
