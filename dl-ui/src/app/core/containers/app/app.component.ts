import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EditUserProfileDialogComponent } from 'src/app/modules/auth/components/edit-user-profile-dialog/edit-user-profile-dialog.component';
import { User } from 'src/app/modules/auth/models';

import * as AuthActions from '../../../modules/auth/state/auth.actions';
import * as AuthSelectors from '../../../modules/auth/state/auth.selectors';
import * as AuthState from '../../../modules/auth/state/auth.state';
import * as appReducer from '../../../state/app.reducer';
import * as appState from '../../../state/app.state';
import * as LayoutActions from '../../state/layout.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  showSidenav$: Observable<boolean>;
  loggedIn$: Observable<boolean>;
  user$: Observable<User>;
  loggedUser$: Observable<Account>;
  loggedUser: Account;
  burggerShow = false;
  isAdmin$: Observable<boolean>;

  constructor(
    private store: Store<appState.State & AuthState.State>,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
    this.showSidenav$ = this.store.pipe(select(appReducer.selecthowSidenav));
    this.loggedIn$ = this.store.pipe(select(AuthSelectors.selectLoggedIn));
    this.user$ = this.store.pipe(select(AuthSelectors.selectUser));
    this.loggedUser$ = this.store.pipe(select(AuthSelectors.selectLoggedUser));
    this.loggedUser$.subscribe((user: any) => (this.loggedUser = user));
    this.isAdmin$ = this.store.pipe(
      select(AuthSelectors.selectRoles),
      map((userRoles) => userRoles && userRoles.includes('ROLE_ADMIN'))
    );

    const uiIconsPath = 'assets/images/icons/ui/';

    this.matIconRegistry.addSvgIcon(
      'library-export-file',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}download-file.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'library-table-settings',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}table-settings.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'expand-view-icon',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}expand-view-icon.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'expand-view-icon-desktop',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}expand-view-icon-desktop.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'reset',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}reset.svg`)
    );

    this.matIconRegistry.addSvgIcon(
      'no-image',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`${uiIconsPath}no-image.svg`)
    );
  }

  editUserProfile() {
    const dialogRef = this.dialog.open(EditUserProfileDialogComponent, {
      width: '300px',
      data: { ...this.loggedUser },
    });
    dialogRef.afterClosed().subscribe(async (account) => {
      if (account) {
        this.store.dispatch(AuthActions.updateAccount({ account }));
      }
    });
  }

  closeSidenav() {
    this.burggerShow = false;
  }

  openSidenav() {
    this.store.dispatch(LayoutActions.openSidenav());
  }

  logout() {
    this.closeSidenav();

    this.store.dispatch(AuthActions.logoutConfirmation());
  }

}
