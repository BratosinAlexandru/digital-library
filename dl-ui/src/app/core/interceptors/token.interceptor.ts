import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { BehaviorSubject, EMPTY, Observable, throwError } from "rxjs";
import { catchError, filter, skip, switchMap, take } from "rxjs/operators";
import { AuthService } from "src/app/modules/auth/services";
import * as AuthActions from '../../modules/auth/state/auth.actions';
import { selectIsLoggedIn, selectLoggedIn } from '../../modules/auth/state/auth.selectors';
import * as AuthState from '../../modules/auth/state/auth.state';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(
        public authService: AuthService,
        private store$: Store<AuthState.State>
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes('oauth/token')) {
            return next.handle(this.addRequestHeader(req));
        }

        return next.handle(this.addRequestHeader(req)).pipe(
            catchError((error) => {
                if (error instanceof HttpErrorResponse && error.status === 401) {
                    return this.handle401Error(req, next);
                } else {
                    return throwError(error);
                }
            })
        );
    }

    private addRequestHeader(request: HttpRequest<any>) {
        return request.clone({
            withCredentials: true,
        });
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            this.store$.dispatch(AuthActions.refreshToken());
            const accessToken$ = this.store$.pipe(select(selectIsLoggedIn)).pipe(skip(1));

            return accessToken$.pipe(
                switchMap((isLogged: boolean) => {
                    if (!isLogged) {
                        return EMPTY;
                    }
                    this.isRefreshing = false;
                    this.refreshTokenSubject.next(isLogged);
                    return next.handle(this.addRequestHeader(request));
                })
            );
        } else {
            return this.refreshTokenSubject.pipe(
                filter((isLogged) => isLogged = true),
                take(1),
                switchMap((accessToken) => {
                    return next.handle(this.addRequestHeader(request));
                })
            );
        }
    }
}