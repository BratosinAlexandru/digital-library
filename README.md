Status: in dezvoltare

Numele proiectului: Digitalizarea Bibliotecii
Scopul: Incercarea de a imbunatatii experienta oferita de sistemul actual din cadrul bibliotecii traditionale

Proiectul este impartit in doua parti:
	- partea de back-end: aplicatie dezvoltata in Spring Boot, utilizand o baza de date nerelationala ( MongoDB )
	- partea de front-end: aplicatie dezvoltata utilizand Angular

Pentru a putea testa aplicatia:
	- cont admin: admin@library.ro  admin
	
Aplicatia dispune de un meniu destinat administratorilor, cu ajutorul caruia pot monitoriza si asigura buna functionare a aplicatiei
Utilizatorii dispun de o sectiune in care pot vedea toate materialele educationale dispoibile in momentul actual pe platforma.
In cazul in care acestia doresc sa imprumute o carte, se va realiza o solicitare in care vor cere dreptul la aceasta.
In cazul in care solicitarea a fost aprobata,
utilizatorul poate citii cartea tot in cadrul aplicatiei.