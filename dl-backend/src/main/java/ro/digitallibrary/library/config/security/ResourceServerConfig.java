package ro.digitallibrary.library.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    private static final String RESOURCE_ID = "kron";
    private static final String ADMIN = "ADMIN";
    private static final String OFFICE = "OFFICE";
    private static final String SUPPORT = "SUPPORT";

    @Autowired
    @Qualifier("accessTokenExtractor")
    private TokenExtractor tokenExtractor;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false)
                .tokenExtractor(tokenExtractor);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/actuator/**")
                    .hasRole(ADMIN)
                .antMatchers(HttpMethod.POST, "/account/create")
                    .hasRole(ADMIN)
                .antMatchers(HttpMethod.POST, "/account/update")
                    .hasAnyRole(ADMIN, OFFICE, SUPPORT)
                .antMatchers(HttpMethod.DELETE, "/account/delete")
                    .hasRole(ADMIN)
                .antMatchers(HttpMethod.GET, "/account/**/isUnique")
                    .permitAll()
                .antMatchers("/account/**")
                    .authenticated();
        http.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
