package ro.digitallibrary.library.config.security.token;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class AccessTokenExtractor implements TokenExtractor {

    @Value("${library.cookies.access-token-cookie-name}")
    private String accessTokenCookieName;

    public Authentication extract(HttpServletRequest request) {
        String tokenValue = extractToken(request);
        if (tokenValue != null) {
            PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(tokenValue, "");
            return authentication;
        }
        return null;
    }

    private String extractToken(HttpServletRequest request) {
        return getCookieValue(request, accessTokenCookieName);
    }

    private String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int index = 0; index < cookies.length; index++) {
                if (cookies[index].getName().equalsIgnoreCase(cookieName)) {
                    return cookies[index].getValue();
                }
            }
        }
        return null;
    }
}
