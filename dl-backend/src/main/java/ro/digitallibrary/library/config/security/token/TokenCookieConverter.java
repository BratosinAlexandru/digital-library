package ro.digitallibrary.library.config.security.token;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TokenCookieConverter {
    
    @Autowired
    private HttpServletResponse response;

    @Value("${library.cookies.access-token-cookie-name}")
    private String accessTokenCookieName;
    @Value("${library.cookies.refresh-token-cookie-name}")
    private String refreshTokenCookieName;
    @Value("${library.auth.access-token-validity}")
    private int accessTokenValidityInSeconds;
    @Value("${library.auth.refresh-token-validity}")
    private int refreshTokenValidityInSeconds;
    @Value("${library.auth.secure-cookie}")
    private boolean secureCookie;

    @Around("execution(* org.springframework.security.oauth2.provider.endpoint.TokenEndpoint.postAccessToken(..))")
    public Object convertTokenToCookie(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();
        if (result instanceof ResponseEntity<?>) {
            ResponseEntity<?> response = (ResponseEntity<?>) result;
            if (response.getStatusCode() == HttpStatus.OK) {
                OAuth2AccessToken accessToken = (OAuth2AccessToken) response.getBody();
                transferTokenToSecureCookie(accessToken);
            }
            return ResponseEntity.ok().headers(response.getHeaders())
                    .build();
        }
        return result;
    }

    private void transferTokenToSecureCookie(OAuth2AccessToken accessToken) {
        final Cookie accessTokenCookie = new Cookie(accessTokenCookieName, accessToken.getValue());
        accessTokenCookie.setHttpOnly(true);
        accessTokenCookie.setSecure(secureCookie);
        accessTokenCookie.setPath("/");
        accessTokenCookie.setMaxAge(accessTokenValidityInSeconds);
        response.addCookie(accessTokenCookie);

        OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
        final Cookie refreshTokenCookie = new Cookie(refreshTokenCookieName, refreshToken.getValue());
        refreshTokenCookie.setHttpOnly(true);
        refreshTokenCookie.setSecure(secureCookie);
        refreshTokenCookie.setPath("/");
        refreshTokenCookie.setMaxAge(refreshTokenValidityInSeconds);
        response.addCookie(refreshTokenCookie);
    }
}
