package ro.digitallibrary.library.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories("ro.digitallibrary.library.persistence.repository")
@EnableMongoAuditing
public class DatabaseConfig {
    @Autowired
    private MongoDbFactory mongoFactory;
    @Autowired
    private MongoMappingContext mongoMappingContext;

    @Profile("!default")
    @Bean
    public MongoTransactionManager mongoTransactionManager(MongoDbFactory mongoDbFactory) {
        MongoTransactionManager txManager = new MongoTransactionManager(mongoDbFactory);
        txManager.setOptions(TransactionOptions.builder()
                .writeConcern(WriteConcern.MAJORITY)
                .readConcern(ReadConcern.SNAPSHOT)
                .readPreference(ReadPreference.primary())
                .build());
        return txManager;
    }

    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoFactory);
        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver,
                mongoMappingContext);
        mongoConverter.setMapKeyDotReplacement("_");
        mongoConverter.afterPropertiesSet();
        return mongoConverter;
    }

    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoFactory, mongoConverter(), "uploads");
    }

}
