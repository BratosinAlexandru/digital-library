package ro.digitallibrary.library.config.security.token;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RefreshTokenExtractor {
    @Autowired
    private HttpServletRequest request;

    @Value("${library.cookies.refresh-token-cookie-name}")
    private String refreshTokenCookieName;

    @Before("execution(* org.springframework.security.oauth2.provider.endpoint.TokenEndpoint.postAccessToken(..))")
    public void populateRefreshTokenParameter(JoinPoint joinPoint) {
        String refreshToken = extractRefreshToken(request);
        if (refreshToken != null) {
            Object[] args = joinPoint.getArgs();
            Map<String, String> parameters = (Map<String, String>) args[1];
            parameters.put("refresh_token", refreshToken);
        }
    }

    private String extractRefreshToken(HttpServletRequest request) {
        return getCookieValue(request, refreshTokenCookieName);
    }

    private String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int index = 0; index < cookies.length; index++) {
                if (cookies[index].getName().equalsIgnoreCase(cookieName)) {
                    return cookies[index].getValue();
                }
            }
        }
        return null;
    }
}
