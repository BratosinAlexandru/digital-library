package ro.digitallibrary.library.beans.templates;

public enum NotificationKey {
    RESET_PASSWORD,
    NOTIFY_NEW_REQUEST,
    EXPIRATION_NOTIFICATION,
}
