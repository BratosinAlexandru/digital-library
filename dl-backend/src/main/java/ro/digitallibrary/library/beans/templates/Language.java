package ro.digitallibrary.library.beans.templates;

import java.io.Serializable;

public class Language implements Serializable {

    private static final long serialVersionUID = -686188533807944742L;

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
