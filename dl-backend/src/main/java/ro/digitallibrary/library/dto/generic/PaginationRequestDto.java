package ro.digitallibrary.library.dto.generic;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("PaginationRequestDto")
public class PaginationRequestDto<F> implements Serializable {
    private static final long serialVersionUID = 6816181803575843817L;

    private F filtersRequest;
    private List<String> sortProperties;
    private String sortDirection;
    private int pageNumber;
    private int pageSize;
    private String query;

    public F getFiltersRequest() {
        return filtersRequest;
    }

    public void setFiltersRequest(F filtersRequest) {
        this.filtersRequest = filtersRequest;
    }

    public List<String> getSortProperties() {
        return sortProperties;
    }

    public void setSortProperties(List<String> sortProperties) {
        this.sortProperties = sortProperties;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "PaginationRequestDto{" +
                "filtersRequest=" + filtersRequest +
                ", sortProperties=" + sortProperties +
                ", sortDirection='" + sortDirection + '\'' +
                ", pageNumber=" + pageNumber +
                ", pageSize=" + pageSize +
                ", query='" + query + '\'' +
                '}';
    }
}
