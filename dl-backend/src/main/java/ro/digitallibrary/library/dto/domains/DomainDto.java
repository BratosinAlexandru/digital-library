package ro.digitallibrary.library.dto.domains;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("Domain")
public class DomainDto extends AbstractDto {

    private static final long serialVersionUID = -994495238940856756L;
    private String name;
    private long orderId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
}
