package ro.digitallibrary.library.dto.app;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("AppKey")
public class AppKeyDto extends AbstractDto {

    private static final long serialVersionUID = -6662969326325751331L;

    private String key;
    private Object value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AppKeyDto{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
