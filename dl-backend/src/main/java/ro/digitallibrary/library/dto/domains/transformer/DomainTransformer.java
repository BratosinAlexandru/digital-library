package ro.digitallibrary.library.dto.domains.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.domains.DomainDto;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.persistence.entity.domains.Domain;
import ro.digitallibrary.library.persistence.repository.domains.DomainRepository;

import java.util.Optional;

@Component
public class DomainTransformer extends AbstractTransformer<Domain, DomainDto> {
    @Autowired
    private DomainRepository domainRepository;

    @Override
    public Domain transformToEntity(DomainDto dto) {
        Domain entity = findById(dto.getId()).orElseGet(Domain::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public DomainDto transformToDto(Domain entity) {
        DomainDto dto = new DomainDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<Domain> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return domainRepository.findById(id);
    }
}
