package ro.digitallibrary.library.dto.templates.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.templates.NotificationDto;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.repository.templates.NotificationRepository;

import java.util.Optional;

@Component
public class NotificationTransformer extends AbstractTransformer<Notification, NotificationDto> {

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public Notification transformToEntity(NotificationDto dto) {
        Notification entity = findById(dto.getId()).orElseGet(Notification::new);
        BeanUtils.copyProperties(dto, entity, "variables", "key");
        return entity;
    }

    @Override
    public NotificationDto transformToDto(Notification entity) {
        NotificationDto dto = new NotificationDto();
        BeanUtils.copyProperties(entity, dto);
        if (entity.getKey() != null) {
            dto.setKey(entity.getKey().toString());
        }
        return dto;
    }

    @Override
    protected Optional<Notification> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return notificationRepository.findById(id);
    }
}
