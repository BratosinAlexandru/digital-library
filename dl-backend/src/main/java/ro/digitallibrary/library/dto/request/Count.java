package ro.digitallibrary.library.dto.request;

public class Count {

    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
