package ro.digitallibrary.library.dto.categories;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.domains.DomainDto;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("Category")
public class CategoryDto extends AbstractDto {

    private static final long serialVersionUID = 62513503193518961L;

    private DomainDto domain;
    private String name;
    private long orderId;

    public DomainDto getDomain() {
        return domain;
    }

    public void setDomain(DomainDto domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
}
