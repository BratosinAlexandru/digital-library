package ro.digitallibrary.library.dto.request;

import java.util.Arrays;
import java.util.List;

public class RequestPage {

    private List<Count> totalElements;
    private RequestStatusCount[] requestStatus;
    private PaginationRequests pagination;

    public List<Count> getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(List<Count> totalElements) {
        this.totalElements = totalElements;
    }

    public RequestStatusCount[] getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatusCount[] requestStatus) {
        this.requestStatus = requestStatus;
    }

    public PaginationRequests getPagination() {
        return pagination;
    }

    public void setPagination(PaginationRequests pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "RequestPage{" +
                "totalElements=" + totalElements +
                ", requestStatus=" + Arrays.toString(requestStatus) +
                ", pagination=" + pagination +
                '}';
    }
}
