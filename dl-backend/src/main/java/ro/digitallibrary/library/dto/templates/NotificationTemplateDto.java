package ro.digitallibrary.library.dto.templates;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("NotificationTemplate")
public class NotificationTemplateDto extends AbstractDto {

    private static final long serialVersionUID = 4267573550281690103L;

    private NotificationDto notification;
    private String content;
    private String subject;
    private String language;
    private String type;
    private Boolean isDefault;

    public NotificationDto getNotification() {
        return notification;
    }

    public void setNotification(NotificationDto notification) {
        this.notification = notification;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    @Override
    public String toString() {
        return "NotificationTemplateDto{" +
                "notification=" + notification +
                ", content='" + content + '\'' +
                ", subject='" + subject + '\'' +
                ", language='" + language + '\'' +
                ", type='" + type + '\'' +
                ", isDefault=" + isDefault +
                '}';
    }
}
