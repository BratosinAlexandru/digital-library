package ro.digitallibrary.library.dto.request;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.book.BookDto;
import ro.digitallibrary.library.dto.book.BookOverviewDto;
import ro.digitallibrary.library.dto.generic.AbstractDto;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel("Request")
public class RequestDto extends AbstractDto {

    private static final long serialVersionUID = -199597938563282413L;

    private CustomerDto customer;
    private String college;
    private String otherMentions;
    private Boolean importantRequest;
    private LocalDateTime requestDate;
    private BookOverviewDto book;
    private String accountId;
    private List<RequestTypeDto> requestType;
    private RequestStatusDto requestStatus;
    private List<CommunicationChannelDto> communicationChannels;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getOtherMentions() {
        return otherMentions;
    }

    public void setOtherMentions(String otherMentions) {
        this.otherMentions = otherMentions;
    }

    public Boolean getImportantRequest() {
        return importantRequest;
    }

    public void setImportantRequest(Boolean importantRequest) {
        this.importantRequest = importantRequest;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public BookOverviewDto getBook() {
        return book;
    }

    public void setBook(BookOverviewDto book) {
        this.book = book;
    }

    public List<RequestTypeDto> getRequestType() {
        return requestType;
    }

    public void setRequestType(List<RequestTypeDto> requestType) {
        this.requestType = requestType;
    }

    public RequestStatusDto getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatusDto requestStatus) {
        this.requestStatus = requestStatus;
    }

    public List<CommunicationChannelDto> getCommunicationChannels() {
        return communicationChannels;
    }

    public void setCommunicationChannels(List<CommunicationChannelDto> communicationChannels) {
        this.communicationChannels = communicationChannels;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }
}
