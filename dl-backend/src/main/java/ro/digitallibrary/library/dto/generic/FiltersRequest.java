package ro.digitallibrary.library.dto.generic;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.request.CustomerDto;
import ro.digitallibrary.library.dto.request.RequestStatusDto;
import ro.digitallibrary.library.dto.request.RequestTypeDto;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("FiltersRequest")
public class FiltersRequest implements Serializable {

    private static final long serialVersionUID = 6816181803575843818L;

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Boolean enabled;
    private String type;
    private String category;
    private RequestTypeDto[] requestType;
    private Boolean importantRequest;
    private RequestStatusDto requestStatus;
    private CustomerDto customer;

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public RequestTypeDto[] getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestTypeDto[] requestType) {
        this.requestType = requestType;
    }

    public Boolean getImportantRequest() {
        return importantRequest;
    }

    public void setImportantRequest(Boolean importantRequest) {
        this.importantRequest = importantRequest;
    }

    public RequestStatusDto getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatusDto requestStatus) {
        this.requestStatus = requestStatus;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }
}
