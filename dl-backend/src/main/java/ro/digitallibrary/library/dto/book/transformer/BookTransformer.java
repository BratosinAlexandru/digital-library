package ro.digitallibrary.library.dto.book.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.book.BookDto;
import ro.digitallibrary.library.dto.book.BookOverviewDto;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;
import ro.digitallibrary.library.persistence.repository.categories.CategoryRepository;
import ro.digitallibrary.library.persistence.repository.domains.DomainRepository;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BookTransformer extends AbstractTransformer<Book, BookDto> {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private DomainRepository domainRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Book transformToEntity(BookDto dto) {
        Book entity = findById(dto.getId()).orElseGet(Book::new);
        BeanUtils.copyProperties(dto, entity, "createdOn", "updatedOn");
        if (dto.getAttachment() != null) {
            entity.setAttachment(dto.getAttachment().stream().collect(Collectors.toList()));
        } else {
            entity.setAttachment(null);
        }
        return entity;
    }

    @Override
    public BookDto transformToDto(Book entity) {
        if (entity == null) {
            return null;
        }
        BookDto dto = new BookDto();
        BeanUtils.copyProperties(entity, dto, "image");

        if (entity.getAttachment() != null) {
            dto.setAttachment(entity.getAttachment().stream().collect(Collectors.toList()));
        } else {
            entity.setAttachment(null);
        }
        return dto;
    }

    @Override
    protected Optional<Book> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return bookRepository.findById(id);
    }

    public BookOverviewDto toBookOverview(Book book) {
        if (book == null) {
            return null;
        }
        BookOverviewDto bookOverview = new BookOverviewDto();
        BeanUtils.copyProperties(book, bookOverview);

        if (book.getCategoryId() != null) {
            categoryRepository.findById(book.getCategoryId()).ifPresent(category -> {
                bookOverview.setCategoryName(category.getName());
            });
        }

        if (book.getDomainId() != null) {
            domainRepository.findById(book.getDomainId()).ifPresent(domain -> {
                bookOverview.setDomainName(domain.getName());
            });
        }
        return bookOverview;
    }
}
