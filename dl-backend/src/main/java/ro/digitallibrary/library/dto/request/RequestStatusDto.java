package ro.digitallibrary.library.dto.request;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("RequestStatus")
public class RequestStatusDto extends AbstractDto {

    private static final long serialVersionUID = -2036019359394953762L;

    private String name;
    private String color;
    private Long orderId;
    private Boolean isDefaultValue;
    private Boolean approachRequest;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Boolean getDefaultValue() {
        return isDefaultValue;
    }

    public void setDefaultValue(Boolean defaultValue) {
        isDefaultValue = defaultValue;
    }

    public Boolean getApproachRequest() {
        return approachRequest;
    }

    public void setApproachRequest(Boolean approachRequest) {
        this.approachRequest = approachRequest;
    }

    @Override
    public String toString() {
        return "RequestStatusDto{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", orderId=" + orderId +
                ", isDefaultValue=" + isDefaultValue +
                ", approachRequest=" + approachRequest +
                '}';
    }
}
