package ro.digitallibrary.library.dto.request.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.request.RequestTypeDto;
import ro.digitallibrary.library.persistence.entity.request.RequestType;
import ro.digitallibrary.library.persistence.repository.request.RequestTypeRepository;

import java.util.Optional;

@Component
public class RequestTypeTransformer extends AbstractTransformer<RequestType, RequestTypeDto> {

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    @Override
    public RequestType transformToEntity(RequestTypeDto dto) {
        RequestType entity = findById(dto.getId()).orElseGet(RequestType::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public RequestTypeDto transformToDto(RequestType entity) {
        RequestTypeDto dto = new RequestTypeDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<RequestType> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return requestTypeRepository.findById(id);
    }
}
