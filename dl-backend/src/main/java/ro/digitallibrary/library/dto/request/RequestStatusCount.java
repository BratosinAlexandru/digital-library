package ro.digitallibrary.library.dto.request;

import java.util.Arrays;

public class RequestStatusCount {

    private RequestStatusDto[] requestStatus;
    private int count;

    public RequestStatusDto[] getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatusDto[] requestStatus) {
        this.requestStatus = requestStatus;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "RequestStatusCount{" +
                "requestStatus=" + Arrays.toString(requestStatus) +
                ", count=" + count +
                '}';
    }
}
