package ro.digitallibrary.library.dto.request;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractHistory;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@ApiModel("RequestHistoryDto")
public class RequestHistoryDto extends AbstractHistory<RequestDto> implements Serializable {

    private static final long serialVersionUID = -199597938563282417L;
    private Set<String> changes;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public RequestDto getState() {
        return state;
    }

    public void setState(RequestDto state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getChanged() {
        return changed;
    }

    public void setChanged(Set<String> changed) {
        this.changed = changed;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Set<String> getChanges() {
        return changes;
    }

    public void setChanges(Set<String> changes) {
        this.changes = changes;
    }

    @Override
    public String toString() {
        return "RequestHistoryDto [author=" + author + ", updatedOn=" + updatedOn + ", state=" + state + ", type="
                + type + ", changed=" + changed + ", changes=" + changes + ", version=" + version + "]";
    }
}
