package ro.digitallibrary.library.dto.request;

import org.bson.types.ObjectId;

import java.util.List;

public class RequestList {

    private String _id;
    private List<ObjectId> content;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<ObjectId> getContent() {
        return content;
    }

    public void setContent(List<ObjectId> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "RequestList{" +
                "_id='" + _id + '\'' +
                ", content=" + content +
                '}';
    }
}
