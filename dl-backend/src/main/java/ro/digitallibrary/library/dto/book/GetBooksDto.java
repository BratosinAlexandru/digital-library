package ro.digitallibrary.library.dto.book;

import io.swagger.annotations.ApiModel;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@ApiModel("GetModel")
public class GetBooksDto {

    private boolean disabledOnly;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate from;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate to;
    private List<String> domainIds;
    private List<String> categoryIds;
    private String name;
    private String authorName;
    private Long numberOfPages;
    private boolean allBooks;
    private Boolean myBooks;

    public boolean isDisabledOnly() {
        return disabledOnly;
    }

    public void setDisabledOnly(boolean disabledOnly) {
        this.disabledOnly = disabledOnly;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public List<String> getDomainIds() {
        return domainIds;
    }

    public void setDomainIds(List<String> domainIds) {
        this.domainIds = domainIds;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Long getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Long numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public boolean isAllBooks() {
        return allBooks;
    }

    public void setAllBooks(boolean allBooks) {
        this.allBooks = allBooks;
    }

    public Boolean getMyBooks() {
        return myBooks;
    }

    public void setMyBooks(Boolean myBooks) {
        this.myBooks = myBooks;
    }
}
