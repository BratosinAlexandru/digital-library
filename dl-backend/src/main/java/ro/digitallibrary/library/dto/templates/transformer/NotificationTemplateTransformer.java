package ro.digitallibrary.library.dto.templates.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.beans.templates.NotificationType;
import ro.digitallibrary.library.dto.templates.NotificationTemplateDto;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;
import ro.digitallibrary.library.persistence.repository.templates.NotificationTemplateRepository;

import java.util.Optional;

@Component
public class NotificationTemplateTransformer {

    @Autowired
    private NotificationTemplateRepository notificationTemplateRepository;

    @Autowired
    private NotificationTransformer notificationTransformer;

    public NotificationTemplate transformToEntity(NotificationTemplateDto dto) {
        NotificationTemplate entity = findById(dto.getId()).orElseGet(NotificationTemplate::new);
        BeanUtils.copyProperties(dto, entity);
        if (dto.getNotification() != null) {
            entity.setNotification(notificationTransformer.transformToEntity(dto.getNotification()));
        }
        if (dto.getType() != null && dto.getType().length() > 0) {
            entity.setType(NotificationType.valueOf(dto.getType()));
        }
        return entity;
    }

    public NotificationTemplateDto transformToDto(NotificationTemplate entity) {
        NotificationTemplateDto dto = new NotificationTemplateDto();
        BeanUtils.copyProperties(entity, dto);
        if (entity.getNotification() != null) {
            dto.setNotification(notificationTransformer.transformToDto(entity.getNotification()));
        }
        if (entity.getType() != null) {
            dto.setType(entity.getType().toString());
        }
        return dto;
    }

    public Optional<NotificationTemplate> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return notificationTemplateRepository.findById(id);
    }


}
