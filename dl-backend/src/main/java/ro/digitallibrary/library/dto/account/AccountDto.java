package ro.digitallibrary.library.dto.account;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("Account")
public class AccountDto extends AbstractDto {

    private static final long serialVersionUID = 4308140317965649856L;

    private String email;
    private String password;
    private String name;
    private String[] roles;
    private String profilePicture;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
