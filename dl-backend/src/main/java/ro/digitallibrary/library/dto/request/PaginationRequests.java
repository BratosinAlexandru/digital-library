package ro.digitallibrary.library.dto.request;

import java.util.List;

public class PaginationRequests {

    private Integer totalElements;
    private Integer pageNumber;
    private Integer pageSize;
    private List<RequestDto> content;

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<RequestDto> getContent() {
        return content;
    }

    public void setContent(List<RequestDto> content) {
        this.content = content;
    }
}
