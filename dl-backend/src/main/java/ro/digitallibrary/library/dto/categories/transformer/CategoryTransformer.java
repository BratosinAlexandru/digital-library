package ro.digitallibrary.library.dto.categories.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.categories.CategoryDto;
import ro.digitallibrary.library.dto.domains.transformer.DomainTransformer;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.persistence.entity.categories.Category;
import ro.digitallibrary.library.persistence.repository.categories.CategoryRepository;

import java.util.Optional;

@Component
public class CategoryTransformer extends AbstractTransformer<Category, CategoryDto> {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private DomainTransformer domainTransformer;

    @Override
    public Category transformToEntity(CategoryDto dto) {
        Category entity = findById(dto.getId()).orElseGet(Category::new);
        BeanUtils.copyProperties(dto, entity, "domain");
        if (dto.getDomain() != null) {
            entity.setDomain(domainTransformer.transformToEntity(dto.getDomain()));
        }
        return entity;
    }

    @Override
    public CategoryDto transformToDto(Category entity) {
        CategoryDto dto = new CategoryDto();
        BeanUtils.copyProperties(entity, dto, "domain");
        if (entity.getDomain() != null) {
            dto.setDomain(domainTransformer.transformToDto(entity.getDomain()));
        }
        return dto;
    }

    @Override
    protected Optional<Category> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return categoryRepository.findById(id);
    }
}
