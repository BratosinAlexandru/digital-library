package ro.digitallibrary.library.dto.request.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.request.RequestStatusDto;
import ro.digitallibrary.library.persistence.entity.request.RequestStatus;
import ro.digitallibrary.library.persistence.repository.request.RequestStatusRepository;

import java.util.Optional;

@Component
public class RequestStatusTransformer extends AbstractTransformer<RequestStatus, RequestStatusDto> {

    @Autowired
    private RequestStatusRepository requestStatusRepository;

    @Override
    public RequestStatus transformToEntity(RequestStatusDto dto) {
        RequestStatus entity = findById(dto.getId()).orElseGet(RequestStatus::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public RequestStatusDto transformToDto(RequestStatus entity) {
        RequestStatusDto dto = new RequestStatusDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<RequestStatus> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return requestStatusRepository.findById(id);
    }
}
