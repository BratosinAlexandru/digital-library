package ro.digitallibrary.library.dto.templates;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

import java.util.List;

@ApiModel("Notification")
public class NotificationDto extends AbstractDto {

    private static final long serialVersionUID = 4267573550281690104L;

    private String key;
    private List<String> emails;
    private Boolean enableToMain;
    private List<String> variables;
    private String name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public Boolean getEnableToMain() {
        return enableToMain;
    }

    public void setEnableToMain(Boolean enableToMain) {
        this.enableToMain = enableToMain;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NotificationDto{" +
                "key='" + key + '\'' +
                ", emails=" + emails +
                ", enableToMain=" + enableToMain +
                ", variables=" + variables +
                ", name='" + name + '\'' +
                '}';
    }
}
