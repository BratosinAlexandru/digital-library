package ro.digitallibrary.library.dto.generic;

import java.io.Serializable;

public abstract class AbstractDto implements Serializable {
    private static final long serialVersionUID = -2096847692115504939L;

    private String id;
    private boolean enabled = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
