package ro.digitallibrary.library.dto.request.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.book.transformer.BookTransformer;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.request.RequestDto;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RequestTransformer extends AbstractTransformer<Request, RequestDto> {

    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private CustomerTransformer customerTransformer;
    @Autowired
    private CommunicationChannelTransformer communicationChannelTransformer;
    @Autowired
    private RequestTypeTransformer requestTypeTransformer;
    @Autowired
    private RequestStatusTransformer requestStatusTransformer;
    @Autowired
    private BookTransformer bookTransformer;
    @Autowired
    private BookRepository bookRepository;

    @Override
    public Request transformToEntity(RequestDto dto) {
        Request entity = findById(dto.getId()).orElseGet(Request::new);
        BeanUtils.copyProperties(dto, entity, "createdOn", "updatedOn");

        if (dto.getCustomer() != null) {
            entity.setCustomer(customerTransformer.transformToEntity(dto.getCustomer()));
        } else {
            entity.setCustomer(null);
        }

        if (dto.getCommunicationChannels() != null) {
            entity.setCommunicationChannels(dto.getCommunicationChannels().stream().filter(Objects::nonNull)
                    .map(communicationChannelTransformer::transformToEntity).collect(Collectors.toList()));
        } else {
            entity.setCommunicationChannels(null);
        }

        if (dto.getRequestType() != null) {
            entity.setRequestType(dto.getRequestType().stream().filter(Objects::nonNull)
                    .map(requestTypeTransformer::transformToEntity).collect(Collectors.toList()));
        } else {
            entity.setRequestType(null);
        }

        if (dto.getRequestStatus() != null) {
            entity.setRequestStatus(requestStatusTransformer.transformToEntity(dto.getRequestStatus()));
        } else {
            entity.setRequestStatus(null);
        }

        if (dto.getBook() != null) {
            Optional<Book> book = bookRepository.findById(dto.getBook().getId());
            if (book.isPresent()) {
                entity.setBook(book.get());
            }
        } else {
            entity.setBook(null);
        }
        return entity;
    }

    @Override
    public RequestDto transformToDto(Request entity) {
        RequestDto dto = new RequestDto();
        BeanUtils.copyProperties(entity, dto);

        if (entity.getCustomer() != null) {
            dto.setCustomer(customerTransformer.transformToDto(entity.getCustomer()));
        } else {
            entity.setCustomer(null);
        }

        if (entity.getCommunicationChannels() != null) {
            dto.setCommunicationChannels(entity.getCommunicationChannels().stream().filter(Objects::nonNull)
                    .map(communicationChannelTransformer::transformToDto).collect(Collectors.toList()));
        } else {
            entity.setCommunicationChannels(null);
        }

        if (entity.getRequestType() != null) {
            dto.setRequestType(entity.getRequestType().stream().filter(Objects::nonNull)
                    .map(requestTypeTransformer::transformToDto).collect(Collectors.toList()));
        } else {
            entity.setRequestType(null);
        }

        if (entity.getRequestStatus() != null) {
            dto.setRequestStatus(requestStatusTransformer.transformToDto(entity.getRequestStatus()));
        } else {
            entity.setRequestStatus(null);
        }
        if (entity.getBook() != null) {
            dto.setBook(bookTransformer.toBookOverview(entity.getBook()));
        } else {
            entity.setBook(null);
        }

        return dto;
    }

    @Override
    protected Optional<Request> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return requestRepository.findById(id);
    }
}
