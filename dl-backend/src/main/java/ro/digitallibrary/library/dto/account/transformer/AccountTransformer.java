package ro.digitallibrary.library.dto.account.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;

import java.util.Objects;
import java.util.Optional;

@Component
public class AccountTransformer extends AbstractTransformer<Account, AccountDto> {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account transformToEntity(AccountDto dto) {
        Account entity = findById(dto.getId()).orElseGet(Account::new);
        BeanUtils.copyProperties(dto, entity, "password");
        if (dto.getPassword() != null && !Objects.equals(dto.getPassword(), entity.getPassword())) {
            entity.setPassword(dto.getPassword());
        }
        return entity;
    }

    @Override
    public AccountDto transformToDto(Account entity) {
        AccountDto dto = new AccountDto();
        BeanUtils.copyProperties(entity, dto, "password");
        return dto;
    }

    @Override
    protected Optional<Account> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return accountRepository.findById(id);
    }
}
