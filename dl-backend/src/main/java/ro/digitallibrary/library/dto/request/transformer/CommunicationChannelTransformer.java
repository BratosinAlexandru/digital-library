package ro.digitallibrary.library.dto.request.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.request.CommunicationChannelDto;
import ro.digitallibrary.library.persistence.entity.request.CommunicationChannel;
import ro.digitallibrary.library.persistence.repository.request.CommunicationChannelRepository;

import java.util.Optional;

@Component
public class CommunicationChannelTransformer extends AbstractTransformer<CommunicationChannel, CommunicationChannelDto> {

    @Autowired
    private CommunicationChannelRepository communicationChannelRepository;

    @Override
    public CommunicationChannel transformToEntity(CommunicationChannelDto dto) {
        CommunicationChannel entity = findById(dto.getId()).orElseGet(CommunicationChannel::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public CommunicationChannelDto transformToDto(CommunicationChannel entity) {
        CommunicationChannelDto dto = new CommunicationChannelDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<CommunicationChannel> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return communicationChannelRepository.findById(id);
    }
}
