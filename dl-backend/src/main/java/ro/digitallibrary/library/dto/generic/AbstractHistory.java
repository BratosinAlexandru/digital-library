package ro.digitallibrary.library.dto.generic;

import ro.digitallibrary.library.dto.request.RequestDto;

import java.time.LocalDateTime;
import java.util.Set;

public abstract class AbstractHistory<T> {

    protected String author;
    protected LocalDateTime updatedOn;
    protected RequestDto state;
    protected String type;
    protected Set<String> changed;
    protected long version;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public RequestDto getState() {
        return state;
    }

    public void setState(RequestDto state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getChanged() {
        return changed;
    }

    public void setChanged(Set<String> changed) {
        this.changed = changed;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
