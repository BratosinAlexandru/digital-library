package ro.digitallibrary.library.dto.generic.transformer;

import ro.digitallibrary.library.dto.generic.AbstractDto;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

import java.util.Optional;

/**
 * base class for entity - dto transformers
 * @param <T> - the entity
 * @param <X> - the dto
 */

public abstract class AbstractTransformer<T extends AbstractMongoEntity, X extends AbstractDto> {

    /**
     * transform the matching dto object to its entity
     * @param dto
     * @return the entity
     */
    public abstract T transformToEntity(X dto);

    /**
     * transform the matching entity to its dto
     * @param entity
     * @return the dto
     */
    public abstract X transformToDto(T entity);

    /**
     * find entity by id
     * @param id
     * @return the optional ( with entity )
     */
    protected abstract Optional<T> findById(String id);
}
