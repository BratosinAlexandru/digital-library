package ro.digitallibrary.library.dto.app.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.app.AppKeyDto;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.persistence.entity.app.AppKey;
import ro.digitallibrary.library.persistence.repository.app.AppKeyRepository;

import java.util.Optional;

@Component
public class AppKeyTransformer extends AbstractTransformer<AppKey, AppKeyDto> {
    @Autowired
    private AppKeyRepository appKeyRepository;

    @Override
    public AppKey transformToEntity(AppKeyDto dto) {
        AppKey entity = findById(dto.getId()).orElseGet(AppKey::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public AppKeyDto transformToDto(AppKey entity) {
        AppKeyDto dto = new AppKeyDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<AppKey> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return appKeyRepository.findById(id);
    }
}
