package ro.digitallibrary.library.dto.request;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;

@ApiModel("RequestType")
public class RequestTypeDto extends AbstractDto {

    private static final long serialVersionUID = 2626447385685093407L;

    private String name;
    private Boolean isDefaultValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDefaultValue() {
        return isDefaultValue;
    }

    public void setDefaultValue(Boolean defaultValue) {
        isDefaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return "ReuestTypeDto{" +
                "name='" + name + '\'' +
                ", isDefaultValue=" + isDefaultValue +
                '}';
    }
}
