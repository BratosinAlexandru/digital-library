package ro.digitallibrary.library.dto.request.transformer;

import org.javers.core.commit.CommitMetadata;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.core.metamodel.object.InstanceId;
import org.javers.shadow.Shadow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.book.transformer.BookTransformer;
import ro.digitallibrary.library.dto.generic.transformer.AbstractHistoryTransformer;
import ro.digitallibrary.library.dto.request.RequestDto;
import ro.digitallibrary.library.dto.request.RequestHistoryDto;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class RequestHistoryTransformer extends AbstractHistoryTransformer<RequestHistoryDto> {

    @Autowired
    private RequestTransformer requestTransformer;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookTransformer bookTransformer;


    @Override
    public RequestHistoryDto transformToHistory(Shadow<Request> shadow, List<CdoSnapshot> snapshots) {
        RequestHistoryDto history = new RequestHistoryDto();

        history.setState(requestTransformer.transformToDto(shadow.get()));
        CdoSnapshot mainCdoSnapshot = null;

        if (shadow.getCommitMetadata() != null) {
            CommitMetadata meta = shadow.getCommitMetadata();
            if (meta.getAuthor() != null) {
                history.setAuthor(meta.getAuthor());
            }
            if (meta.getCommitDate() != null) {
                history.setUpdatedOn(meta.getCommitDate());
            }
        }

        Set<String> changed = new HashSet<>();

        for (CdoSnapshot cdoSnapshot : snapshots) {
            if (cdoSnapshot.getManagedType().getBaseJavaType().getTypeName().equals(Request.class.getCanonicalName())) {
                history.setType(cdoSnapshot.getType().toString());
                history.setVersion(cdoSnapshot.getVersion());
                changed.addAll(cdoSnapshot.getChanged());
                mainCdoSnapshot = cdoSnapshot;
            }
        }
        history.setChanged(changed);
        addMissedProperties(history, mainCdoSnapshot);
        return history;
    }

    private void addMissedProperties(RequestHistoryDto history, CdoSnapshot snapshot) {
        Set<String> changed = history.getChanged();
        RequestDto state = history.getState();

        if (changed.contains("book") && state.getBook() == null) {
            InstanceId propertyType = (InstanceId) snapshot.getPropertyValue("book");
            Optional<Book> bookOpt = bookRepository.findById(propertyType.getCdoId().toString());
            if (bookOpt.isPresent()) {
                Book book = bookOpt.get();
                state.setBook(bookTransformer.toBookOverview(book));
            }
        }
    }
}
