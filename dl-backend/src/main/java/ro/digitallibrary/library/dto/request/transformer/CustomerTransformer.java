package ro.digitallibrary.library.dto.request.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.dto.generic.transformer.AbstractTransformer;
import ro.digitallibrary.library.dto.request.CustomerDto;
import ro.digitallibrary.library.persistence.entity.request.Customer;
import ro.digitallibrary.library.persistence.repository.request.CustomerRepository;

import java.util.Optional;

@Component
public class CustomerTransformer extends AbstractTransformer<Customer, CustomerDto> {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer transformToEntity(CustomerDto dto) {
        Customer entity = findById(dto.getId()).orElseGet(Customer::new);
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    @Override
    public CustomerDto transformToDto(Customer entity) {
        CustomerDto dto = new CustomerDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    protected Optional<Customer> findById(String id) {
        if (id == null) {
            return Optional.empty();
        }
        return customerRepository.findById(id);
    }
}
