package ro.digitallibrary.library.dto.request;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;
@ApiModel("CommunicationChannel")
public class CommunicationChannelDto extends AbstractDto {

    private static final long serialVersionUID = -1053954074185438386L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CommunicationChannelDto{" +
                "name='" + name + '\'' +
                '}';
    }
}
