package ro.digitallibrary.library.dto.generic.transformer;

import ro.digitallibrary.library.dto.generic.AbstractHistory;

import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.shadow.Shadow;
import ro.digitallibrary.library.persistence.entity.request.Request;

import java.util.List;

public abstract class AbstractHistoryTransformer<X extends AbstractHistory<?>> {

    public abstract X transformToHistory(Shadow<Request> shadow, List<CdoSnapshot> snapshots);
}
