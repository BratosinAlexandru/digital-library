package ro.digitallibrary.library.dto.book;

import io.swagger.annotations.ApiModel;
import ro.digitallibrary.library.dto.generic.AbstractDto;
import ro.digitallibrary.library.persistence.entity.book.Attachment;

import java.util.Arrays;
import java.util.List;

@ApiModel("Book")
public class BookDto extends AbstractDto {

    private static final long serialVersionUID = -4894070242730763114L;

    private String name;
    private String authorName;
    private long numberOfPages;
    private String categoryId;
    private String domainId;
    private byte[] image;
    private Integer from;
    private Integer to;
    private boolean authorRights;
    private List<Attachment> attachment;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public long getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(long numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public boolean isAuthorRights() {
        return authorRights;
    }

    public void setAuthorRights(boolean authorRights) {
        this.authorRights = authorRights;
    }

    public List<Attachment> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<Attachment> attachment) {
        this.attachment = attachment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BookDto{" +
                "name='" + name + '\'' +
                ", authorName='" + authorName + '\'' +
                ", numberOfPages=" + numberOfPages +
                ", categoryId='" + categoryId + '\'' +
                ", domainId='" + domainId + '\'' +
                ", image=" + Arrays.toString(image) +
                ", from=" + from +
                ", to=" + to +
                ", authorRights=" + authorRights +
                ", attachment=" + attachment +
                ", description='" + description + '\'' +
                '}';
    }
}
