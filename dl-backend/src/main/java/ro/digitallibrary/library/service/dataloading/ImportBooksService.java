package ro.digitallibrary.library.service.dataloading;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static ro.digitallibrary.library.service.dataloading.Constants.BOOKS;
import static ro.digitallibrary.library.service.dataloading.Constants.IMPORT_BOOKS_SCRIPT;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ImportBooksService {
    private static final Logger LOG = LogManager.getLogger(ImportBooksService.class);
    private static final String DATA_DIR ="data/";

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AppKeyService appKeyService;

    @PostConstruct
    private void loadBooks() throws ValidationException, ServiceException {
        if (appKeyService.hasScript(IMPORT_BOOKS_SCRIPT)) {
            LOG.info("[IMPORT Books SERVICE] Import should run once. It has been run before and it stopped");
            return;
        }

        bookRepository.deleteAll();
        List<Book> books = loadData(BOOKS, new TypeReference<List<Book>>() {
        });
        for (Book book : books) {
            bookRepository.save(book);
        }
        appKeyService.addScript(IMPORT_BOOKS_SCRIPT);
    }


    private <T>List<T> loadData(String resourceName, TypeReference<List<T>> typeReference) {
        ObjectMapper mapper = new ObjectMapper();
        Resource inputResource = new ClassPathResource(String.format("%s.json", DATA_DIR + resourceName));
        try {
            return mapper.readValue(inputResource.getInputStream(), typeReference);
        } catch (IOException e) {
            LOG.error("Error while loading resource: " + resourceName, e);
        }
        return Collections.emptyList();
    }

}
