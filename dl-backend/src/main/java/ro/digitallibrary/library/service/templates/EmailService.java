package ro.digitallibrary.library.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.config.DLProperties;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EmailService {
    private static final Logger LOG = LogManager.getLogger(EmailService.class);

    @Autowired
    public JavaMailSender emailSender;
    @Autowired
    private DLProperties digitalLibraryProps;

    public void sendHtmlMessage(String[] to, String htmlText, String subject)
            throws ValidationException, ServiceException {
        sendHtmlMessageWithCC(to, htmlText, subject, null);
    }

    public void sendHtmlMessageWithCC(String[] to, String htmlText, String subject, String[] cc)
            throws ValidationException, ServiceException {
        String from = digitalLibraryProps.getEmails().getFrom();
        LOG.info("[SERVICE] Send Notification Template Message from: " + from + " to: " + Arrays.toString(to)
                + " template: ");

        try {
            validateHtmlMail(from, to, htmlText, subject);
            to = getValidEmails(to);
            MimeMessage message = emailSender.createMimeMessage();
            message.setSubject(subject);
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setReplyTo(from);
            helper.setText(htmlText, true);
            if (cc != null) {
                helper.setCc(cc);
            }
            emailSender.send(message);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void validateHtmlMail(final String from, final String[] to, final String htmlText, final String subject)
            throws ValidationException {
        validateMail(from);
        if (to == null || to.length == 0) {
            throw new ValidationException("Notification templates has invalid to.");
        }
        if (htmlText == null || htmlText.trim().length() == 0) {
            throw new ValidationException("Email content is invalid.");
        }
        if (subject == null || subject.trim().length() == 0) {
            throw new ValidationException("Email subject is invalid.");
        }
    }

    private void validateMail(final String email) throws ValidationException {
        if (email == null || email.trim().length() == 0) {
            throw new ValidationException("Email is invalid.");
        }
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (Exception e) {
            throw new ValidationException("Email is invalid." + e.getMessage());
        }
    }

    private String[] getValidEmails(String[] to) {
        List<String> toList = new ArrayList<String>();
        for (String email : to) {
            try {
                validateMail(email);
                toList.add(email);
            } catch (ValidationException e) {
                LOG.error("[SERVICE] Validation exception occured", e);
            }
        }
        String[] array = new String[toList.size()];
        toList.toArray(array);
        return array;
    }
}
