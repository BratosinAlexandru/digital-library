package ro.digitallibrary.library.service.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import ro.digitallibrary.library.dto.request.RequestStatusDto;
import ro.digitallibrary.library.dto.request.transformer.RequestStatusTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.request.RequestStatus;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestStatusRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RequestStatusService {
    private static final Logger LOG = LogManager.getLogger(RequestStatusService.class);

    @Autowired
    private RequestStatusRepository requestStatusRepository;
    @Autowired
    private RequestStatusTransformer requestStatusTransformer;
    @Autowired
    private RequestRepository requestRepository;

    public List<RequestStatusDto> getRequestStatues(boolean includeDisabled) throws ServiceException {
        LOG.info("[SERVICE] Get request status " + includeDisabled);
        try {
            List<RequestStatus> requestStatuses = includeDisabled ? requestStatusRepository.findAll() :
                    requestStatusRepository.findByEnabledTrue();
            return requestStatuses == null ? null : requestStatuses.stream()
                    .map(requestStatusTransformer::transformToDto).collect(Collectors.toList());
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public RequestStatusDto createRequestStatus(RequestStatusDto requestStatus)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create request status");

        try {
            validate(requestStatus);
            updateUniqueValue(requestStatus);
            return requestStatusTransformer.transformToDto(
                    requestStatusRepository.save(requestStatusTransformer.transformToEntity(requestStatus)));

        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateRequestStatus(RequestStatusDto requestStatus) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update request status");
        try {
            validateUpdate(requestStatus);
            updateUniqueValue(requestStatus);
            requestStatusRepository.save(requestStatusTransformer.transformToEntity(requestStatus));

        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void deleteRequestStatus(String id) throws ValidationException {
        LOG.info("[SERVICE] Delete request status " + id);
        try {
            validateDelete(id);
            requestStatusRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        }
    }

    public RequestStatusDto getDefaultRequestStatus() throws ServiceException {
        LOG.info("[SERVICE] Find default request status");
        try {
            List<RequestStatus> requestStatuses = requestStatusRepository.findByIsDefaultValueTrue();
            if (requestStatuses != null && requestStatuses.size() > 0) {
                return requestStatusTransformer.transformToDto(requestStatuses.get(0));
            }
            return null;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public RequestStatus getRequestStatus(String name) {
        LOG.info("[SERVICE] Getting request status " + name);

        return requestStatusRepository.findByName(name);
    }

    private void updateUniqueValue(RequestStatusDto status) {
        if (status.getDefaultValue()) {
            List<RequestStatus> statuses = requestStatusRepository.findByIsDefaultValueTrue();
            if (statuses != null && statuses.size() > 0) {
                for (RequestStatus requestStatus : statuses) {
                    requestStatus.setDefaultValue(false);
                }
                requestStatusRepository.saveAll(statuses);
            }
        }
    }

    private void validateUpdate(final RequestStatusDto dto) throws ValidationException {
        validate(dto);
        if (dto.getId() == null || dto.getId().trim().length() == 0) {
            throw new ValidationException("Reseller status has invalid id.");
        }
    }

    private void validate(final RequestStatusDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Reseller status is null.");
        }
        if (dto.getName() == null || dto.getName().trim().length() == 0) {
            throw new ValidationException("Reseller status has invalid parameter name.");
        }
        if (dto.getDefaultValue() == null) {
            throw new ValidationException("Reseller status has invalid parameter IsDefaultValue.");
        }
    }

    private void validateDelete(final String resellerStatusId) throws ValidationException {
        if (resellerStatusId == null || resellerStatusId.trim().length() == 0) {
            throw new ValidationException("Reseller status id is null or invalid.");
        }
        if (requestRepository.countByRequestStatus(resellerStatusId) != 0) {
            throw new ValidationException("Reseller status is used into some request.");
        }
    }
}
