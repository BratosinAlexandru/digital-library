package ro.digitallibrary.library.service.request;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javers.core.Javers;
import org.javers.core.commit.CommitMetadata;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.JqlQuery;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.dto.request.*;
import ro.digitallibrary.library.dto.request.transformer.RequestHistoryTransformer;
import ro.digitallibrary.library.dto.request.transformer.RequestStatusTransformer;
import ro.digitallibrary.library.dto.request.transformer.RequestTransformer;
import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.entity.book.BorrowedBooks;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.entity.request.RequestStatus;
import ro.digitallibrary.library.persistence.repository.book.BorrowedBooksRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;
import ro.digitallibrary.library.service.account.AccountService;
import ro.digitallibrary.library.service.templates.SendNotificationService;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class RequestService {
    private static final Logger LOG = LogManager.getLogger(RequestService.class);

    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private RequestTransformer requestTransformer;
    @Autowired
    private RequestStatusService requestStatusService;
    @Autowired
    private Javers javers;
    @Autowired
    private SendNotificationService sendNotificationService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private RequestHistoryTransformer requestHistoryTransformer;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private BorrowedBooksRepository borrowedBooksRepository;
    @Autowired
    private RequestStatusTransformer requestStatusTransformer;


    public Optional<RequestDto> getRequest(String requestId) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get request by id: " + requestId);
        try {
            validateString(requestId, "requestId");
            return requestRepository.findById(requestId).map(requestTransformer::transformToDto);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public RequestDto createRequest(RequestDto request) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create request " + request.toString());
        try {
            validate(request);
            updateCustomer(request);
            putDefaults(request);
            RequestDto response = requestTransformer.transformToDto(
                    requestRepository.save(requestTransformer.transformToEntity(request)));
            List<String> to = getEmails();
            notifyRequestCreated(response, to);
            return response;
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateRequest(RequestDto request) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update request " + request.toString());
        try {
            validate(request);
            updateCustomer(request);
            requestRepository.save(requestTransformer.transformToEntity(request));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }


    public RequestPage getPagedObjects(PaginationRequestDto<FiltersRequest> paginationRequest, Authentication auth)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get paged objects");
        try {
            validatePagination(paginationRequest);
            RequestPage response = requestRepository.getCustomPagedObjects(paginationRequest, auth);
            return response;
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service validation occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void deleteAll(List<String> requestIds) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Delete all requests");
        try {
            validateArray(requestIds);
            requestRepository.deleteAll(requestIds.stream().map(id -> {
                Request request = new Request();
                request.setId(id);
                return request;
            }).collect(Collectors.toList()));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateEnable(List<String> requestIds, Boolean enabled) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update enabled for" + requestIds.toString());
        try {
            validateUpdateEnable(requestIds, enabled);
            List<Request> entities = (List<Request>) requestRepository.findAllById(requestIds);
            entities.forEach(request -> request.setEnabled(enabled));
            requestRepository.saveAll(entities);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void acceptRequests(List<String> requestIds) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Accepting requests: " + requestIds.toString());
        try {
            validateArray(requestIds);
            RequestStatus acceptedRequestStatus = requestStatusService.getRequestStatus("Confirmat");
            List<Request> entities =  (List<Request>) requestRepository.findAllById(requestIds);
            entities.forEach(request -> {
                request.setRequestStatus(acceptedRequestStatus);
                BorrowedBooks borrowedBook = new BorrowedBooks();
                borrowedBook.setAccountId(request.getAccountId());
                borrowedBook.setBookId(request.getBook().getId());
                borrowedBooksRepository.save(borrowedBook);
            });
            requestRepository.saveAll(entities);
        } catch (ValidationException ve) {
            LOG.info("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.info("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<RequestHistoryDto> getHistory(String requestId) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get request history: " + requestId);
        try {
            validateString(requestId, "requestId");
            return getHistoryData(requestId);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation service occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void updateCustomer(RequestDto request) throws ValidationException, ServiceException {
        CustomerDto customer = request.getCustomer();
        if (customer.getId() != null && customer.getId().length() > 0) {
            customer = customerService.updateCustomer(customer);
        } else {
            customer = customerService.createCustomer(customer);
        }
        request.setCustomer(customer);
    }

    private void putDefaults(final RequestDto request) throws ServiceException {
        if (!request.getBook().isAuthorRights()) {
            RequestStatusDto requestStatusDto = requestStatusTransformer
                    .transformToDto(requestStatusService.getRequestStatus("Confirmat"));
            request.setRequestStatus(requestStatusDto);

            BorrowedBooks borrowedBook = new BorrowedBooks();
            borrowedBook.setBookId(request.getBook().getId());
            borrowedBook.setAccountId(request.getAccountId());
            borrowedBook.setExpiryDate(request.getRequestDate().plusDays(30));
            borrowedBooksRepository.save(borrowedBook);
            return;
        }

        if (request.getRequestStatus() == null) {
            RequestStatusDto requestStatus = requestStatusService.getDefaultRequestStatus();
            request.setRequestStatus(requestStatus);
        }
    }

    private List<String> getEmails() {
        return accountService.getAccountsForNotification().stream().map(Account::getEmail).collect(Collectors.toList());
    }

    private void validate(final RequestDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Request is null");
        }
        if (dto.getBook() == null) {
            throw new ValidationException("Request has no book specified");
        }
        if (dto.getCustomer().getEmails() == null || dto.getCustomer().getPhones() == null) {
            throw new ValidationException("Request has no account information");
        }
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("Request has invalid " + name);
        }
    }

    private void validateArray(final List<String> requestIds) throws ValidationException {
        if (requestIds == null || requestIds.size() == 0) {
            throw new ValidationException("Request has invalid ids array");
        }
    }

    private void validateUpdateEnable(final List<String> requestIds, final Boolean enable) throws ValidationException {
        validateArray(requestIds);
        if (enable == null) {
            throw new ValidationException("Request has invalid enabled parameter");
        }
    }

    private Integer getCountElements(final PaginationRequestDto<FiltersRequest> paginationRequest, Authentication auth)
            throws ValidationException, RepositoryException {
        paginationRequest.setPageSize(1);
        paginationRequest.setPageNumber(0);
        RequestPage requestPage = requestRepository.getCustomPagedObjects(paginationRequest, auth);
        Integer count = requestPage.getTotalElements().get(0).getCount();
        if (count != null && count > 0) {
            return count;
        } else {
            return 0;
        }
    }

    private boolean checkIfUserIsAdmin() throws ValidationException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().size() == 0) {
            throw new ValidationException("Oauth token is invalid!");
        }
        return authentication.getAuthorities().stream().anyMatch(e -> e.getAuthority().equals("ROLE_ADMIN"));
    }

    private void notifyRequestCreated(RequestDto request, List<String> to) {
        try {
            sendNotificationService.sendNotificationRequestCreated(request, to);
        } catch (Exception e) {
            LOG.error("[SERVICE] Send email exception occurred", e);
        }
    }

    private void validatePagination(final PaginationRequestDto<FiltersRequest> paginationRequest) throws ValidationException {
        if (paginationRequest == null) {
            throw new ValidationException("Pagination is null");
        }
    }

    private List<RequestHistoryDto> getHistoryData(String requestId) {
        JqlQuery query = QueryBuilder.byInstanceId(requestId, Request.class).withChildValueObjects()
                .withNewObjectChanges().withScopeCommitDeep().withScopeDeepPlus().build();
        List<CdoSnapshot> snapshots = javers.findSnapshots(query);
        Map<CommitMetadata, List<CdoSnapshot>> mapSnapshots = snapshots.stream().collect(
                Collectors.groupingBy(CdoSnapshot::getCommitMetadata, LinkedHashMap::new, Collectors.toList()));
        Stream<Shadow<Request>> shadows = javers.findShadowsAndStream(query);
        return shadows.map(shadow -> requestHistoryTransformer.transformToHistory(shadow,
                mapSnapshots.get(shadow.getCommitMetadata()))).collect(Collectors.toList());

    }
}
