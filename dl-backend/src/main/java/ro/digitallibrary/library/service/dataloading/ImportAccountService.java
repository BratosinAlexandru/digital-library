package ro.digitallibrary.library.service.dataloading;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ImportAccountService {

    private static final Logger LOG = LogManager.getLogger(ImportAccountService.class);
    private static final String DATA_DIR = "data/";

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AppKeyService appKeyService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void loadAccounts() throws ValidationException, ServiceException {
        LOG.info("[IMPORT ACCOUNT SERVICE] Performing initial data import");
        if (appKeyService.hasScript(Constants.IMPORT_ACCOUNTS_SCRIPT)) {
            LOG.info("[IMPORT ACCOUNT SERVICE] Import should run once. it has been run before!");
            return;
        }

        initAdminAccount();
        appKeyService.addScript(Constants.IMPORT_ACCOUNTS_SCRIPT);
    }

    private void initAdminAccount() {
        List<Account> accounts = loadData("adminaccount", new TypeReference<List<Account>>() {});
        accounts.forEach(account -> {
            Optional<Account> accountRep = accountRepository.findByEmailIgnoreCase(account.getEmail());
            if (!accountRep.isPresent()) {
                account.setPassword(encodePassword(account.getPassword()));
                accountRepository.save(account);
            }
        });
    }

    private String encodePassword(String rawPassword) {
        String encodedPassword = passwordEncoder.encode(rawPassword);
        return encodedPassword.replace("{sha256}", "");
    }

    private <T> List<T> loadData(String resourceName, TypeReference<List<T>> typeReference) {
        ObjectMapper mapper = new ObjectMapper();
        Resource inputResource = new ClassPathResource(String.format("%s.json", DATA_DIR + resourceName));

        try {
            return mapper.readValue(inputResource.getInputStream(), typeReference);
        } catch (IOException io) {
            LOG.error("[IMPORT ACCOUNT SERVICE] Error while loading resource: " + resourceName, io);
        }
        return Collections.emptyList();
    }
}
