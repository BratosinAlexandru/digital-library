package ro.digitallibrary.library.service.categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.categories.CategoryDto;
import ro.digitallibrary.library.dto.categories.transformer.CategoryTransformer;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.categories.Category;
import ro.digitallibrary.library.persistence.repository.categories.CategoryRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService {
    private static final Logger LOG = LogManager.getLogger(CategoryService.class);

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryTransformer categoryTransformer;
    @Autowired
    private AppKeyService appKeyService;

    public List<CategoryDto> getCategories(boolean includeDisabled) {
        LOG.info("[SERVICE] Getting categories by include disabled: " + includeDisabled);
        List<Category> categories = includeDisabled ? categoryRepository.findAll() : categoryRepository.findByEnabledTrue();
        return categories.stream().map(categoryTransformer::transformToDto).collect(Collectors.toList());
    }

    public List<CategoryDto> getCategoriesByDomain(List<String> domainIds, boolean includeDisabled) {
        LOG.info("[SERVICE] Getting categories by domains and include disabled");
        List<Category> categories = new ArrayList<>();
        domainIds.forEach(domainId -> categories.addAll(includeDisabled ? categoryRepository.findByDomain(domainId)
                : categoryRepository.findByDomainAndEnabledTrue(domainId)));
        return categories.stream().map(categoryTransformer::transformToDto).collect(Collectors.toList());
    }

    public CategoryDto getCategory(String categoryId) {
        LOG.info("[SERVICE] Getting category by id " + categoryId);
        return categoryRepository.findById(categoryId).map(categoryTransformer::transformToDto)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public CategoryDto createCategory(CategoryDto categoryDto) {
        LOG.info("[SERVICE] Creating category" + categoryDto.toString());
        return categoryTransformer.transformToDto(categoryRepository.save(categoryTransformer.transformToEntity(categoryDto)));
    }

    public void updateCategory(CategoryDto categoryDto) {
        LOG.info("[SERVICE] Update category" + categoryDto.getName());
        categoryRepository.save(categoryTransformer.transformToEntity(categoryDto));
    }

    public void deleteCategory(String id) throws ValidationException {
        try {
            validateDelete(id);
            categoryRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred" + ve);
            throw ve;
        }
    }

    private void validateDelete(final String id) throws ValidationException {
        if (id == null || id.trim().length() == 0) {
            throw new ValidationException("Id is null or invalid");
        }

    }
}
