package ro.digitallibrary.library.service.templates;

public interface Constants {

    public static final String ACCOUNT_LOGIN_URL = "accountLoginUrl";
    public static final String ACCOUNT_FORGOT_PASSWORD_URL = "accountForgotPasswordUrl";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String APP_TITLE = "appTitle";

    public static final String RESET_PASSWORD_EMAIL = "resetPassword_email";

    public static final String RO = "RO";
    public static final String EN = "EN";

    public static final String LOGIN = "/#/login";
    public static final String REQUESTS = "/#/requests/";
    public static final String FORGOT_PASSWORD = "/#/login";
}
