package ro.digitallibrary.library.service.dataloading;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.request.CommunicationChannel;
import ro.digitallibrary.library.persistence.entity.request.RequestStatus;
import ro.digitallibrary.library.persistence.entity.request.RequestType;
import ro.digitallibrary.library.persistence.repository.request.CommunicationChannelRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestStatusRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestTypeRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import static ro.digitallibrary.library.service.dataloading.Constants.*;

import javax.annotation.PostConstruct;
import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ImportRequestUtilsService {

    private static final Logger LOG = LogManager.getLogger(ImportRequestUtilsService.class);
    private static final String DATA_DIR ="data/";

    @Autowired
    private CommunicationChannelRepository communicationChannelRepository;
    @Autowired
    private RequestTypeRepository requestTypeRepository;
    @Autowired
    private RequestStatusRepository requestStatusRepository;
    @Autowired
    private AppKeyService appKeyService;

    @PostConstruct
    private void importInitialData() throws ValidationException, ServiceException {
        LOG.info("[IMPORT REQUEST UTILS SERVICE] Performing initial data import");

        try {
            if (appKeyService.hasScript(IMPORT_REQUESTS_SCRIPT)) {
                LOG.info("[IMPORT REQUEST UTILS SERVICE] Import should run once. It has been run before and it stopped");
                return;
            }
            importCommunicationChannels();
            importRequestTypes();
            importRequestStatues();
            appKeyService.addScript(IMPORT_REQUESTS_SCRIPT);
        } catch (ValidationException ve) {
            LOG.error("[IMPORT REQUEST UTILS SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[IMPORT REQUEST UTILS SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void importCommunicationChannels() throws IOException {
        LOG.info("[IMPORT REQUEST UTILS SERVICE] Performing initial import for Communication Channels");
        communicationChannelRepository.deleteAll();

        Map<String, CommunicationChannel> sources = loadData(COMMUNICATION_CHANNELS, new TypeReference<Map<String, CommunicationChannel>>() {
        });
        if (sources != null) {
            sources.forEach((key, value) -> {
                communicationChannelRepository.save(value);
            });
        }
    }

    private void importRequestTypes() throws IOException {
        LOG.info("[IMPORT REQUEST UTILS SERVICE] Performing initial import for Request Types");
        requestTypeRepository.deleteAll();

        Map<String, RequestType> sources = loadData(REQUEST_TYPE, new TypeReference<Map<String, RequestType>>() {
        });
        if (sources != null) {
            sources.forEach((key, value) -> {
                requestTypeRepository.save(value);
            });
        }
    }

    private void importRequestStatues() throws IOException {
        LOG.info("[IMPORT REQUEST UTILS SERVICE] Performing initial import for Request Statuses");
        requestStatusRepository.deleteAll();

        Map<String, RequestStatus> sources = loadData(REQUEST_STATUS, new TypeReference<Map<String, RequestStatus>>() {
        });
        if (sources != null) {
            sources.forEach((key, value) -> {
                requestStatusRepository.save(value);
            });
        }
    }

    private <K,V>Map<K, V> loadData(String resourceName, TypeReference<Map<K, V>> typeReference) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Resource inputResource = new ClassPathResource(String.format("%s.json", DATA_DIR + resourceName));
        return mapper.readValue(inputResource.getInputStream(), typeReference);
    }
}
