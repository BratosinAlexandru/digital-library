package ro.digitallibrary.library.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.beans.templates.NotificationType;
import ro.digitallibrary.library.dto.templates.NotificationTemplateDto;
import ro.digitallibrary.library.dto.templates.transformer.NotificationTemplateTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;
import ro.digitallibrary.library.persistence.repository.templates.NotificationTemplateRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class NotificationTemplateService {
    private static final Logger LOG = LogManager.getLogger(NotificationTemplateService.class);

    @Autowired
    private NotificationTemplateRepository notificationTemplateRepository;
    @Autowired
    private NotificationTemplateTransformer notificationTemplateTransformer;

    public Map<String, List<NotificationTemplateDto>> getTemplatesByNotificationId(String key) throws ServiceException {
        LOG.info("[SERVICE] Get all notification templates by key: " + key);
        try {
            validateString("key", "key");
            Notification notification = new Notification();
            notification.setId(key);
            List<NotificationTemplate> notificationTemplates = notificationTemplateRepository.findByNotification(notification);
            if (notificationTemplates == null || notificationTemplates.size() == 0) {
                return null;
            } else {
                return notificationTemplates.stream().map(notificationTemplateTransformer::transformToDto)
                        .collect(Collectors.groupingBy(NotificationTemplateDto::getType));
            }
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateNotificationTemplate(NotificationTemplateDto notificationTemplateDto)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update notification templates " + notificationTemplateDto.toString());
        try {
            validateUpdate(notificationTemplateDto);
            NotificationTemplate template = notificationTemplateTransformer.transformToEntity(notificationTemplateDto);
            updateUniqueValue(template);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public NotificationTemplateDto createTemplate(NotificationTemplateDto notificationTemplateDto)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create notification templates " + notificationTemplateDto.toString());
        try {
            validate(notificationTemplateDto);
            NotificationTemplate template = notificationTemplateTransformer.transformToEntity(notificationTemplateDto);
            updateWithDefaults(template);
            return notificationTemplateTransformer.transformToDto(notificationTemplateRepository.save(template));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void deleteTemplate(String id) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Delete notification templates " + id);
        try {
            validateString(id, "id");
            notificationTemplateRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public NotificationTemplate getNotificationTemplate(Notification notification, NotificationType type, String lang)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get notification : " + notification.getId() + " and Language" + lang + " type: " + type);
        try {
            validateGetNotificationTemplate(notification, type);
            NotificationTemplate template = null;
            if (lang != null) {
                template = notificationTemplateRepository
                        .findOneByNotificationAndTypeAndLanguageAndEnabledTrue(notification, type, lang);
            }
            if (template == null) {
                template = notificationTemplateRepository
                        .findOneByNotificationAndTypeAndIsDefaultTrueAndEnabledTrue(notification, type);
            }
            return template;
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }

    }

    public List<String> getTypes() {
        LOG.info("[SERVICE] Get notification templates types");
        return Stream.of(NotificationType.values()).map(NotificationType::name).collect(Collectors.toList());
    }

    private void updateUniqueValue(NotificationTemplate template) throws ValidationException {
        List<NotificationTemplate> templates = notificationTemplateRepository
                .findByNotificationAndTypeAndIsDefaultTrue(template.getNotification(), template.getType());
        if (templates != null && templates.size() > 0) {
            for (NotificationTemplate t : templates) {
                t.setIsDefault(false);
            }
            notificationTemplateRepository.saveAll(templates);
        }
    }

    private void updateWithDefaults(NotificationTemplate template) throws ValidationException {
        List<NotificationTemplate> templates = notificationTemplateRepository
                .findByNotificationAndTypeAndIsDefaultTrue(template.getNotification(), template.getType());
        if (templates != null && templates.size() > 1) {
            throw new ValidationException("Notification templates has multiple defauls ");
        } else if (templates != null && templates.size() == 1) {
            NotificationTemplate t = templates.get(0);
            template.setContent(t.getContent());
            template.setSubject(t.getSubject());
            template.setIsDefault(false);
        } else {
            template.setIsDefault(true);
        }
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("Notification templates has invalid " + name);
        }
    }

    private void validateUpdate(final NotificationTemplateDto dto) throws ValidationException {
        validate(dto);
        if (dto.getId() == null || dto.getId().trim().length() == 0) {
            throw new ValidationException("Notification templates has invalid id");
        }
    }

    private void validateGetNotificationTemplate(final Notification notification, final NotificationType type)
            throws ValidationException {
        if (notification == null || notification.getId().trim().length() == 0) {
            throw new ValidationException("Notification has invalid id.");
        }
        if (type == null) {
            throw new ValidationException("Notification templates has invalid type.");
        }
    }


    private void validate(final NotificationTemplateDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Notification templates is null");
        }
        if (dto.getNotification() == null) {
            throw new ValidationException("Notification templates has invalid parameter Notification");
        }
        if (dto.getLanguage() == null || dto.getLanguage().trim().length() == 0) {
            throw new ValidationException("Notification templates has invalid parameter Language");
        }
    }
}
