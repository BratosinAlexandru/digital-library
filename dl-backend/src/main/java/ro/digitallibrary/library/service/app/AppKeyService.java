package ro.digitallibrary.library.service.app;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.dto.app.AppKeyDto;
import ro.digitallibrary.library.dto.app.transformer.AppKeyTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.app.AppKey;
import ro.digitallibrary.library.persistence.repository.app.AppKeyRepository;
import ro.digitallibrary.library.service.dataloading.Constants;

@Service
public class AppKeyService {
    private static final Logger LOG = LogManager.getLogger(AppKeyService.class);

    @Autowired
    private AppKeyRepository appKeyRepository;
    @Autowired
    private AppKeyTransformer appKeyTransformer;

    @Transactional
    public List<AppKeyDto> getAppKeys() {
        LOG.info("[SERVICE] Get app keys");
        List<AppKey> appKeys = appKeyRepository.findAll();
        return appKeys.stream().map(appKeyTransformer::transformToDto).collect(Collectors.toList());
    }

    @Transactional
    public void addAppKey(AppKeyDto appKeyDto) throws ServiceException {
        LOG.info("[SERVICE] Create app key: " + appKeyDto.toString());

        try {
            appKeyRepository.save(appKeyTransformer.transformToEntity(appKeyDto));
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
        }
    }

    @Transactional
    public void updateAppKey(AppKeyDto appKey) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update app key: " + appKey.toString());
        try {
            validateUpdate(appKey);
            appKeyRepository.save(appKeyTransformer.transformToEntity(appKey));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public AppKeyDto findByKey(String key) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Find app key: " + key);
        try {
            validateString(key, "key");
            AppKey entity = appKeyRepository.findOneByKey(key);
            return entity != null ? appKeyTransformer.transformToDto(entity) : null;
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public boolean hasScript(String script) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Find app script: " + script);
        try {
            validateString(script, "script");
            AppKey key = appKeyRepository.findOneByKey(Constants.ALL_RUNNED_SCRIPTS);
            if (key != null && key.getValue() != null) {
                @SuppressWarnings("unchecked")
                List<String> scripts = (List<String>) key.getValue();
                return scripts.contains(script);
            }
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
        return false;
    }

    public void addScript(String script) throws ValidationException, ServiceException {
        try {
            validateString(script, "script");
            AppKey entity = appKeyRepository.findOneByKey(Constants.ALL_RUNNED_SCRIPTS);
            if (entity == null) {
                entity = new AppKey();
                entity.setKey(Constants.ALL_RUNNED_SCRIPTS);
            }
            @SuppressWarnings("unchecked")
            List<String> value = (List<String>) entity.getValue();
            if (value == null) {
                value = new ArrayList<String>();
            }
            if (value.size() == 0 || !value.contains(script)) {
                value.add(script);
            }
            entity.setValue(value);
            appKeyRepository.save(entity);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void validateUpdate(final AppKeyDto dto) throws ValidationException {
        validateString(dto.getId(), "id");
        validateString(dto.getKey(), "key");
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("App key has invalid " + name);
        }
    }
}
