package ro.digitallibrary.library.service.book;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.transaction.annotation.Transactional;
import com.mongodb.client.gridfs.model.GridFSFile;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.book.Attachment;
import ro.digitallibrary.library.persistence.repository.book.AttachmentRepository;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class AttachmentService {
    private static final Logger LOG = LogManager.getLogger(AttachmentService.class);
    private static final String ID = "_id";

    @Autowired
    private GridFsTemplate gridFsTemplate;
    @Autowired
    private AttachmentRepository attachmentRepository;

    @Transactional
    public GridFsResource find(String id) throws Exception {
        LOG.info("[SERVICE] Download source with id: " + id);

        try {
            validateId(id);
            GridFSFile file = gridFsTemplate.findOne(Query.query(Criteria.where(ID).is(id)));
            validateFile(file);
            return gridFsTemplate.getResource(file);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public Attachment upload(MultipartFile file) throws Exception {
        LOG.info("[SERVICE] Upload source with name: " + file.getOriginalFilename());

        try {
            validateFile(file);
//            getContents(file);

            String id = gridFsTemplate
                    .store(file.getInputStream(), file.getOriginalFilename(), file.getContentType()).toHexString();
            Document thumb = null;
            if (file.getContentType().contains("image")) {
                thumb = generateThumb(file.getInputStream());
            }
            Attachment attachment = new Attachment();
            attachment.setId(id);
            attachment.setContentType(file.getContentType());
            attachment.setFilename(file.getOriginalFilename());
            attachment.setMetadata(thumb);
            return attachment;
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public void deleteAttachment(List<String> ids) throws Exception {
        LOG.info("[SERVICE] Delete file ids: " + ids);

        try {
            for (String id : ids) {
                validateId(id);
            }
            gridFsTemplate.delete(Query.query(Criteria.where(ID).in(ids)));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private Document generateThumb(InputStream inputStream) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedImage thumb = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        thumb.createGraphics().drawImage(ImageIO.read(inputStream).getScaledInstance(100, 100, Image.SCALE_SMOOTH), 0,
                0, null);
        ImageIO.write(thumb, "jpg", baos);
        String encoded = Base64.getEncoder().encodeToString(baos.toByteArray());
        return new Document("thumb", encoded);
    }

    private void validateId(final String id) throws ValidationException {
        if (id == null) {
            throw new ValidationException("Id is null");
        }
        if (id.trim().length() == 0) {
            throw new ValidationException("Id length is 0");
        }
    }

    private void validateFile(final GridFSFile file) throws ValidationException {
        if (file == null) {
            throw new ValidationException("File can't be found");
        }
    }

    private void validateFile(final MultipartFile file) throws ValidationException {
        if (file == null) {
            throw new ValidationException("File is null");
        }
        if (file.getName().trim().length() == 0) {
            throw new ValidationException("File name is missing");
        }
        if (file.getContentType().trim().length() == 0) {
            throw new ValidationException("File content type is missing");
        }
        String type = file.getContentType().trim().toLowerCase();
        if (!type.contains("pdf")) {
            throw new ValidationException("File format type is wrong. Accept only PDF");
        }
        if (file.getSize() > 5242880) {
            throw new ValidationException("File size is greater than 5MB");
        }
    }

    private void getContents(MultipartFile file) throws ServiceException {
        try {
            PDDocument document = PDDocument.load(file.getInputStream());
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            pdfTextStripper.setLineSeparator("\n");
            pdfTextStripper.setStartPage(5);
            pdfTextStripper.setEndPage(5);
            String text = pdfTextStripper.getText(document);

            System.out.println(text);

//            String[] formatted = text.split("(\\d+\\.\\d+)\\s[a-zA-Z \\:\\d+[a-zA-Z \\(\\)\\’]]+\\D+\\d+\\s+");
//
//            String[] formatted = text.split("\\d+\\s[[A-Za-z] (FAQ:)\\?\\!\\(\\)]+\\d+\\n");
//            System.out.println(Arrays.toString(formatted));
//
//            final String regex = "\\d+\\s[[A-Za-z] (FAQ:)\\?\\!\\(\\)]+\\d+\\n";
//
//            final Pattern pattern = Pattern.compile(regex);
//            final Matcher matcher = pattern.matcher(text);
//
//            while (matcher.find()) {
//                System.out.println("Full match: " + matcher.group(0));
//
//                for (int i = 1; i <= matcher.groupCount(); i++) {
//                    System.out.println("Group " + i + ": " + matcher.group(i));
//                }
//            }

            document.close();
        } catch (Exception e) {
            LOG.error("Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

}
