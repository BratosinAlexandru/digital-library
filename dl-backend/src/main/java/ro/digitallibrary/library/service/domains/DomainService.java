package ro.digitallibrary.library.service.domains;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.domains.DomainDto;
import ro.digitallibrary.library.dto.domains.transformer.DomainTransformer;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.categories.Category;
import ro.digitallibrary.library.persistence.entity.domains.Domain;
import ro.digitallibrary.library.persistence.repository.categories.CategoryRepository;
import ro.digitallibrary.library.persistence.repository.domains.DomainRepository;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DomainService {
    private static final Logger LOG = LogManager.getLogger(DomainService.class);

    @Autowired
    private DomainRepository domainRepository;
    @Autowired
    private DomainTransformer domainTransformer;
    @Autowired
    private CategoryRepository categoryRepository;

    public List<DomainDto> findByEnabledTrue(boolean includeDisabled) {
        LOG.info("[SERVICE] Finding domains by disabled: " + includeDisabled);
        List<Domain> domains = includeDisabled
                ? domainRepository.findAll() : domainRepository.findByEnabledTrue();
        return domains.stream().map(domainTransformer::transformToDto).collect(Collectors.toList());
    }

    public DomainDto createDomain(DomainDto domainDto) {
        LOG.info("[SERVICE] Creating domain: " + domainDto.toString());
        return domainTransformer.transformToDto(domainRepository.save(domainTransformer.transformToEntity(domainDto)));
    }

    public void deleteDomain(String id) throws ValidationException {
        LOG.info("[SERVICE] Delete domain: " + id);
        try {
            validateDeleteDomain(id);
            domainRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred" + ve);
            throw ve;
        }
    }

    public DomainDto updateDomain(DomainDto domainDto) {
        LOG.info("[SERVICE] Update domain");
        List<Category> categories = categoryRepository.findByDomain(domainDto.getId());
        categories.forEach(category -> category.setEnabled(domainDto.isEnabled()));
        categoryRepository.saveAll(categories);
        return domainTransformer.transformToDto(domainRepository.save(domainTransformer.transformToEntity(domainDto)));
    }

    private void validateDeleteDomain(final String domainId) throws ValidationException {
        if (domainId.trim().length() == 0) {
            throw new ValidationException("Domain id is null or invalid");
        }
    }
}
