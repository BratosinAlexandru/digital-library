package ro.digitallibrary.library.service.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.dto.request.CommunicationChannelDto;
import ro.digitallibrary.library.dto.request.transformer.CommunicationChannelTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.request.CommunicationChannel;
import ro.digitallibrary.library.persistence.repository.request.CommunicationChannelRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommunicationChannelService {

    private static final Logger LOG = LogManager.getLogger(CommunicationChannelService.class);

    @Autowired
    private CommunicationChannelRepository communicationChannelRepository;
    @Autowired
    private CommunicationChannelTransformer communicationChannelTransformer;
    @Autowired
    private RequestRepository requestRepository;

    @Transactional
    public List<CommunicationChannelDto> getCommunicationChannels(boolean includeDisabled) throws ServiceException {
        LOG.info("[SERVICE] Get communication channels " + includeDisabled);

        try {
            List<CommunicationChannel> communicationChannels = includeDisabled
                    ? communicationChannelRepository.findAll()
                    : communicationChannelRepository.findByEnabledTrue();

            return communicationChannels == null ? null
                    : communicationChannels.stream().map(communicationChannelTransformer::transformToDto)
                        .collect(Collectors.toList());
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred ", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public CommunicationChannelDto createCommunicationChannel(CommunicationChannelDto communicationChannel)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create communication channel");
        try {
            validate(communicationChannel);
            return communicationChannelTransformer.transformToDto(
                    communicationChannelRepository.save(
                            communicationChannelTransformer.transformToEntity(communicationChannel)));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public void deleteCommunicationChannel(String id) throws ValidationException {
        LOG.info("[SERVICE] Delete communication channel with id" + id);
        try {
            validateDelete(id);
            communicationChannelRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        }

    }

    @Transactional
    public void updateCommunicationChannel(CommunicationChannelDto communicationChannel)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update communication channel");
        try {
            validateUpdate(communicationChannel);
            communicationChannelRepository.save(
                    communicationChannelTransformer.transformToEntity(communicationChannel));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void validate(final CommunicationChannelDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Communication channel is null");
        }
        if (dto.getName() == null || dto.getName().trim().length() == 0) {
            throw new ValidationException("Communication channel has invalid parameter name.");
        }
    }

    private void validateDelete(final String communicationChannelId) throws ValidationException {
        if (communicationChannelId == null || communicationChannelId.trim().length() == 0) {
            throw new ValidationException("Communication channel id is null or invalid.");
        }
        if (requestRepository.countByCommunicationChannels(communicationChannelId) != 0) {
            throw new ValidationException("Communication channel is used into some request.");
        }
    }

    private void validateUpdate(final CommunicationChannelDto dto) throws ValidationException {
        validate(dto);
        if (dto.getId() == null || dto.getId().trim().length() == 0) {
            throw new ValidationException("Communication channel has invalid id.");
        }
    }
}
