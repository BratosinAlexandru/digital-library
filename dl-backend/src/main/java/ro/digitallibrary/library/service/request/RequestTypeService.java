package ro.digitallibrary.library.service.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.request.RequestTypeDto;
import ro.digitallibrary.library.dto.request.transformer.RequestTypeTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.request.RequestType;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;
import ro.digitallibrary.library.persistence.repository.request.RequestTypeRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RequestTypeService {

    private static final Logger LOG = LogManager.getLogger(RequestTypeService.class);

    @Autowired
    private RequestTypeRepository requestTypeRepository;
    @Autowired
    private RequestTypeTransformer requestTypeTransformer;
    @Autowired
    private RequestRepository requestRepository;

    public List<RequestTypeDto> getRequestTypes(boolean includeDisabled) throws ServiceException {
        LOG.info("[SERVICE] Get request types: " + includeDisabled);

        try {
            List<RequestType> requestTypes = includeDisabled
                    ? requestTypeRepository.findAll()
                    : requestTypeRepository.findByEnabledTrue();
            return requestTypes.stream().map(requestTypeTransformer::transformToDto).collect(Collectors.toList());
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public RequestTypeDto createRequestType(RequestTypeDto requestType) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create request type");

        try {
            validate(requestType);
            updateUniqueValue(requestType);
            return requestTypeTransformer.transformToDto(
                    requestTypeRepository.save(requestTypeTransformer.transformToEntity(requestType)));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateRequestType(RequestTypeDto requestType) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update request type");
        try {
            validateUpdate(requestType);
            updateUniqueValue(requestType);
            requestTypeRepository.save(requestTypeTransformer.transformToEntity(requestType));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void deleteRequestType(String id) throws ValidationException {
        LOG.info("[SERVICE] Delete request type");
        try {
            validateDelete(id);
            requestTypeRepository.deleteById(id);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        }
    }

    private void validate(final RequestTypeDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Request type is null.");
        }
        if (dto.getName() == null || dto.getName().trim().length() == 0) {
            throw new ValidationException("Request type has invalid parameter name.");
        }
    }

    private void validateUpdate(final RequestTypeDto dto) throws ValidationException {
        validate(dto);
        if (dto.getId() == null || dto.getId().trim().length() == 0) {
            throw new ValidationException("Request type has invalid id");
        }
    }

    private void updateUniqueValue(RequestTypeDto requestTypeDto) {
        if (requestTypeDto.getDefaultValue()) {
            List<RequestType> requestTypes = requestTypeRepository.findByIsDefaultValueTrue();
            if (requestTypes != null && requestTypes.size() > 0) {
                for (RequestType requestType : requestTypes) {
                    requestType.setDefaultValue(false);
                }
                requestTypeRepository.saveAll(requestTypes);
            }
        }
    }

    private void validateDelete(final String requestTypeId) throws ValidationException {
        if (requestTypeId == null || requestTypeId.trim().length() == 0) {
            throw new ValidationException("Request type id is null or invalid.");
        }

        if (requestRepository.countByRequestType(requestTypeId) != 0) {
            throw new ValidationException("Request type id is used into some request.");
        }
    }
}
