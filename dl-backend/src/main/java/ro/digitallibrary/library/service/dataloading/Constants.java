package ro.digitallibrary.library.service.dataloading;

public interface Constants {
    static final String ALL_RUNNED_SCRIPTS = "ALL_RUNNED_SCRIPTS";
    static final String IMPORT_ACCOUNTS_SCRIPT = "IMPORT_ACCOUNTS_SCRIPT";
    static final String IMPORT_NOTIFICATION_SCRIPT = "IMPORT_NOTIFICATION_SCRIPT";
    static final String NOTIFICATIONS = "notifications";
    static final String NOTIFICATION_LANGUAGES = "notification_languages";
    static final String IMPORT_CATEGORIES_SCRIPT = "IMPORT_CATEGORIES_SCRIPT";
    static final String IMPORT_BOOKS_SCRIPT = "IMPORT_BOOKS_SCRIPT";
    static final String BOOKS = "books";
    static final String IMPORT_REQUESTS_SCRIPT = "IMPORT_REQUESTS_SCRIPT";
    static final String COMMUNICATION_CHANNELS = "communication_channel";
    static final String REQUEST_STATUS = "request_status";
    static final String REQUEST_TYPE = "request_type";
}
