package ro.digitallibrary.library.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.beans.templates.NotificationKey;
import ro.digitallibrary.library.beans.templates.NotificationType;
import ro.digitallibrary.library.config.DLProperties;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.request.RequestDto;
import ro.digitallibrary.library.dto.request.RequestTypeDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;
import ro.digitallibrary.library.persistence.repository.templates.NotificationRepository;


import java.util.*;
import java.util.stream.Collectors;

import static ro.digitallibrary.library.service.templates.Constants.*;

@Service
@Transactional
public class SendNotificationService {
    private static final Logger LOG = LogManager.getLogger(SendNotificationService.class);

    @Autowired
    public EmailService emailService;
    @Autowired
    public NotificationTemplateService notificationTemplateService;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private DLProperties digitalLibraryProps;

    public void sendResetPasswordEmailAccount(AccountDto accountDto) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Send reset password email account: email: " + accountDto.getEmail() + " newPassword: "
                + accountDto.getPassword() + ".");

        try {
            validateAccount(accountDto);
            Notification notification = notificationRepository.findOneByKey(NotificationKey.RESET_PASSWORD);
            if (!notification.isEnabled()) {
                return;
            }
            String language = "ro";
            Map<String, String> properties = getSistemVariables();
            properties.put(NEW_PASSWORD, accountDto.getPassword());
            List<String> toEmails = Arrays.asList(accountDto.getEmail());

            sendEmail(notification, properties, null, toEmails);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void sendNotificationRequestCreated(RequestDto request,List<String> to)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Send notification request created: " + request.toString());
        try {
            notificationRequestCreated(request, NotificationKey.NOTIFY_NEW_REQUEST, to);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void sendNotificationExpirationInOneWeek(AccountDto account) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Send expiration email " + account.getEmail());

        try {
            validateAccount(account);
            Notification notification = notificationRepository.findOneByKey(NotificationKey.EXPIRATION_NOTIFICATION);
            if (!notification.isEnabled()) {
                return;
            }

            Map<String, String> properties = getExpirationVariables(account);
            List<String> toEmails = Collections.singletonList(account.getEmail());

            sendEmail(notification, properties, null, toEmails);

        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @SuppressWarnings("serial")
    private Map<String, String> getSistemVariables() {
        return new HashMap<String, String>() {
            {
                put(ACCOUNT_LOGIN_URL, digitalLibraryProps.getHost() + LOGIN);
                put(ACCOUNT_FORGOT_PASSWORD_URL, digitalLibraryProps.getHost() + FORGOT_PASSWORD);
                put(APP_TITLE, digitalLibraryProps.getAppTitle());
            }
        };
    }

    private List<String> sendEmail(Notification notification, Map<String, String> properties,
                                   String lang, List<String> toMain) throws ValidationException, ServiceException {
        List<String> to = new ArrayList<>();
        List<String> cc = new ArrayList<>();
        if (notification.getEmails() != null && notification.getEmails().size() > 0) {
            cc.addAll(notification.getEmails());
        }
        if (notification.getEnableToMain() != null && notification.getEnableToMain() && toMain != null) {
            to.addAll(toMain);
        }
        if (to.size() == 0 && cc.size() == 0) {
            LOG.info("[SERVICE] There are no emails to send ");
            return null;
        }
        NotificationTemplate template = notificationTemplateService.getNotificationTemplate(notification,
                NotificationType.EMAIL, lang);
        if (template == null) {
            throw new ValidationException("There are no email template notification to send");
        }
        String htmlText = processNotificationVariables(template.getContent(), properties, notification.getVariables());
        String subject = processNotificationVariables(template.getSubject(), properties, notification.getVariables());
        LOG.info("[SERVICE] Send notification Email to: " + to + cc + " notification: " + notification.getKey());
        if (to.size() == 0 || cc.size() == 0) {
            if (to.size() == 0) {
                emailService.sendHtmlMessage(cc.stream().toArray(String[]::new), htmlText, subject);
            } else {
                emailService.sendHtmlMessage(to.stream().toArray(String[]::new), htmlText, subject);
            }
        } else {
            emailService.sendHtmlMessageWithCC(to.stream().toArray(String[]::new), htmlText, subject,
                    cc.stream().toArray(String[]::new));
        }
        to.addAll(cc);
        return to;
    }

    private String processNotificationVariables(String response, Map<String, String> properties, List<String> variables)
            throws ValidationException {
        for (String variable : variables) {
            if (properties.containsKey(variable)) {
                response = response.replaceAll(variable,
                        properties.get(variable) == null ? "" : properties.get(variable));
            } else {
                throw new ValidationException("Variable: " + variable + " does not exists for this template");
            }
        }
        return response;
    }

    private void notificationRequestCreated(RequestDto request, NotificationKey key, List<String> to)
            throws ServiceException, ValidationException {
        LOG.info("[SERVICE] SEnd notification request created");
        Notification notification = notificationRepository.findOneByKey(key);
        if (!notification.isEnabled()) {
            return;
        }
        Map<String, String> properties = getRequestVariables(request);
        sendEmail(notification, properties, RO, to);
    }

    private Map<String, String> getRequestVariables(RequestDto request) {
        Map<String, String> properties = getSistemVariables();
        properties.put("customerName", request.getCustomer() != null ? request.getCustomer().getName() : "");
        properties.put("emails", request.getCustomer().getEmails().toString());
        properties.put("phones", request.getCustomer().getPhones().toString());
        properties.put("otherMentions", request.getOtherMentions());
        properties.put("requestTypes",
                request.getRequestType() != null
                    ? request.getRequestType().stream().map(RequestTypeDto::getName)
                        .collect(Collectors.toList()).toString() : "");
        properties.put("requestUrl", digitalLibraryProps.getHost() + REQUESTS + request.getId());
        return properties;
    }

    private Map<String, String> getExpirationVariables(AccountDto account) {
        Map<String, String> properties = getSistemVariables();
        properties.put("customerName", account.getName());
        properties.put("email", account.getEmail());
        return properties;
    }

    private void validateAccount(final AccountDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Account is null");
        }
        if (dto.getEmail() == null || dto.getEmail().trim().length() == 0) {
            throw new ValidationException("Account has invalid parameter email");
        }
    }

}
