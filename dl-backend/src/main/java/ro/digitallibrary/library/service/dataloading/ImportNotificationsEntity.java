package ro.digitallibrary.library.service.dataloading;

import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;

import java.util.List;

public class ImportNotificationsEntity {

    private Notification notification;
    private List<NotificationTemplate> templates;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public List<NotificationTemplate> getTemplates() {
        return templates;
    }

    public void setTemplates(List<NotificationTemplate> templates) {
        this.templates = templates;
    }
}
