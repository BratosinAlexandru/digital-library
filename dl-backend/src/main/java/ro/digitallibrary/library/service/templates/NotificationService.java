package ro.digitallibrary.library.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.templates.NotificationDto;
import ro.digitallibrary.library.dto.templates.transformer.NotificationTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.repository.templates.NotificationRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class NotificationService {
    private static final Logger LOG = LogManager.getLogger(NotificationService.class);

    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private NotificationTransformer notificationTransformer;

    public List<NotificationDto> getAll(boolean includeDisabled) throws ServiceException {
        LOG.info("[SERVICE] Get all notifications with includeDisabled: " + includeDisabled);

        try {
            List<Notification> notifications = includeDisabled
                    ? notificationRepository.findAll() : notificationRepository.findByEnabledTrue();
            return notifications.stream().map(notificationTransformer::transformToDto).collect(Collectors.toList());
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred");
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateNotification(NotificationDto notificationDto) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update notification " + notificationDto);
        try {
            validateUpdateNotification(notificationDto);
            notificationRepository.save(notificationTransformer.transformToEntity(notificationDto));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
        }
    }

    private void validateUpdateNotification(final NotificationDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Notification is null");
        }
        if (dto.getId() == null || dto.getId().trim().length() == 0) {
            throw new ValidationException("Notification has invalid id");
        }
    }
}
