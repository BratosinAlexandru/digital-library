package ro.digitallibrary.library.service.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import ro.digitallibrary.library.dto.request.CustomerDto;
import ro.digitallibrary.library.dto.request.transformer.CustomerTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.request.Customer;
import ro.digitallibrary.library.persistence.repository.request.CustomerRepository;
import ro.digitallibrary.library.service.account.AccountService;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class CustomerService {
    private static final Logger LOG = LogManager.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerTransformer customerTransformer;
    @Autowired
    private AccountService accountService;

    @Transactional
    public Optional<CustomerDto> getCustomer(@PathVariable String customerId)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get customer by id: " + customerId);
        try {
            validateString(customerId, "customerId");
            return customerRepository.findById(customerId).map(customerTransformer::transformToDto);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public CustomerDto createCustomer(CustomerDto customer) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Create customer");
        try {
            validate(customer);
            List<Customer> existingCustomers = null;
            if (customer.getEmails() != null && customer.getEmails().size() > 0 && customer.getPhones() != null
                        && customer.getPhones().size() > 0) {
                existingCustomers = customerRepository.findByEmailsOrPhones(customer.getEmails(), customer.getPhones());
            } else if (customer.getEmails() != null && customer.getEmails().size() > 0) {
                existingCustomers = customerRepository.findByEmails(customer.getEmails());
            } else if (customer.getPhones() != null && customer.getPhones().size() > 0) {
                existingCustomers = customerRepository.findByPhones(customer.getPhones());
            }

            if (existingCustomers != null && existingCustomers.size() > 0) {
                if (existingCustomers.size() > 1) {
                    LOG.info("[SERVICE] Customer has duplicates");
                }
                return customerTransformer.transformToDto(existingCustomers.get(0));
            } else {
                return customerTransformer
                        .transformToDto(customerRepository.save(customerTransformer.transformToEntity(customer)));
            }

        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }
    @Transactional
    public CustomerDto updateCustomer(CustomerDto customer) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update customer");
        try {
            validateUpdate(customer);
            return customerTransformer
                    .transformToDto(customerRepository.save(customerTransformer.transformToEntity(customer)));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public List<CustomerDto> getCustomersByQuery(String query, Authentication auth, Boolean hasRequestsEnabled)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get customers by query");
        try {
            validateString(query, "query");
            List<Customer> customers = customerRepository.findByText(query, hasRequestsEnabled);
            return customers == null ? null
                    : customers.stream().map(customerTransformer::transformToDto).collect(Collectors.toList());
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<CustomerDto> findByEmail(String email) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Find customer by email: " + email);
        try {
            validateString(email, "email");
            List<Customer> customers = customerRepository.findByEmail(email);
            return customers == null ? null
                    : customers.stream().map(customerTransformer::transformToDto).collect(Collectors.toList());
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occured", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occured", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void validateUpdate(final CustomerDto dto) throws ValidationException {
        validate(dto);
        validateString(dto.getId(), "id");
    }

    private void validate(final CustomerDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Customer is null.");
        }
        validateString(dto.getName(), "name");
        validatePhone(dto);
        validateEmail(dto);
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("Customer has invalid " + name);
        }
    }

    private void validatePhone(final CustomerDto dto) throws ValidationException {
        if (dto.getPhones() == null || dto.getPhones().size() == 0) {
            throw new ValidationException("Customer has invalid phones");
        }
        List<String> phones = new ArrayList<String>();
        for (String phone : dto.getPhones()) {
            validateString(phone, "Phone");
            phones.add(phone.replaceAll("[^\\d]", ""));
        }
        dto.setPhones(phones);
    }

    private void validateEmail(final CustomerDto dto) throws ValidationException {
        if (dto.getEmails() != null) {
            for (String email : dto.getEmails()) {
                try {
                    InternetAddress emailAddr = new InternetAddress(email);
                    emailAddr.validate();
                } catch (AddressException ex) {
                    throw new ValidationException("Customer has invalid email" + ex.getMessage());
                }
            }
        }
    }
}
