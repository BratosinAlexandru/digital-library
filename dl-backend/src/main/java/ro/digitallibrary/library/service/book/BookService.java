package ro.digitallibrary.library.service.book;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.book.BookDto;
import ro.digitallibrary.library.dto.book.BookOverviewDto;
import ro.digitallibrary.library.dto.book.BookOverviewPageDto;
import ro.digitallibrary.library.dto.book.GetBooksDto;
import ro.digitallibrary.library.dto.book.transformer.BookTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.entity.book.BorrowedBooks;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;
import ro.digitallibrary.library.persistence.repository.book.BorrowedBooksRepository;
import ro.digitallibrary.library.service.account.AccountService;
import ro.digitallibrary.library.service.templates.SendNotificationService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class BookService {
    private static final Logger LOG = LogManager.getLogger(BookService.class);

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookTransformer bookTransformer;
    @Autowired
    private AccountService accountService;
    @Autowired
    private AttachmentService attachmentService;
    @Autowired
    private BorrowedBooksRepository borrowedBooksRepository;
    @Autowired
    private SendNotificationService sendNotificationService;

    public BookOverviewPageDto getBooks(GetBooksDto getBooksDto, Pageable pageable) {
        LOG.info("[SERVICE] Getting books page");
        if (getBooksDto.getFrom() == null) {
            getBooksDto.setFrom(LocalDate.now().minusMonths(6));
        }
        if (getBooksDto.getTo() == null) {
            getBooksDto.setTo(LocalDate.now());
        }

        Page<Book> page = bookRepository.getBooks(getBooksDto, pageable);
        BookOverviewPageDto bookOverviewPage = new BookOverviewPageDto();
        BeanUtils.copyProperties(page, bookOverviewPage, "content");

        List<BookOverviewDto> content = page.getContent().stream().map(bookTransformer::toBookOverview)
                .collect(Collectors.toList());
        bookOverviewPage.setContent(content);
        bookOverviewPage.setPage(page.getNumber());
        return bookOverviewPage;
    }

    public Optional<BookDto> getBook(String bookId) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Getting book with id: " + bookId);
        try {
            validateString(bookId, "bookId");
            return bookRepository.findById(bookId).map(bookTransformer::transformToDto);
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<BookOverviewDto> getBooksOverview(Boolean includeDisabled) {
        LOG.info("[SERVICE] Getting all books with attachmehts");

        List<Book> books = includeDisabled != null
                ? bookRepository.getBookByAttachmentCountAndEnabled(!includeDisabled)
                : bookRepository.getBookByAttachmentCount();

        return books.stream().map(bookTransformer::toBookOverview).collect(Collectors.toList());
    }

    public void enableBook(String bookId) {
        LOG.info("[SERVICE] Enable book with id: " + bookId);
        bookRepository.findById(bookId).ifPresent(book -> {
            book.setEnabled(true);
            bookRepository.save(book);
        });
    }

    public void disableBook(String bookId) {
        LOG.info("[SERVICE] Disable book with id: " + bookId);
        bookRepository.findById(bookId).ifPresent(book -> {
            book.setEnabled(false);
            bookRepository.save(book);
        });
    }

    public void updateBook(BookDto bookDto) {
        LOG.info("[SERVICE] Update book" + bookDto.getId());
        bookRepository.findById(bookDto.getId()).ifPresent(book -> {
            if (bookDto.getImage() == null && book.getImage() != null) {
                bookDto.setImage(book.getImage());
                bookRepository.save(bookTransformer.transformToEntity(bookDto));
            } else {
                bookRepository.save(bookTransformer.transformToEntity(bookDto));
            }
        });
    }

    public void deleteBook(String bookId) {
        LOG.info("[SERVICE] Delete book with id: " + bookId);
        Optional<Book> bookOpt = bookRepository.findById(bookId);
        bookOpt.ifPresent(book -> {
            boolean deleteBook = false;
            Account account = accountService.getCurrentAccount();
            if (account != null) {
                deleteBook = Stream.of(account.getRoles()).anyMatch(role -> role.equals("admin"));
            } else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                        .anyMatch(role -> role.contains("ADMIN"))) {
                    deleteBook = true;
                }
            }
            if (deleteBook) {
                bookRepository.deleteById(bookId);
            }
        });
    }

    public ByteArrayResource getBookImage(String bookId) {
        return bookRepository.findById(bookId).filter(book -> book.getImage() != null)
                .map(book -> new ByteArrayResource(book.getImage()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public BookDto createBook(BookDto bookDto) throws Exception {
        LOG.info("[SERVICE] Create book" + bookDto.toString());
        if (bookDto.getFrom() != null && bookDto.getTo() != null) {
            bookDto.setDescription(getContent(attachmentService.find(
                    bookDto.getAttachment().get(0).getId()),bookDto.getFrom(), bookDto.getTo()));
        }

        return bookTransformer.transformToDto(bookRepository.save(bookTransformer.transformToEntity(bookDto)));
    }

    private String getContent(GridFsResource resource, Integer from, Integer to) throws ServiceException {
        try {
            PDDocument document = PDDocument.load(resource.getInputStream());
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            pdfTextStripper.setLineSeparator("\n");
            pdfTextStripper.setStartPage(from);
            pdfTextStripper.setEndPage(to);
            String text = pdfTextStripper.getText(document);
            document.close();
            return text;
//            String[] formatted = text.split("(\\d+\\.\\d+)\\s[a-zA-Z \\:\\d+[a-zA-Z \\(\\)\\’]]+\\D+\\d+\\s+");
//
//            String[] formatted = text.split("\\d+\\s[[A-Za-z] (FAQ:)\\?\\!\\(\\)]+\\d+\\n");
//            System.out.println(Arrays.toString(formatted));
//
//            final String regex = "\\d+\\s[[A-Za-z] (FAQ:)\\?\\!\\(\\)]+\\d+\\n";
//
//            final Pattern pattern = Pattern.compile(regex);
//            final Matcher matcher = pattern.matcher(text);
//
//            while (matcher.find()) {
//                System.out.println("Full match: " + matcher.group(0));
//
//                for (int i = 1; i <= matcher.groupCount(); i++) {
//                    System.out.println("Group " + i + ": " + matcher.group(i));
//                }
//            }
        } catch (Exception e) {
            LOG.error("Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("Request has invalid " + name);
        }
    }


//    @Scheduled(fixedRateString = "#{T(java.lang.Long).valueOf(${library.expiration-check.borrowedBooks})}")
//    private void checkExpiredBooks() {
//        LOG.info("Checking expired books");
//        List<BorrowedBooks> expiredBorrowedBooks = borrowedBooksRepository.findExpiredBooks(LocalDateTime.now());
//
//        if (expiredBorrowedBooks.size() > 0) {
//            expiredBorrowedBooks.forEach(borrowedBook -> borrowedBooksRepository.deleteById(borrowedBook.getId()));
//        }
//    }

//    @Scheduled(fixedRateString = "#{T(java.lang.Long).valueOf(${library.expiration-check.borrowedBooks})}")
//    private void sendExpirationNotification() {
//        LOG.info("Checking for expiration within a week");
//        LocalDateTime now = LocalDateTime.now();
//
//        List<BorrowedBooks> expiredBorrowedBooks = borrowedBooksRepository
//                                                        .findExpiredBooksWithinWeek(now, now.plusDays(7));
//
//        if (expiredBorrowedBooks.size() > 0) {
//            expiredBorrowedBooks.forEach(borrowedBook -> {
//                try {
//                    sendNotificationService
//                            .sendNotificationExpirationInOneWeek(accountService.getAccountById(borrowedBook.getAccountId()));
//                } catch (ValidationException | ServiceException e) {
//                    e.printStackTrace();
//                }
//            });
//        }
//    }
}
