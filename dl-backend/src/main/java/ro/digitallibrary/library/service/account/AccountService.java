package ro.digitallibrary.library.service.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.account.transformer.AccountTransformer;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;
import ro.digitallibrary.library.service.templates.SendNotificationService;

import javax.sql.rowset.serial.SerialException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class AccountService {
    private static final Logger LOG = LogManager.getLogger(AccountService.class);
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountTransformer accountTransformer;
    @Autowired
    private SendNotificationService sendNotificationService;

    public Account getCurrentAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return accountRepository.findByEmailIgnoreCase(authentication.getName()).orElseGet(() -> null);
        }
        return null;
    }

    @Transactional
    public Account createAccount(Account account) {
        Optional<Account> accountOpt = accountRepository.findByEmailIgnoreCase(account.getEmail());
        if (accountOpt.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        } else {
            account.setPassword(encodePassword(account.getPassword()));
            return accountRepository.save(account);
        }
    }

    @Transactional
    public void updateAccount(Account account, Authentication auth) throws ValidationException, ServiceException {
        try {
            Optional<Account> accountOpt = accountRepository.findByEmailIgnoreCase(account.getEmail());
            validateUpdateAccount(auth, accountOpt);
            accountOpt.ifPresent(persistedAccount -> {
                String newPassword = account.getPassword();
                if (newPassword != null && newPassword.length() > 0 && !persistedAccount.getPassword().equals(newPassword)) {
                    account.setPassword(encodePassword(newPassword));
                } else {
                    account.setPassword(persistedAccount.getPassword());
                }
                accountRepository.save(account);
            });
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public AccountDto updateAccount(AccountDto account) throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Update account for request");
        try {
            validateUpdate(account);
            return accountTransformer
                    .transformToDto(accountRepository.save(accountTransformer.transformToEntity(account)));
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public void deleteAccount(String accountId) {
        accountRepository.deleteById(accountId);
    }

    public List<String> getRoles() {
        List<String> roles = new ArrayList<>();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).forEach(roles::add);
        }
        return roles;
    }

    public boolean isEmailUnique(String email) {
        return accountRepository.countByEmail(email) == 0;
    }

    @Transactional
    public void resetPassword(String email) throws ValidationException, ServiceException {
        try {
            validateString(email, "email");
            Optional<Account> accountOpt = accountRepository.findByEmailIgnoreCase(email);
            String newPassword = generateNewPassword();
            if (accountOpt.isPresent()) {
                Account persistedAccount = accountOpt.get();
                persistedAccount.setPassword(encodePassword(newPassword));
                AccountDto account = accountTransformer.transformToDto(accountRepository.save(persistedAccount));
                account.setPassword(newPassword);
                sendResetPasswordEmailAccount(account);
                return;
            }
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public Page<AccountDto> getPagedObjects(PaginationRequestDto<FiltersRequest> pageRequest) {
        PageRequest page = null;
        if (pageRequest.getSortDirection() == null || pageRequest.getSortDirection().equals("")) {
            page = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize());
        } else {
            Sort sort = new Sort(
                    pageRequest.getSortDirection().equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC,
                    pageRequest.getSortProperties());
            page = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize(), sort);
        }
        if (pageRequest.getQuery() == null) {
            return accountRepository.findAll(page).map(accountTransformer::transformToDto);
        } else {
            return accountRepository.findByQuery(pageRequest.getQuery(), page).map(accountTransformer::transformToDto);
        }
    }


    @Transactional
    public List<AccountDto> getAccountsByQuery(String query, Boolean hasRequestEnabled)
            throws ValidationException, ServiceException {
        LOG.info("[SERVICE] Get accounts by query: " + query);
        try {
            validateString(query, "query");
            List<Account> accounts = accountRepository.findByText(query, hasRequestEnabled);
            return accounts == null ? null
                    : accounts.stream().map(accountTransformer::transformToDto).collect(Collectors.toList());
        } catch (ValidationException ve) {
            LOG.error("[SERVICE] Validation exception occurred", ve);
            throw ve;
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Account> getAccountsForNotification() {
        LOG.info("[SERVICE] Get accounts for notifications");
        List<String> roles = new ArrayList<>();
        roles.add("admin");
        return accountRepository.findByRolesAndEnabledTrue(roles);
    }

    public AccountDto getAccountById(String id) {
        return accountTransformer.transformToDto(accountRepository.getById(id));
    }

    private String generateNewPassword() {
        String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
        StringBuilder stringBuilder = new StringBuilder(6);
        for (int i=0; i < 6; ++i) {
            int index = (int)(alphaNumericString.length() * Math.random());
            stringBuilder.append(alphaNumericString.charAt(index));
        }
        return stringBuilder.toString();
    }

    private void sendResetPasswordEmailAccount(AccountDto accountDto) throws ValidationException, SerialException {
        try {
            sendNotificationService.sendResetPasswordEmailAccount(accountDto);
        } catch (Exception e) {
            LOG.error("[SERVICE] Service exception occurred", e);
        }
    }


    private boolean hasAuthRole(Authentication auth, String role) {
        return auth.getAuthorities().stream().anyMatch(e -> e.getAuthority().equals(role));
    }

    private String encodePassword(String rawPassword) {
        String encodedPassword = passwordEncoder.encode(rawPassword);
        return encodedPassword.replace("{sha256}", "");
    }

    private void validateString(final String key, final String name) throws ValidationException {
        if (key == null || key.trim().length() == 0) {
            throw new ValidationException("Request has invalid " + name);
        }
    }

    private void validateUpdateAccount(Authentication auth, Optional<Account> accountOpt)
            throws ValidationException,ServiceException {
        if (accountOpt == null || !accountOpt.isPresent()) {
            throw new ValidationException("Provided account cannot be found!");
        }
        String email = accountOpt.get().getEmail();
        if (auth == null || auth.getAuthorities() == null || auth.getPrincipal() == null ||
                (!hasAuthRole(auth, ROLE_ADMIN) && auth.getPrincipal().equals(email))) {
            throw new ValidationException("Account does not have privileges to update");
        }
    }

    private void validateUpdate(final AccountDto dto) throws ValidationException {
        if (dto == null) {
            throw new ValidationException("Account is null");
        }
        validateString(dto.getName(), "name");
    }

}
