package ro.digitallibrary.library.service.dataloading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ro.digitallibrary.library.dto.app.AppKeyDto;
import ro.digitallibrary.library.dto.app.transformer.AppKeyTransformer;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.categories.Category;
import ro.digitallibrary.library.persistence.entity.domains.Domain;
import ro.digitallibrary.library.persistence.repository.app.AppKeyRepository;
import ro.digitallibrary.library.persistence.repository.categories.CategoryRepository;
import ro.digitallibrary.library.persistence.repository.domains.DomainRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import javax.annotation.PostConstruct;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static ro.digitallibrary.library.service.dataloading.Constants.IMPORT_CATEGORIES_SCRIPT;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ImportCategoriesService {

    private static final Logger LOG = LogManager.getLogger(ImportCategoriesService.class);
    private static final String DATA_DIR = "data/";

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private DomainRepository domainRepository;
    @Autowired
    private AppKeyService appKeyService;
    @Autowired
    private AppKeyRepository appKeyRepository;
    @Autowired
    private AppKeyTransformer appKeyTransformer;

    @PostConstruct
    private void loadInitialData() throws ValidationException, ServiceException {
        LOG.info("[IMPORT SERVICE Categories] Performing initial data import");

        if (appKeyService.findByKey(IMPORT_CATEGORIES_SCRIPT) != null) {
            LOG.info("[IMPORT SERVICE Categories] Import should run once. It has been run before and stops");
            return;
        }

        List<Category> categories = loadData("categories", new TypeReference<List<Category>>(){
        });

        categories.forEach(category -> {
            String domainName = category.getDomain().getName();
            Optional<Domain> domainOpt = domainRepository.findByName(domainName);
            if (domainOpt.isPresent()) {
                category.setDomain(domainOpt.get());
            } else {
                category.setDomain(domainRepository.save(category.getDomain()));
            }
        });
        categoryRepository.saveAll(categories);
        AppKeyDto appKeyDto = new AppKeyDto();
        appKeyDto.setKey(IMPORT_CATEGORIES_SCRIPT);
        appKeyDto.setValue(true);
        appKeyRepository.save(appKeyTransformer.transformToEntity(appKeyDto));
    }

    private <T> List<T> loadData(String resourceName, TypeReference<List<T>> typeReference) {
        ObjectMapper mapper = new ObjectMapper();
        Resource inputResource = new ClassPathResource(String.format("%s.json", DATA_DIR + resourceName));

         try {
             return mapper.readValue(inputResource.getInputStream(), typeReference);
         } catch (IOException e) {
             LOG.error("Error while loading resource: " + resourceName, e);
         }
         return Collections.emptyList();
    }

}
