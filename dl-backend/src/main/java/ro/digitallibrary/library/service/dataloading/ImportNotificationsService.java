package ro.digitallibrary.library.service.dataloading;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ro.digitallibrary.library.beans.templates.Language;
import ro.digitallibrary.library.dto.app.AppKeyDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;
import ro.digitallibrary.library.persistence.repository.templates.NotificationRepository;
import ro.digitallibrary.library.persistence.repository.templates.NotificationTemplateRepository;
import ro.digitallibrary.library.service.app.AppKeyService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static ro.digitallibrary.library.service.dataloading.Constants.*;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ImportNotificationsService {

    private static final Logger LOG = LogManager.getLogger(ImportNotificationsService.class);
    private static final String DATA_DIR = "data/";

    @Autowired
    private NotificationTemplateRepository notificationTemplateRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private AppKeyService appKeyService;

    @PostConstruct
    private void loadNotifications() throws ValidationException, ServiceException {
        if (appKeyService.hasScript(IMPORT_NOTIFICATION_SCRIPT)) {
            LOG.info("[IMPORT Notification SERVICE] Import should run once. It has been run before and it stopped");
            return;
        }
        notificationTemplateRepository.deleteAll();
        notificationRepository.deleteAll();

        List<Language> languages = loadData(NOTIFICATION_LANGUAGES, new TypeReference<List<Language>>() {
        });
        AppKeyDto appKey = appKeyService.findByKey(NOTIFICATION_LANGUAGES);
        if (appKey == null) {
            appKey = new AppKeyDto();
            appKey.setEnabled(true);
            appKey.setKey(NOTIFICATION_LANGUAGES);
        }
        appKey.setValue(languages);
        appKeyService.addAppKey(appKey);

        List<ImportNotificationsEntity> notifications = loadData(NOTIFICATIONS,
                new TypeReference<List<ImportNotificationsEntity>>() {
                });
        Iterator<ImportNotificationsEntity> iterator = notifications.iterator();
        while (iterator.hasNext()) {
            ImportNotificationsEntity item = iterator.next();

            Notification notification = item.getNotification();
            if (notification.getEmails() == null) {
                notification.setEmails(new ArrayList<>());
            }
            notificationRepository.save(notification);

            for (NotificationTemplate template : item.getTemplates()) {
                template.setNotification(notification);
                notificationTemplateRepository.save(template);
            }
        }
        appKeyService.addScript(IMPORT_NOTIFICATION_SCRIPT);
    }


    private <T>List<T> loadData(String resourceName, TypeReference<List<T>> typeReference) {
        ObjectMapper mapper = new ObjectMapper();
        Resource inputResource = new ClassPathResource(String.format("%s.json", DATA_DIR + resourceName));
        try {
            return mapper.readValue(inputResource.getInputStream(), typeReference);
        } catch (IOException e) {
            LOG.error("Error while loading resource: " + resourceName, e);
        }
        return Collections.emptyList();
    }


}
