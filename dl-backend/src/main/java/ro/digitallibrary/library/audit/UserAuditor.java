package ro.digitallibrary.library.audit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;

import java.util.Optional;

@Component
public class UserAuditor implements AuditorAware<AbstractMongoEntity> {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Optional<AbstractMongoEntity> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return Optional.empty();
        }
        Optional<Account> accountOpt = accountRepository.findByEmailIgnoreCase(authentication.getName());
        if (accountOpt.isPresent()) {
            return Optional.of(accountOpt.get());
        }
        return Optional.empty();
    }
}
