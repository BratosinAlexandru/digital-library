package ro.digitallibrary.library.controller.domains;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.domains.DomainDto;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.domains.DomainService;

import java.util.List;

@RestController
@RequestMapping(value = "/domain")
public class DomainController {
    private static final Logger LOG = LogManager.getLogger(DomainController.class);

    @Autowired
    private DomainService domainService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DomainDto> getDomains(@RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) {
        LOG.info("[REST] Call to /domain");
        return domainService.findByEnabledTrue(includeDisabled);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public DomainDto createDomain(@RequestBody DomainDto domainDto) {
        LOG.info("[REST] Call to /domain/create");
        return domainService.createDomain(domainDto);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public DomainDto updateDomain(@RequestBody DomainDto domainDto) {
        LOG.info("[REST] Call to /domain/update");
        return domainService.updateDomain(domainDto);
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDomain(@RequestParam(name = "domainId") String domainId) throws ValidationException {
        LOG.info("[REST] Call to /domain/delete");
        domainService.deleteDomain(domainId);
    }
}
