package ro.digitallibrary.library.controller.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.templates.NotificationTemplateDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.templates.NotificationTemplateService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/notification/templates")
public class NotificationTemplateController {

    private static final Logger LOG = LogManager.getLogger(NotificationTemplateController.class);

    @Autowired
    private NotificationTemplateService notificationTemplateService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<NotificationTemplateDto>> getTemplatesByNotificationId(@RequestParam(name = "id") String id)
            throws ServiceException {
        LOG.info("[REST] Get all notification templates by key: " + id);
        return notificationTemplateService.getTemplatesByNotificationId(id);
    }

    @GetMapping(value = "/types", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getTypes() throws ServiceException {
        LOG.info("[REST] Get notification template types");
        return notificationTemplateService.getTypes();
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateNotificationTemplate(@RequestBody NotificationTemplateDto notificationTemplateDto)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Update notification template " + notificationTemplateDto.toString());
        notificationTemplateService.updateNotificationTemplate(notificationTemplateDto);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public NotificationTemplateDto createTemplate(@RequestBody NotificationTemplateDto template)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Create notification template " + template.toString());
        return notificationTemplateService.createTemplate(template);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTemplate(@PathVariable String id) throws ValidationException, ServiceException {
        LOG.info("[REST] Delete notification template " + id);
        notificationTemplateService.deleteTemplate(id);
    }
}
