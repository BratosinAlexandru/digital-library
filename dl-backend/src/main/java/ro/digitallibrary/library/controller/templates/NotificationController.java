package ro.digitallibrary.library.controller.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.templates.NotificationDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.templates.NotificationService;

import java.util.List;

@RestController
@RequestMapping("/notification")
public class NotificationController {

    private static final Logger LOG = LogManager.getLogger(NotificationController.class);

    @Autowired
    private NotificationService notificationService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NotificationDto> getNotifications(
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) throws ServiceException {
        LOG.info("[REST] Get all notification with include disabled: " + includeDisabled);
        return notificationService.getAll(includeDisabled);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateNotification(@RequestBody NotificationDto notification)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Update notification " + notification.toString());
        notificationService.updateNotification(notification);
    }

}
