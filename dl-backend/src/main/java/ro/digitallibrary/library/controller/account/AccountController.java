package ro.digitallibrary.library.controller.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.account.AccountDto;
import ro.digitallibrary.library.dto.account.transformer.AccountTransformer;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.repository.account.AccountRepository;
import ro.digitallibrary.library.service.account.AccountService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@RestController
@RequestMapping("/account")
public class AccountController {

    private static final Logger LOG = LogManager.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountTransformer accountTransformer;

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping(value = "/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountDto getByEmail(@PathVariable String email) {
        LOG.info("[REST] Call get /account/{email} for" + email);
        return accountRepository.findByEmailIgnoreCase(email).map(accountTransformer::transformToDto)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getRoles() {
        LOG.info("[REST] Call get roles");
        return accountService.getRoles();
    }

    @GetMapping(value = "/{email}/isUnique", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean isUnique(@PathVariable String email) {
        LOG.info("[REST] Call /account/{email}/isUnique for " + email);
        return accountService.isEmailUnique(email);
    }

    @GetMapping(value = "/getAccountsByQuery", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AccountDto> getAccountsByQuery(@RequestParam(name = "query") String query,
                                              @RequestParam(name = "hasRequestEnabled", required = false) Boolean hasRequestEnabled)
            throws ValidationException, ServiceException{
        LOG.info("[REST] Call /account/getAccountByQuery");
        return accountService.getAccountsByQuery(query, hasRequestEnabled);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto createAccount(@RequestBody AccountDto accountDto) {
        LOG.info("[REST] Call /account/create");
        Account newAccount = accountTransformer.transformToEntity(accountDto);
        return accountTransformer.transformToDto(accountService.createAccount(newAccount));
    }

    @PostMapping(value = "/getPaged", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<AccountDto> getPaged(@RequestBody PaginationRequestDto<FiltersRequest> pageRequest) {
        LOG.info("[REST] Call /account/getPaged" + pageRequest.toString());
        return accountService.getPagedObjects(pageRequest);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAccount(@RequestBody AccountDto accountDto, @ApiIgnore Authentication auth)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call /account/update for" + accountDto.getEmail());
        accountService.updateAccount(accountTransformer.transformToEntity(accountDto), auth);
    }

    @PutMapping(value = "/resetPassword")
    public void resetPassword(@RequestBody String email) throws ValidationException, ServiceException {
        LOG.info("[REST] Call /account/resetPassword for " + email);
        accountService.resetPassword(email);
    }

    @DeleteMapping(value = "/delete/{accountId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAccount(@PathVariable String accountId) {
        LOG.info("[REST] Call /account/delete/{accountId} for: " + accountId);
        accountService.deleteAccount(accountId);
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAccounts(@RequestParam List<String> ids) {
        LOG.info("[REST] Call /account/delete");
        ids.forEach(accountService::deleteAccount);
    }


}
