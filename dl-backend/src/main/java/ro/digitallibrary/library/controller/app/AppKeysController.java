package ro.digitallibrary.library.controller.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.app.AppKeyDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.app.AppKeyService;

import java.util.List;

@RestController
@RequestMapping("/appkey")
public class AppKeysController {
    private static final Logger LOG = LogManager.getLogger(AppKeysController.class);

    @Autowired
    private AppKeyService appKeyService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AppKeyDto> getAppKeys() {
        LOG.info("[REST] Get app keys");
        return appKeyService.getAppKeys();
    }

    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AppKeyDto findByKey(@PathVariable String key) throws ValidationException, ServiceException {
        LOG.info("[REST] Find app key " + key);
        return appKeyService.findByKey(key);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAppKey(@RequestBody AppKeyDto appKey) throws ValidationException, ServiceException {
        LOG.info("[REST] Update app key" + appKey.toString());
        appKeyService.updateAppKey(appKey);
    }
}
