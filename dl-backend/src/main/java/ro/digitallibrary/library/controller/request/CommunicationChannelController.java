package ro.digitallibrary.library.controller.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.request.CommunicationChannelDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.request.CommunicationChannelService;

import javax.print.attribute.standard.Media;
import java.util.List;

@RestController
@RequestMapping("/communication")
public class CommunicationChannelController {

    private static final Logger LOG = LogManager.getLogger(CommunicationChannelController.class);

    @Autowired
    private CommunicationChannelService communicationChannelService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CommunicationChannelDto> getCommunicationChannels(
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) throws ServiceException {
        LOG.info("[REST] Call to /communication");
        return communicationChannelService.getCommunicationChannels(includeDisabled);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CommunicationChannelDto createCommunicationChannel(@RequestBody CommunicationChannelDto communicationChannel)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /communication/create");
        return communicationChannelService.createCommunicationChannel(communicationChannel);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCommunicationChannel(@RequestBody CommunicationChannelDto communicationChannel)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /communication/update");
        communicationChannelService.updateCommunicationChannel(communicationChannel);
    }

    @DeleteMapping(value = "/delete/{communicationChannelId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCommunicationChannel(@PathVariable String communicationChannelId) throws ValidationException {
        LOG.info("[REST] Call to /communication/delete");
        communicationChannelService.deleteCommunicationChannel(communicationChannelId);
    }
}
