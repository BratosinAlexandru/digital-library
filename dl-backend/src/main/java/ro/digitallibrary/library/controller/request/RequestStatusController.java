package ro.digitallibrary.library.controller.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.request.RequestStatusDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.request.RequestStatusService;

import java.util.List;

@RestController
@RequestMapping("/request/status")
public class RequestStatusController {

    private static final Logger LOG = LogManager.getLogger(RequestStatusController.class);

    @Autowired
    private RequestStatusService requestStatusService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RequestStatusDto> getRequestStatues(
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) throws ServiceException {
        LOG.info("[REST] Call to /request/status");
        return requestStatusService.getRequestStatues(includeDisabled);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public RequestStatusDto createRequestStatus(@RequestBody RequestStatusDto requestStatus)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /request/status/create");
        return requestStatusService.createRequestStatus(requestStatus);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateRequestStatus(@RequestBody RequestStatusDto requestStatus) throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /request/status/update");
        requestStatusService.updateRequestStatus(requestStatus);
    }

    @DeleteMapping(value = "/delete/{requestStatusId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRequestStatus(@PathVariable String requestStatusId) throws ValidationException {
        LOG.info("[REST] Call to /request/status/delete");
        requestStatusService.deleteRequestStatus(requestStatusId);
    }
}
