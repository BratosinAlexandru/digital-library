package ro.digitallibrary.library.controller.categories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ro.digitallibrary.library.dto.categories.CategoryDto;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.categories.CategoryService;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {
    private static final Logger LOG = LogManager.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getCategories(
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) {
        LOG.info("[REST] Call to /category");
        return categoryService.getCategories(includeDisabled);
    }

    @GetMapping(value = "/getByDomains", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getCategoriesByDomain(
            @RequestParam("domainIds") List<String> domainIds,
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) {
        LOG.info("[REST] Call to /category/getByDomains");
        return categoryService.getCategoriesByDomain(domainIds, includeDisabled);
    }

    @GetMapping(value = "/getCategory", produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto getCategory(@RequestParam("categoryId") String categoryId) {
        LOG.info("[REST] Call to /category/getCategory");
        return categoryService.getCategory(categoryId);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryDto createCategory(@RequestBody CategoryDto categoryDto) {
        LOG.info("[REST] Call to /category/create");
        return categoryService.createCategory(categoryDto);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCategory(@RequestBody CategoryDto categoryDto) {
        LOG.info("[REST] Call to /category/update");
        categoryService.updateCategory(categoryDto);
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategory(@RequestParam String categoryId) throws ValidationException {
        LOG.info("[REST] Call to /category/delete");
        categoryService.deleteCategory(categoryId);
    }
}
