package ro.digitallibrary.library.controller.request;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ro.digitallibrary.library.dto.request.RequestTypeDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.request.RequestTypeService;

import java.util.List;

@RestController
@RequestMapping("/request/type")
public class RequestTypeController {

    private static final Logger LOG = LogManager.getLogger(RequestTypeController.class);

    @Autowired
    private RequestTypeService requestTypeService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RequestTypeDto> getRequestTypes(
            @RequestParam(name = "includeDisabled", required = false) boolean includeDisabled) throws ServiceException {
        LOG.info("[REST] Call to /request/types");
        return requestTypeService.getRequestTypes(includeDisabled);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public RequestTypeDto createRequestType(@RequestBody RequestTypeDto requestType)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /request/type/create");
        return requestTypeService.createRequestType(requestType);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateRequestType(@RequestBody RequestTypeDto requestType) throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /request/type/update");
        requestTypeService.updateRequestType(requestType);
    }

    @DeleteMapping(value = "/delete/{requestTypeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRequestType(@PathVariable String requestTypeId) throws ValidationException {
        LOG.info("[REST] Cal lto /request/type/delete");
        requestTypeService.deleteRequestType(requestTypeId);
    }
}
