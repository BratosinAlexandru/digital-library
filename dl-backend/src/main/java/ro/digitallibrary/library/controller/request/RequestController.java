package ro.digitallibrary.library.controller.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.dto.request.RequestDto;
import ro.digitallibrary.library.dto.request.RequestHistoryDto;
import ro.digitallibrary.library.dto.request.RequestPage;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.request.RequestService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/request")
public class RequestController {

    private static final Logger LOG = LogManager.getLogger(RequestController.class);

    @Autowired
    private RequestService requestService;

    @GetMapping(value = "/{requestId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestDto getRequest(@PathVariable String requestId) throws ValidationException, ServiceException {
        LOG.info("[REST] Get request with id: " + requestId);
        return requestService.getRequest(requestId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/history/{requestId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RequestHistoryDto> getHistory(@PathVariable String requestId)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Get request history");
        return requestService.getHistory(requestId);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public RequestDto createRequest(@RequestBody RequestDto request) throws ValidationException, ServiceException {
        LOG.info("[REST] Create request");
        return requestService.createRequest(request);
    }

    @PostMapping(value = "/getPaged", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestPage getPaged(@RequestBody PaginationRequestDto<FiltersRequest> pageRequest,
                                @ApiIgnore Authentication auth) throws ValidationException, ServiceException {
        LOG.info("[REST] Get requests page");
        return requestService.getPagedObjects(pageRequest, auth);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateRequest(@RequestBody RequestDto request) throws ValidationException, ServiceException {
        LOG.info("[REST] Update request");
        requestService.updateRequest(request);
    }

    @PutMapping(value = "/updateEnable", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateEnable(@RequestBody List<String> requestIds, @RequestParam Boolean enable)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Update request enable for " + requestIds.toString());
        requestService.updateEnable(requestIds, enable);
    }

    @PutMapping(value = "/acceptRequest", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void acceptRequest(@RequestBody List<String> requestIds)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Update request enable for " + requestIds.toString());
        requestService.acceptRequests(requestIds);
    }

    @DeleteMapping(value = "/deleteAll", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAll(@RequestBody List<String> requestIds) throws ValidationException, ServiceException {
        LOG.info("[REST] Delete following requests: " + requestIds.toString());
        requestService.deleteAll(requestIds);
    }

}
