package ro.digitallibrary.library.controller.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import ro.digitallibrary.library.persistence.entity.book.Attachment;
import ro.digitallibrary.library.service.book.AttachmentService;

import java.util.List;


@RestController
@RequestMapping("/attachment")
public class AttachmentController {

    private static final Logger LOG = LogManager.getLogger(AttachmentController.class);

    @Autowired
    private AttachmentService attachmentService;

    @GetMapping(value = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<GridFsResource> download(@RequestParam("id") String id) throws Exception {
        LOG.info("[REST] Download file with id: " + id);

        GridFsResource resource = attachmentService.find(id);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(resource.getContentType()))
                .contentLength(resource.contentLength()).body(resource);
    }

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Attachment> upload(@RequestParam(name = "file", required = false) MultipartFile file)
            throws Exception {
        LOG.info("[REST] Upload file " + file.getOriginalFilename());

        Attachment attachment = attachmentService.upload(file);
        return ResponseEntity.ok().body(attachment);
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAttachments(@RequestParam List<String> ids) throws Exception {
        LOG.info("[REST] Delete file with ids: " + ids);

        attachmentService.deleteAttachment(ids);
    }
}
