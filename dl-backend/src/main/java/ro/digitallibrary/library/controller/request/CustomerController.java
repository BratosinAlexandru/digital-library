package ro.digitallibrary.library.controller.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.digitallibrary.library.dto.request.CustomerDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.service.request.CustomerService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private static final Logger LOG = LogManager.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/getCustomersByQuery", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CustomerDto> getCustomersByQuery(@RequestParam(name = "query") String query,
            @ApiIgnore Authentication auth,
            @RequestParam(name = "hasRequestsEnabled", required = false) Boolean hasRequestsEnabled)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /customer/getCustomersByQuery");
        return customerService.getCustomersByQuery(query, auth, hasRequestsEnabled);
    }

    @GetMapping(value = "/getCustomerByEmail", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CustomerDto> getCustomerByEmail(@RequestParam(name = "email") String email)
            throws ValidationException, ServiceException {
        LOG.info("[REST] Call to /customer/getCustomerByEmail");
        return customerService.findByEmail(email);
    }
}
