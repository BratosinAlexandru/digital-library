package ro.digitallibrary.library.controller.book;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ro.digitallibrary.library.dto.book.BookDto;
import ro.digitallibrary.library.dto.book.BookOverviewDto;
import ro.digitallibrary.library.dto.book.BookOverviewPageDto;
import ro.digitallibrary.library.dto.book.GetBooksDto;
import ro.digitallibrary.library.exceptionhandling.ServiceException;
import ro.digitallibrary.library.exceptionhandling.ValidationException;
import ro.digitallibrary.library.persistence.repository.book.BookRepository;
import ro.digitallibrary.library.service.book.BookService;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {
    private static final Logger LOG = LogManager.getLogger(BookController.class);

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookService bookService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookOverviewPageDto getBooks(@RequestBody GetBooksDto getBooksDto,
                                        @RequestParam(required = false, defaultValue = "0") int page,
                                        @RequestParam(required = false, defaultValue = "25") int size) {
        LOG.info("[REST] Getting books");
        return bookService.getBooks(getBooksDto, PageRequest.of(page, size));
    }

    @GetMapping(value = "/{bookId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BookDto getBook(@PathVariable String bookId) throws ValidationException, ServiceException {
        LOG.info("[REST] Getting book");
        return bookService.getBook(bookId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/{bookId}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public ByteArrayResource getImage(@PathVariable String bookId) {
        LOG.info("[REST] Getting image for book with id: " + bookId);
//        return bookService.getBookImage(bookId);
        return bookRepository.findById(bookId).filter(book -> book.getImage() != null)
                .map(book -> new ByteArrayResource(book.getImage()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/getOverviews", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookOverviewDto> getBooksOverview(
            @RequestParam(name = "includeDisabled", required = false) Boolean includeDisabled) {
        LOG.info("[REST] Call to /book/getOverviews");
        return bookService.getBooksOverview(includeDisabled);
    }

    @PutMapping(value = "/{bookId}/enable")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enableBook(@PathVariable String bookId) {
        LOG.info("[REST] Call to /book/bookId/enable");
        bookService.enableBook(bookId);
    }

    @PutMapping(value = "/{bookId}/disable")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void disableBook(@PathVariable String bookId) {
        LOG.info("[REST] Call to /book/bookId/disable");
        bookService.disableBook(bookId);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBook(@RequestBody BookDto bookDto) {
        LOG.info("[REST] Call to /book/update");
        bookService.updateBook(bookDto);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto createBook(@RequestBody BookDto book) throws Exception {
        LOG.info("[REST] Call to /book/create)");
        return bookService.createBook(book);
    }

    @DeleteMapping(value = "/delete/{bookId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable String bookId) {
        LOG.info("[REST] Call to /book/delete");
        bookService.deleteBook(bookId);
    }
}
