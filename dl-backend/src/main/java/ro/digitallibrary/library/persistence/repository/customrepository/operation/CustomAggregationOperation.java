package ro.digitallibrary.library.persistence.repository.customrepository.operation;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;

import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

public class CustomAggregationOperation implements AggregationOperation {

    private Document operation;
    protected  final Logger LOG = LoggerFactory.getLogger(this.getClass());

    public CustomAggregationOperation(String operation, String query) {
        try {
            this.operation = new Document(operation, new JSONParser(-1).parse(query));
        } catch (ParseException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public Document toDocument(AggregationOperationContext context) {
        return context.getMappedObject(operation);
    }
}
