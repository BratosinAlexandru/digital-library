package ro.digitallibrary.library.persistence.repository.templates;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.beans.templates.NotificationType;
import ro.digitallibrary.library.persistence.entity.templates.Notification;
import ro.digitallibrary.library.persistence.entity.templates.NotificationTemplate;

import java.util.List;

@Repository
public interface NotificationTemplateRepository extends MongoRepository<NotificationTemplate, String> {
    List<NotificationTemplate> findByNotification(Notification notification);

    NotificationTemplate findOneByNotificationAndTypeAndLanguageAndEnabledTrue(Notification notification,
                                                                               NotificationType type, String language);

    NotificationTemplate findOneByNotificationAndTypeAndIsDefaultTrueAndEnabledTrue(Notification notification,
                                                                                    NotificationType type);

    List<NotificationTemplate> findByNotificationAndTypeAndIsDefaultTrue(Notification notification,
                                                                         NotificationType type);
}
