package ro.digitallibrary.library.persistence.repository.book;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.book.Book;

import java.util.List;

@Repository
public interface BookRepository extends MongoRepository<Book, String>, CustomBookRepository {
    @Query(value = "{attachment : {$ne: null}}")
    List<Book> getBookByAttachmentCount();

    @Query(value = "{enabled : ?0, attachment : {$ne: null}}")
    List<Book> getBookByAttachmentCountAndEnabled(boolean enabled);

    Book getBookById(String id);
}
