package ro.digitallibrary.library.persistence.entity.request;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;
import ro.digitallibrary.library.persistence.entity.book.Book;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "requests")
public class Request extends AbstractMongoEntity {

    @DBRef
    private Customer customer;
    private String college;
    private List<String> phones;
    private List<String> emails;
    private String otherMentions;
    private Boolean importantRequest;
    private String accountId;
    @CreatedDate
    private LocalDateTime requestDate;
    @DBRef
    private Book book;
    @DBRef
    private List<RequestType> requestType;
    @DBRef
    private RequestStatus requestStatus;
    @DBRef
    private List<CommunicationChannel> communicationChannels;


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public String getOtherMentions() {
        return otherMentions;
    }

    public void setOtherMentions(String otherMentions) {
        this.otherMentions = otherMentions;
    }

    public Boolean getImportantRequest() {
        return importantRequest;
    }

    public void setImportantRequest(Boolean importantRequest) {
        this.importantRequest = importantRequest;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public List<RequestType> getRequestType() {
        return requestType;
    }

    public void setRequestType(List<RequestType> requestType) {
        this.requestType = requestType;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public List<CommunicationChannel> getCommunicationChannels() {
        return communicationChannels;
    }

    public void setCommunicationChannels(List<CommunicationChannel> communicationChannels) {
        this.communicationChannels = communicationChannels;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "Request{" +
                "customer=" + customer +
                ", college='" + college + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", otherMentions='" + otherMentions + '\'' +
                ", importantRequest=" + importantRequest +
                ", accountId='" + accountId + '\'' +
                ", requestDate=" + requestDate +
                ", book=" + book +
                ", requestType=" + requestType +
                ", requestStatus=" + requestStatus +
                ", communicationChannels=" + communicationChannels +
                '}';
    }
}
