package ro.digitallibrary.library.persistence.entity.book;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

import java.time.LocalDateTime;

@Document(collection = "borrowed_books")
public class BorrowedBooks extends AbstractMongoEntity {

    private String accountId;
    private String bookId;
    private LocalDateTime expiryDate;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }
}
