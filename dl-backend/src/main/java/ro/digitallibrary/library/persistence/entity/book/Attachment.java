package ro.digitallibrary.library.persistence.entity.book;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "uploads.files")
public class Attachment {
    @Id
    private String id;
    private Long length;
    private Integer chunkSize;
    @CreatedDate
    private LocalDateTime uploadDate;
    private String filename;
    private String md5;
    private String contentType;
    private org.bson.Document metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Integer getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(Integer chunkSize) {
        this.chunkSize = chunkSize;
    }

    public LocalDateTime getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(LocalDateTime uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public org.bson.Document getMetadata() {
        return metadata;
    }

    public void setMetadata(org.bson.Document metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id='" + id + '\'' +
                ", length=" + length +
                ", chunkSize=" + chunkSize +
                ", uploadDate=" + uploadDate +
                ", filename='" + filename + '\'' +
                ", md5='" + md5 + '\'' +
                ", contentType='" + contentType + '\'' +
                ", metadata=" + metadata +
                '}';
    }
}
