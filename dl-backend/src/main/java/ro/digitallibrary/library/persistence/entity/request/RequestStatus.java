package ro.digitallibrary.library.persistence.entity.request;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

@Document(collection = "request_statuses")
public class RequestStatus extends AbstractMongoEntity {

    private String name;
    private String color;
    private Long orderId;
    private Boolean isDefaultValue;
    private Boolean approachRequest;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Boolean getDefaultValue() {
        return isDefaultValue;
    }

    public void setDefaultValue(Boolean defaultValue) {
        isDefaultValue = defaultValue;
    }

    public Boolean getApproachRequest() {
        return approachRequest;
    }

    public void setApproachRequest(Boolean approachRequest) {
        this.approachRequest = approachRequest;
    }
}
