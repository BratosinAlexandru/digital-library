package ro.digitallibrary.library.persistence.repository.book;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ro.digitallibrary.library.dto.book.GetBooksDto;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.entity.book.Book;
import ro.digitallibrary.library.persistence.entity.book.BookPage;
import ro.digitallibrary.library.persistence.entity.book.BorrowedBooks;
import ro.digitallibrary.library.persistence.repository.request.RequestRepository;
import ro.digitallibrary.library.service.account.AccountService;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.count;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.facet;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.lookup;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CustomBookRepositoryImpl implements CustomBookRepository {
    private static final String CREATED_ON = "createdOn";
    private static final String TOTAL_COUNT = "totalCount";
    private static final String CONTENT = "content";

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private BorrowedBooksRepository borrowedBooksRepository;
    @Autowired
    private AccountService accountService;

    @Override
    public Page<Book> getBooks(GetBooksDto getBooksDto, Pageable pageable) {
        List<AggregationOperation> operations = new ArrayList<>();
        operations.add(match(getBookMatchingCriteria(getBooksDto, null)));
        List<AggregationOperation> sortAndPaginationOperations = new ArrayList<>();
        sortAndPaginationOperations.add(sort(Direction.DESC, prefixKey(CREATED_ON, null)));
        addPagingOperations(sortAndPaginationOperations, pageable);

        operations.add(facet().and(count().as("count")).as(TOTAL_COUNT)
                .and(sortAndPaginationOperations.toArray(new AggregationOperation[0])).as(CONTENT));
        operations.add(unwind(TOTAL_COUNT, false));
        operations.add(project(CONTENT, "totalCount.count"));

        return getPageFromAggregation(operations, "books", pageable);
    }

    private Criteria getBookMatchingCriteria(GetBooksDto getBooksDto, String keyPrefix) {
        Criteria criteria = Criteria.where(prefixKey(CREATED_ON, keyPrefix))
                .gte(getBooksDto.getFrom()).lte(getBooksDto.getTo())
                .and(prefixKey("enabled", keyPrefix)).is(!getBooksDto.isDisabledOnly());

        if (getBooksDto.getDomainIds() != null) {
            criteria = criteria.and(prefixKey("domainId", keyPrefix)).in(getBooksDto.getDomainIds());
        }

        if (getBooksDto.getCategoryIds() != null) {
            criteria = criteria.and(prefixKey("categoryId", keyPrefix)).in(getBooksDto.getCategoryIds());
        }

        if (getBooksDto.getName() != null) {
            criteria = criteria.and(prefixKey("name", keyPrefix))
                    .regex(Pattern.compile(Pattern.quote(getBooksDto.getName()).replaceAll("\\W\\s", ""),
                            Pattern.CASE_INSENSITIVE));
        }

        if (getBooksDto.getAuthorName() != null) {
            criteria = criteria.and(prefixKey("authorName", keyPrefix))
                    .regex(Pattern.compile(Pattern.quote(getBooksDto.getAuthorName()).replaceAll("\\W\\s", ""),
                            Pattern.CASE_INSENSITIVE));
        }

        if (getBooksDto.getMyBooks() != null && getBooksDto.getMyBooks()) {
            List<ObjectId> borrowedBooksIds = borrowedBooksRepository
                    .findAllByAccountId(accountService.getCurrentAccount().getId())
                        .stream().map(book -> new ObjectId(book.getBookId())).collect(Collectors.toList());
            criteria = criteria.and("_id").in(borrowedBooksIds);
        }

        /** TODO: Testing this idea */
//        if (getBooksDto.getNumberOfPages() != null) {
//            criteria = criteria.and("numberOfPages").is(getBooksDto.getNumberOfPages());
//        }

        return criteria;
    }

    private void addPagingOperations(List<AggregationOperation> operations, Pageable pageable) {
        operations.add(skip((long) pageable.getPageNumber() * pageable.getPageSize()));
        operations.add(limit(pageable.getPageSize()));
    }

    private Page<Book> getPageFromAggregation(List<AggregationOperation> operations, String collectionName,
                                              Pageable pageable) {
        Aggregation aggregation = newAggregation(operations);
        AggregationResults<BookPage> results = mongoTemplate.aggregate(aggregation, collectionName, BookPage.class);
        BookPage bookPage = results.getUniqueMappedResult() != null ? results.getUniqueMappedResult() : new BookPage();
        return PageableExecutionUtils.getPage(bookPage.getContent(), pageable, () -> bookPage.getCount());
    }

    private String prefixKey(String key, String prefix) {
        return prefix == null || prefix.isEmpty() ? key : String.format("%s.%s", prefix, key);
    }
}
