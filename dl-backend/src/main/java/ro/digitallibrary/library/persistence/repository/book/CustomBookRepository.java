package ro.digitallibrary.library.persistence.repository.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ro.digitallibrary.library.dto.book.GetBooksDto;
import ro.digitallibrary.library.persistence.entity.book.Book;

public interface CustomBookRepository {

    Page<Book> getBooks(GetBooksDto getBooksDto, Pageable pageable);
}
