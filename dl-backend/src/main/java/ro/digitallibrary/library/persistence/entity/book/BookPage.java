package ro.digitallibrary.library.persistence.entity.book;

import java.util.ArrayList;
import java.util.List;

public class BookPage {
    private List<Book> content = new ArrayList<>();
    private int count = 0;

    public List<Book> getContent() {
        return content;
    }

    public void setContent(List<Book> content) {
        this.content = content;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
