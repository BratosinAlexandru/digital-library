package ro.digitallibrary.library.persistence.entity.templates;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.beans.templates.NotificationType;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

@Document(collection = "notification_template")
public class NotificationTemplate extends AbstractMongoEntity {

    @DBRef
    private Notification notification;
    private String content;
    private String subject;
    private String language;
    private NotificationType type;
    private Boolean isDefault = false;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }
}
