package ro.digitallibrary.library.persistence.repository.app;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.app.AppKey;

import java.util.List;

@Repository
public interface AppKeyRepository extends MongoRepository<AppKey, String> {

    List<AppKey> findByKey(String key);
    AppKey findOneByKey(String key);
    boolean existsByKey(String key);
    AppKey getByKey(String key);

}
