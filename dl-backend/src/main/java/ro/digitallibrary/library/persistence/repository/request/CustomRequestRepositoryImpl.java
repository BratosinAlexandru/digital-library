package ro.digitallibrary.library.persistence.repository.request;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.dto.request.CustomerDto;
import ro.digitallibrary.library.dto.request.RequestPage;
import ro.digitallibrary.library.dto.request.RequestTypeDto;
import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.persistence.entity.request.Customer;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.repository.customrepository.operation.CustomAggregationOperation;
import ro.digitallibrary.library.service.account.AccountService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ro.digitallibrary.library.persistence.repository.customrepository.operation.Constants.*;

public class CustomRequestRepositoryImpl implements CustomRequestRepository {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private AccountService accountService;
    @Autowired
    private CustomerRepository customerRepository;


    @Override
    public RequestPage getCustomPagedObjects(PaginationRequestDto<FiltersRequest> paginationRequest, Authentication auth)
            throws RepositoryException {
        LOG.info("[RequestPage Repository] Call getCustomPagedObjects");

        List<AggregationOperation> operationList = new ArrayList<>();

        AggregationOperation match = getMatchAggregation(paginationRequest.getFiltersRequest(), auth);
        if (match != null) {
            operationList.add(match);
        }
        AggregationOperation facet = getFacetAggregationOperation(paginationRequest);
        if (facet != null) {
            operationList.add(facet);
        }

        AggregationOperation formatOutput = new CustomAggregationOperation($ADD_FIELDS, CUSTOM_ADD_FIELDS_OPERATION);
        if (formatOutput != null) {
            operationList.add(formatOutput);
        }

        AggregationOperation[] operations = new AggregationOperation[operationList.size()];
        operationList.toArray(operations);
        Aggregation aggregation = Aggregation.newAggregation(Request.class, operations)
                .withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());
        AggregationResults<RequestPage> result = mongoTemplate.aggregate(aggregation, REQUESTS, RequestPage.class);
        if (result.getMappedResults() != null && result.getMappedResults().size() > 0) {
            return result.getMappedResults().get(0);
        } else {
            return null;
        }
    }

    private AggregationOperation getMatchAggregation(FiltersRequest filtersRequest, Authentication auth)
            throws RepositoryException {
        List<Criteria> criteriaList = new ArrayList<>();

        if (filtersRequest != null) {
            criteriaList.add(Criteria.where(ENABLED)
                    .is(filtersRequest.getEnabled() == null || filtersRequest.getEnabled()));

            if (filtersRequest.getStartDate() != null) {
                criteriaList.add(Criteria.where(CREATED_ON).gt(filtersRequest.getStartDate()));
            }

            if (filtersRequest.getEndDate() != null) {
                criteriaList.add(Criteria.where(CREATED_ON).lt(filtersRequest.getEndDate()));
            }

            if (filtersRequest.getCustomer() != null) {
                CustomerDto customer = filtersRequest.getCustomer();
                if (customer.getId() != null) {
                    criteriaList.add(Criteria.where(CUSTOMER + DOT + $ + ID)
                        .is(new ObjectId(filtersRequest.getCustomer().getId())));
                } else if (customer.getName() != null && customer.getName().length() > 0) {
                    List<Customer> customers = customerRepository.findByText(customer.getName(), null);
                    List<ObjectId> ids = customers.stream().map(e -> new ObjectId(e.getId()))
                            .collect(Collectors.toList());
                    criteriaList.add(Criteria.where(CUSTOMER + DOT + $ + ID).in(ids));
                }
            }

            if (filtersRequest.getRequestType() != null && filtersRequest.getRequestType().length > 0) {
                List<ObjectId> in = new ArrayList<ObjectId>();
                for (RequestTypeDto requestType : filtersRequest.getRequestType()) {
                    in.add(new ObjectId(requestType.getId()));
                }
                criteriaList.add(Criteria.where(REQUEST_TYPE + DOT + $ + ID).all(in));
            }

            if (filtersRequest.getImportantRequest() != null) {
                criteriaList.add(Criteria.where(IMPORTANT_REQUEST).is(filtersRequest.getImportantRequest()));
            }

            if (filtersRequest.getRequestStatus() != null) {
                criteriaList.add(Criteria.where(REQUEST_STATUS + DOT + $ + ID)
                        .is(new ObjectId(filtersRequest.getRequestStatus().getId())));
            }
        }

        if (auth != null) {
            List<String> roles = auth.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());
            if (!roles.contains(ROLE_ADMIN)) {
                criteriaList.add(Criteria.where(ACCOUNT_ID).is(accountService.getCurrentAccount().getId()));
            }
        }

        if (criteriaList.size() > 0) {
            Criteria[] criteria = new Criteria[criteriaList.size()];
            criteriaList.toArray(criteria);
            return Aggregation.match(new Criteria().andOperator(criteria));
        } else {
            return null;
        }
    }

    private AggregationOperation getFacetAggregationOperation(PaginationRequestDto<FiltersRequest> paginationRequest) {
        return Aggregation.facet().and(Aggregation.count().as(COUNT)).as(TOTAL_ELEMENTS)
                .and(Aggregation.unwind($ + REQUEST_STATUS, false),
                        new CustomAggregationOperation($PROJECT,
                                String.format(MAP_REF_TO_ID_TEMPLATE, REQUEST_STATUS, REQUEST_STATUS)),
                        Aggregation.group(Fields.fields($ + REQUEST_STATUS)).count().as(COUNT),
                        Aggregation.lookup(REQUEST_STATUSES, _ID, _ID, REQUEST_STATUS),
                        Aggregation.sort(new Sort(Direction.DESC, COUNT)))
                .as(REQUEST_STATUS).and(getFacetPageAggregation(paginationRequest)).as(PAGINATION);
    }

    private AggregationOperation[] getFacetPageAggregation(PaginationRequestDto<FiltersRequest> paginationRequest) {
        List<AggregationOperation> operationList = new ArrayList<>();
        Sort objSort;
        if (paginationRequest.getSortDirection() != null
                && (paginationRequest.getSortDirection().equals(ASC)
                || paginationRequest.getSortDirection().equals(DESC))
                && paginationRequest.getSortProperties() != null && paginationRequest.getSortProperties().size() > 0) {
            switch (paginationRequest.getSortProperties().get(0)) {
                case REQUEST_STATUS:
                    operationList.add(new CustomAggregationOperation($ADD_FIELDS,
                            String.format(MAP_REF_TO_ID_TEMPLATE, INSENSITIVE, REQUEST_STATUS)));
                    operationList.add(new CustomAggregationOperation($LOOKUP,
                            String.format(LOOKUP_TEMPLATE, REQUEST_STATUSES, INSENSITIVE, _ID, INSENSITIVE)));
                    operationList.add(Aggregation.unwind($ + INSENSITIVE));
                    operationList.add(new CustomAggregationOperation($ADD_FIELDS,
                            String.format(ADD_INSENSITIVE_TEMPLATE, INSENSITIVE)));
                    objSort = new Sort(paginationRequest.getSortDirection().equals(ASC) ? Direction.ASC : Direction.DESC,
                            INSENSITIVE);
                    operationList.add(Aggregation.sort(objSort));
                    break;
                case IMPORTANT_REQUEST:
                    objSort = new Sort(paginationRequest.getSortDirection().equals(ASC)
                            ? Direction.ASC : Direction.DESC, IMPORTANT_REQUEST);
                    operationList.add(Aggregation.sort(objSort));
                    break;
                case CREATED_ON:
                    objSort = new Sort(paginationRequest.getSortDirection().equals(ASC)
                            ? Direction.ASC : Direction.DESC, CREATED_ON);
                    operationList.add(Aggregation.sort(objSort));
                    break;
                case REQUEST_DATE:
                    objSort = new Sort(paginationRequest.getSortDirection().equals(ASC)
                            ? Direction.ASC : Direction.DESC, REQUEST_DATE);
                    operationList.add(Aggregation.sort(objSort));
                    break;
                case CUSTOMER:
                    operationList.add(new CustomAggregationOperation($ADD_FIELDS,
                            String.format(MAP_REF_TO_ID_TEMPLATE, INSENSITIVE, CUSTOMER)));
                    operationList.add(new CustomAggregationOperation($LOOKUP,
                            String.format(LOOKUP_TEMPLATE, CUSTOMERS, INSENSITIVE, _ID, INSENSITIVE)));
                    operationList.add(Aggregation.unwind($ + INSENSITIVE));
                    operationList.add(new CustomAggregationOperation($ADD_FIELDS,
                            String.format(ADD_INSENSITIVE_TEMPLATE, INSENSITIVE)));
                    objSort = new Sort(paginationRequest.getSortDirection().equals(ASC) ? Direction.ASC : Direction.DESC,
                            INSENSITIVE);
                    operationList.add(Aggregation.sort(objSort));
                    break;
                default:
                    break;
            }
        } else {
            objSort = new Sort(Direction.DESC, CREATED_ON);
            operationList.add(Aggregation.sort(objSort));
        }
        long offset = paginationRequest.getPageNumber() > 0
                ? paginationRequest.getPageNumber() * paginationRequest.getPageSize()
                : 0;
        operationList.add(Aggregation.skip(offset));
        operationList.add(Aggregation.limit(paginationRequest.getPageSize()));
        operationList.add(Aggregation.project().andExclude(INSENSITIVE));
        operationList.add(Aggregation.group().addToSet(paginationRequest.getPageNumber())
                .as(PAGE_NUMBER).addToSet(paginationRequest.getPageSize()).as(PAGE_SIZE).push(ROOT).as(CONTENT));

        operationList.add(Aggregation.project().andInclude(PAGE_SIZE, PAGE_NUMBER)
                .and(ArrayOperators.Slice.sliceArrayOf(CONTENT).itemCount(paginationRequest.getPageSize()))
                .as(CONTENT));

        AggregationOperation[] operations = new AggregationOperation[operationList.size()];
        operationList.toArray(operations);
        return operations;
    }
}
