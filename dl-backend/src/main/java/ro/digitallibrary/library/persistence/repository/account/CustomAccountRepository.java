package ro.digitallibrary.library.persistence.repository.account;

import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.persistence.entity.account.Account;

import java.util.List;

public interface CustomAccountRepository {

    public List<Account> findByText(String text, Boolean hasRequestsEnabled) throws RepositoryException;
}
