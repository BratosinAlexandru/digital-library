package ro.digitallibrary.library.persistence.entity.categories;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;
import ro.digitallibrary.library.persistence.entity.domains.Domain;

@Document(collection = "categories")
public class Category  extends AbstractMongoEntity {

    @DBRef
    private Domain domain;
    private String name;
    private long orderId;

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Category{" +
                "domain=" + domain +
                ", name='" + name + '\'' +
                ", orderId=" + orderId +
                '}';
    }
}
