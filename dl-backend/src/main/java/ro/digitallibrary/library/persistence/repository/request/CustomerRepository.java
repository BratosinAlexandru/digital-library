package ro.digitallibrary.library.persistence.repository.request;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.request.Customer;

import java.util.List;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String>, CustomCustomerRepository {

    List<Customer> findByEnabledTrue();

    @Query(value = "{$or: [{'emails' : {'$in' : [?0]}}, {'phones' : {'$in' : [?1]}}]}")
    List<Customer> findByEmailsOrPhones(List<String> emails, List<String> phones);

    @Query(value = "{'emails' : {'$in' : ?0}}")
    List<Customer> findByEmails(List<String> emails);

    @Query(value = "{'phones' : {'$in' : [?0]}}")
    List<Customer> findByPhones(List<String> phones);

    @Query(value = "{'emails' : {'$in' : [{'$regex' : ?0, '$options' : 'i'}]}}")
    List<Customer> findByEmail(String email);

    @Query(value = "{'phones' : {'$in' : [{'$regex' : ?0, '$options' : 'i'}]}}")
    List<Customer> findByPhone(String phone);

}
