package ro.digitallibrary.library.persistence.repository.account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.account.Account;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends MongoRepository<Account, String>, CustomAccountRepository {

    Optional<Account> findByEmailIgnoreCase(String email);
    long countByEmail(String email);


    /**
     * getting paged accounts, queried by name or email, ignoring case
     * @param query - name / email
     * @param pageable - pageable request
     * @return - paged accounts
     */
    @Query(value = "{'$or': [{ 'name' : {'$regex' : ?0, '$options' : 'i'}}, { 'email' : {'$regex' : ?0, '$options' : 'i'}}]}")
    Page<Account> findByQuery(String query, Pageable pageable);

    @Query(value = "{roles: {$in: ?0}, enabled: true}")
    List<Account> findByRolesAndEnabledTrue(List<String> roles);

    Account getById(String id);

}
