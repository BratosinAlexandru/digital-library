package ro.digitallibrary.library.persistence.repository.request;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.request.Request;

import java.util.List;

@Repository
@JaversSpringDataAuditable
public interface RequestRepository extends MongoRepository<Request, String>, CustomRequestRepository {

    List<Request> findByEnabledTrue();

    List<Request> findAllByIdIn(List<String> requestIds);

    long countByCommunicationChannels(String id);

    long countByRequestType(String id);

    long countByRequestStatus(String id);
}
