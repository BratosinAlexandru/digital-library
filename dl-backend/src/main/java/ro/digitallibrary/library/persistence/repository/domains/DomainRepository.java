package ro.digitallibrary.library.persistence.repository.domains;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.domains.Domain;

import java.util.List;
import java.util.Optional;

@Repository
public interface DomainRepository extends MongoRepository<Domain, String> {

    Optional<Domain> findByName(String name);

    List<Domain> findByEnabledTrue();

    Domain getByName(String name);
}
