package ro.digitallibrary.library.persistence.entity.domains;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

@Document(collection = "domain")
public class Domain extends AbstractMongoEntity {
    private String name;
    private long orderId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "name='" + name + '\'' +
                ", orderId=" + orderId +
                '}';
    }
}
