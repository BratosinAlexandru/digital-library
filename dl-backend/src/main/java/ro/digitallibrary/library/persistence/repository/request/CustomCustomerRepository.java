package ro.digitallibrary.library.persistence.repository.request;

import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.persistence.entity.request.Customer;

import java.util.List;

public interface CustomCustomerRepository {

    public List<Customer> findByText(String text, Boolean hasRequestsEnabled) throws RepositoryException;
}
