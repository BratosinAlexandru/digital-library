package ro.digitallibrary.library.persistence.repository.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import ro.digitallibrary.library.dto.request.RequestList;
import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.persistence.entity.account.Account;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.repository.customrepository.operation.CustomAggregationOperation;

import java.util.ArrayList;
import java.util.List;

import static ro.digitallibrary.library.persistence.repository.customrepository.operation.Constants.*;

public class CustomAccountRepositoryImpl implements CustomAccountRepository {

    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Account> findByText(String text, Boolean hasRequestsEnabled) throws RepositoryException {
        LOG.info("[Custom Account Repository] Get accounts by name/email");

        try {
            Query query = new Query();
            String re = handleSpecialCharacters(text);
            query.addCriteria(new Criteria().orOperator(Criteria.where(NAME).regex(re, I)));
            if (hasRequestsEnabled != null) {
                Criteria criteria = getAccountCriteria(hasRequestsEnabled);
                if (criteria != null) {
                    query.addCriteria(criteria);
                } else {
                    return null;
                }
            }
            query.limit(25);
            return mongoTemplate.find(query, Account.class);
        } catch (Exception e) {
            LOG.error("[Custom Account Repository] Exception occurred", e);
            throw new RepositoryException(e.getMessage());
        }
    }

    private Criteria getAccountCriteria(Boolean hasRequestsEnabled) {
        List<AggregationOperation> operationList = new ArrayList<>();
        Criteria matchCriteria = new Criteria();

        if (hasRequestsEnabled != null) {
            matchCriteria.and(ENABLED).is(hasRequestsEnabled);
        }

        operationList.add(Aggregation.match(matchCriteria));
        operationList.add(new CustomAggregationOperation($PROJECT,
                String.format(PROJECT_FIELD_AS_OBJID_TEMPLATE, ACCOUNT, ACCOUNT)));
        operationList.add(Aggregation.group().push($ + ACCOUNT).as(CONTENT));

        AggregationOperation[] operations = new AggregationOperation[operationList.size()];
        operationList.toArray(operations);
        Aggregation aggregation = Aggregation.newAggregation(Request.class, operations)
                .withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());
        AggregationResults<RequestList> result = mongoTemplate.aggregate(aggregation, REQUESTS, RequestList.class);
        if (result.getMappedResults() != null && result.getMappedResults().size() > 0) {
            RequestList list = result.getMappedResults().get(0);
            return Criteria.where(_ID).in(list.getContent());
        } else {
            return null;
        }

    }

    private String handleSpecialCharacters(String query) {
        String text = new String(query);
        text = text.replaceAll("[-\\[\\]{}()*+?.,\\^$|#\\\\]", "\\$&");
        text = text.replaceAll("[\u021bt\u021aT]+", "[\u021bt\u021aT]");
        text = text.replaceAll("[\u0103a\u00e2A\u0102\u00c2]+", "[\u0103a\u00e2A\u0102\u00c2]");
        text = text.replaceAll("[i\u00ee\u00ceI]+", "[i\u00ee\u00ceI]");
        text = text.replaceAll("[\u0219sS\u0218]+", "[\u0219sS\u0218]");
        return "^" + text + "|(?=.*" + text.replaceAll("\\s", ")(?=.*") + ")";
    }
}
