package ro.digitallibrary.library.persistence.repository.request;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.request.RequestType;

import java.util.List;

@Repository
public interface RequestTypeRepository extends MongoRepository<RequestType, String> {

    List<RequestType> findByEnabledTrue();

    List<RequestType> findByIsDefaultValueTrue();
}
