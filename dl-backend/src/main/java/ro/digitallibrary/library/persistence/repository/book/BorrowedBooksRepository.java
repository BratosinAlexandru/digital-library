package ro.digitallibrary.library.persistence.repository.book;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.book.BorrowedBooks;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BorrowedBooksRepository extends MongoRepository<BorrowedBooks, String> {

    @Query(value = "{'accountId' : ?0}", fields = "{'bookId' : 1}")
    List<BorrowedBooks> findAllByAccountId(String accountId);

    @Query(value = "{'expiryDate' : {'$lte' : ?0}}", fields = "{'accountId' : 1}")
    List<BorrowedBooks> findExpiredBooks(LocalDateTime date);

    @Query(value ="{'expiryDate' : {'$gte' : ?0, '$lte' : ?1}}", fields = "{'accountId' : 1}")
    List<BorrowedBooks> findExpiredBooksWithinWeek(LocalDateTime start, LocalDateTime end);

}
