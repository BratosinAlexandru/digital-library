package ro.digitallibrary.library.persistence.repository.templates;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.beans.templates.NotificationKey;
import ro.digitallibrary.library.persistence.entity.templates.Notification;

import java.util.List;

@Repository
public interface NotificationRepository extends MongoRepository<Notification, String> {
    List<Notification> findByEnabledTrue();

    Notification findOneByKey(NotificationKey key);
}
