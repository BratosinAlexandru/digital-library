package ro.digitallibrary.library.persistence.repository.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ro.digitallibrary.library.dto.request.RequestList;
import ro.digitallibrary.library.exceptionhandling.RepositoryException;
import ro.digitallibrary.library.persistence.entity.request.Customer;
import ro.digitallibrary.library.persistence.entity.request.Request;
import ro.digitallibrary.library.persistence.repository.customrepository.operation.CustomAggregationOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static ro.digitallibrary.library.persistence.repository.customrepository.operation.Constants.*;

public class CustomCustomerRepositoryImpl implements CustomCustomerRepository {
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Customer> findByText(String text, Boolean hasRequestsEnabled) throws RepositoryException {
        LOG.info("[Custom Customer Repository] Get accounts by name/email");

        try {
            Query query = new Query();
            String re = handleSpecialCharacters(text);
            query.addCriteria(new Criteria().orOperator(Criteria.where(NAME).regex(re, I),
                    Criteria.where(EMAILS).regex(Pattern.quote(text), I), Criteria.where(PHONES).regex(re, I)));
            if (hasRequestsEnabled != null) {
                Criteria criteria = getCustomerCriteria(hasRequestsEnabled);
                if (criteria != null) {
                    query.addCriteria(criteria);
                } else {
                    return null;
                }
            }
            query.limit(25);
            return mongoTemplate.find(query, Customer.class);
        } catch (Exception e) {
            LOG.error("[Custom Customer Repository] Exception occurred", e);
            throw new RepositoryException(e.getMessage());
        }
    }

    private Criteria getCustomerCriteria(Boolean hasRequestsEnabled) {
        List<AggregationOperation> operationList = new ArrayList<>();
        Criteria matchCriteria = new Criteria();

        if (hasRequestsEnabled != null) {
            matchCriteria.and(ENABLED).is(hasRequestsEnabled);
        }

        operationList.add(Aggregation.match(matchCriteria));
        operationList.add(new CustomAggregationOperation($PROJECT,
                String.format(PROJECT_FIELD_AS_OBJID_TEMPLATE, CUSTOMER, CUSTOMER)));
        operationList.add(Aggregation.group().push($ + CUSTOMER).as(CONTENT));

        AggregationOperation[] operations = new AggregationOperation[operationList.size()];
        operationList.toArray(operations);
        Aggregation aggregation = Aggregation.newAggregation(Request.class, operations)
                .withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());
        AggregationResults<RequestList> result = mongoTemplate.aggregate(aggregation, REQUESTS, RequestList.class);
        if (result.getMappedResults() != null && result.getMappedResults().size() > 0) {
            RequestList list = result.getMappedResults().get(0);
            return Criteria.where(_ID).in(list.getContent());
        } else {
            return null;
        }

    }

    private String handleSpecialCharacters(String query) {
        String text = new String(query);
        text = text.replaceAll("[-\\[\\]{}()*+?.,\\^$|#\\\\]", "\\$&");
        text = text.replaceAll("[\u021bt\u021aT]+", "[\u021bt\u021aT]");
        text = text.replaceAll("[\u0103a\u00e2A\u0102\u00c2]+", "[\u0103a\u00e2A\u0102\u00c2]");
        text = text.replaceAll("[i\u00ee\u00ceI]+", "[i\u00ee\u00ceI]");
        text = text.replaceAll("[\u0219sS\u0218]+", "[\u0219sS\u0218]");
        return "^" + text + "|(?=.*" + text.replaceAll("\\s", ")(?=.*") + ")";
    }
}
