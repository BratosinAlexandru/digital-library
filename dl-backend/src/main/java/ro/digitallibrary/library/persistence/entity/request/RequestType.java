package ro.digitallibrary.library.persistence.entity.request;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

@Document(collection = "request_types")
public class RequestType extends AbstractMongoEntity {

    private String name;
    private Boolean isDefaultValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDefaultValue() {
        return isDefaultValue;
    }

    public void setDefaultValue(Boolean defaultValue) {
        isDefaultValue = defaultValue;
    }
}
