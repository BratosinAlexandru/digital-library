package ro.digitallibrary.library.persistence.entity.templates;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.beans.templates.NotificationKey;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

import java.util.List;

@Document(collection = "notifications")
public class Notification extends AbstractMongoEntity {

    private NotificationKey key;
    private List<String> emails;
    private Boolean enableToMain = false;
    private List<String> variables;
    private String name;

    public NotificationKey getKey() {
        return key;
    }

    public void setKey(NotificationKey key) {
        this.key = key;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public Boolean getEnableToMain() {
        return enableToMain;
    }

    public void setEnableToMain(Boolean enableToMain) {
        this.enableToMain = enableToMain;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
