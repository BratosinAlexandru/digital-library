package ro.digitallibrary.library.persistence.entity.request;

import org.javers.core.metamodel.annotation.ValueObject;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

import java.util.List;

@ValueObject
@Document(collection = "customers")
public class Customer extends AbstractMongoEntity {
    private String name;
    private List<String> emails;
    private List<String> phones;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }
}
