package ro.digitallibrary.library.persistence.repository.categories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.categories.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {

    @Query(value = "{'domain' : ?0}")
    List<Category> findByDomain(String domainId);

    List<Category> findByEnabledTrue();

    @Query(value = "{'domain' : ?0, 'enabled' : true}")
    List<Category> findByDomainAndEnabledTrue(String domainId);

    Category getById(String id);

    Optional<Category> findByName(String name);

    List<Category> findAllByNameIn(List<String> names);
}
