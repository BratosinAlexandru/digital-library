package ro.digitallibrary.library.persistence.repository.request;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.request.CommunicationChannel;

import java.util.List;

@Repository
public interface CommunicationChannelRepository extends MongoRepository<CommunicationChannel, String> {

    List<CommunicationChannel> findByEnabledTrue();
}
