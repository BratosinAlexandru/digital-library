package ro.digitallibrary.library.persistence.entity.account;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

import java.time.LocalDateTime;
import java.util.Arrays;

@Document(collection = "accounts")
public class Account  extends AbstractMongoEntity {

    @Indexed(unique = true, name="email_1")
    private String email;
    private String password;
    private String name;
    private String[] roles;
    private LocalDateTime lastLoginOn;
    private String profilePicture;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public LocalDateTime getLastLoginOn() {
        return lastLoginOn;
    }

    public void setLastLoginOn(LocalDateTime lastLoginOn) {
        this.lastLoginOn = lastLoginOn;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    @Override
    public String toString() {
        return "Account{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", roles=" + Arrays.toString(roles) +
                ", lastLoginOn=" + lastLoginOn +
                ", profilePicture='" + profilePicture + '\'' +
                '}';
    }
}
