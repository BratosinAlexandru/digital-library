package ro.digitallibrary.library.persistence.repository.request;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.digitallibrary.library.persistence.entity.request.RequestStatus;

import java.util.List;

@Repository
public interface RequestStatusRepository extends MongoRepository<RequestStatus, String> {

    List<RequestStatus> findByEnabledTrue();

    List<RequestStatus> findByIsDefaultValueTrue();

    RequestStatus findByName(String name);
}
