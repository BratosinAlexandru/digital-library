package ro.digitallibrary.library.persistence.entity.request;

import org.springframework.data.mongodb.core.mapping.Document;
import ro.digitallibrary.library.persistence.entity.AbstractMongoEntity;

@Document(collection = "communication_channels")
public class CommunicationChannel extends AbstractMongoEntity {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
