package ro.digitallibrary.library.persistence.repository.customrepository.operation;

public interface Constants {

    /** Operators used in aggregation operations */
    public static final String ROOT = "$$ROOT";
    public static final String $PROJECT = "$project";
    public static final String $ADD_FIELDS = "$addFields";
    public static final String $LOOKUP = "$lookup";
    public static final String $GROUP = "$group";
    public static final String I = "i";


    /** Used fields */
    public static final String ID = "id";
    public static final String _ID = "_id";
    public static final String COUNT = "count";
    public static final String TOTAL_ELEMENTS = "totalElements";
    public static final String ENABLED = "enabled";
    public static final String REQUEST_TYPE = "requestType";
    public static final String IMPORTANT_REQUEST = "importantRequest";
    public static final String REQUEST_STATUS = "requestStatus";
    public static final String REQUEST_STATUSES = "request_statuses";
    public static final String ACCOUNT_ID = "accountId";
    public static final String NAME = "name";
    public static final String EMAILS = "emails";
    public static final String PHONES = "phones";

    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String CREATED_ON = "createdOn";
    public static final String REQUEST_DATE = "requestDate";
    public static final String $ = "$";
    public static final String DOT = ".";
    public static final String INSENSITIVE = "insensitive";
    public static final String PAGINATION = "pagination";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String PAGE_SIZE = "pageSize";
    public static final String CONTENT = "content";
    public static final String ACCOUNT = "account";
    public static final String CUSTOMER = "customer";
    public static final String CUSTOMERS = "customers";
    /** Templates */
    public static final String MAP_REF_TO_ID = "{$map:{input:{$map:{input:['$%s'],in:{$arrayElemAt:[{$objectToArray:'$$this'},1]},}},in:'$$this.v'}}";
    public static final String MAP_REF_TO_ID_TEMPLATE = "{%s:" + MAP_REF_TO_ID + "}";
    public static final String LOOKUP_TEMPLATE = "{from: %s, localField: %s, foreignField: %s, as: %s}";
    public static final String ADD_INSENSITIVE_TEMPLATE = "{'insensitive': {$trim : {input: {'$toLower': '$%s.name'}} }}";
    public static final String ADD_INSENSITIVE_FIELD_TEMPLATE = "{'insensitive': {$trim : {input: {'$toLower': '$%s'}} }}";
    public static final String CUSTOM_ADD_FIELDS_OPERATION = "{pagination: {'$ifNull': [{ '$arrayElemAt': ['$pagination',0]}, {totalElements: 0, pageNumber: 0, pageSize: 0, content: []}]}}";

    public static final String MAP_REF_TO_OBJID_TEMPLATE = "{$arrayElemAt:[" + MAP_REF_TO_ID + ", 0]}";
    public static final String PROJECT_FIELD_AS_OBJID_TEMPLATE = "{%s:" + MAP_REF_TO_OBJID_TEMPLATE + "}";
    public static final String ADD_FIELD_TEMPLATE = "{'%s': '%s'}";
    public static final String PROJECT_ARRAY_TO_FIRST_ELEMENT = "{%s: {$arrayElemAt: ['$%s', 0]}}";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String REQUESTS = "requests";
}
