package ro.digitallibrary.library.persistence.repository.request;

import org.springframework.security.core.Authentication;
import ro.digitallibrary.library.dto.generic.FiltersRequest;
import ro.digitallibrary.library.dto.generic.PaginationRequestDto;
import ro.digitallibrary.library.dto.request.RequestPage;
import ro.digitallibrary.library.exceptionhandling.RepositoryException;

public interface CustomRequestRepository {

    public RequestPage getCustomPagedObjects(PaginationRequestDto<FiltersRequest> paginationRequest, Authentication auth) throws RepositoryException;
}
