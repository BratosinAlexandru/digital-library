package ro.digitallibrary.library.exceptionhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class RepositoryException extends Exception implements Serializable {

    private static final long serialVersionUID = -2283070827515320724L;

    public RepositoryException(final String message) {
        super(message);
    }

    public RepositoryException(final Throwable cause) {
        super(cause);
    }

    public RepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
